Debian GNU/Linux 5.0 "Lenny" リリース!!!

<p>以前のリリースから約２年近くを経て、<strong>Debian GNU/Linux 5.0 コードネーム “Lenny” が 2009/02/14、ついにリリースされました</strong>  (日本時間 2/15 にユーザから見えるようになっています)。リリース作業の最後の大詰めがどんなものであったか、その模様がパッケージアーカイブ管理者「ftpmaster」の <a href="http://blog.ganneff.de/blog/2009/02/14/lenny-release.html">Joerg Jaspert さんの Blog</a>に記載されています。リリースに携わった多数の開発者、バグ報告者、利用者の皆様に感謝致します。パッケージの大幅な追加／削除／更新を含んでいる今回の更新ですが、アップグレード作業の注意点など、詳細については<a href="http://www.debian.org/releases/lenny/releasenotes">Debian GNU/Linux 5.0 リリースノート</a>を参照してください。</p>
<p>
リリースノートには、Debian GNU/Linux 5.0 で追加・変更されたさまざまな機能説明を各アーキテクチャごとに掲載しているほか、前リリース (Debian GNU/Linux 4.0 コードネーム“Etch”) からのアップグレードを行うユーザーのための手順と注意も記述されています。いくつかの重要なパッケージに大幅な変更があるため、前リリースからのアップグレードを行う場合には、必ずこのリリースノートに目を通してください (例: <a href="http://www.debian.org/releases/lenny/i386/release-notes/ch-upgrading.ja.html">i386</a>、<a href="http://www.debian.org/releases/lenny/amd64/release-notes/ch-upgrading.ja.html">amd64</a>)。</p>
<p>
リリースノートの翻訳にあたっても、Debian-JP Doc メーリングリスト上で多数の翻訳者・査読者が力を合わせました。この場を借りて感謝いたします。</p>
<p>
なお、このリリースにより旧安定版(oldstable) Debian GNU/Linux 4.0 "Etch" のサポートは残り約１年ほどとなります。Etchの利用者の方は移行計画を立てて速やかに更新ください。</p>
