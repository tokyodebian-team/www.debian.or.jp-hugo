＠IT自分戦略研究所にて東京エリア Debian 勉強会が取り上げられました

<p>
＠IT自分戦略研究所　コミュニティ活動支援室のコーナーにて２回に分けて <a href="https://tokyodebian-team.pages.debian.net/">東京エリア Debian 勉強会</a>を取り上げて頂きました。</p>
<ol>
<li><a href="http://jibun.atmarkit.co.jp/lcom01/special/comeve01/comeve01.html">「積極的な発言が勉強会を盛り上げる」</a></li>
<li><a href="http://jibun.atmarkit.co.jp/lcom01/special/semi/semi01.html">「先達を見つけ、自分なりの勉強会を開催せよ」</a></li></ol>
<p>
東京エリア Debian 勉強会はどの様な雰囲気のなかで行われているのか、既に３年間に渡って開催を続けている秘訣は何か、などが語られています。今まで参加された事が無い方も、今後の参考に是非ご覧になってください（東京エリア Debian 勉強会は、直近では<a href="http://www.debian.or.jp/blog/events/tokyodebian-37-osc2008spring.html">３月１日に OSC にて開催予定</a>です）。</p>

