Debian GNU/Linux 5.0.1 および 4.0r8 アップデートリリース

<p>Debian GNU/Linux 5.0 コードネーム “Lenny” の最初のマイナーアップデート、ポイントリリースが 4 月 11 日に行われ、バージョンが 5.0.1 となりました。（これまでのマイナーアップデートでは「r」つまりは revision number が上がっていく形でしたが、5.0 からは 5.0.x というようなバージョン表記に変わりました）。</p>
<p>
本アップデートは、Debian GNU/Linux 5.0 の機能向上ではなく、既存パッケージのセキュリティ修正および重要な問題の更新 (debian-installer の新バージョンとカーネル ABI 変更や変更に伴うモジュールの再ビルドなど) を目的としたものです。更新は、通常のセキュリティアップデート同様 apt/aptitude を利用してインターネット経由で実施可能です。</p>
<p>
更新された内容の詳細については、<a href="http://www.debian.org/News/2009/20090411">5.0.1 についてのニュースリリース</a>、および<a href="http://ftp.debian.org/debian/dists/lenny/ChangeLog">Changelog</a>の内容を参照してください。
</p>
<p>
また、これと前後して旧安定版である Debian GNU/Linux 4.0 "Etch" r8 もリリースされました。こちらも同様に既存パッケージのセキュリティ修正および重要な問題の更新を目的としたものです。詳細については詳細については、<a href="http://www.debian.org/News/2009/20090408">4.0r8 についてのニュースリリース</a>、および<a href="http://ftp.debian.org/debian/dists/etch/ChangeLog">Changelog</a>の内容を参照してください。</p>
