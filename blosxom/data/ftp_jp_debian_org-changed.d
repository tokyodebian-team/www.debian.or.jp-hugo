国内プライマリミラー ftp.jp.debian.org の CDN 化と諸注意について

<p>Debian Project と Debian JP Project は、本日、Debian の日本のプライマリ
ミラーサイトである ftp.jp.debian.org を CDN ミラーに移行いたしました。</p>

<p>CDN ミラーは複数の (比較的高速な接続を持つ) Debian ミラーサーバを
用いた DNS ラウンドロビン (+ サーバによる重み付け) によって構成され、
特定ミラーサーバの障害影響を回避できます。また、単なる DNS ラウンド
ロビンとは異なり、対象となっているサーバが障害で停止していたりミラーが
正常に更新されていなかったりした場合は自動切り離しを実施し、利用者から
は特に何も意識することなく、最新のミラーを利用できます (<a href="http://www.debian.or.jp/using/mirror.html">参考</a>)。</p>

<p>本移行による影響は、通常のユーザにとっては軽微で、特に変更を加える
必要はありません。</p>

<dl>
<dt>影響を受けない/または移行によって CDN の恩恵を得られるケース</dt>
<dd><ul>
  <li>/etc/apt/sources.list で http://ftp.jp.debian.org/debian または
    http://cdn.debian.or.jp/debian を指定している場合</li>
  <li>ring プロジェクトなどの、ftp.jp.debian.org や cdn.debian.or.jp
    以外のホストを指定している場合 (※CDN ミラーへの移行をお勧めします)。</li>
</ul></dd>
<dt>影響を受けるケース (FTP および rsync)</dt>
<dd><ul>
  <li>ftp://ftp.jp.debian.org/debian を指定している場合<br />
    → CDN ミラーのいくつかは FTP サービスを提供していますが、必須要件
      としていないため、接続がうまくいかないことがあります。<br />
      http に置き換えるか、「ftp://ftp1.debian.or.jp/debian」
      または「ftp://ftp.nara.wide.ad.jp/debian」をご利用ください。</li>
  <li>ftp.jp.debian.org から rsync を行っている場合<br />
    → CDN ミラーのいくつかは rsync サービスを提供していますが、必須要件
      としていないため、接続がうまくいかないことがあります。<br />
      従来の WIDE Project 様のものを利用したいときには、
     「ftp1.debian.or.jp::debian」または「ftp.nara.wide.ad.jp::debian」
      を指定してください。<br />
      Debian JP Project ではより高速なサーバホストの提案もして
      います。詳細については「<a href="http://www.debian.or.jp/community/push-mirror.html">CDN 対応ミラーの設定</a>」を参照して
      ください。</li>
</ul></dd>
</dl>

<p>謝辞</p>

<p>長年にわたりプライマリミラーとしての重責を一手に引き受け、今後とも
FTP サーバおよび CDN ホストの 1 つとしてご活躍される WIDE Project 様
に感謝いたします。</p>
<p>私たちの CDN に参加している HANZUBON.jp 様、Top Studio 株式会社様、
株式会社fonfun 様、地球流体電脳倶楽部 様、OYU-NET.JP 様に感謝いた
します。</p>
