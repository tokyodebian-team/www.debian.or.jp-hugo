Debian 6.0.7 リリース

<p>Debian GNU/Linux 6.0 コードネーム “Squeeze” のポイントリリースが 2 月 23 日に行われ、バージョンが 6.0.7 となりました。</p>
<p>
本アップデートには、既存パッケージのセキュリティ修正および重要な問題への対応が行われています。更新は、通常のセキュリティアップデート同様 apt/aptitude を利用してインターネット経由で実施可能です。</p>
<p>
更新された内容の詳細については、<a href="http://www.debian.org/News/2013/20130223">6.0.7 についてのニュースリリース</a>、および<a href="http://ftp.debian.org/debian/dists/squeeze/ChangeLog">Changelog</a>の内容を参照してください。
</p>

