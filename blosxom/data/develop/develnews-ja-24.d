開発ニュース寄せ集め (第 24 号)

<p>このニュースは <a href="http://wiki.debian.org/DeveloperNews">http://wiki.debian.org/DeveloperNews</a> でまとめたものです。
寄稿はご自由にどうぞ。</p><h5>packages.d.o のスクリーンショット</h5><p>もうお気づきかもしれませんが、packages.d.o に <a href="http://screenshots.debian.net/">Debian スクリーンショットサイト</a>へのリンクとともに
<a href="http://rhonda.deb.at/blog/debian/packages-screenshots.html">スクリーンショット</a>が表示されるようになっています。"No screenshot available"
の画像が出てスクリーンショットが表示されないところは、ユーザにスクリーンショットの投稿を求めているところです。
誰でも簡単に<a href="http://screenshots.debian.net/upload">寄稿</a>出来て、承認後には、スクリーンショットが<a href="http://screenshots.debian.net/packages">ウェブページ</a>や
<a href="http://rhonda.deb.at/blog/debian/packages-screenshots.html">packages.d.o</a>、<a href="http://blog.workaround.org/2008/11/14/screenshotsdebiannet-goes-live/">synaptic</a>、<a href="http://packages.debian.org/sid/aptitude-gtk">aptitude-gtk</a>、<a href="http://packages.debian.org/sid/software-center">software-center</a>、さらに、
<a href="http://bugs.debian.org/589928">games-thumbnails</a>、<a href="http://packages.debian.org/sid/goplay">goplay</a> を経由して利用可能になります。Debian
スクリーンショットサイトはすでに<a href="http://blog.workaround.org/2008/11/10/screenshotsdebiannet-goes-beta/">ほぼ 2 年間</a>運営されており、1700
もの<a href="http://screenshots.debian.net/packages">パッケージ</a>から 2200 を越すスクリーンショットを集めていますが、
多くの<a href="http://screenshots.debian.net/packages/without_screenshots">パッケージ</a>のスクリーンショットが足りていないので、今日にでもアップロードするといいと思います。
スクリーンショットのメタデータは JSON (<a href="http://screenshots.debian.net/json/packages">1</a>、<a href="http://screenshots.debian.net/json/screenshots">2</a>) か <a href="http://udd.debian.org/schema/udd.html#public.table.screenshots">UDD</a> で利用可能です。Debian
スクリーンショットサイトの裏で動いている <a href="http://debshots.workaround.org/trac/">debshots</a> で作業したいなら、git
のリポジトリをチェックアウトして、Christoph Haas さんにパッチを送ってください。
特に、packages.d.o との統合に関して、様々な改善が<a href="http://debshots.workaround.org/trac/report/1">予定されています</a>。</p><p>-- Paul Wise</p><h5>BTS のバージョン追跡の解説</h5><p>BTS においてどの様にバージョンを追跡しているのかについて、未だに多くの混
乱がみうけられます。この件に関する多少長い<a href="http://rhonda.deb.at/blog/debian/on-BTS-usage.html">ブログ記事</a>を書きました。
この記事では、しばらく前に閉じられているにもかかわらず、アーカイブされな
いバグがあるのは何故かを説明しています。</p><p>-- Gerfried Fuchs</p><h5>dpkg-dev の最新機能</h5><p>Raphaël Hertzog さんが dpkg-dev の最新機能について<a href="http://raphaelhertzog.com/2010/10/30/latest-features-of-dpkg-dev-debian-packaging-tools/">ブログに書いています</a>。
また、彼はフランスのパリで最近開催された MiniDebConf でもトークセッションを
行いました (<a href="http://raphaelhertzog.com/files/2010/10/latest-dpkg-dev-features.pdf">スライド</a>)。彼のブログ記事とトークは、いくつかの領域をカバーする
もので、次のような内容を含んでいます。新しいソースパッケージ形式、ソースパッケージの
ビルドオプション、メンテナスクリプトのヘルパー、ベンダー差分のサポート (パッチと
ビルドルール)、ライブラリ向けのシンボルファイル、それからビルドフラグのカスタマイズ、
などなど。</p><p>-- Paul Wise</p><h5>Ultimate Debian Database を用いたバグの検索</h5><p>バグ (RC バグを含む) の検索に、いくつもの条件を指定できる
検索エンジンを提供する Ultimate Debian Database 用の
<a href="http://udd.debian.org/bugs.cgi">新しいウェブインタフェースがあります</a>。</p><p>-- Lucas Nussbaum</p><h5>バックポートされたパッケージの状態を再評価</h5><p>バックポートされたパッケージの状態を再評価する呼びかけを<a href="http://lists.debian.org/debian-backports/2010/11/msg00008.html">メールしました</a>。
squeeze 版よりもバックポートが古くなっている理由が、もはや妥当でなくなって
いるなら、再度パッケージを同期させておくとよいでしょう。セキュリティ修正や
ポイントリリースによる更新がやりやすくなります。</p><p>-- Gerfried Fuchs</p><h5>この記事について</h5><p>この記事は <a href="http://lists.debian.org/debian-devel-announce/2010/11/msg00002.html">debian-devel-announce に流れたニュース</a>の翻訳です。
翻訳は石井一夫さん・今井伸広さん・佐々木洋平さん・倉敷悟が行いました。
また、victory さん・やまねひできさんから多数のコメントをいただきました。
ありがとうございます。</p>
