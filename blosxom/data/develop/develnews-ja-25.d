開発ニュース寄せ集め (第 25 号)

<p>このニュースは <a href="http://wiki.debian.org/DeveloperNews">http://wiki.debian.org/DeveloperNews</a> でまとめたものです。
寄稿はご自由にどうぞ。</p><h5>Debian QA ミーティング</h5><p>Debian QA ミーティングがフランス、パリにある <a href="http://www.irill.org">Center for Research and
Innovation on Free Software(IRILL)</a> で<a href="http://wiki.debian.org/DebianQAParis2011">開催されます</a>。このミーティン
グは 3月18日(金)午後から始まり、3月20日(日)に終わる予定です。参加予定で
旅費の支援が必要な場合には、sprint への援助を受けられるように2月8日(火)
までに wiki にあなたの情報を追加して下さい。</p><h5>squeeze のリリースがマイクロブログで実況されます</h5><p>Debian squeeze のリリースが Debian の <a href="http://identi.ca/debian">identica アカウント</a> で<a href="http://lists.debian.org/20110130130240.GC30200@melusine.alphascorpii.net">マイクロブログ</a> に実況されます。
リリースプロセスのいくつかの段階はかなり長く、退屈です。
そのためこれらの静かな段階は Debian に関する冗談やその他興味深い実話で埋められるでしょう。
冗談や興味深い実話伝えることで手伝ってもよいという方がいたら、<a href="http://lists.debian.org/debian-publicity/2011/01/threads.html#00055">スレッド</a> に返信してください。</p><p>-- Paul Wise</p><h5>Debian派生物調査</h5><p>Debian <a href="http://wiki.debian.org/DerivativesFrontDesk">派生物フロントデスク</a>は、すべての Debian 派生物の<a href="http://wiki.debian.org/Derivatives/Census">調査</a>を<a href="http://www.debian.org/News/2011/20110124">行っています</a>。
あなたが Debian 派生物の代表である場合、<a href="http://wiki.debian.org/Derivatives/CensusTemplate">テンプレート</a>に基づいてあなたの派生物についての
ページを追加してください。Debian派生物調査における将来の計画には、Debian 派生物の経過状況を把握したり、
Debian で作業している人向けに便利な情報をもっと追加したり、機械的に処理できる情報を含めるなどして、
結果的に派生物についての情報を Debian のインフラに統合する、といったことが含まれています。
さらに、Debian から派生したディストリビューション代表者に Debian <a href="http://wiki.debian.org/Derivatives">派生物</a> <a href="http://wiki.debian.org/DerivativesFrontDesk">フロントデスク</a>、
<a href="irc://irc.debian.org/debian-derivatives">IRC チャンネル</a> および<a href="http://lists.debian.org/debian-derivatives/">メーリングリスト</a> への参加を招待したいと考えています。</p><p>-- Paul Wise</p><h5>planet.debian.org のポリシー</h5><p><a href="http://planet.debian.org">Planet Debian</a> の管理者が、<a href="http://wiki.debian.org/PlanetDebian">Planet Debian のポリシー</a>を改訂して、Planet
のホームページからリンクしました。このポリシーは、(読み手にせよ、書き手にせよ、) すべての Planet Debian
ユーザが読むように勧められていて、誰が Planet Debian に登場できるか、何が Planet Debian
に投稿されるか、また、各種の Planet Debian サービスからメリットを得るための技術的な文書などのトピックをカバーしています。</p><p>主な変更点は、Planet Debian ではフィルタリングされていない webbugs に難色を示し、
フィード削除の理由となり得るという点です。現時点では、feedburner
のトラッキング画像だけが削除されています。自動的にフィードに挿入される、
ブログが配信されているのとは別のホストにある画像は、追跡目的に利用されることが知られており、webbug
とみなされます。もし、フィードに webbugs が含まれてしまっている場合は、
フィードを除外されないための追加のフィルターを提供してください。パッチは、planet@debian.org に送ってください。</p><p>-- Stefano Zacchiroli &amp; Raphaël Hertzog</p><h5>override の差異一覧</h5><p><a href="http://www.debian.org/doc/debian-policy/ch-archive.html#s-priorities">ポリシーの 2.5 節</a> によれば、パッケージは自分よりもプライオリティが低いパッケージに
依存してはいけないことになっています。これをよりよく順守するため、FTP チームでは yaml 形式
で <a href="http://ftp-master.debian.org/override-disparity.gz">override に差異のあるパッケージ一覧</a> を出力しています。
自分のパッケージが間違っていると思う場合は、次のアップロードで修正してください。間違っている
のが私たちの override だとお考えなら、自分のパッケージの override を変更するよう、
ftp.debian.org にバグ登録してください。
より詳しくは、http://lists.debian.org/4D2CDDF5.2000600@debian.org をご覧ください。</p><p>-- Alexander Reichle-Schmehl</p><h5>この記事について</h5><p>この記事は <a href="http://lists.debian.org/debian-devel-announce/2011/02/msg00000.html">debian-devel-announce に流れたニュース</a>の翻訳です。
翻訳は石井一夫さん・今井伸広さん・佐々木洋平さん・中尾隆さん・倉敷悟が行いました。
また、victory さん・やまねひできさんから多数のコメントをいただきました。
ありがとうございます。</p>
