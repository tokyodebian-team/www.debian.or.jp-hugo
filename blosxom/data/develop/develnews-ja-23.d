開発ニュース寄せ集め (第 23 号)

<p>このニュースは <a href="http://wiki.debian.org/DeveloperNews">http://wiki.debian.org/DeveloperNews</a> でまとめたものです。
寄稿はご自由にどうぞ。</p><h5>Debian MiniConfなど</h5><p>そういうわけで、<a href="http://wiki.debian.org/DebConf">Debconf</a> が終わって、すぐに寂しく思うことでしょう(というか寂しがってるでしょう？)。
いつでもこれを少しばかり悲しむ人はいますが、 それゆえに必然として来年再び皆さんと会うために
私たちは帰ってきます。また、来年までの一年間、皆さんが寂しい思いをする必要はありません。</p><p>今年はじめに、MiniDebCampがタイで開催され、およそ30人の地元の人と、ヨーロッパや、オーストラリア、
日本、台湾から5人が<a href="http://wiki.debian.org/DebianThailand/MiniDebCamp2010">出席しました</a>。伝えられるところでは、DebConf の終了時と日程が重なったインドの
MiniConf では、ほぼ200人の<a href="http://in2010.mini.debconf.org/">参加者がありました</a>。</p><p>10月の終わりには、MiniConfがフランスの<a href="http://wiki.debconf.org/wiki/Miniconf-Paris/2010">パリで開催されます</a>。11月の初めには、Debian MiniConf が、
ベトナムのホーチミン（サイゴン）市の<a href="http://fossasia.org/">FOSSASIA で開催されます</a>。このようなイベントは、世界中の Debian
コミュニティの発展を助けます。どうかそれに出席し、トークを行ったり、あなたのブログや、ミニブログ、
メーリングリスト、irc/チャットチャンネルでPRしたりすることでサポートしてください。
もし、これらのイベントのうちの1つを組織しているならば、あなたの Debian MiniConf のために
&lt;country&gt;&lt;year&gt;.mini.debconf.org ドメインを得るためにDebConfグローバルチームへの
連絡を考慮してください。 また、あなたの Debian 関連イベントを<a href="http://www.debian.org/events/">Debianウェブサイトのリスト</a>に
掲載するために、Debian イベント関連のアドレスに連絡してください。</p><p>-- Paul Wise</p><h5>Debian Maintainer index</h5><p>Enrico Zini が現在の全 Debian メンテナ (DM) とその保守するパッケージを一覧する
すばらしい<a href="https://nm.debian.org/dm_list.html">概観ページ</a>を作ってくれました。</p><p>-- Stefano Zacchiroli</p><h5>GoogleCode redirector</h5><p>最近 <a href="http://googlecode.debian.net/">GoogleCode redirector</a> のベータテストを<a href="http://www.hanskalabs.net/posts/googlecode-debian-net/">始めました</a>。
これは debian/watch (by uscan(1) and DEHS) で使われることを想定しています。
自由に使って、バグ報告や機能要求をしてください</p><p>-- David Paleino</p><h5>Debian プロジェクトニュースへの DD のコミット権限</h5><p>Debian プロジェクトニュースに(編集、レビュー、翻訳の形で)寄稿している Debian 開発者は、
特別に権限を要求することなくコミット可能です。
alioth の subversion リポジトリでは、既に全 Debian 開発者に書き込み権限を与えています。
さらなる詳細については、 http://wiki.debian.org/ProjectNews/HowToContribute にあります。</p><p>-- Alexander Reichle-Schmehl</p><h5>Debian 開発者のポートフォリオサービス</h5><p>最近、<a href="http://ddportfolio.debian.net/">Debian 開発者のポートフォリオサービス</a> にいくつか新しい情報の種別を追加しました。</p><ul>
<li>ユーザの Debian wiki ホームページへのリンク (URL パラメータ wikihomepage
で指定されるものか、大文字で始める名前から生成されるもの)</li>
<li>ユーザの Debian フォーラムページへの任意のリンク (フォーラムの id が URL パラメータ
forumsid として指定された場合のみ)</li>
<li>MIA 情報向け ssh コマンドライン、保有している debian.net ドメインと所属グループ</li>
<li>db.debian.org から GPG 鍵を取得するための finger コマンドと URL</li>
<li>db.debian.org から開発者情報を取得するための finger コマンド</li>
</ul><p>日次で gpg 鍵情報を keyring.debian.org と同期するための cron ジョブも追加しました。
DDPortfolio は、Debian maintainer および Debian developer キーリングの
情報を使っています。</p><p>Debian Maintainer の GPG 指紋とフルネームは、Debian Developer の場合と同じ方法で
email アドレスから自動的に取得されるようになりました。</p><p>ddportfolioservice に新しいアイデアを淀みなく提供してくれた Paul Wise (pab) さん
に感謝します。</p><p>-- Jan Dittberner</p><h5>#debian-ubuntu on OFTC</h5><p>Lucas Nussbaum が OFTC の IRC チャンネル #debian-ubuntu について<a href="http://www.lucas-nussbaum.net/blog/?p=500">ブログ記事</a>を書いています。
あるパッケージがUbuntuではどんな状態なのか(そしてその逆も)、疑問がある場合に非常に有用です。</p><p>-- Raphaël Hertzog</p><h5>チームアップロード</h5><p>一つのメーリングリストに全てのバグレポートを集めていて、Maintainer や Uploader
への直接記載なしに、メンバーによる通常のアップロードを許可しているパッケージング
チームがあります。こういった場合に、特別な一行目 <code>* Team Upload.</code> があると、
アップロードが NMUと間違われるのを避けることができます。<code>devscripts</code> パッケージ
の <code>debchange</code> (<code>dch</code>) ツールは、<code>--team</code> コマンドラインオプションを使う
ことで <a href="http://wiki.debian.org/TeamUpload">チームアップロード</a> をサポートします。</p><p>-- Charles Plessy</p><h5>この記事について</h5><p>この記事は <a href="http://lists.debian.org/debian-devel-announce/2010/08/msg00001.html">debian-devel-announce に流れたニュース</a>の翻訳です。
翻訳は victory さん・石井一夫さん・倉敷悟が行いました。
ありがとうございます。</p>
