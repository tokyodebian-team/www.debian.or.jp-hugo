開発ニュース寄せ集め (第 13 号)

<p>このニュースは <a href="http://wiki.debian.org/DeveloperNews">http://wiki.debian.org/DeveloperNews</a> でまとめたものです。
寄稿はご自由にどうぞ。</p><h5>新しいテスト版 (squeeze) のセキュリティサポートは遅れます</h5><p>squeeze のセキュリティサポートは lenny リリース後すぐには
開始されない、とテスト版 (testing) セキュリティチームが発表しました。
セキュリティサポートが必要なユーザは squeeze のセキュリティサポート
開始が発表されるまで lenny のままにしてください。</p><p><a href="http://lists.debian.org/debian-testing-security-announce/2008/12/msg00019.html">http://lists.debian.org/debian-testing-security-announce/2008/12/msg00019.html</a></p><p>-- Stefan Fritsch</p><h5>あるパッケージを持っている他のディストリビューションを表示する新しい whohas ツール</h5><p>不安定版の新しいツール、<code>whohas</code> は任意のパッケージを
ディストリビューションのリスト全体から検索して、そのバージョンとともに表示します。
現時点では Arch, Debian, Fedora, Gentoo, openSUSE, Slackware,
Source Mage, Ubuntu, FreeBSD, NetBSD, OpenBSD, Fink, MacPorts
を対象としています。メンテナが新しいパッケージ方法やパッチを見つけるのに
役立つ可能性があります。</p><p><a href="http://packages.debian.org/whohas">http://packages.debian.org/whohas</a></p><p>-- Thijs Kinkhorst</p><h5>python-apt の文書</h5><p>API について網羅している python-apt の文書が用意されました。
Sphinx と reStructuredText を使って半週で作りました。これまで
ひどい資料不足だった python-apt で開発していたあらゆる人にとって
助けになります。この完全な文書は python-apt のバージョン 0.7.9~exp2
で入ります。</p><p><a href="http://apt.alioth.debian.org/python-apt-doc/">http://apt.alioth.debian.org/python-apt-doc/</a></p><p>-- Julian Andres Klode</p><h5>sbuild と wanna-build の状況更新</h5><p>これまで 3 か月以上に渡って大きな変更がいろいろと加えられてきました。
他にも近いうちに、また Squeeze に向けても計画しています。sbuild は
最新の wanna-build 及び buildd、また多くの改善も図られたことにより
存分に使えるようになっています。
ここ最近の作業は wanna-build に集中し、バグ修正はもちろんですが
使い勝手をいくらか改善しています。これまでの buildd への変更で、公開
SCM に入っていなかったものもマージされ、今では wanna-build には
稼働している wanna-build にある機能は全て入っています。継続中の
作業にはバックエンドの MLDBM データベースをもっと柔軟な PostgreSQL
に置き換えるための wanna-build のモジュール化等があります。</p><p><a href="http://lists.debian.org/debian-devel/2009/01/msg00061.html">http://lists.debian.org/debian-devel/2009/01/msg00061.html</a></p><p>-- Roger Leigh</p><h5>Kernel の仮想パッケージを削除しました</h5><p>仮想パッケージ "kernel" は削除されました。現在ではカーネルイメージは
すべて 1 つのソースパッケージ "linux-2.6" からビルドされているからです。
reportbug は既に実験的に "kernel" に向けて報告されるあらゆるバグを
linux-2.6 に向けるように変更されています。</p><p>-- Moritz Muehlenhoff</p><h5>この記事について</h5><p>この記事は <a href="http://lists.debian.org/debian-devel-announce/2009/01/msg00007.html">debian-devel-announce に流れたニュース</a>の翻訳です。
翻訳は victory さんが行いました。
また、武井伸光さんから多数のコメントをいただきました。
ありがとうございます。</p>
