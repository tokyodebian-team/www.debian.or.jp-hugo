開発ニュース寄せ集め (第 18 号)

<p>このニュースは <a href="http://wiki.debian.org/DeveloperNews">http://wiki.debian.org/DeveloperNews</a> でまとめたものです。
寄稿はご自由にどうぞ。</p><h5>debian/changelog ファイルの 3-way マージ</h5><p>Debian changelog ファイルのスマートな 3-way マージを行う新しいスクリプトが
<a href="http://www.ouaza.com/wp/2009/10/08/3-way-merge-of-debian-changelog-files/">アナウンスされています</a>。git は自動的にこれを使うように設定でき、これによって、
よく見られる通常の行ベースによるマージによって生成されるコンフリクトを回避できる
ようになります。</p><p>-- Raphaël Hertzog</p><h5>PTS のページで対処されていないセキュリティ問題が表示されるように</h5><p>Raphael Geissert さんによるパッチのおかげで、PTS (http://packages.qa.debian.org)
が security tracker (http://security-tracker.debian.net) ときちんと
統合されました: パッケージのページは、入力されたソースパッケージに対して、
(もしあれば) 対処されていないセキュリティ問題の数を表示します。</p><p>-- Stefano Zacchiroli</p><h5>git format-patch との互換性のため DEP-3 が更新</h5><p>最近アナウンスされた <a href="http://dep.debian.net/deps/dep3/">Patch Tagging ガイドライン</a> が更新されました。
これは、git の format-patch と他の VCS でメールで変更点を交換するのに、より互換性を高めるためです。
この形式の利用を検討してください。使い方についてのコメントの交換はご自由にどうぞ。</p><p>-- Raphaël Hertzog</p><h5>debian-devel メーリングリストと ITP</h5><p>DebConf9 で、debian-devel メーリングリストをより有用にするためのディスカッションがありました。
まとめると、<a href="http://www.debian.org/doc/developers-reference/pkgs.html#newpackage">開発者リファレンスの 5.1 項</a> にあるお勧めについての簡単なリマインダです。
複数パッケージの ITP を debian-devel に送らないで、代わりに後で ITP をまとめてサマリを
debian-devel に送ってください。複数のパッケージ群が何かしら関連がある場合は特にそうです。</p><p>-- Paul Wise</p><h5>wiki.debian.org でのバグの状態表示</h5><p>Debian wiki では、クローズされたバグを打ち消し線でオープンになっている
バグと区別している DebianBug:123456 のようなリンクを含んでいる wiki ページの
更新に Javascript を使うようになりました。さらに、マウスポインタをタイトルの上に載せると、
バグの状態 + 修正されたバージョン + バグ番号 + バグの件名に変更されます。
この機能は debian-installer チームの Max Volzeler (mvz) さんによって始められ、
コードが書かれました。彼はこれを <a href="http://wiki.debian.org/DebianInstaller/SqueezeGoals">squeeze goals</a> wiki ページに使いたいと思ったのです。
実相の詳細は <a href="http://wiki.debian.org/Teams/DebianWiki">Debian wiki チームの git リポジトリ</a> で参照できます。 
wiki チームは、通常の interwiki アイコンを置き換えるのに使う、死んでいるバグと生きている
バグの<a href="http://art.debian.net/?ccm=/media/thread/24">アイコンのデザイン</a>をする人も探しています。
wiki にある既存のバグへのリンクを DebianBug:123456 形式のリンクに変更したい場合は、
<a href="http://lists.debian.org/debian-www/2009/08/msg00154.html">DebianBugification</a> の wiki ページ案のリストを参照してください。</p><p>-- Paul Wise</p><h5>この記事について</h5><p>この記事は <a href="http://lists.debian.org/debian-devel-announce/2009/10/msg00001.html">debian-devel-announce に流れたニュース</a>の翻訳です。
翻訳は victory さん・やまねひできさんが行いました。
ありがとうございます。</p>
