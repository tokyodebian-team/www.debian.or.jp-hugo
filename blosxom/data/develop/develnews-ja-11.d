開発ニュース寄せ集め (第 11 号)

<p>このニュースは <a href="http://wiki.debian.org/DeveloperNews">http://wiki.debian.org/DeveloperNews</a> でまとめたものです。
寄稿はご自由にどうぞ。</p><h5>Lenny 用の CD/DVD イメージ</h5><p>Debian CD チームでは、Lenny で利用可能にしようとしている CD と DVD イメ
ージへのいくつかの変更について話し合っています。その変更点は、ブートメニューか
らどのデスクトップ環境をインストールするかを選択できるようにする一連のパッチが
中心です。</p><p>もし興味があれば、以下の 2 つのメールを読んでください。最初のメールには、その
パッチをテストし、新しいオプションをデモできるようにしたイメージへのリンクが含
まれています。</p><ul>
<li><a href="http://lists.debian.org/debian-cd/2008/12/msg00000.html">http://lists.debian.org/debian-cd/2008/12/msg00000.html</a></li>
<li><a href="http://lists.debian.org/debian-cd/2008/12/msg00019.html">http://lists.debian.org/debian-cd/2008/12/msg00019.html</a></li>
</ul><p>debian-cd メーリングリストでの議論をお願いします。</p><p>-- Frans Pop</p><h5>AGPL v3.0 ライセンスは、main に適合している</h5><p>ftpmaster は、GNU AGPL 3.0 パブリック・ライセンスのもとでライセンスされたソフトウェアは、
Debian アーカイブ の main に適合していると決定したことを、Joerg Jaspert さんは発表しました。</p><p>これはいくつかのバグレポート(#495721, #506402 など)で議論された長期の問題を解決します。</p><ul>
<li><p>さらに以下を読んでください:</p>

<p><a href="http://blog.ganneff.de/blog/2008/11/28/ftpmaster-meeting-in-extremadu.html">http://blog.ganneff.de/blog/2008/11/28/ftpmaster-meeting-in-extremadu.html</a></p></li>
<li><p>AGPL 3.0 ライセンスは、以下で参照可能です:</p>

<p><a href="http://www.fsf.org/licensing/licenses/agpl-3.0.html">http://www.fsf.org/licensing/licenses/agpl-3.0.html</a></p></li>
<li>関連しているバグのうちのいくつかについては、以下でログを参照可能です:</li>
<li><a href="http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=495721">http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=495721</a></li>
<li><a href="http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=506402">http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=506402</a></li>
</ul><h5>CD/DVD イメージの作成を簡単に</h5><p>公式の Debian CD イメージを作成するのに使われている debian-cd パッケー
ジには、easy-build.sh という新しいスクリプトが含まれています。このスク
リプトは、(何組もの) カスタムイメージの作成を、かなり簡単なものにするよ
う意図されています。派生ディストリビューションを作るのにも使えますが、
テスト目的のイメージをアドホックに作成するのにぴったりです。</p><p>このスクリプトの主な特徴は、極めて柔軟性が高いところにあります。異なる
引数を渡したり、スクリプト内の設定をちょっと変更するだけで、例えば、異
なるアーキテクチャ (単一もしくは複数) や Debian リリース、選択できるデ
フォルトのデスクトップ環境、カスタムパッケージの収録、カスタムの D-I
イメージおよびコンポーネントなどの、異なるタイプのイメージをビルドでき
ます。</p><p>更なる情報は、スクリプトと一緒に入っている README.easy-build ファイル
を見るか、debian-cd メーリングリストに尋ねてください。</p><p>もちろん、ローカルにミラーが必要なのは変わりありませんが...</p><p>-- Frans Pop</p><h5>Mono 2.0 への移行が進行中</h5><p>Debian Mono チームは目下全ての CLI アプリケーションを Mono 2.0 へ移行中
です。現在は全てのスタックを移行し、パッケージについて作業しています。
破損する可能性を減らすために、ライブラリ群の移行は最後になるでしょう。</p><p>「リアルタイムの」(に近い)進捗情報については、Wiki を参照して下さい：</p><p><a href="http://wiki.debian.org/Teams/DebianMonoGroup/Mono20TransitionTODO">http://wiki.debian.org/Teams/DebianMonoGroup/Mono20TransitionTODO</a></p><p>-- David Paleino</p><h5>PTS のための SOAP インターフェイス</h5><p>PTS は、サードパーティ製プログラムから SOAP プロトコルを利用して
ウェブページに掲載された情報にアクセスするための SOAP インターフェイスを
提供するようになりました。
現在、インターフェイスはアルファ版であり、テスト、フィードバック、バグレポートが
求められています。</p><ul>
<li><p>前置きは次の場所に書いてあります。</p>

<p><a href="http://upsilon.cc/~zack/blog/posts/2008/11/PTS_SOAP_interface/">http://upsilon.cc/~zack/blog/posts/2008/11/PTS_SOAP_interface/</a></p></li>
<li><p>SOAP インターフェイスの基本情報は次の場所にあります。</p>

<p><a href="http://wiki.debian.org/qa.debian.org/pts/SoapInterface">http://wiki.debian.org/qa.debian.org/pts/SoapInterface</a></p></li>
<li>SOAP インターフェイスの HTML API リファレンスは次の場所にあります。</li>
</ul><h5>GCC 4.4 に関するビルドエラーの追跡</h5><p>私はビルドエラーを報告するために GCC 4.4 で Debian アーカイブをビルドし、
約 220 のバグを (大半はパッチと一緒に) 登録しました。おおよそ 30 ほどの
ビルド失敗については、まだ調べていません。約 35 のパッケージについては、
boost のヘッダが GCC 4.4 では使えないためにビルドができません。これらに
ついては、boost のヘッダが修正されたらビルドしてみます。</p><p>これらのバグを追いかけるのに、ftbfs-gcc-4.4 ユーザタグを
(debian-gcc@lists.debian.org をユーザとして) 使いました：</p><p><a href="http://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=ftbfs-gcc-4.4;users=debian-gcc@lists.debian.org">http://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=ftbfs-gcc-4.4;users=debian-gcc@lists.debian.org</a></p><p>私が見逃した GCC 4.4 関連のビルドエラーに出くわしたら、同じユーザタグを使ってください。</p><p>ほとんどの GCC 4.4 のビルドエラーは些細なものです。失敗の大多数は、
<a href="http://www.cyrius.com/journal/gcc/gcc-4.4-include">http://www.cyrius.com/journal/gcc/gcc-4.4-include</a> に説明があるように
#include 文の抜けが原因です。</p><p><a href="http://www.cyrius.com/journal/gcc/gcc-4.4-preprocessor-errors">http://www.cyrius.com/journal/gcc/gcc-4.4-preprocessor-errors</a> で説明
しているような、プリプロセッサチェックが改善されたことによるビルドエラーも
20 ほどあります。</p><p>-- Martin Michlmayr</p><h5>git リポジトリの Alioth でのミラー</h5><p>Alioth 以外のどこかに (debian 関連の) 公開 Git リポジトリを持っているなら、
Alioth 自体でそれをミラーさせることも可能です。Alioth では git サーバ経由の
アクセスも提供するため、それにより gitweb インターフェースが得られるとともに、
一部のユーザのリポジトリへのアクセス手段を単純化できる可能性もあります。</p><p>ミラーは現在 30 分毎に更新されています。ミラーに新しいリポジトリを追加したい場合は、
リポジトリの URL と 外部向けの説明を添えて、admin@alioth.debian.org まで
連絡をください。このサービスの最初のユーザは DSA チームで、3 つのリポジトリ
(<a href="http://git.debian.org/?p=mirror/dsa-exim.git;a=summary">dsa-exim</a>, <a href="http://git.debian.org/?p=mirror/dsa-getclamsigs.git;a=summary">dsa-getclamsigs</a>, <a href="http://git.debian.org/?p=mirror/dsa-misc.git;a=summary">dsa-misc</a>) があります。</p><p>-- Raphael Hertzog</p><h5>この記事について</h5><p>この記事は <a href="http://lists.debian.org/debian-devel-announce/2008/12/msg00001.html">debian-devel-announce に流れたニュース</a>の翻訳です。
翻訳は荒木淳さん・石井一夫さん・今井伸広さん・佐々木洋平さん・中尾隆さん・victory さん・倉敷が行いました。
また、かねこせいじさん・武井伸光さん・西山和広さん・やまねひできさん・victory さんから多数のコメントをいただきました。
ありがとうございます。</p>
