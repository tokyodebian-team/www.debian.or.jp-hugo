開発ニュース寄せ集め (第 5 号)

<p>このニュースは <a href="http://wiki.debian.org/DeveloperNews">http://wiki.debian.org/DeveloperNews</a> でまとめたものです。
寄稿はご自由にどうぞ。</p><h5>Debian ドキュメンテーションプロジェクト は SVN に切り替えました</h5><p>Debian ドキュメンテーションプロジェクトは CVS リポジトリを Subversion (SVN)
リポジトリへと<a href="http://lists.debian.org/debian-doc/2008/03/msg00002.html">変換しました</a>。
ドキュメンテーションコントリビュータは全員、svn.debian.org
に構築されている新しい SVN リポジトリ上で作業を継続しなければなりません。</p><ul>
<li>認証チェックアウト: svn co svn+ssh://svn.debian.org/svn/ddp/manuals/trunk manuals</li>
<li>匿名チェックアウト: svn co svn://svn.debian.org/ddp/manuals/trunk manuals</li>
</ul><p>-- Raphael Hertzog</p><h5>機械的に処理が可能な debian/copyright ファイル</h5><p>2007 年 8 月、Sam Hocevar さんが debian/copyright
ファイルを機械的に処理が可能なものにしようと提案しました。</p><p>「このファイルは、Debian でのパッケージングにおいて最も重要なものの 1 つですが、
いまだにそのフォーマットはあいまいで、各パッケージによって大きく異なっており、
自動で解析するのが困難です。フリーソフトウェアのライセンスは多様なので、Debian
は任意のソフトウェアのフリーさだけでなく、Debian
で利用している他のソフトウェアのライセンスとの親和性についても注意を払う必要があります。」</p><p>ここ数ヵ月で、いくつかのパッケージがこの新しいフォーマットを採用しました。
この提案の現状については、Debian wiki ページの <a href="http://wiki.debian.org/Proposals/CopyrightFormat">CopyrightFormat</a> を見てください。</p><p>-- Joost van Baal</p><h5>PTS の購読者に WNPP のステータス変更が通知されます</h5><p>スクリプトによって、みなしごパッケージの一覧をモニタして、
パッケージトラッキングシステム (PTS) でそのパッケージを ("summary" キーワードで)
購読している人々に、ステータス変更についての情報を送信するようになりました。
関心のあるパッケージを購読する、相応の理由がまた一つできました。</p><p>-- Lucas Nussbaum</p><h5>grub-install と update-grub での新しいデバイス探索方法</h5><p>grub パッケージ (GRUB Legacy) の experimental
における最新版 (バージョン 0.97-33) は、 /etc/fstab や /etc/mtab
をパースする必要のない新しいデバイス探索機構を利用します。
この機構は、GRUB 2 からバックポートされた grub-probe (/dev を走査し、
指定されたファイルやディレクトリにマッチするデバイスを見つけるユーティリティ)
を用いたもので、
シンプルなセットアップで動作することが、テストを経て分かっています。
この機構によって、最終的には、(問題の一般的原因であった) grub-install
と update-grub でのデバイス探索がさらに堅牢になるでしょう。しかし、
この機構は (特にあまり一般的でないセットアップでは)
まだ広くテストされていないので、sid
へのアップロード前に多少テストをしてもらえるとありがたいです。</p><p>-- Robert Millan</p><h5>ポリシーチェッカーの linda が testing/unstable から削除されました</h5><p>lintian とよく似ている Debian パッケージチェッカーの linda が unstable
から削除されました。linda は lintian を python で書き換えたものとして作られ、
lintian の設計上の制限の多くを克服しました。しかしながら、lintian
の方がよく知られていて、その設計上の問題のほとんどを修正する変更がかなり見られたので、
2 つのポリシーチェッカーを維持する負担は今では不必要と考えられ、linda
が削除されました。</p><p>-- Marc Brockschmidt</p><h5>この記事について</h5><p>この記事は <a href="http://lists.debian.org/debian-devel-announce/2008/03/msg00009.html">debian-devel-announce に流れたニュース</a>の翻訳です。
翻訳は荒木淳さん・石井一夫さん・今井伸広さん・倉敷悟さん・中尾隆さん・小林儀匡が行いました。</p>
