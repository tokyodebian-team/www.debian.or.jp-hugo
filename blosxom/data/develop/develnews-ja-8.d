開発ニュース寄せ集め (第 8 号)

<p>このニュースは <a href="http://wiki.debian.org/DeveloperNews">http://wiki.debian.org/DeveloperNews</a> でまとめたものです。
寄稿はご自由にどうぞ。</p><h5>~/.ssh/authorized_keys はデフォルトでは無効なままとなります</h5><p>Peter Palfrader さんは <a href="http://lists.debian.org/debian-infrastructure-announce/2008/05/msg00004.html">debian-infrastructure-announce</a> にて、Debian
システム管理者 (DSA) は ~/.ssh/authorized_keys
を再度使えるようにはしない、と発表しました。debian.org のマシンへ鍵ベースの SSH
接続をセットアップするには、各自、公式の <a href="https://db.debian.org/doc-mail.html">LDAP インフラ</a>を使ってください。
といっても例外もあります。Peter さんの引用ですが：</p><blockquote>
  <p>何らかの更新や、プロジェクトのマシン間でのファイル同期、といった自動化された
タスクのために特定のホストにおいてのみ鍵が必要な場合があるはずです。
そのようなときには、特定のホスト上の特定のユーザに対して、authorized_keys
ファイルを編集できるようにすることも可能です。たいていの場合、
それらの鍵は特定のホストから (from="<var>xyz</var>" で) のみ使ったり、
特定のコマンド (command="<var>foobar</var>" で)
のみ実行を許可するように制限されることになります。そういう場合は、DSA
にコンタクトしてください。</p>
</blockquote><p>-- Raphael Hertzog</p><h5>メーリングリスト利用上の注意のマイナーアップデート</h5><p>Debian メーリングリストに適用される<a href="http://www.debian.org/MailingLists/#codeofconduct">利用上の注意</a>が少しアップデートされました。
公開された返答と一緒に、望まない個人宛てコピーを受け取った人は、
そのことについて公開の場で文句をつけないのが望ましく、そのかわりに、
その行為を行った人に個人宛にメールをすべきです。</p><p>-- Raphael Hertzog</p><h5>上流に向けた文書を準備しています</h5><p>上流の開発者がディストリビュータの要望について調べるときの、
単一の情報源とするため、Debian Wiki 内の <a href="http://wiki.debian.org/GettingPackaged">http://wiki.debian.org/GettingPackaged</a>
に、ページが作成されました。(プログラミング言語、アプリケーションサーバ、
ビルドシステムといった) フレームワークのメンテナから、
それらのフレームワーク内のコンポーネントにおけるベストプラクティス (例えば「python
モジュールは distutils を使用すべきである」など)
を文書化する手助けが要望されています。</p><p>-- 2008 年 5 月 18 日 Simon Richter</p><h5>d-i ベータ 2 の準備でベータ 1 が利用不可能に</h5><p>Lenny 用 Debian Installer ベータ 2 のリリース準備が始まっています。
Debian アーカイブへの変更により、Lenny ベータ 1 リリースの、
いくつかのインストール用メディアが壊れていることが知られています。</p><p>該当するのは、(netboot、mini.iso、floppy といった)
ネットワークミラーからインストーラコンポーネントを読み込むイメージです。
代わりにデイリービルドイメージを使ってください。</p><p>-- 2008 年 5 月 8 日 Otavio Salvador</p><h5>新たな「移行チェック」ツール</h5><p>devscripts パッケージのバージョン 2.10.27 で、"transition-check"
という名前の新たなツールが導入されました。</p><p>このツールは、あるパッケージ (もしくは一連のパッケージ群) が、
リリースチームが現時点で不安定版 (unstable)
へのアップロードをブロックしている移行に関係しているかどうか判断してくれます。</p><p>-- Adam D. Barratt</p><h5>この記事について</h5><p>この記事は <a href="http://lists.debian.org/debian-devel-announce/2008/05/msg00013.html">debian-devel-announce に流れたニュース</a>の翻訳です。
翻訳は荒木淳さん・石井一夫さん・今井伸広さん・倉敷悟さん・中尾隆さんが行いました。
また、倉敷悟さん・武井伸光さんから多数のコメントをいただきました。
ありがとうございます。</p>
