開発ニュース寄せ集め (第 27 号)

<p>このニュースは <a href="http://wiki.debian.org/DeveloperNews">http://wiki.debian.org/DeveloperNews</a> でまとめたものです。
寄稿はご自由にどうぞ。</p><h5>Planet Debian 派生物</h5><p><a href="http://wiki.debian.org/Derivatives/Census">Debian 派生物調査</a>からの最初の具体的成果は、<a href="http://planet.debian.org/deriv/">Planet Debian 派生物</a>
の作成です。Debian から派生したディストリビューションに興味をもっている皆さんの
ために、派生物調査で挙げられているすべてのディストリビューションについてのブログと
プラネットを集積しています。
調査に参加するディストリビューションが増え、調査ページのメンテナが新しくブログや
planet の URL を追加するにつれて、フィードのリストは半自動的に拡大されます。
さらに Debian のインフラストラクチャーへの派生物に関する情報の<a href="http://wiki.debian.org/Derivatives/Integration">統合</a>が計画
されており、次の目標はパッケージ情報です。</p><p>-- Paul Wise</p><h5>win32-loader.exe が GNU/Hurd Debian-Installer にアクセスしやすくなりました</h5><p>バージョン 0.7.4 以降、Samuel Thibault さんのお陰で、(ミラーネットワークから利用出来る)
<a href="http://ftp.ch.debian.org/debian/tools/win32-loader/unstable/win32-loader.exe">win32-loader.exe</a> のスタンドアロンフレーバにより、Windows のホストを GNU/Hurd
Debian-Installer で簡単にリブート出来るようになりました。</p><p>Windows ホストから実行する際、この実行ファイルは Debian-Installer をダウンロードし、
Debian-Installer がシームレスに起動するように Windows のブートローダを設定します。
どのバージョンの Windows でも動作すると思われ、GNU/Linux、GNU/kFreeBSD および
GNU/Hurd フレーバの Debian-Installer をダウンロード出来ます。</p><p>-- Didier Raboud</p><h5>Wiki のバグステータスが Launchpad のバグに対応</h5><p>wiki にあるバグステータス JavaScript はバグのステータスをチェックして、有用な
タイトルをつけて、クローズされているかオープンになっているかによって異なるスタイル
を適用します。<a href="http://wiki.debian.org/PaulTagliamonte">Paul Tagliamonte さん</a>の助力のおかげで、既存の Debian バグ
トラッカーに加えて Launchpad バグトラッカーへのリンクがサポートされました。これを
利用するには、UbuntuBug もしくは LaunchpadBug の <a href="http://wiki.debian.org/InterWiki">InterWiki</a>リンクを
使うか、Launchpad のバグに通常のリンクをはってください。これを拡張して他のバグ
トラッカーもサポートするようにしたい人がいたら、<a href="http://wiki.debian.org/Teams/DebianWiki#Infrastructure">wiki 設定</a>を確認の上、パッチ
をつけて<a href="http://bugs.debian.org/wiki.debian.org">wiki.debian.org</a>疑似パッケージにバグを登録してください。</p><p>-- Paul Wise</p><h5>dh-exec が不安定版に入りました</h5><p>Debhelper (compat レベルが 9 以上) では、設定ファイルを実行可能にして、そういった
スクリプトの出力を設定ファイルの中身として使うことができます。</p><p><a href="https://github.com/algernon/dh-exec">dh-exec</a> は、そのようなスクリプトの作成を、標準化されて理解しやすい形で
補助するためのスクリプトとプログラムの集合です。</p><p>これらによって、次のようなタスクにヘルパーが提供されます：</p><ul>
<li>様々な debhelper ファイルの内部で変数を展開 (環境変数や、multi-arch 関連も含めた
dpkg-architecture による変数)</li>
<li>dh_install を拡張し、拡張書式を使うことでコピー時のファイル名変更をサポート</li>
</ul><p>dh-exec は不安定版で利用可能で、上記の機能のいずれかを必要とする人は誰でもすぐに
使うことができます。</p><p>-- Gergely Nagy</p><h5>派生物とのパッチ生成を試作</h5><p>Debian <a href="http://wiki.debian.org/Derivatives/Census">派生物調査</a>の<a href="http://wiki.debian.org/Derivatives/Integration">統合</a>における、2番目の具体的な成果は
<a href="http://lists.debian.org/debian-derivatives/2011/10/msg00061.html">Debian と派生物間の debdiff を生成</a>することです。
Debian から派生したディストリビューションで、どのようなパッチが適用されているのか
興味がある人向けに、このサービスでは、派生物調査で示されているディストリビューションで、
その apt sources.list コードに deb-src エントリがあるもの全てについて、
[snapshots.debian.org[13] のデータベースとファイルを使って一連のパッチを生成します。
多くの問題があって、サービスはまだ稼動していません。サービスが稼動すれば、一連のパッチは
より多くのディストリビューションが調査に参加し、調査ページのメンテナが deb-src 行を彼らの
apt sources.list に追加するにつれて、自動的に拡大するでしょう。
Debian インフラストラクチャへの派生物情報の<a href="http://wiki.debian.org/Derivatives/Integration">統合</a>はさらに進められる予定で、次の
目標は派生物のパッケージ情報をまとめていくことです。</p><p>-- Paul Wise</p><h5>この記事について</h5><p>この記事は <a href="http://lists.debian.org/debian-devel-announce/2011/12/msg00001.html">debian-devel-announce に流れたニュース</a>の翻訳です。
翻訳は石井一夫さん・今井伸広さん・倉敷悟が行いました。
また、victory さんから多数のコメントをいただきました。
ありがとうございます。</p>
