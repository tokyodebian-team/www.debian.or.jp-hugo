開発ニュース寄せ集め (第 20 号)

<p>このニュースは <a href="http://wiki.debian.org/DeveloperNews">http://wiki.debian.org/DeveloperNews</a> でまとめたものです。
寄稿はご自由にどうぞ。</p>

<h5>ビルド失敗時の通知</h5>
<p>自動ビルドシステムが、ビルド失敗時に、パッケージ追跡システム (PTS) 宛にメールで通知するようになりました (キーワード buildd)。
2009 年 1 月 7 日以降に、キーワード "default" でパッケージを購読した人は、これらの通知を受けとるようになります。
また、キーワード buildd は、新規にパッケージを購読する際のデフォルトキーワードに含まれます。
これらの通知は、必ずしもパッケージに対するバグ報告だとは限らず、他のパッケージや一時的なインストール不可、
アーキテクチャ固有の破損に起因する場合もあることに留意してください。いずれにせよ、ビルドログを見ていただけると幸いです。</p>
<p>-- Philipp Kern</p>

<h5>experimental, non-free, backports.org, debian-edu そして volatile 
の公式自動ビルドインフラストラクチャへの統合</h5>
<p>これまでは、非公式自動ビルドネットワークによってそれぞれ独自に
 wanna-build の状態とログが処理されてきました。これらは今後
 buildd.debian.org で動いている公式な設備へ統合されます。
非公式ビルドの過去のログもまた、統合される予定です。これによってビルドの状況とログは、
 <a href="https://buildd.debian.org">リンク先</a> を通じて、普段通り取得できるようになります。
実際のビルドは、多様なアーキテクチャにおいて、ビルドの負荷に依存して、
公式もしくは非公式のホストへと分けられるでしょう。</p>
<p>-- Philipp Kern</p>

<h5>buildd ソフトウェアの統一</h5>
<p>自動ビルドインフラストラクチャに使われる sbuild と buildd のバージョンを統一する作業が進行しています。
不安定版からの公式なバージョンに依存するようにはまだなっていませんが、
使用されている sbuild はすでにそれらに近づいていて、
<a href="https://buildd.debian.org/apt/">リンク先</a> で見つけることができます。
ほぼすべての buildd がすでに単一のバージョンの buildd と sbuild を使用するように変更されていて、
今月末までに移行を完了出来ると見ています。
これはパッケージの展開やインストールに、chroot 環境に含まれるツールを使用するようにもなっています。</p>
<p>-- Philipp Kern</p>

<h5>debtags に基づく WNPP のバグの閲覧</h5>
<p><a href="http://members.hellug.gr/serzan/wnpp/">wnpp-by-tags</a> により、バグの属しているパッケージの debtags に基づいて
WNPP のバグ (RFP を除く) を閲覧することが可能となりました。
たとえば、どんなバグが <a href="http://members.hellug.gr/serzan/wnpp/suite/gnome.html">suite::gnome</a> に関係するか、
または、どんなバグが <a href="http://members.hellug.gr/serzan/wnpp/implemented-in/python.html">implemented-in::python</a> 
にあるパッケージに属しているかを見ることができます。</p>
<p>-- Serafeim Zanikolas</p>

<h5>PTS の CSS スキンが無効化されています</h5>
<p>ここのところ、PTS ではデフォルト以外の CSS スキンが壊れた状態です。結果として、CSS 選択フォームが一時的に無効にされました。
デフォルト以外の CSS を本当に気にしている人は、debian-qa@lists.debian.org に連絡して、メンテナンスを申し出てください。</p>
<p>-- Stefano Zacchiroli</p>

<h5>この記事について</h5>
<p>この記事は <a href="http://lists.debian.org/debian-devel-announce/2010/01/msg00005.html">debian-devel-announce に流れたニュース</a>の翻訳です。
翻訳は石井一夫さん・今井伸広さん・佐々木洋平さん・中尾隆さん・倉敷悟が行いました。
また、victory さん・かねこせいじさんから多数のコメントをいただきました。ありがとうございます。</p>
