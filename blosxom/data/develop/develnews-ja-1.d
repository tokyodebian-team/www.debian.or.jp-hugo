開発ニュース寄せ集め (第 1 号)

<p>こんにちは。</p><p>このメールは、Wiki ページ <a href="http://wiki.debian.org/DeveloperNews">http://wiki.debian.org/DeveloperNews</a>
から単純にカット・アンド・ペーストしたものです。
この Wiki ページでは、すべての Debian 開発者 (DD) が知っておくべきである一方で、
通知のために debian-devel-announce (d-d-a) にわざわざメールを 1 通投稿するほどのことではないような、
開発関連の小さなニュースをまとめようと試みています。
ページの編集や次号に載せるニュースの追加は御自由にどうぞ (発行は不定期になるでしょう。
十分な内容が (つまりニュースが 5 つほど) 揃ったときに、メールが投稿されます)。
第 1 号については、主に dpkg 関連のニュースを扱っています。
将来的には、他のチームも何かの情報を共有するのにこの場を使うとよいでしょう。</p><h5>debian/control の Homepage フィールド</h5><p>かなり長い間、パッケージ説明の中に上流プロジェクトの URL
を記述することになっていました。現在では control ファイルの source
節にある新しい Homepage フィールドに記述する方法が推奨されています。Homepage
フィールドは、
ソースパッケージやバイナリパッケージに伝播します (その結果 Sources.gz
と Packages.gz ができます)。control ファイルにある対応した binary 節に 2
つ目の Homepage フィールドを記述することで、特定のバイナリパッケージの Homepage
フィールドを上書きできます。</p><p>これは dpkg 1.14.6 で実装されました。</p><h5>debian/control の Vcs-* フィールド</h5><p>パッケージをバージョン管理システム (VCS) で管理している場合は Vcs-*
フィールドにそのリポジトリの場所を指定してください。
リポジトリの内容をウェブ上で閲覧できるようにしている場合は Vcs-Browser
フィールドにその URL を指定します。</p><p>dpkg が公式にサポートしているフィールドは以下の通りです。</p><ul>
<li>Vcs-Arch (arch and baz)</li>
<li>Vcs-Bzr (bazaar)</li>
<li>Vcs-Cvs</li>
<li>Vcs-Darcs</li>
<li>Vcs-Git</li>
<li>Vcs-Hg (Mercurial)</li>
<li>Vcs-Mtn (Monotone)</li>
<li>Vcs-Svn (Subversion)</li>
</ul><p>いくつか例を示します。</p><pre><code>Vcs-Browser: http://ikiwiki.info/cgi-bin/viewvc.cgi/trunk/?root=ikiwiki
Vcs-Svn: svn://svn.kitenet.net/ikiwiki/trunk
</code></pre><p>これは dpkg 1.14.6 で実装されました。</p><h5>dpkg-buildpackage がデフォルトで fakeroot を使うように</h5><p>もう '-rfakeroot' オプションを付ける必要はありません。root
権限で実行しておらず、fakeroot が利用できる場合には、自動的に fakeroot
が使われるようになりました。</p><p>これは、dpkg 1.14.7 で実装されました。</p><h5>dpkg-buildpackage が並列ビルドをサポート</h5><p>パッケージのビルドシステムが make に基づいている場合は、新しいオプション '-j'
を使って、並列ビルドを有効化できます。
それ以外の場合、パッケージが並列ビルドをきちんとサポートする必要があります。</p><p>これは dpkg 1.14.7 で実装されました。</p><h5>dpkg-shlibdeps が、シンボルに基く依存性をサポート</h5><p>ライブラリパッケージ用に新たに導入された symbols 制御ファイルを生成するのに、
dpkg-gensymbols が使えます (生成されたこの制御ファイルは、dpkg-shlibdeps
によって、これまでよりも正確な依存関係を作るのに使用されます)。
さらに詳しくは、後ほど説明します。現在、
ガイドラインに分かりやすい使用法を追記してくれるアーリーアダプターを募集中です。
さらに詳しい情報は、
<a href="http://lists.debian.org/debian-devel/2007/11/msg00611.html">http://lists.debian.org/debian-devel/2007/11/msg00611.html</a> をご参照ください。</p><p>これは、dpkg 1.14.8で実装されました。</p><h5>この記事について</h5><p>この記事は <a href="http://lists.debian.org/debian-devel-announce/2007/11/msg00006.html">debian-devel-announce に流れたニュース</a>の翻訳です。
翻訳は荒木淳さん・石井一夫さん・今井伸広さん・倉敷悟さん・中尾隆さん・小林儀匡が行いました。
また、倉敷悟さん・武井伸光さん・やまねひできさんから多数のコメントをいただきました。
ありがとうございます。</p>
