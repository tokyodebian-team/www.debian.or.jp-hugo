開発ニュース寄せ集め (第 16 号)

<p>このニュースは <a href="http://wiki.debian.org/DeveloperNews">http://wiki.debian.org/DeveloperNews</a> でまとめたものです。
寄稿はご自由にどうぞ。</p><h5>GCC 4.4 に関するビルドの失敗が BTS で追跡されています</h5><p>GCC 4.4 は 4 月 21 日にリリースされました。私は 11 月に GCC 4.4 のスナップショットで
アーカイブをコンパイルし、BTS に多くの問題を報告しました。
私は GCC 4.4 でそのアーカイブを再びコンパイルし、いくつか追加の問題を報告しただけです。
合計で、<a href="http://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=ftbfs-gcc-4.4;users=debian-gcc@lists.debian.org">315 のバグ</a> が報告され、現在このうち 180 が(ほとんどがパッチがありますが)まだ開いたままです。
GCC 4.4 であなたのパッケージをコンパイルしてみてください。</p><p>-- Martin Michlmayr</p><h5>Debian は EGLIBC へ切り替えようとしています</h5><p>たった今、Embedded GLIBC (EGLIBC) をアーカイブにアップロードしました (現在
NEW queue の中で処理されるのを待っています)。まもなく GNU C ライブラリ (GLIBC)
からそれに置き換わるでしょう。EGLIBC は、元の GLIBC と互換性のあるソースとバイナ
リを保持した GLIBC の改良版です。ユーザの視点からは、パッケージ名は何も変わってい
ない (ソースパッケージとソースを含むバイナリパッケージを除く) ので、移行処理の必要
はありません。</p><p>この変更の動機についてのさらなる詳細は以下のページで見られます:
http://blog.aurel32.net/?p=47</p><p>-- Aurelien Jarno</p><h5>Debian Common Language Infrastructure (CLI) メーリングリスト</h5><p>リストシステム管理者はとても素晴らしいことに、新規の <a href="http://lists.debian.org/debian-cli/">debian-cli メーリング
リスト</a>を設置しました。
このメーリングリストは、Debian Common Language Infrastructure (CLI) の使用や、
開発に関するものです。もし、Monoや、GNU Portable.NET、LLVM VMKit を用いて
ソフトウェアを開発したい、あるいは Debian パッケージを作成したい人は、この
メーリングリストに入るのは正解です。また、C# や、VB.NET、Boo、Nemerle、その他の
CLI  による(が使える) プログラミング言語でソフトウェアを書いている人も、
このメーリングリストに歓迎です。</p><p>-- Mirco Bauer</p><h5>ヨーロッパの開発者向けの新たなアップロードキュー</h5><p>アメリカへのアップロードは遅くなることがあり、ries のキューからの警告
メールにつながるので、ヨーロッパのどこかにアップロードキューを設置して
ほしいという要望を受けました。という訳で、以下のホストを設置しました:</p><p>ftp.eu.upload.debian.org</p><p>ftp.upload.debian.org と同じように使え、バックグラウンドでは同じソフト
ウェアが動いているので、コマンドファイルもすべてサポートされています。</p><p>また、初めて IPv6 で到達可能なアップロードキューでもあります。</p><p>-- Jörg Jaspert</p><h5>LXDE の更新</h5><p>LXDE(軽量な X11 デスクトップ環境)が近日中に更新されます。既存の多くの
コンポーネントが更新されます。また、新しいコンポーネントが導入されてディ
スク I/O の軽減と、キーボードとマウスの設定を追加する GUI の提供が図ら
れます。</p><p>-- Andrew Lee (李健秋)</p><h5>この記事について</h5><p>この記事は <a href="http://lists.debian.org/debian-devel-announce/2009/05/msg00011.html">debian-devel-announce に流れたニュース</a>の翻訳です。
翻訳は荒木淳さん・石井一夫さん・今井伸広さん・佐々木洋平さん・中尾隆さんが行いました。
また、やまねひできさん、victory さん、岩松信洋さんから多数のコメントをいただきました。
ありがとうございます。</p>
