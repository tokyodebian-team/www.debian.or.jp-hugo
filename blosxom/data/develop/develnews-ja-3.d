開発ニュース寄せ集め (第 3 号)

<p>このニュースは <a href="http://wiki.debian.org/DeveloperNews">http://wiki.debian.org/DeveloperNews</a> でまとめたものです。
寄稿はご自由にどうぞ。</p><h5>symbols ファイルを含むようにライブラリを更新中</h5><p>いくつかのライブラリパッケージは既に更新されて symbols
ファイルを提供しています (zlib1g、libxml2、libogg0、liburiparser1、libvorbis0a
など)。適用の進度はまだゆっくりですが、dpkg 1.14.14 で dpkg-shlibdeps
がビルド用の依存関係からバージョンを抽出し、
実際の依存関係に注入するようになったので、今後は増加する見込みです。
この安全対策を<a href="http://lists.debian.org/debian-devel/2007/12/msg00036.html">提案した</a>のは Loïc Minier さんです。</p><p>libgtk2-0 のように、
シンボルをもとにすると実行時に本当は必要となるバージョンより高いバージョンへの依存関係が生成されるパッケージについては、
そのパッケージの使用するライブラリの最小バージョンの依存関係を指定する手段を提供します。
libgtk2-0 の場合、ビルド時に (Build-Depends ヘッダで判別されて)
必要になる libgtk2-dev の最小バージョンが、
実行時の依存関係に追記される最小バージョンとなります。このために、symbols
ファイルは対応している -dev パッケージ名を dpkg-shlibdeps に示す必要があります。
これは "Build-Depends-Package"
という特別なヘッダでなされます (詳細と正確な記法については、man dpkg-shlibdeps
と man deb-symbols を見てください)。</p><p>パッケージに symbols ファイルを追加する時は、<a href="http://wiki.debian.org/UsingSymbolsFiles">ガイドライン</a>に従ってください。
この wiki ページへの寄稿もご自由に。
多くのパッケージが依存しているライブラリを調べる、それらにバグ報告をする、
そして重要なライブラリのほとんどで symbols
ファイルが使われることをリリースゴールとするために作業をとりまとめる、
といったことのために、有志も募集しています。</p><p>-- Raphaël Hertzog</p><h5>新参の貢献者に助けてもらえるよう、易しいバグをマークしよう</h5><p>Lucas Nussbaum さんは、彼のブログで、Debian
プロジェクトが新参の貢献者にいかにより親切になれるかについて<a href="http://www.lucas-nussbaum.net/blog/?p=268">議論しました</a>。
彼はまた、このために新しい wiki ページ <a href="http://wiki.debian.org/HelpDebian/Start">http://wiki.debian.org/HelpDebian/Start</a>
も<a href="http://www.lucas-nussbaum.net/blog/?p=269">作りました</a>。</p><p>彼の提案のもう一つの重要な点は、
すぐに取り込まれる重要で有用な作業を新参の貢献者ができるよう、
易しいバグを特別なタグでマークすることです。
<a href="http://wiki.debian.org/qa.debian.org/GiftTag">http://wiki.debian.org/qa.debian.org/GiftTag</a> の説明を見てください。</p><p>-- Raphaël Hertzog</p><h5>D-I 国際化ニュース</h5><p>Christian Perrier さんが debian-installer (d-i)
の翻訳作業に関する<a href="http://www.perrier.eu.org/weblog/2007/12/23#d-i_i18n-news">ニュース</a>を出しました。アフリカの言語 (アムハラ語)
が新しく利用可能となりました。また、d-i の翻訳文は内容の重要性に応じて 5
つのレベルに分けられました (常に使用される文向けのレベル 1 から、
主流でないアーキテクチャ上の特別な場合のみに使用される文向けのレベル 5 です)。
この分類により、まだインストールシナリオの大部分しか翻訳されていなくとも、
より早く翻訳版 d-i を有効にすることができます。</p><p>-- Raphaël Hertzog</p><h5>packages.qa.debian.org (PTS) のアップデート</h5><p>たぶんすでにお気づきのことかと思いますが、<a href="http://packages.qa.debian.org">PTS</a> が改装されました。
新たなレイアウトと、 Debian "らしい" 色づかいの css になりました。</p><p>-- Stefano Zacchiroli</p><h5>bugs.qa.debian.org が更新</h5><p>遅ればせながら、Debian QA チームの<a href="http://bugs.qa.debian.org/">バグ退治用ウェブページ</a>が lenny
向けに更新され、ベースシステムの内容についての構想も決まったので、
もう手動で更新する必要はなくなりました。</p><p>-- Colin Watson</p><h5>フォントレビューを更新します</h5><p><a href="http://wiki.debian.org/Fonts">Fonts Task Force</a> では、
<a href="http://pkg-fonts.alioth.debian.org/review/">Debian に含まれるフォントのレビュー</a>を毎週更新できるようにしました。
このレビューは non-free のフォントや重複したフォント、
分割したパッケージにすべきフォントを特定するために使います。</p><p>-- Paul Wise</p><h5>この記事について</h5><p>この記事は <a href="http://lists.debian.org/debian-devel-announce/2007/12/msg00011.html">debian-devel-announce に流れたニュース</a>の翻訳です。
翻訳は荒木淳さん・石井一夫さん・今井伸広さん・倉敷悟さん・中尾隆さん・小林儀匡が行いました。
また、石井一夫さん・今井伸広さん・かねこせいじさん・武井伸光さん・やまねひできさんから多数のコメントをいただきました。
ありがとうございます。</p>
