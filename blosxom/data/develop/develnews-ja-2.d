開発ニュース寄せ集め (第 2 号)

<p>このニュースは <a href="http://wiki.debian.org/DeveloperNews">http://wiki.debian.org/DeveloperNews</a> でまとめたものです。
寄稿はご自由にどうぞ。</p><h5>非公式アーキテクチャのための新アーカイブ</h5><p><a href="http://ftp.debian-ports.org/">非公式アーキテクチャのアーカイブ</a>のホスティング用に、
<a href="http://www.debian-ports.org/">新しいホスト</a>がセットアップされました。このホストは、gnuab.org
に代わって、現在、armel、
hurd-i386 (リリースされていないもののみ)、kfreebsd-i386 と kfreebsd-amd64
のアーキテクチャのホストになっています。</p><p>開発者は、自分の担当するパッケージの<a href="http://buildd.debian-ports.org/">ビルドログ</a>を見て、
その<a href="http://buildd.debian-ports.org/status/">状況</a>をチェックすることができます。</p><h5>PTS の Web インタフェース</h5><p><a href="http://packages.qa.debian.org/">PTS</a> の Web インタフェースにいくつか変更がありました。</p><ul>
<li>PTS ページに「Latest news (最新ニュース)」部分の RSS フィードが付け加えられました
(zack さんによる変更)。</li>
<li>パッケージごとの <a href="http://svnbuildstat.debian.net/">svn-buildstat 情報</a>へのリンクが付け加えられました
(luk さんと kibi さんによる変更)。</li>
<li>新しい情報の表現がいくつか付け加えられました。それらは、DM-Upload-Allowed
フィールド、メンテナが Wiki の <a href="http://wiki.debian.org/LowThresholdNmu">LowThresholdNmu ページ</a>に名前を連ねているかどうか、
Homepage フィールドです (zack さんによる変更)。</li>
</ul><h5>deb ファイルでの他の圧縮アルゴリズム</h5><p>dpkg-dev および dpkg パッケージには、バイナリパッケージを gzip
以外のもの (例えば bzip2 など)
を使って圧縮する機能が<a href="http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=34727">バージョン 1.11 以来</a>ずっと存在しています。</p><p>dpkg-deb の "-Z" スイッチを使うことでその機能を使うことが出来ます。
または、debian/rules 内から "dh_builddeb -- -Z bzip2" を使います。</p><p>残念なことに、<a href="http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=447257">lintian</a> と <a href="http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=452329">linda</a>
の双方とも、この機能を使うべきではないという誤った警告を出します。</p><h5>高機能なパッケージ検索のプロトタイプが experimental に</h5><p>Enrico Zini さんが、システム全体にわたるパッケージの <a href="http://www.xapian.org/">Xapian</a>
ベースのインデックスを生成するインデクサのプロトタイプ、<a href="http://packages.debian.org/experimental/apt-xapian-index">apt-xapian-index</a>
を作成しました。</p><p>このインデックスを使うと、パッケージの説明やタグが非常に高速に検索できます。
また、インデックスに特別な情報を付加するのに、
任意のパッケージをプラグインとしてインストールできます。</p><p>さらに、インデックスの使い方を書いた、詳しい<a href="http://www.enricozini.org/2007/debtags/apt-xapian-index.html">チュートリアル</a>も利用できます。</p><p><a href="http://packages.debian.org/experimental/apt-xapian-index">apt-xapian-index</a> (現在は experimental にあります) は、
特にインデックスの構造やプラグインのインタフェースに関する技術的なフィードバックが得られ次第、
不安定版 (unstable) にアップロードされる予定です (訳注:
すでに<a href="http://packages.debian.org/sid/apt-xapian-index">不安定版 (unstable) にアップロードされ</a>、experimental
からは削除されています)。</p><h5>changelog エントリには変更を記述すること</h5><p>(debian-devel-changes@l.d.o に流れる) changelog を読んでいると、
どのように修正したのかを明示せずに修正した問題を書いたようなお粗末な changelog
エントリがやたらと目につきます。changelog
では、その変更に至った経緯はもちろん重要ですが、変更内容の説明も重要です。
いくつかの例ではっきりすればいいのですが：</p><p>悪い例：</p><pre><code>* lintian のエラーを修正
</code></pre><p>良い例：</p><pre><code>* (lintian が検出した) description 中のスペルミス (maintainance → maintenance) を修正
</code></pre><p>悪い例：</p><pre><code>* 新しい dpkg-shlibdeps 用にパッケージビルド方法を修正
</code></pre><p>良い例：</p><pre><code>* LD_LIBRARY_PATH=/usr/lib/mypackage を dpkg-shlibdeps に渡して、プライベート
  ライブラリが適切に検出されるようにした
</code></pre><p>changelog エントリを書くときは、わざわざ両方のバージョンの debdiff
を読まなくても、それなりにきちんと変更内容がわかるようにしなくてはならない、
ということを頭に置いてください。</p><h5>この記事について</h5><p>この記事は <a href="http://lists.debian.org/debian-devel-announce/2007/12/msg00007.html">debian-devel-announce に流れたニュース</a>の翻訳です。
翻訳は荒木淳さん・石井一夫さん・今井伸広さん・倉敷悟さん・中尾隆さん・小林儀匡が行いました。
また、倉敷悟さん・武井伸光さん・やまねひできさんから多数のコメントをいただきました。
ありがとうございます。</p>
