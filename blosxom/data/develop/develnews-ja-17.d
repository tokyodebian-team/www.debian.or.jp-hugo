開発ニュース寄せ集め (第 17 号)

<p>このニュースは <a href="http://wiki.debian.org/DeveloperNews">http://wiki.debian.org/DeveloperNews</a> でまとめたものです。
寄稿はご自由にどうぞ。</p><h5>PTS ニュース: アクセシビリティ、piuparts、地域化 (l10n)</h5><p>Debconf9 の期間中に PTS (<a href="http://packages.qa.debian.org">http://packages.qa.debian.org</a>) に多数の変更が導入されました。</p><ul>
<li><p>PTS のページレイアウトがデザインし直されました。ほとんどが目につきにくい部分についてです。
table タグによるレイアウトをしなくなったので、利用しやすくなっているはずです。
少々不便なところは、デフォルト以外の CSS スタイルシートが現在壊れていることです。</p></li>
<li><p>PTS は、piuparts によって発見されたパッケージのインストール問題のエラーを表示するように
なっています (詳細な情報については <a href="http://piuparts.debian.org">http://piuparts.debian.org</a> を参照してください)。</p></li>
<li><p>PTS は、パッケージの翻訳状況と、Debian 固有の文字列 (例えば debconf のテンプレート)
が翻訳を必要としている時に警告の情報を表示するようになっています。</p>

<p>-- Stefano Zacchiroli</p></li>
</ul><h5>cvs.debian.org (と webwml レポジトリ) が Alioth へ移動</h5><p>cvs.debian.org が gluck.debian.org から引っ越しました。
新しい場所は、その他全てのバージョン管理システムを持っている alioth.debian.org
です。SSH ホスト鍵が変更されることと、パスワードが (Debian の公式 LDAP
から利用されるのではなく) alioth のパスワードを使うようになることから、主に webwml
リポジトリ (ウェブサイト) に変更をコミットする人にこの変更の影響があります。</p><p>-- Raphaël Hertzog</p><h5>2009 年 Git ユーザ調査</h5><p><a href="http://www.survs.com/survey?id=2PIMZGU0&amp;channel=Q0EKJ3NF54">2009 年 Git ユーザ調査</a>が始まりました!
Git の改善があなたの Debian 関連作業に対するニーズをより満たすと思うなら、
数分ほど回答にお時間をください。調査は 2009 年 9 月 15 日まで、です。</p><p>-- Raphaël Hertzog</p><h5>新しいソース形式</h5><p>新しいソース形式への移行検討に関連する状況の更新が <a href="http://lists.debian.org/debian-devel/2009/07/msg00937.html">debian-devel に投稿
されました</a>。
テスト用ソースパッケージ (と関連する APT リポジトリ) が利用可能です。
主な障害は、新しいソース形式のサポートを dak (Debian Archive Kit) に
追加するブランチをマージすることです。(これまでに) 行われてきたことや
(今後の) 予定についてのさらなる詳細については、メールを確認してください。</p><p>-- Raphaël Hertzog</p><h5>新規メーリングリスト debian-user-dutch</h5><p>オランダ語を使う Debian ユーザの新しいメーリングリストが作成されました。
デスクトップを英語で使っていても参加は可能です。
ここからご参加ください: <a href="http://lists.debian.org/debian-user-dutch/">http://lists.debian.org/debian-user-dutch/</a></p><p>-- Paul van der Vlis</p><h5>この記事について</h5><p>この記事は <a href="http://lists.debian.org/debian-devel-announce/2009/08/msg00002.html">debian-devel-announce に流れたニュース</a>の翻訳です。
翻訳はやまねひできさんが行いました。
また、武井伸光さん、victory さんから多数のコメントをいただきました。
ありがとうございます。</p>
