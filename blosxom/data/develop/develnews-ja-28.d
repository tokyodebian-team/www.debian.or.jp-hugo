開発ニュース寄せ集め (第 28 号)

<p>このニュースは <a href="http://wiki.debian.org/DeveloperNews">http://wiki.debian.org/DeveloperNews</a> でまとめたものです。
寄稿はご自由にどうぞ。</p><h5>Debian メンバーのポートフォリオサービス</h5><p>Debian メンバーのポートフォリオサービス (以前は Debian 開発者のポートフォリオ
サービスとして知られていました) が http://portfolio.debian.net/ で利用できる
ようになりました。この変更の目的は、パッケージングをしない人も私達のメンバーとなった
ことへの対応です。サービスはまだ開発中で、私がホストしています。サービスの改善の提案
や、皆さんの言語への翻訳を歓迎します。サービスのソースコード (Python/Pylons) は、
GNU Affero General Public ライセンスによる許諾のもと、<a href="http://debianstuff.dittberner.info/gitweb.cgi?p=ddportfolioservice.git">公開 git リポジトリ</a>
で入手できます。</p><p>-- Jan Dittberner</p><h5>PTS でのリリースゴールバグの表示</h5><p>パッケージトラッキングシステムでは、あなたが担当しているリリースゴール
の一部となっている未解決のバグを、常にTODOとして強調して表示するように
なりました。例えば http://packages.qa.debian.org/twinkle のように。</p><p>-- Raphaël Hertzog</p><h5>トランジションの状況が PTS に表示されます</h5><p>パッケージ追跡システムで、進行中もしくは予定されている testing のトランジションにその
パッケージが含まれている場合、目立つ警告が表示されるようになります。これによって、トランジション
をさまたげかねないアップロードをメンテナが避けるようにし、<a href="http://release.debian.org/transitions/">トランジションのトラッカー</a>、
<a href="http://lists.debian.org/debian-release/">debian-release アーカイブ</a>、<a href="http://bugs.debian.org/cgi-bin/pkgreport.cgi?users=release.debian.org@packages.debian.org;tag=transition">移行関連バグ一覧</a>へのリンクを提供します。</p><p>-- Raphaël Hertzog</p><h5>dh_linktree ヘルパーツール</h5><p>パッケージに PHP/javascript ファイルの埋め込みコピーが含まれている (もしくはその他の
ファイルが as-is として直接配布される) 場合、それをパッケージされたものへのシンボリック
リンクに置き換えるのは面倒なものです。新しい dh_linktree ツール (dh-linktree
パッケージ) がこの作業を手助けしてくれます。
埋め込みコピーに対応する Debian パッケージをビルド依存に含めて、何をするのか書かれた
debian/foo.linktrees ファイルを作成するだけで構いません。dh_linktree はシンボリック
リンクのツリーを作成し、(必要に応じて) ${misc:Depends} に依存関係を追加してくれます。
いくつかの動作モードがあります:</p><ul>
<li>埋め込み: 条件つきで、指定された場所にディレクトリ全体を統合します</li>
<li>置き換え: パッケージがインストールしたファイルを、対応する公式パッケージのファイル
へのリンクで単に置き換えます</li>
<li>重複排除: 置き換えと同様ですが、ファイルが全く同一の場合のみ処理を行います</li>
</ul><p>詳細については、dh-linktree パッケージをインストールして、dh_linktree(1) の
マニュアルを読んでください。</p><p>-- Raphaël Hertzog</p><h5>パッチのタグ付けガイドライン: DEP-3 のステータスが ACCEPTED になりました</h5><p><a href="http://dep.debian.net/deps/dep3/">DEP-3</a> は ACCEPTED ステータスに昇格しました。このフォーマットによって、
Debian パッケージに追加したパッチを文書化するにあたって使用が推奨されている
一連のヘッダが標準化されます。</p><p>-- Raphaël Hertzog</p><h5>この記事について</h5><p>この記事は <a href="http://lists.debian.org/debian-devel-announce/2012/01/msg00005.html">debian-devel-announce に流れたニュース</a>の翻訳です。
翻訳は佐々木洋平さん・倉敷悟が行いました。
また、victory さん・河田鉄太郎さんから多数のコメントをいただきました。
ありがとうございます。</p>
