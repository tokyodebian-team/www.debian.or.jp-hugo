security.debian.org ミラーの新規設置について

<p>
Debian JP Project は、debian.org のインフラストラクチャ管理を行っている <a href="https://dsa.debian.org/">Debian System Administration チーム</a>と協力し、
<a href="http://www.sakura.ad.jp/">さくらインターネット</a>樣からの支援の上で Debian の主要サービスの一つである security.debian.org のアジアパシフィック地域向けミラーサーバーを日本国内に設置致しました。
これにより、日本国内ならびにアジア地域の Debian ユーザーはセキュリティ更新時に、より高速で安定したパッケージ取得が可能となっております。ご支援を頂きました さくらインターネット樣に感謝致します。</p>
<p>
今回の security.debian.org ミラーに限らず、
<a href="http://www.debian.or.jp/project/organization.html">Debian JP Project</a>では、ミラーサーバや開発用インフラの支援を随時募集しています。また、既存の pull 型ミラーサイトを push ミラー化することも推奨しております。安定したミラーサーバー、あるいは開発用インフラを提供頂ける組織／企業樣等ございましたら、<a href="mailto:board@debian.or.jp?subject=Debian%20%a5%df%a5%e9%a1%bc%a5%b5%a1%bc%a5%d0%a1%bc%a4%cb%a4%c4%a4%a4%a4%c6">メール</a>あるいは<a href="https://twitter.com/debianjp">Twitter</a>等でご相談ください。</p>

