Debian GNU/Linux 4.0 のリリースノートの日本語訳が完成

<p>
先ごろリリースされた Debian GNU/Linux 4.0 コードネーム“Etch”の更新点を記した、<a href="http://www.debian.org/releases/stable/releasenotes">Debian GNU/Linux 4.0 リリースノート</a>の日本語訳が完成し、公開の運びとなりました。
</p><p>
本リリースノートには、Debian GNU/Linux 4.0 で追加・変更されたさまざまな機能説明を各アーキテクチャごとに掲載しているほか、前リリース (Debian GNU/Linux 3.1 コードネーム“Sarge”) からのアップグレードを行うユーザーのための手順と注意も記述されています。いくつかの重要なパッケージに大幅な変更があるため、前リリースからのアップグレードを行う場合には、必ずこのリリースノートに目を通してください (例: <a href="http://www.debian.org/releases/stable/i386/release-notes/ch-upgrading.ja.html">i386</a>、<a href="http://www.debian.org/releases/stable/amd64/release-notes/ch-upgrading.ja.html">amd64</a>)。
</p><p>
新規インストールの方法については、<a href="http://www.debian.org/releases/stable/installmanual">Debian GNU/Linux 4.0 インストールガイド</a>も参照してください。
</p><p>
今回のリリースノートの翻訳にあたっては、多数の翻訳者・査読者が力を合わせました。この場を借りて感謝いたします。
</p>
