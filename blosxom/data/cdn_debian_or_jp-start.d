cdn.debian.or.jp 試験運用開始

<p>
Debian JP Project は、利用者の利便性を考慮した Debian ミラーサーバとして cdn.debian.or.jp 
の試験運用を開始しました。</p>
<p>
cdn.debian.or.jp は複数の Debian ミラーサーバを用いた DNS ラウンドロビン（＋サーバによる重み付け）
を行っており、特定ミラーサーバの障害影響を回避することが可能です
（また、単なる DNS ラウンドロビンとは異なり、対象となっているサーバが障害で停止している場合は
死活監視による自動切り離しを実施し、利用者からは特に何も意識することなく利用できるようにしてあります）。</p>
<p>
利用したい方は、/etc/apt/sources.list のサーバ指定を ftp.jp.debian.org などから以下のように変更ください （stable の部分は目的にあわせて testing/unstable などに適宜置き換えてください）。</p>
<pre>
deb http://cdn.debian.or.jp/debian/ stable main contrib non-free</pre>
<p>
利用された方は、ぜひフィードバックを<a href="http://www.debian.or.jp/community/ml/openml.html#usersML">メーリングリスト</a>や<a
href="http://www.debian.or.jp/project/organization.html">システム管理チーム</a>までお寄せください。</p>
