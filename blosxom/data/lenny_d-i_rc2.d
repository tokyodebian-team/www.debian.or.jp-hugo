Lenny 用 Debian-Installer RC2 リリース

<p>
次期リリースバージョン<a href="http://www.debian.org/devel/debian-installer/News/2009/20090131">「Lenny」用インストーラの最初のリリース候補第 2 版 (Release Candicate、RC版) の公開アナウンス</a>がありました（注：Debian 5.0 Lenny そのものではなく、<strong>Lenny 用インストーラの RC2 がリリースされた</strong>、ということ）。</p>
<p>
Lenny に修正を間に合わせる最後のチャンスですので、皆さんも可能な限り様々な環境でテストして、<a href="http://www.debian.org/devel/debian-installer/errata">正誤表</a>を確認の上、結果をメーリングリストやバグ報告システムなどにて教えて下さい。今ならまだ修正が間に合うでしょう :-)
</p>
<p>
今回のリリースでは、既知の問題点の修正の他、カーネルのバージョン更新、Sparc アーキテクチャや s390 アーキテクチャでの機能追加、
サポート言語の追加などが行われています。</p>
<p>
既知の問題点として、以下の点が追加されています。</p>
<ul>
<li>
ソフトウェア RAID アレイが使われているシステムでの rescue モードの利用によって、不具合が起こる可能性</ul>

