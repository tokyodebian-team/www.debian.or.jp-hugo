Software Design総集編 【2001~2012】

<p>
<a href="http://gihyo.jp/book/2013/978-4-7741-5593-7"><img src="../image/book/sd200101-201212.jpg"></a>
Debian JP Project 会員の執筆記事が雑誌に掲載されていますので、皆様にお知らせです。<br>
<a href="http://gihyo.jp/book/2013/978-4-7741-5593-7">Software Design総集編 【2001~2012】</a> にて「Linuxディストリビューションの変遷と逸話」として「自由と使いやすさを求め続けてきたDebian/Ubuntu」にてDebianの歴史や概要、派生ディストリビューションであるUbuntuを取り上げています。担当は やまねひでき です。</p>
<p>
どうぞご覧になって、是非感想等を Software Design 編集部樣にお寄せください (twitter で <a href="https://twitter.com/gihyosd">SoftwareDesign (@gihyosd)</a> や <a href="https://twitter.com/debianjp">DebianJP広報局 (@debianjp)</a> などへもどうぞ)。</p>
