Sarge の最終アップデートリリース (Debian GNU/Linux 3.1r8)

<p>
以前の Debian GNU/Linux 安定版 (oldstable) であるバージョン 3.1、コードネーム Sarge の 8 回目のマイナーアップデートとなる 3.1r8 が、2008 年 4 月 13 日に Debian Project からリリースされています。</p>
<p>
本アップデートは、Debian GNU/Linux 3.1 の機能向上ではなく、2008 年 3 月 31 日までに加えられた既存パッケージのセキュリティおよび深刻な問題の更新を目的としたものです。<strong>そして、このアップデートリリースは Sarge に対する作業の完全な終了を告げるものでもあります</strong>。</p>
<p>
CD および DVD イメージは<a href="http://cdimage.debian.org/cdimage/archive/3.1_r8/">アーカイブから取得可能</a>です。</p>
<p>
更新された内容の詳細については、<a href="http://www.debian.org/News/2008/20080413">3.1r8 についてのニュースリリース</a>、<a href="http://release.debian.org/stable/3.1/3.1r8/">3.1r8 の準備</a>、および<a href="http://ftp.jp.debian.org/debian/dists/sarge/ChangeLog">Changelog</a>の内容を参照してください。
</p>
