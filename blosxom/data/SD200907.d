Software Design 2009 年 7 月号記事掲載「Debian GNU/Linux 5.0 をお勧めする理由」

<p>
<a href="http://gihyo.jp/magazine/SD/archive/2009/200907"><img src="../image/book/sd200907.jpg"></a>
Debian JP Project 会員の執筆記事が雑誌に掲載されていますので、皆様にお知らせです。<br>
<a href="http://gihyo.jp/magazine/SD/archive/2009/200907">Software Design 2009 年 7 月号<strong>「Debian GNU/Linux 5.0 をお勧めする理由」</strong></a>で、武藤健志、やまねひでき、山下尊也、のがたじゅんの4名が担当しました。</p>
<p>
Debian GNU/Linux 5.0 "Lenny" の導入についてと活用について主にデスクトップやネットブックなどを中心に説明してあります。
また、それ以外でも Debian プロジェクトの概要やパッケージ作成についてなども取り上げてあります。
興味のある方は、現在発売中ですので、これを機会にぜひ書店でお手に取ってみてください。</p>

