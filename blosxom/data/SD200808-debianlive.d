Software Design 2008 年 8 月号「Debian live-helper」記事

<p>
<a href="http://gihyo.jp/magazine/SD/archive/2008/200808"><img src="../image/book/sd200808.jpg"></a>
Debian JP Project 会員の執筆記事が雑誌に掲載されていますので、皆様にお知らせです。<br>
<a href="http://gihyo.jp/magazine/SD/archive/2008/200808">Software Design 2008 年 8 月号<strong>「live-helperで構築するオリジナルLive CD」</strong></a>で、岩松信洋が担当しました。</p>
<p>
live-helper は、Debian をベースとした自分の LiveCD/DVD を作ることが出来るソフトウェアです。興味のある方は、
現在発売中ですので、これを機会にぜひ書店でお手に取ってみてください。</p>

