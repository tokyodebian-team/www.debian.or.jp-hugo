Software Design 2014 年 7 月号「Debian Hot Topics」

<p>
<a href="http://gihyo.jp/magazine/SD/archive/2014/201407"><img src="../image/book/sd201407.jpg"></a>
Debian JP Project 会員の執筆記事が雑誌に掲載されていますので、皆様にお知らせです。<br>
<a href="http://gihyo.jp/magazine/SD/archive/2014/201407">Software Design 2014 年 7 月号 にて<strong>
「Debian Hot Topics」</strong></a>が連載中です。
今回は
<ul>
<li>Ruby2.1への移行
<li>「Jessie」に向けての各アーキテクチャの状態とsparcの除外
<li>BeagleBone BlackがDebianをデフォルトイメージに採用
<li>パッケージキャッシュプロキシの導入について
</ul>
などを紹介しています。<br>

どうぞご覧になって、是非感想等を Software Design 編集部樣にお寄せください (twitter で <a href="https://twitter.com/gihyosd">SoftwareDesign (@gihyosd)</a> や <a href="https://twitter.com/debianjp">DebianJP広報局 (@debianjp)</a> などへもどうぞ)。</p>
