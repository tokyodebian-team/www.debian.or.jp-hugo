Software Design 2013 年 5 月号「Debian Hot Topics」

<p>
<a href="http://gihyo.jp/magazine/SD/archive/2013/201305"><img src="../image/book/sd201305.jpg"></a>
Debian JP Project 会員の執筆記事が雑誌に掲載されていますので、皆様にお知らせです。<br>
<a href="http://gihyo.jp/magazine/SD/archive/2013/201305">Software Design 2013 年 5 月号 にて<strong> 
「Debian Hot Topics」</strong></a>が連載中です。今回は、PostgreSQL の追加リポジトリやパッケージングに使う「dh」の話などとなっています。どうぞご覧になって、是非感想等を Software Design 編集部にお寄せください (twitter で <a href="https://twitter.com/gihyosd">SoftwareDesign (@gihyosd)</a> や <a href="https://twitter.com/debianjp">DebianJP広報局 (@debianjp)</a> などへもどうぞ)。</p>

