Software Design 2015 年 06 月号「Debian Hot Topics」

<p>
<a href="http://gihyo.jp/magazine/SD/archive/2015/201506"><img src="http://www.debian.or.jp/image/book/sd201506.jpg"></a>
Debian JP Project 会員の執筆記事が雑誌に掲載されていますので、皆様にお知らせです。<br>
<a href="http://gihyo.jp/magazine/SD/archive/2015/201506">Software Design 2015 年 06 月号 にて<strong>
「Debian Hot Topics」</strong></a>が連載中です。
今回は Debian プロジェクトリーダーの短信、「bits from DPL」に記載されたTodoリストから今後の方向性を探ってみています。</p>
<p>
ぜひ一度記事をご覧になってください。
<br>
また、是非感想等を Software Design 編集部樣にお寄せください (twitter で <a href="https://twitter.com/gihyosd">SoftwareDesign (@gihyosd)</a> や <a href="https://twitter.com/debianjp">DebianJPこうほうチーム (@debianjp)</a> などへもどうぞ)。</p>
