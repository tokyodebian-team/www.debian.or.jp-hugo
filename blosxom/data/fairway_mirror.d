ftp.jp.debian.orgにフェアーウェイが参加

<p>
<a href="http://fairway-corp.co.jp/">株式会社フェアーウェイ</a>樣にご協力いただき、同社提供の<a href="http://mirror.fairway.ne.jp/debian/">Debian ミラーサーバー</a>を ftp.jp.debian.org CDNミラーサーバーの一つとして利用できるようにいたしました。

ご協力頂きましたフェアーウェイ樣に感謝致します。</p>
<p>
<a href="http://www.debian.or.jp/project/organization.html">システム管理チーム</a>では、CDN 対応ミラーサーバを随時募集しています。また、既存の pull 型ミラーサイトを push ミラー化することも推奨しております。同様に安定したミラーを提供頂ける組織／企業樣等ございましたら、ご相談ください。</p>
