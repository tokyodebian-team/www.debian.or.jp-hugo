Paypal によるドネーション (寄付) 受け付けを始めました

<p>
Debian JP Project では
<a href="http://www.debian.or.jp/project/donations.html">Paypal による寄付の受け付け</a>
を開始しました。これにより、クレジットカードや PayPal 口座からの寄付が可能になりました。</p>
<p>
Paypal は少額からでも送金可能・振込手数料が不要となっております 。
Debian へ何らかの支援を行ってはみたいが、
なかなか開発・翻訳・その他ユーザへのサポート活動などは難しいな…などという方々、
一度ご利用を検討ください 
（ただし、金額が少額過ぎる場合は受取時の手数料でほとんど相殺されてしまいますので、
その点はご勘案願います）。</p>
<p>
なお、皆さんから頂いた寄付については以下のような用途に利用されます。</p>
<p>
<ul>
<li>パッケージリポジトリ・Web サイト・メーリングリストサーバなどのサービスを提供する *.debian.or.jp サーバ群の増強・維持・運用</li>
<li>debian.or.jp ドメインの維持</li>
<li>日本における Debian 商標の維持</li>
<li>その他 Debian の開発・発展・普及に寄与すると思われる活動</li></ul></p>
<p>
また、Debian JP Project では Paypal に限らず寄付・スポンサーは随時募集しております。
詳細については<a 
href="http://www.debian.or.jp/project/donations.html">支援/スポンサーのページ</a>
をご覧ください。</p>
