Debian on PS3 / Windows 版 Debian インストーラ起動ツール / Debian GNU/kFreeBSD

<p>
ちょっと変わった Debian の話を３つほど。</p>
<dl>
<dt>
Debian on PS3</dt>
<dd>
<p>
さまざまなアーキテクチャで動作する Debian ですが、当然のように最近話題の 
PLAYSTATION 3 でも動作します。</p>
<p>
Debian 開発者の八重樫さんの blog では、<a 
href="http://www.keshi.org/blog/2007/01/mythtv_on_ps3_linux.html">テレビ録画用ソフト
MythTV を PS3 上で動かしている様子</a>もあります。
試してみたい方は
<a href="http://ps3.keshi.org/debian-live/">Live CD</a>
で動かしてみるのが良いのではないでしょうか?</p></dd>
<dt>
Windows 版 Debian インストーラ起動ツール</dt>
<dd>
<p>
次は、インストール前の準備が少し楽になるかもしれない話です。</p>
<p>
Debian 開発者の一人、Robert Millan さんが
<a href="http://goodbye-microsoft.com/">Windows 版の Debian インストーラ起動ツールを
作成</a>しました。これは何かというと、 HDD の空き領域があれば Windows から Debian 
のインストーラを起動して Debian がインストールできるという優れものです。
<a href="http://goodbye-microsoft.com/screenshots/">
スクリーンショット</a>があるのでどんな感じかを見てください。</p>
<p>
残念ながら、Windows Vista では Windows 側の仕様が変わったために正しく動作しないそうなので、
この点は注意です。</p></dd>
<dt>
Debian GNU/kFreeBSD</dt>
<dd>
<p>
そしてもうひとつ。Debian が「ユニバーサルOS」と名乗っているのは伊達じゃないぞ、という話。</p>
<p>
カーネル部分を Linux ではなく FreeBSD を使った Debian
「Debian GNU/kFreeBSD」の開発が進んでいるぞ、という
<a href="http://lists.debian.org/debian-devel-announce/2007/02/msg00003.html">
開発チームからのメッセージ</a>が投稿されていました。
Etch の次のリリース（コードネーム：Lenny）あたりでのリリースを目指して作業中らしいので、
興味のある方、ちょっと変わった Debian 
を試してみたい方は使ってみて感想をフィードバックしてあげてください。</p></dd>
</dl>
