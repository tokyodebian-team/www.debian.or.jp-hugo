第 1 回 Debian JP Project Bug Squash Party のおしらせ

<p>
第 1 回 Debian JP Project Bug Squash Party（以下、BSP) を開催します。<br>

<a href="http://wiki.debian.org/BSP">BSP</a> とはバグつぶしパーティのことです。
Debian の場合ですと、安定版 (stable) のリリース前や定期的に行われており、Release Critical Bug 
を減らしたり問題のあるパッケージを集中的に直します。
また、各々がメンテナンスしているパッケージのメンテナンスや抱えている問題を参加者で議論し、
解決する事も行います。
</p>
<p>
今回の BSP では、実験的に<a
href="http://lists.debian.or.jp/debian-users/200708/msg00043.html">ユーザからのバグ修正要望の受付</a>も行う予定です。</p>
<p>
前回行われた Debian JP Project による成果は <a
href="http://wiki.debian.org/BSP/DebianJP">http://wiki.debian.org/BSP/DebianJP<a>、
他国による BSP に関しては、<a
href="http://wiki.debian.org/BSP">http://wiki.debian.org/BSP</a>
からリンクされている各国のBSPを参考にしてください。
</p>

<h5>開催内容</h5>

<dl>
<dt>開催日時・会場</dt>
<dd>
<ul>
 <li>日時：2007年8月25日(土) 10:00-18:00</li>
 <li>場所 : 秋葉原ダイビル11F (独)産総研 大会議室</li>
 <li>IRC : #debianjp at irc.debian.or.jp</li>
 <li>Wiki : <a href="http://wiki.debian.org/BSP/DebianJP_01">http://wiki.debian.org/BSP/DebianJP_01</a><br>
            前回の BSP : <a href=" http://wiki.debian.org/BSP/DebianJP">http://wiki.debian.org/BSP/DebianJP</a><li>
</ul>
</dd>
<dt>参加について</dt>
<dd>
<ul>
 <li>参加費<br>
   無料です。</li>
 <li>参加募集人数<br>
   20 名まで。
   (なお、定員に達した場合は Debian JP Project メンバー および過去の
   Debian 勉強会に参加されたなど、実績のある方を優先させていただきます)</li>
 <li><a href="http://www.debian.or.jp/community/irc.html">IRC による参加</a>も可能です。特に許可は必要ありません。</li>
 <li>見学や差し入れは申し訳ありませんがご遠慮願います。</li>
 <li>参加締切りは 2007/08/24 24:00 までとさせていただきます。</li>
 <li>参加希望者は担当：岩松 <a href="mailto:iwamatsu@debain.or.jp">&lt;iwamatsu@debian.or.jp&gt;</a> までご連絡ください。</li>
</ul>
</dd>
<dt>内容</dt>
<dd>
<ol>
 <li>参加者による交流</li>
 <li>Debian Package のバグを修正</li>
 <li>Debian Package メンテナンス</li>
 <li>po-debconf 等の翻訳など</li>
</ol>
</dd>
<dt>その他</dt>
<dd>
<ul>
 <li>BSP 用のパソコン、周辺機器等は各々で用意してください。</li>
 <li>ネットワークは使用可能です。</li>
 <li>会場のビデオ録画とストリーミング中継を計画しております。ご協力をお願いします。<br>
 （参加者の方で不都合がある方は対応いたします。ご連絡ください。）</li>
</ul>
</dd>
</dl>

</p>
何かありましたら、担当：岩松 <a href="mailto:iwamatsu@debain.or.jp">&lt;iwamatsu@debian.or.jp&gt;</a> にご連絡ください。<br>
多数の参加をお待ちしております。
</p>
