大統一Debian勉強会2013 参加登録および懇親会登録 開始のお知らせ

  <p>2013年6月29日(土曜日)に日本大学 駿河台キャンパス 駿河台校舎1号館 131, 132, 133, 134教室において、
  全国のDebian勉強会合同の勉強会「大統一Debian勉強会 2013」を開催します。
  <a href="http://gum.debian.or.jp/2013/time_schedule_1">セッションのリストとタイムテーブル</a>が公開され、
  大統一Debian勉強会の開催まで
  残りちょうど1ヶ月となりました。
  先日(5/29)夕方より<a href="http://www.zusaar.com/event/739003">参加登録（無料）</a>および
  <a href="http://gumdebianjp2013.peatix.com/">懇親会登録</a>を開始しました。
  参加を予定されている方、是非ご登録をお願いします。また、会社や学校、SNS等で大統一Debian勉強会に
  興味がありそうな方がいれば、紹介してみて下さい。

  <dl>
    <dt>大統一Debian勉強会　参加登録サイト</dt>
    <dd>
      <ul>
	<li><a href="http://www.zusaar.com/event/739003">http://www.zusaar.com/event/739003</a></li>
      </ul>
    </dd>

    <dt>大統一Debian勉強会　懇親会登録サイト</dt>
    <dd>
      <ul>
	<li><a href="http://gumdebianjp2013.peatix.com/">http://gumdebianjp2013.peatix.com/</a></li>
      </ul>
    </dd>
    <dd>
  </dl>

  それぞれ別の登録となっています。<br>
  また、Debianおよびフリーソフトウェアの開発に重要なPGP/GPGキーサインが行える
  <a href="http://gum.debian.or.jp/2013/key_signing_party.html">PGP/GPGキーサインパーティ</a>についても登録が開始されています。
  こちらもよろしくお願いします。</br>
  大統一Debian勉強会 2013 および PGP/GPGキーサインパーティの参加は無料です。
  懇親会については有料（3700円）となっています。懇親会では発表者の方および参加者の方と交流ができますので、是非ご参加ください。
  皆様のご参加、ご登録よろしくおねがいします。
  </p>

  <h4>大統一Debian勉強会2013 開催概要</h4>

  <dl>
    <dt>開催日時</dt>
    <dd>
      <ul>
	<li>2013年6月29日(土曜日) 10時00分〜18時00分まで(予定)</li>
      </ul>
    </dd>

    <dt>開催場所</dt>
    <dd>
      <ul>
	<li>日本大学 駿河台キャンパス 駿河台校舎1号館 131, 132, 133, 134教室</li>
	<li><a href="http://www.arch.cst.nihon-u.ac.jp/bldgno1/bldgno1.html">http://www.arch.cst.nihon-u.ac.jp/bldgno1/bldgno1.html</a></li>
        <li><a href="http://www.arch.cst.nihon-u.ac.jp/life/access.html">http://www.arch.cst.nihon-u.ac.jp/life/access.html</a></li>
      </ul>
    </dd>

    <dt>参加費</dt>
    <dd>無料</dd>

    <dt>公式タグ</dt>
    <dd>Twitter: <a href="https://twitter.com/search?q=%23gumdebianjp&src=hash">#gumdebianjp</a></dd>
    <dd>ブログ: gumdebianjp2013</dd>
  </dl>

  <h4>お問い合わせ</h4>
  <p>
  大統一Debian勉強会 2013に関するお問い合わせは、gum2013 _at_ debian.or.jp までメールにてご連絡ください。
  </p>
