第58回 東京エリア Debian 勉強会のお知らせ

<p>
東京近辺にいらっしゃる皆様こんにちは。今年も Debian 勉強会が開かれます！
</p>

<p>
Debian 勉強会とは、Debian 
の開発者になれることをひそかに夢見るユーザたちと、
ある時にはそれを優しく手助けをし、またある時には厳しく叱咤激励する Debian 
開発者らが Face to Face で Debian GNU/Linux のさまざまなトピック
（新しいパッケージ、Debian 特有の機能の仕組について、Debian 界隈で起こった出来事、etc）
について語り合うイベントです。</p>
<p>
参加される方は主に東京を中心に関東近郊の国籍・性別不問の Debian ユーザです 
(Debian をまだ使ったことが無いが興味があるので…とか、かなり遠い所から来てくださる方もいます)。
開発の最前線にいるDebian の公式開発者や開発者予備軍の方も居ますので、
普段は聞けないような様々な情報を得るチャンスです。
興味と時間があった方、是非御参加下さい。
（また、勉強会で話をしてみたいという方も随時募集しています）。</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
   <li>日時：2009年11月14日(土曜日) 18:00-21:00</li>
   <li>費用：500円</li>
   <li>会場：<a href="http://nyc.niye.go.jp/">代々木 国立オリンピック記念青少年総合センター センター棟</a></li>
   </ul>
</dd>
<dt>内容</dt>
<dd>
  <ol>
  <li><strong>DPN trivia quiz</strong> (担当: やまもと )<br>
Debian 関連のニュースにおいついていますか? 簡単なクイズでDebian常識テストしちゃいます。</li>
  <li><strong>Debianでの数学ことはじめ。(担当: まえだ)</strong><br>
DebianでのOctave, GNU R, Gnuplotなどの数値演算、統計処理、グラフ作成などをします。</li>
  <li><strong>auto-builder ネットワークの仕組み</strong> (担当: 岩松)<br>
Debian Project の主要なシステムの一つである 自動パッケージビルドシステム auto-builder ネットワークの仕組みについて説明します。</li>
  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
参加希望者は事前登録の上、事前課題の回答を勉強会用メーリングリストに送付してくだ
さい。期限は 11月12日(木)中です。
<strong>
事前登録については <a href="https://tokyodebian-team.pages.debian.net/2009-11.html">Debian 勉強会の Web ページ</a> を参照下さい。
また作業方法については<a href="https://tokyodebian-team.pages.debian.net/pdf2008/debianmeetingresume200811.pdf">2008年11月の資料</a>を参考にしてください。
</strong>
</dd>
<dt>勉強会中継について</dt>
<dd>
<strong>
今回は 勉強会中継を行いません。
</strong>
</dd>
</dl>
<p>
この件に関するお問い合わせは Debian 勉強会メンバー：まえだこうへい (mkouhei&#64;debian.or.jp )
までお願いいたします。</p>
