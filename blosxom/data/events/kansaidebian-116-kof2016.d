第 116 回 関西 Debian 勉強会 in 関西オープンソース2016

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting/20161112">第116回 関西 Debian 勉強会@関西オープンソース2016 </a>」
のお知らせです。
関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで Debian GNU/Linux のさまざまな
トピックについて Face to Face で楽しく話し合っていく集まりです。
</p>

<dl>

<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：11月12日(土) 10:00〜18:00
  </li>
  <li>会場：大阪南港ATC (行き方は<a href="http://k-of.jp/2016/access.html">こちら</a>) <br>
 (大阪市交通局 ニュートラム 南港ポートタウン線 トレードセンター前駅より徒歩5分)</li>
  <li>主催：関西オープンフォーラム</li>
  <li>費用：セミナー／ミニセミナーは無料。その他有志による物品の販売を予定</li>
  </ul>
</dd>

<dt>
  <a href="https://k-of.jp/2016/session/890">セミナー<br>
  <strong>「Debian Updates」</strong><br>
  (会場:10F-ショーケース1 11月12日(土) 17:00-17:50)</a>
</dt>
<dd>
  <ul>
  <li>Debian 8(Jessie)および次期リリース候補 Stretch の開発状況を中心に、最近のDebian Projectのトピックを紹介します。</li>
  <li>担当：佐々木 洋平(Debian JP Project)</li>
  </ul>
</dd>

<dt>ブース展示・配布物</dt>
<dd>
  <ol>
  <li><strong>Debian 稼働マシン展示</strong></li>
  <li><strong>インストールメディア、ステッカー、などの頒布</strong></li>
  </ol>
</dd>
</dl>

<p>
この件に関するお問い合わせは 関西 Debian 勉強会担当：佐々木洋平 (uwabami&#64;{debian.or.jp} ) までお願いいたします。
</p>
