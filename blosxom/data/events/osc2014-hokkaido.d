オープンソースカンファレンス 2014 Hokkaido 参加 (6/14) のお知らせ

<p>北海道にお住まいのみなさまにお知らせです。6月14日(土) に札幌にて開催される「<a href="http://www.ospn.jp/osc2014-do/">オープンソースカンファレンス 2014 Hokkaido</a>」にDebian JP Projectが出展します。</p>

<p>
普段、継続的に勉強会を開催している東京／関西地区と違い、北海道地区のユーザー／コントリビューター／開発者が face to face で交流できる貴重な機会ですので、逃さず参加していただければと思います。
</p>

<p>
また、「できれば出展者といろいろ話をしてみたいけど、OSCは初参加なので何から聞いたらよいのかわからない...」という方には、ガイドがみなさんと一緒に展示会場を見て回り、出展者から各ブースの見所や最新のトピック等を紹介する<a href="http://www.ospn.jp/osc2014-do/modules/eguide/event.php?eid=58">「展示ツアー」</a>もございます。
ぜひ、足をお運びください。<p>

<dl>
<dt>開催日時・会場</dt>
<dd>
<ul>
  <li>日程: 2014年6月14日(土) 10:00-18:00 (展示は16:00まで)</li>
  <li>会場: <a href="http://www.sapporosansin.jp/access/">札幌市産業振興センター 産業振興棟2F</a> 3F・4F (地下鉄東西線東札幌駅から徒歩5分)</li>
  <li>参加費: 無料</li>
  <li>Twitter: <a href="https://twitter.com/OSC_official">@OSC_official</a>／ ハッシュタグ <a href="https://twitter.com/search?f=realtime&q=%23osc14do&src=hash">#osc14do</a>
</ul>
</dd>

<dt>セミナー発表</dt>
<dd>
<ul>
<li>タイトル：<a href="https://www.ospn.jp/osc2014-do/modules/eguide/event.php?eid=29"><strong>カウガールは赤い渦巻きの夢を見るか？~Debian8「Jessie」への展望</strong></a>
<li>時間：13時00分〜13時45分
<li>内容：Debian8「Jessie」に向けて開発が着々と進行中の現在、開発コミュニティ界隈ではどんなことが起きているのか？をお伝えします。<br>
また、当日回答して欲しいDebianに関する疑問／質問を募集しています。<a href="https://twitter.com/debianjp">twitter: @debianjp</a> までメンションを送っていただくか、当日ブースで記入などをお願い致します。 
<li>対象：Debian関連情報を聞いてみたい方。または開発の裏話や開発者への質問を楽しめる方。
<li>講師：やまねひでき (Debian JP Project/Debian Developer)
</ul>
</dd>

<dt>ブース展示内容</dt>
<dd>
<ul>
  <li>Debian "Jessie" 稼働マシンの展示</li>
  <li>東京エリア/関西Debian勉強会の紹介</li>
  <li>Software Design誌／ムックでのDebian記事紹介</li>
  <li>コードネーム関連グッズ展示</li>
<!--
  <li>「Debian極小ステッカー」配布</li>
-->
</ul>
</dd>
</dl>

<p>この件に関するお問い合わせは Debian JP プロジェクト理事会 board at {debian.or.jp} までお願いいたします。</p>
