第32回 関西 Debian 勉強会のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting20100228">第32回 関西 Debian 勉強会</a>」 
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで 
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
  <li>日時：2010 年 2 月 28 日 (日) 13:30 - 17:00 (13:15 より受付)</li>
  <li>会場：<a href="http://www.city.osaka.lg.jp/shimin/page/0000016603.html">大阪 福島区民センター 303会議室</a>
(定員：22名)</li>
  <li>費用：500円 (部屋代、印刷代などの諸費用のため)</li>
  <li>事前課題: Debianを使ったサーバーを管理している人は「運用しているサーバのセキュリティについてどうしているか、どこに気をつけているか」を教えてください。そうでない人は、「今回から新しく使う<a href="http://debianmeeting.appspot.com/event?eventid=30ae12bd501e1d4460574111d1d4d16d0d44c276">Debian勉強会予約管理システム</a>を利用してみての感想」を教えてください。</li>
  </ul>
</dd>

<dt>内容</dt>
<dd>
  <ol>
  <li><strong>Intro</strong><br>
この勉強会の簡単な説明をします。</li>

  <li><strong>「あなたに 5 分あげます」</strong> (担当：参加者全員)<br>
5 分間で事前課題について自己紹介をしてください。
普通にしゃべるもよし、ネタをしゃべるもよし。</li>

  <li><strong>「Debian GNU/Linuxを実際にビジネスで使ったレンタルサーバー」</strong> (担当：前田 学(<a href="http://www.obitastar.co.jp/">オビタスター</a>))<br>
Debian GNU/Linux使ってビジネスで、レンタルサーバーを行なっているオビタスター。 実際どのような形で利用し、何が課題で、どのようなことを今後望んでいるのか、実際に利用しているオビタスターから発表します。 </li>

  <li><strong>「PythyonもGoogle App Engineも知らない人が『Debian勉強会予約管理システム』のソースを見てみたよ」</strong> (担当：のがたじゅん)<br>
関西Debian勉強会でも<a href="http://debianmeeting.appspot.com/event?eventid=30ae12bd501e1d4460574111d1d4d16d0d44c276">Debian勉強会予約管理システム</a>を今月から使い始めますが、そのソースは公開されています。ということで、PythyonもGoogle App Engineもまったく知らない人が、ほげってみた事を発表します。</li>

  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
<strong>2 月 26 日 (金) 24:00 までに</strong>
<a href="http://debianmeeting.appspot.com/event?eventid=30ae12bd501e1d4460574111d1d4d16d0d44c276">Debian勉強会予約管理システム</a>を参照して、
事前登録をしてください。登録にはGoogleアカウントが必要になりますが、事情によりGoogleアカウントを使えない/登録できない方は担当者まで連絡してください
（締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。
懇親会 (予定) に参加する場合は、二次会参加の欄にチェックしてください）。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：野方 純 (nogajun&#64;{debian.or.jp} ) までお願いいたします。</p>
