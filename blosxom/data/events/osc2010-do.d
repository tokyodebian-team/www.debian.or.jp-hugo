Debian勉強会出張版＠オープンソースカンファレンス2010 Hokkaido

<p>
6月26日に開かれる<a href="http://www.ospn.jp/osc2010-do/">オープンソースカンファレンス2010 Hokkaido</a>にて、
北海道Linuxユーザーズクラブ(DoLUC)さんの枠（10:15〜11:00）に<a href="http://www.ospn.jp/osc2010-do/modules/eguide/event.php?eid=22">「Debian 勉強会出張版」</a>として Debian JP Project から荒木靖宏、岩松信洋、佐々木洋平の3名が参加します。</p>
<p>
札幌近郊の皆様、「Debian開発者を交え、Debianについてゆるーく語る」45分間にぜひご参加ください。</p>

<dl>
<dt>開催日時・会場・参加費用</dt>
<dd>
  <ul>
  <li>日時：2010年6月26日(土) 10:00~18:30</li>
  <li>会場：<a href="http://www.sapporosansin.jp/access/">札幌市産業振興センター 産業振興棟2F(総合受付)</a>
(地下鉄東西線東札幌駅から徒歩5分)</li>
  <li>参加費用：無料</li>
  </ul></dd>
<dt>セミナー (10:15-11:00＠ルームC 産業振興棟(2F))</dt>
<dd>
<strong><a href="http://www.ospn.jp/osc2010-do/modules/eguide/event.php?eid=22">「Debian 勉強会出張版」</a></strong>（担当：荒木靖宏、岩松信洋）<br>
	Debian開発者を交え、Debianについてゆるーく語ります。<br>
	<ol>
	<li> DebianとDebian JPアップデート (担当: 荒木靖宏)</li>
	<li>「あなたはどっち，CloudでDebian, DebianでCloud」(担当: 荒木靖宏)</li>
	<li>「squeeze に向けた Debian GNU/kFreeBSD 入門」(担当: 岩松信洋)</li>
	</ol>
	</dd>
</dl>

