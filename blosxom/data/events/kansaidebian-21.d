第21回 関西 Debian 勉強会のお知らせ (Ustream 中継あり)

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting20090125">第21回 関西 Debian 勉強会</a>」 
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで 
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。
今回は「"Debian Meeting" with Coffee in Kansai (DMCK)」と題して、お茶会形式で開催します。</p>
<p>当日は淹れたてのコーヒーを前にして皆さんでDebian話で盛り上がりましょう!
 (コーヒー以外も用意しておく予定です)。ライトニングトークは飛び入り参加も大歓迎です。
プロジェクターやネットワーク回線などがありますので、自由な発表の場にしたいと思います。
なお、今回も第12回勉強会に続いて<a href="http://www.ustream.tv/channel/kansaidebian">Ustream.tv 
でのストリーミング</a>を行う予定です。</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
  <li>日時：2009 年 1 月 25 日 (日) 14:00 - 17:00 (13:345 より受付)</li>
  <li>会場：神戸大学 自然科学総合研究棟 3 号館 508 号室 (<a herf="http://www.kobe-u.ac.jp/info/access/rokko/honbu-kou.htm#themap">神戸大学トップページ / アクセス・キャンパスマップ / 六甲台地区 / 神大本部 工学部前</a>)
(定員：20名)</li>
  <li>費用：500円 (飲み物代・お菓子代等として)</li>
  <li>持ち物：できれば環境を考慮して、「自分用のコップ」をご持参下さい。
紙コップなどの使用を極力抑える方向で考えています :-)<br>
それから、<strong>参加者の皆さんそれぞれのご当地で美味しいお菓子</strong>がございましたら、
是非ふるさと自慢も兼ねてご持参下さい。お茶請けとして、参加者全員で、舌でも楽しみたいと思います :-)</li>
  </ul>
</dd>
<dt>事前課題</dt>
<dd>当日、<strong>「今年はDebianで○○をやります」</strong>という宣言を、
自己紹介も兼ねて一人当たり5分程度で発表していただきますので各自ネタをご用意下さい
(会場にはプロジェクターもありますので、プレゼン資料を用意してくださっても構いません)。</dd>
<dt>内容</dt>
<dd>
  <ol>
  <li><strong>Intro</strong> 山下 尊也 氏<br>
この勉強会の目的などを説明し、その後、第21回関西 Debian 勉強会の内容について話をします。</li>

  <li><strong>自己紹介兼、事前課題の発表</strong> 参加者各位<br></li>
  <li><strong>ライトニングトーク</strong><br>
    <ul>
     <li>「Open Street Map on Debian」by 清野 陽一
     <li>「muse-mode のお話」by 佐々木 洋平
     <li>「IPv6の実演、兼小ネタ」by 川江 浩
     <li>「最近知った git 豆知識」by 山下 尊也
     <li>「Dell Mini 9を触ってみた」by 山下 尊也
     <li> 当日の飛び込み参加枠 :-)</ul></li>

 <li>「まったり」歓談の時間</li>

  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
<strong>1 月 23 日 (金) 12:00 までに</strong>
<a href="http://cotocoto.jp/event/29200">京都.cotocoto</a>を参照して、
事前登録をしてください
（締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。
三宮から神戸大学周辺で予定している懇親会に参加する場合は、コメントに「懇親会参加希望」と記入してください）。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：山下尊也 (takaya&#64;{debian.or.jp} ) までお願いいたします。</p>


