第 53 回 関西 Debian 勉強会 in 関西オープンソース2011

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting20111112">第53回 関西 Debian 勉強会@KOF2011
 </a>」 
のお知らせです。
関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで Debian GNU/Linux のさまざまな
トピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：2011 年 11 月 11 日 (金) 13:00~18:00 / 11 月 12 日 (土) 10:00~18:00
  </li>
  <li>会場：大阪南港ATC (行き方は<a href="http://k-of.jp/2010/access/">こちら</a>) <br>
 (大阪市交通局 ニュートラム 南港ポートタウン線 トレードセンター前駅より徒歩5分)</li>
  <li>主催：関西オープンフォーラム</li>
  <li>費用：セミナー／ミニセミナーは無料。その他有志による物品の販売を予定</li>
  </ul>
</dd>

<dt>
<strong>GPGキーサインパーティ</strong><br>
11月12日 11:35〜 (15 min) [ 会場 : ステージ会場 ]<br>
キーサインコーディネイタ: 岩松 信洋 / iwamatsu at {debian.org}<br>
</dt>
<dd>
<ul>
<li>
キーサインパーティーとは、互いの鍵に署名をすべく、PGP/GPG 鍵を持つ人々が集まるものです。
キーサインパーティーは PGP/GPG 鍵を利用する上で非常に重要な概念である、
信頼の輪(Web of Trust *1)を大規模に拡張するのに有用です。
また、このようなキーサインパーティーは実際に開発者と面と向かって会う良い機会でもあります。
下記のサイトを参考に事前登録をお願いします。
<br>
<a href="https://sites.google.com/site/kspjapanese/kof2011">https://sites.google.com/site/kspjapanese/kof2011</a>
<br>
</li>
<li>
担当：岩松 信洋 (Debian Project/DebianJP Project/ksp-ja)</li>
</ul>
</dd>

<dt>
<strong>第53回 関西Debian勉強会「なれる！ Debian 開発者 ― 45 分でわかる？メンテナ入門</strong><br>
11月12日 13:00〜13:50 (50 min) [ 会場 : 6F M6 ]</dt>
<dd>
<ul>
<li>
普段使っている／お世話になっている Linux ディストリビューションの開発ってどんな風にやってるの？難しくないの？大変じゃないの？<br>
─そんな疑問に「Debian のパッケージメンテナ」からの視点でお答えします。<br>
これさえ分かればキミもディストリビューション開発に参加できる……かもよ!
</li>
<li>
担当：やまねひでき(Debian Project/Debian JP Project)</li>
</ul>
</dd>

<dt>
<strong>書籍紹介、サイン会</strong><br>
11月12日 17:20~ (15 min) [ 会場 : ジュンク堂ブース ]<br>
書籍紹介、執筆者: 岩松 信洋 <br>
</dt>
<dd>
<ul>
<li>
岩松信洋・上川純一・まえだこうへい・小川伸一郎 共著<br>
『Git によるバージョン管理』 Ohmsha<br>
<a href="http://ssl.ohmsha.co.jp/cgi-bin/menu.cgi?&ISBN=978-4-274-06864-5">http://ssl.ohmsha.co.jp/cgi-bin/menu.cgi?&amp;ISBN=978-4-274-06864-5</a>
</li>
</ul>
</dd>

<dt>ブース展示・配布物</dt>
<dd>
  <ol>
  <li><strong>Debian 稼働マシン展示</strong></li>
  <li><strong>Debian Live DVD 配布</strong></li>
  <li><strong>有志作成 Debian グッズ（Tシャツ）販売</strong></li>
  <li><strong>Debian 同人誌「Deb専」および「あんどきゅめんてっどでびあん」販売</strong></li>
  </ol>
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会担当：担当者 かわだてつたろう (Email: t3rkwd AT SPAMFREE debian DOT or DOT jp) までお願いします。</p>
