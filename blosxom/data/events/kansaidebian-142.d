第 142 回 関西 Debian 勉強会 (openSUSE Meetup + 東海道らぐ + LILO + 関西 Debian 勉強会 LT大会)

<p>
「<a href="https://wiki.debian.org/KansaiDebianMeeting/20190127">第142回 関西 Debian 勉強会 (openSUSE Meetup + 東海道らぐ + LILO + 関西 Debian 勉強会 LT大会)</a>」
のお知らせです。今回は OSC 大阪の翌日に、さくらインターネット株式会社様の本社で openSUSE MeetUp + 東海道らぐ + LILO + 関西Debian勉強会を主体とする LT 大会として開催いたします。</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
    <li>日時： 2019 年 1 月 27 日 (日) 13:00 - 17:00</li>
    <li>会場：<a href="https://www.sakura.ad.jp/corporate/corp/office.html">さくらインターネット株式会社 本社</a>(定員：40名)</li>
  </ul>
</dd>

<dt>内容</dt>
<dd>
  <ol>

  <li><strong>Intro</strong><br>
  会場諸注意など
  </li>

  <li><strong>ライトニングトーク</strong></li>

</ol>
</dd>

<dt>参加方法と注意事項</dt>
<dd>

<a href="https://debianjp.connpass.com/event/114929/">connpass</a>を参照して、
事前登録をしてください。事情によりconnpassを使えない/登録できない方は担当者まで連絡してください。
(締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。)
</dd>

</dl>

<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：おおつきようすけ (y.otsuki30&#64;{gmail.com})までお願いいたします。
</p>
