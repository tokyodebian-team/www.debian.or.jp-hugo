第26回 Debian 勉強会 in オープンソースカンファレンス 2007 Tokyo/Spring

<p>
<a href=" https://tokyodebian-team.pages.debian.net/">「Debian 勉強会」</a>
は月に一回、顔をつき合わせて Debian GNU/Linux のさまざまなトピック
（新しいパッケージ、Debian 特有の機能の仕組について、Debian 界隈で起こった出来事、etc）
について話し合う会です。今月は
<a href="http://www.ospn.jp/osc2007-spring">オープンソースカンファレンス 2007 Tokyo/Spring</a> にお邪魔して、
<a href="http://list.ospn.jp/mailman/listinfo/v-tomo">仮想化友の会</a>
との合同セミナーやブースでの展示と説明などを予定しています。</p>
<p>
参加者には Debian ユーザのみならず、開発の最前線に立つ Debian の公式開発者や開発者予備軍の方もおりますので、普段は聞けないようなさまざまな情報を得るチャンスです。<br>
興味を持たれた方は気軽にご参加ください (また、勉強会で話をしてみたいという方も随時募集しています)。</p>


<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：2007年3月17日(土)<br>
            合同セミナーは 10:00-10:45、11:00-11:45<br>
            ミニセミナーは随時開催予定 (ブースは10:00-16:00)
  <li>会場：<a href="http://www.jec.ac.jp/college/access.html">日本電子専門学校 7号館</a>　(JR大久保駅徒歩２分)
  <li>費用：セミナーは無料<br>
      　　　その他有志による物品の販売を予定</ul></dd>
<dt>合同セミナー（B2F-B）</dt>
<dd>
  <ol>
  <li><strong>仮想化友の会、Debian 勉強会の紹介</strong><br>
      ご来場の皆様へ仮想化友の会、Debian 勉強会それぞれの概要などを説明します。
  <li><strong>事前課題紹介 </strong><br>
      「仮想化をこういう利用方法で活用しています」という実例を紹介します。
      テスト環境として以外の様々な利用方法が紹介される…かもしれません。
  <li><strong>仮想化常識 Quiz</strong><br>
       猫も杓子も仮想化と言っている現状ですが、本当に仮想化を正しく理解していますか？
       確認のための仮想化常識クイズです。
  <li><strong>Windows での仮想化技術紹介</strong><br>
     「Windows から Debian を利用するには？」ー Windows 
      で利用できる仮想化ソフトウェアの紹介他をさらっとします。
  <li><strong>Debian の仮想化技術紹介</strong><br> 
      kernel 2.6.20 で正式に採用された仮想化技術、KVM を中心に Debian 
      での仮想化技術利用を説明します。
  </ol>
</dd>
<dt>ブース展示・配布物 (7F-A)</dt>
<dd>
  <ol>
  <li>Debian 関連書籍
  <li>有志作成 Debian グッズ (Ｔシャツ・ステッカー)
  <li>Debian 勉強会資料
  <li>チラシ・CD
  <li>Debian 稼働マシン</ol>
</dd>
<dt>ミニセミナー (7F-A)</dt>
<dd>
  <ol>
  <li>Debian 質問箱 ー 会場その他で集めた Debian に関するあれこれに回答します
  <li>その他未定</ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
セミナー参加希望者は<a href="http://www.ospn.jp/osc2007-spring/">事前登録</a>してください。<br>
ブース展示・ミニセミナーその他については特に登録などはありません。</dd>
</dl>
<p>
この件に関するお問い合わせは Debian 勉強会主催者：上川純一 (dancer&#64;{debian.org,netfort.gr.jp} )
までお願いいたします。</p>
