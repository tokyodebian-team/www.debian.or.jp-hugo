第105回 東京エリア Debian 勉強会 in オープンソースカンファレンス 2013 Tokyo/Fall

<p>
<a href="https://tokyodebian-team.pages.debian.net/2013-10.html">「第105回 東京エリア Debian 勉強会 in オープンソースカンファレンス 2013 Tokyo/Fall」</a>
のお知らせです。</p>
<p> 東京エリア Debian 勉強会とは、Debian ユーザ／ユーザ予備群／開発者らで Debian GNU/Linux のさまざまなトピックについて 東京方面にて集まって語り合うイベントです。</p>
<p> 10 月は、
<a href="http://www.ospn.jp/osc2013-fall/">オープンソースカンファレンス 2013 Tokyo/Fall</a>
に参加を予定しています。 毎回の事ですが様々な Debian ユーザーが集まります。普段は聞けないような様々な情報を交換しあうチャンスです。この機会にぜひお越しください。</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日程：2013 年 10 月 20 日 ( 日 ) <br>
  <li>会場：<a href="http://www.ospn.jp/osc2013-fall/">オープンソースカンファレンス 2013 Tokyo/Fall 会場内</a></li>
  <li>参加費用：無料<br></ul></dd>
<dt>セミナー発表</dt>
<dd>タイトル：<a href="https://www.ospn.jp/osc2013-fall/modules/eguide/event.php?eid=46"><strong>Debian 最近の Update</strong></a></dd>
<dd>内容：Debianの最新動向について語ります
<dd>講師：野島 貴英 ( 東京エリア Debian 勉強会 )</dd>
<dd>対象：Debian に興味、関心のある人</dd>
<dd>開始：10時00分〜10時45分</dd>
</dd>

<p>また、オープンソースカンファレンス 2013 Tokyo/Fall は 19 日( 土 )、20 日( 日）
の ２ 日間行われますが、東京エリア Debian 勉強会は 20 日 ( 日 ) のみの展示を行います。
お越しいただく際は日程にご注意ください。</p>

<dt>ブース展示物</dt>
<dd>
  <ol>
  <li> Wheezy PC の展示 </li>
  <li> あんどきゅめんてっどでびあんの展示/頒布</li>
  <li> 東京エリア / 関西 Debian 勉強会の紹介</li>
　<li> その他 ( ...計画中... ) </li>
</dd>

<p>この件に関するお問い合わせは 講師担当：野島(nozzy at {debian.or.jp})まで。</p>



