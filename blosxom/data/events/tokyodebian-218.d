2023 年 2 月度 東京エリア・関西合同 Debian 勉強会のお知らせ

<p>
日本にお住まいのDebianにご興味がある皆様こんにちは。
今月の勉強会はオンラインによるweb会議のスタイルで
東京エリア Debian 勉強会と関西 Debian 勉強会が合同で開催することにいたしました。
</p>

<dl>
<dt>東京エリア Debian 勉強会</dt>
<dd>
<p>
東京エリア Debian 勉強会とは、Debian Project に関わる事を目的とする
熱いユーザたちと、実際に Debian Project にてすでに日夜活動している
人らが Face to Face で Debian GNU/Linux のさまざまなトピック （パッケージ、
Debian 特有 の機能の仕組について、Debian 界隈で起こった出来事、
etc ） について語り合う場です。当然ですが、Debian Project に何らかの貢献
をしたいという目的を持つ方であれば、自身で活動できるように手助けをする事
も主な趣旨の一つとしています。
</p>
<p>
また、開発の最前線にいる Debian の公式開発者や開発者予備軍の方も参加する場合がありますので、
普段は聞けないような様々な情報を得るチャンスです。
興味がありご都合の合う方はぜひともご参加下さい。
</p>
<p>
また、勉強会で話をしてみたいという方も随時募集しています。
</p>
</dd>

<dt>関西 Debian 勉強会</dt>
<dd>
<p>
関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで Debian のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。
</p>
</dd>
</dl>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
    <li>日時：2023 年 2 月 18 日 ( 土曜日 ) 14:00-16:00 </li>
    <li>会場：Jitsi Meet を使った Web ビデオ会議（URLは<a href="https://debianjp.connpass.com/event/273748/">勉強会のページ</a>を参照ください）</li>
    <li>参加費：無料</li>
  </ul>
</dd>
<dt>内容</dt>
<dd>
  <ol>
    <li><strong>「DDTP及びDDTSSの紹介」（発表者：dictoss さん）</strong></li>
    <ul>
      <li>Debian で提供するドキュメントは有志によってそれぞれの言語に翻訳を行っています。</li>
      <li>その中で Debian パッケージの説明文の翻訳作業を、 <a href="https://www.debian.org/international/l10n/ddtp.ja.html">DDTP（The Debian Description Translation Project）></a> と呼んでいます。</li>
      <li>このセミナーでは、パッケージの説明文の翻訳の仕組みを説明します。</li>
      <li><a href="https://ddtp.debian.org/ddtss/index.cgi/ja">パッケージ説明文翻訳システム DDTSS(ja)</a></li>
      <li><a href="https://ddtp.debian.org/stats/stats-sid.html">パッケージ説明文翻訳の言語別の進捗</a></li>
    </ul>
    <li><strong>ハンズオン「DDTSSを使ってパッケージ説明文を翻訳してみよう」（発表者：参加者全員）</strong></li>
    <ul>
      <li>次期安定版として開発を進めている <a href="https://release.debian.org/bookworm/freeze_policy.html">bookwormはsoft freezeの段階</a> に入りました。</li>
      <li>soft freeze以降、testingに収録されているパッケージは大きなバージョンアップを行わずに安定して動作することを目指して小さな修正をしていきます。</li>
      <li>パッケージを説明する英語の原文も更新が落ち着きつつあるため、 <a href="https://ddtp.debian.org/ddtss/index.cgi/ja">DDTSS(ja)</a> を使ってパッケージ説明文を日本語へ翻訳する作業を開始してみましょう。</li>
    </ul>
  </ol>
</dd>
<dt>事前課題／参加申し込み方法</dt>
<dd>
  <ul>
    <li>事前課題はありません。</li>
    <li><a href="https://debianjp.connpass.com/event/273748/">勉強会のページ</a>を参照して、参加登録をしてください。</li>
  </ul>
</dd>
<dd>
ビデオ会議への参加方法は<a href="https://debianjp.connpass.com/event/273748/">勉強会のページ</a>に記載しています。
</dd>
<dd>
事前課題は参加登録時に表示されるアンケートに入力してください。提出いただいた回答は全員に配布すること、また内容の再利用を実施することを了承ください。 Debian 勉強会資料は GPL で公開します ( Web での公開が可能な内容でお願いします）。
</dd>
<dd>
課題内容の実施についてなにか質問があれば、 Debian ユーザ用公開メーリングリスト ( debian-users@debian.or.jp ) にてお願いします。
</dd>
</dl>

<p>
この勉強会の開催に関するお問い合わせは Debian 勉強会主催者：杉本 (dictoss@{debian.or.jp} ) までお願いいたします。</p>
