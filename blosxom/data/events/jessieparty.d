Debian8 "Jessie" リリースパーティ＠東京

<p>
Debian8 "Jessie" のリリースを祝してリリースパーティを行います（会場提供のサイボウズ様よりピザとドリンクの提供もございます）。2年に一度のことですので、一緒に祝いませんか？</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
   <ul>
   <li>日時：2015 年 4 月 25 日 ( 土曜日 ) 10:30-14:00</li>
   <li>費用：パーティ：2000円</li>
   <li>会場：<a href="http://cybozu.co.jp/company/info/map_tokyo.html">サイボウズ株式会社 16F会議室（東京都文京区後楽1-4-14 後楽森ビル16F)</a></li>
   <li>その他：アルコールやスナック菓子類などの持ち込み歓迎<br>
       頂いた費用の余剰分については Debian JP Project への寄付とさせていただきます。</li>
   </ul>
</dd>

<dt>参加申し込み方法</dt>
<dd>
<a href="http://debianjp.connpass.com/event/14241/">Debian8 "Jessie"リリースパーティ＠東京</a>のページから申し込みください（飲食物の手配の都合上、早めのご登録をお願い致します）</dd>
</dl>

<p>
この件に関するお問い合わせは、やまねひでき (henrich@{debian.or.jp}) までお願いいたします。</p>
