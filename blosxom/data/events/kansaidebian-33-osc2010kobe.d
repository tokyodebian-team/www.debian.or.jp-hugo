第33回 関西 Debian 勉強会 in オープンソースカンファレンス 2010 Kobe

<p>
関西 Debian 勉強会は定期的に顔をつき合わせて、Debian GNU/Linux
のさまざまなトピック(新しいパッケージ、Debian 特有の機能の仕組、Debian
界隈で起こった出来事、などなど）について話し合う会です。今月は 
<a href="http://www.ospn.jp/osc2010-kobe/">オープンソースカンファレンス 2010 Kobe</a> に参加して、
セミナーやブースでの展示と説明などを予定しています。</p>

<p>
参加者には Debian ユーザのみならず、開発の最前線に立つ Debian 
の公式開発者や開発者予備軍の方もおりますので、普段は聞けないようなさまざまな情報を得るチャンスです。
興味を持たれた方は気軽にご参加ください (また、勉強会で話をしてみたい、協力したいという方も随時募集しています)。</p>


<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：2010年3月13日(土) 10：00〜18:00<br>
        <a href="http://www.ospn.jp/osc2010-kobe/modules/eguide/event.php?eid=3">セミナー</a>は 14:00-14:45 (ブースは10:00-17:00を予定しています)
  </li>
  <li>会場：神戸市産業振興センター (神戸ハーバーランド内)</li>
  <li>主催：オープンソースカンファレンス実行委員会</li>
  <li>共催：地域 ICT 推進協議会 (COPLI)<br>
        財団法人神戸市産業振興財団</li>
  <li>費用：セミナーは無料。その他有志による物品の販売を予定 (配布・物販は極少数ですので入手したい方はお早めにお越しください)</li>
  </ul>
</dd>
<dt>セミナー (2F 展示ホール内 (b))</dt>
<dd>
  <ul>
  <li><strong>次期リリースの Debian 6.0 (コード名: Squeeze) を見てみよう</strong> (担当：佐々木 洋平)<br>
  Debian 次期リリース予定の「Squeeze」について、現在の状況やこれからどんな協力をしていけばいいかなどについてお話しします。</li>

  </ul>
</dd>
<dt>ブース展示・配布物</dt>
<dd>
  <ol>
  <li><strong>Debian 同人誌「あんどきゅめんてっどでびあん」の販売</strong></li>
  <li><strong>Debian Live DVD (Sid/Live Installer)</strong></li>
  <li><strong>Debian 稼働マシン (PlayStation3)</strong></li>
  </ol>
</dd>

<dt>その他〜 GPG 鍵キーサインの交換について</dt>
<dd>
<p>
メールへの署名やファイルの暗号化、Debian パッケージへのサイン等などに使われる GnuPG 鍵について、
Debian 関係者が集まるイベントでは信頼の輪 (Web of Trust) を広げるために<a 
href="http://www.debian.org/events/keysigning">キーサインの交換がよく行われます</a>。
オープンソースカンファレンス 2010 Kansai@Kobe でも、Debian 関係者が集まる良い機会ですので、希望する方と適宜キーサインの交換を行う予定です。</p>
<p>
キーサインを希望される方は以下を確認の上、ブースへご来場ください。</p>
</dd>
<dd>
<ul>
<li>事前に 4096bit/RSA での GnuPG 鍵の生成（<a href="http://lists.debian.or.jp/debian-users/200909/msg00008.html">debian-users メーリングリストに流れた以前の案内</a>を参考に）
<li>上記で生成したGnuPG 鍵のキーフィンガープリントの印刷物（gpg --fingerprint <あなたの鍵ID> で確認できます。手書きもできますが、多数の人と交換する場合はかなり煩雑です）
<li>身分を証明できる公的証明書の準備（パスポート、運転免許証など。特に日本語が読めない人と交換を行う場合はパスポートが必須）
<li>作成した鍵の公開鍵をキーサーバ (pgp.nic.ad.jp および pgp.mit.edu) にアップロード
</ul>
<p>
当日鍵交換の手順</p>
<ol>
<li>鍵IDとキーフィンガープリントを印刷したものを交換し会う
<li>相手の身分証明書が有効 (本人であるか・有効期限など) なものか、確認する。
<li>互いの身分証明書を見せる
<li>問題なければOKです（証明書や印刷物に不備がある場合は交換を中止してください）
</ol>

<dt>参加方法と注意事項</dt>
<dd>
セミナー参加希望者は<a href="http://www.ospn.jp/osc2010-kobe/modules/eguide/event.php?eid=3">オープンソースカンファレンスのページ</a>から事前登録を行って下さい。
</dd>

</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：のがたじゅん (nogajun&#64;{debian.or.jp} ) までお願いいたします。</p>
