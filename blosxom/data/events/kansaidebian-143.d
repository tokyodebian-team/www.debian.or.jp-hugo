第 143 回 関西 Debian 勉強会のお知らせ

<p>
「<a href="https://wiki.debian.org/KansaiDebianMeeting/20190224">第143回 関西 Debian 勉強会</a>」
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>

<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
    <li>日時： 2019 年 2 月 24 日 (日) 13:30 - 17:00 (開場 13:00)</li>
    <li>会場：<a href="http://www.sci.kyoto-u.ac.jp/ja/map.html">京都大学 理学部 3 号館 108 号室 京都大学 吉田キャンパス</a>
(定員：20名)</li>
    <li>費用：500円 (部屋代、印刷代などの諸費用のため)</li>
    <li>事前課題: <br>
      <ol>

      <li>以下の資料を読んで来てください</li>

      <li><a href="https://wiki.debian.org/BSP/">BSP</a></li>

      <li><a href="https://wiki.debian.org/BSP/BeginnersHOWTO">BSP Beginner's How to</a></li>

      <li><a href="https://people.debian.org/~vorlon/rc-bugsquashing.html">Squashing Release-Critical Bugs in Debian: A Primer</a></li>

      </ol>
    </li>
  </ul>
</dd>

<dt>内容</dt>
<dd>
  <ol>
  <li>Intro</li>
  <li>Bug Squashing Party (BSP)</li>
  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
  <a href="https://connpass.com/event/120621/">connpass</a>を参照して、
  事前登録をしてください。事情によりconnpassを使えない/登録できない方は担当者まで連絡してください
  (締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。)。
</dd>
</dl>

<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：おおつきようすけ (y.otuki30&#64;{gmail.com}) までお願いいたします。</p>
