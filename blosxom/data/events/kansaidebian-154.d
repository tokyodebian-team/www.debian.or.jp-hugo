第 154 回 関西 Debian 勉強会 (関西Debian + LILO + 東海道らぐ + openSUSE 合同 LT 大会)

<p>
「<a href="https://wiki.debian.org/KansaiDebianMeeting/20200126">第154回 関西 Debian 勉強会 (関西Debian + LILO + 東海道らぐ + openSUSE 合同 LT 大会)</a>」
のお知らせです。Linux 関連の FLOSS 4 団体による合同 LT 大会を今年も開催いたします(<a href="https://debianjp.connpass.com/event/114929/">昨年のイベント</a>)。
</p>

<dl>

<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
    <li>日時： 2020 年 1 月 26 日 (日) 13:00 - 17:00</li>
    <li>会場：<a href="https://www.sakura.ad.jp/corporate/corp/office.html">さくらインターネット株式会社 本社</a>
(定員：40名)</li>
  </ul>
</dd>

<dt>内容</dt>
<dd>
  <ol>

  <li><strong>Intro</strong><br>
  会場諸注意など
  </li>

  <li><strong>ライトニングトーク</strong></li>

</ol>
</dd>

<dt>参加方法と注意事項</dt>
<dd>
<a href="https://debianjp.connpass.com/event/161613/">connpass</a>を参照して、事前登録をしてください。
持ち時間最大 20 分の LT を募集いたします。Web から組み込みまで技術に関連する発表ならば、内容は問いません。LT の申込みは、イベント参加時のアンケートに「発表タイトル」と「発表者」をご記入ください。
事情によりconnpassを使えない/登録できない方は担当者まで連絡してください。
(締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。)
</dd>

</dl>

<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：おおつきようすけ (y.otsuki30&#64;{gmail.com} ) までお願いいたします。
</p>
