第 139 回 関西 Debian 勉強会 in 関西オープンソース2018

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting/20181110">第139回 関西 Debian 勉強会@関西オープンソース2018 </a>」
のお知らせです。
関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで Debian GNU/Linux のさまざまな
トピックについて Face to Face で楽しく話し合っていく集まりです。
</p>

<dl>

<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：11月10日(土) 11:00〜18:00
  </li>
  <li>会場：大阪南港ATC (行き方は<a href="https://www.k-of.jp/2018/access/">こちら</a>) <br>
 (大阪市交通局 ニュートラム 南港ポートタウン線 トレードセンター前駅より徒歩5分)</li>
  <li>主催：関西オープンフォーラム</li>
  <li>費用：セミナー／ミニセミナーは無料。その他有志による物品の販売を予定</li>
  </ul>
</dd>

<dt>
  <a href="https://k-of.jp/backend/session/1212">セミナー<br>
  <strong>「Debian Updates」</strong><br>
  (会場:10F-ショーケース1 11月10日(土) 15:00-15:50)</a>
</dt>
<dd>
  <ul>
  <li>最近のDebian Projectのトピックを紹介します。</li>
  <li>担当：大朏 陽介</li>
  </ul>
</dd>

<dt>ブース展示・配布物</dt>
<dd>
  <ol>
  <li><strong>Debian 9 "Stretch" 稼働マシンと勉強会資料の展示</strong></li>
  </ol>
</dd>
</dl>

<p>
この件に関するお問い合わせは 関西 Debian 勉強会担当：かわだ てつたろう (t3rkwd&#64;{debian.or.jp}) までお願いいたします。
</p>
