オープンソースカンファレンス 2013 Hamamatsu ブース出展のお知らせ

<p>
東海地域にお住まいの皆さん、こんにちは。2月に浜松市で開催される <a href="https://www.ospn.jp/osc2013-hamamatsu/">オープンソースカンファレンス 2013 Hamamatsu</a> に、東京エリア Debian 勉強会として出展します。</p>

<p>Debian JP では、Debian の開発者やユーザーの間のコミュニケーションを深めることを目的として、様々なイベントを開催しています。毎回、国籍や性別を問わず、様々なDebianユーザーが集まります。</p>

<p>今回は展示のみですが、Debian JP の活動について広くお伝えしようと考えています。Debian に触れて間もない方や、Debian JP ってどんなことをしているの？という方は、ぜひお越しください。特に今回は「東京も関西も、どっちも近くないんだよね〜」という方にとっては貴重な機会ですよ！！</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
    <li>日程：2013年2月9日(土) 10:00-17:00</li>
    <li>会場：<a href="http://www.openstreetmap.org/?lat=34.708925&lon=137.73441000000003&zoom=16&layers=M&mlat=34.70959&mlon=137.73459">浜松市市民協働センター</a> 2F ギャラリー（JR浜松駅 徒歩8分、または 遠州鉄道西鹿島線 遠州病院駅 徒歩2分）</li>
    <li>参加費用：無料</li>
  </ul>
</dd>
<dt>ブース展示物</dt>
<dd>
  <ol>
    <li>Wheezy PCの展示</li>
    <li>あんどきゅめんてっどでびあんの展示</li>
    <li>Debian のインフォグラフィック日本語版の展示と配布</li>
    <li>Debianステッカーの配布</li>
    <li>東京エリア/関西Debian勉強会の紹介</li>
  </ol>
</dd>
</dl>
<p>
この件に関するお問い合わせは ブース担当：吉田 俊輔 (koedoyoshida at {gmail.com})、赤部 晃一 (vbkaisetsu at {debian.or.jp}) までお願いいたします。</p>
