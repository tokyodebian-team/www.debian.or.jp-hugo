オープンソースカンファレンス 2015 Tokyo Fall 参加 (10/24) のお知らせ

<p>
東京近辺にいらっしゃる皆様こんにちは。10月24日(土) に
明星大学 日野キャンパスにて開催される
「<a href="http://www.ospn.jp/osc2015-fall/">オープンソースカンファレンス 2015 Tokyo/Fall</a>」に Debian JP Project が出展します。</p>

<p>
Debian 開発者とDebianユーザが集まるため、普段は聞けないような様々な情報を交換しあうチャンスです。是非参加ください。
</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日程：2015年10月24日(土)<br>
  <li>会場：<a href="http://www.meisei-u.ac.jp/access/hino.html">明星大学 日野キャンパス 28号館</a>
（多摩モノレール 「中央大学・明星大学駅」から大学まで直結。会場まで徒歩5分）</li>
  <li>参加費用：無料<br></ul></dd>

<dt><a href="https://www.ospn.jp/osc2015-fall/modules/eguide/event.php?eid=83"><strong>Debianとsystemdについて</strong></a></dt>
<dd>講師：岩松信洋 (Debian JP Project)　担当：Debian JP Project/東京エリアDebian勉強会</dd>
<dd>対象：Debian に興味、関心のある人</dd>
<dd>開始：12時00分〜12時45分</dd>
<dd>会場：28号館605教室</dd>
<dd>
<p>
いろいろ話題になっている systemd ですが、Debian でも影響を受けています。
本セミナーではsystemd によって Debian にどのような変化が起こったのか、また
Debian での systemdと他の init system に関する情報についてお話します。
</p>
</dd>

<dt>ブース展示物</dt>
<dd>
  <ol>
  <li>Debian 勉強会資料「あんどきゅめんてっどでびあん」</li>
  <li>Debian "Jessie" 稼働マシン</li>
  <li>Debian のインフォグラフィック(基礎知識／歴史／派生ディストリビューションの流れを俯瞰できます)</li>
</dd>

<p>この件に関するお問い合わせは 岩松 信洋 iwamatsu at {debian.or.jp} までお願いいたします。</p>
