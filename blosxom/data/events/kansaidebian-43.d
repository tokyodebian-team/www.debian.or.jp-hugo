第43回 関西 Debian 勉強会のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting20110123">第43回 関西 Debian 勉強会</a>」 
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで 
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
  <li>日時：2011 年 1 月 23 日 (日) 13:30 - 17:00 (13:15 より受付)</li>
  <li>会場：<a href="http://www.city.osaka.lg.jp/shimin/page/0000016613.html">大阪 港区民センター 楓の間</a>
(定員：30名)</li>
  <li>費用：500円 (部屋代、印刷代などの諸費用のため)</li>
  <li>事前課題: 一つバグをみつけてきて下さい。ご意見要望でもかまいません。</li>
  </ul>
</dd>

<dt>内容</dt>
<dd>
  <ol>
  <li><strong>Intro</strong><br>
この勉強会の簡単な説明をします。</li>

  <li><strong>バグ報告はバグのためだけじゃないよ</strong> (担当：のがたじゅん)<br>
Debian Bug Tracking System(BTS)というと「自分は不具合にあったこと無い からバグ報告は関係ないよ」そう思う人いませんか? BTSは不具合の管理だけに使われているものではありません。Debianへの要望 やパッケージ作成宣言などDebianのさまざまな報告にも使われます。 今回はバグ報告のためのツールreportbugの使い方のおさらいと、不具合報告 以外に使うBTSのあれこれをお話ししたいと思います。</li>

  <li><strong>Debian GNU/kFreeBSDで便利に暮らすためのTips</strong> (担当：杉本典充)<br>
Debian GNU/kFreeBSDは完成度も徐々に高まってきており、 常用環境として使える域にきていると思います。 しかし、Debian GNU/Linuxで普通にできることが Debian GNU/kFreeBSDでは一工夫必要なこともあるのは確かです。 今回は、常用環境として使っている中で出てきたTipsをご紹介します。</li>

  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
<strong>1 月 21 日 (金) 24:00 までに</strong>
<a href="http://debianmeeting.appspot.com/event?eventid=8845ef141d614497020a728de2405babd6d97f17">Debian勉強会予約管理システム</a>を参照して、
事前登録をしてください。登録にはGoogleアカウントが必要になりますが、事情によりGoogleアカウントを使えない/登録できない方は担当者まで連絡してください
（締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。
懇親会 (予定) に参加する場合は、二次会参加の欄にチェックしてください）。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：倉敷 悟 (lurdan&#64;{debian.or.jp} ) までお願いいたします。</p>
