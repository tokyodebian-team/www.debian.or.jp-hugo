第12回 関西 Debian 勉強会のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting20080429">第12回 関西 Debian 勉強会</a>」 
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで 
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>
<p>
今回は姫路と少し場所は遠いですが、午前中は<a href="#shiro-kashihaku">姫路在住の有志によって観光ツアー</a>を行います。
もしよろしければ、そちらにもご参加下さい。</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：2008 年 4 月 29 日 (火) 13:45 - 17:00 (13:30 より受付)</li>
  <li>会場：<a herf="http://www.himeji-du.ac.jp/satellite/index.html">姫路獨協大学駅前サテライト</a>
(定員：25名)</li>
  <li>費用：500円 (部屋代、印刷代などの諸費用のため)</li>
  </ul>
</dd>
<dt>内容</dt>
<dd>
  <ol>
  <li><strong>Windows な PC でも Debian を楽しもう</strong> 名村 氏<br>
Windows でしか動かないソフトがあったり、会社の PC が Windows だったりと、
なかなか Windows を手放すことができなかったりします。そんな Windows 
な環境にあっても、coLinux があればお気軽に Debian を楽しむことができます。
coLinux の設定から、Debian を 楽しむまでの流れをご紹介します。<br></li>

  <li><strong>Debian GNU/kFreeBSD</strong>　大浦 真 氏<br>
Debian というオペレーティングシステムは、主に Linux カーネルを利用していますが、
それ以外のカーネルでも使えるように移植作業が進められています。
いくつかの移植版がありますが、FreeBSD のカーネルを利用している
Debian GNU/kFreeBSD は、それなりに使えるようになっています。
今回はこの Debian GNU/kFreeBSD について、特にインストール方法について紹介します。<br></li>

  <li><strong>はじめてのSid</strong>　山下 尊也 氏<br>
Desktopなどで使っていると、stableでは物足りないなぁ？とDebianユーザの誰もが思うはず・・・
あなたがSidを触れる何かのキッカケになれたらと言うことで、ちょっとした技などを伝授します。</li>
  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
<strong>4月27日(日) 正午までに</strong>
<a href="http://cotocoto.jp/event/25108">京都.cotocoto</a>を参照して、
事前登録をしてください
（締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。
懇親会に参加する場合は、コメントに「懇親会参加希望」と記入してください）。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：山下尊也 (takaya&#64;{debian.or.jp} ) までお願いいたします。</p>

<dl>
<dt>
<a id="shiro-kashihaku">勉強会の前に姫路城/姫路菓子博いっちゃおうぜツアー</a></dt>
<dd>
この企画は、前回姫路で開催された関西Debian勉強会にて、とある Debian Developper 
が勉強会の前に姫路城に行った話題から発展し「今度は希望者で行ってみようよ」ということで、
有志により企画されました。
姫路城周辺では姫路菓子博も開かれていて「菓子博にも行きたい」という方もいらしたので、
二手に別れて勉強会の前にゆるーく観光してみたいと思います。</dd>
<dd>
<p>スケジュール</p>
<ul>
<li>10:00　<a href="http://www.jr-odekake.net/eki/premises.php?id=0610619">姫路駅中央改札口集合</a></li>
<li>10:15ごろ 徒歩にて姫路城前まで移動</li>
<li>10:30ごろ 姫路城前(姫路城大手門)着。姫路城組と菓子博組に別れて観光。</li>
<li>12:00ごろ 姫路城前(姫路城大手門)集合。勉強会会場付近に戻り各自昼食。</li></ul>
<p>
　詳しくは<a href="http://maps.google.co.jp/maps/ms?ie=UTF8&hl=ja&msa=0&msid=103028174646890807927.00044ab89d235251949f6&ll=34.834659,134.692984&spn=0.017965,0.029697&z=15">こちら</a>をご覧ください。</p>
<p>
参加費：姫路城を観光される方は入城料600円、菓子博に行かれる方は菓子博入場料2000円が必要です。</p>
</dd>
</dl>


