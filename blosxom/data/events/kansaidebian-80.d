第80回 関西 Debian 勉強会のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting/20140126">第80回 関西 Debian 勉強会</a>」
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
    <li>日時： 2014 年 1 月 26 日 (日) 13:30 - 17:00</li>
    <li>会場：<a href="http://www.city.osaka.lg.jp/shimin/page/0000016603.html">福島区民センター304号会議室</a>
(定員：15名)</li>
    <li>費用：500円 (部屋代、印刷代などの諸費用のため)</li>
    <li>事前課題: <br>
      <ol>

      <li>
      Debian で、やろうとしてできていない上手くいってないこと、もしくはやろうとしていることを教えてください。
      </li>

      <li>
      LT 歓迎です. 何かお話したい方はタイトルを下さい.
      </li>

      </ol>
    </li>
  </ul>
</dd>

<dt>内容</dt>
<dd>
  <ol>

  <li><strong>Intro</strong><br>
  この勉強会の簡単な説明をします。
  </li>

  <li><strong>ライトニングトーク</strong>
    <ul>
    <li>最近の Debian 関連ニュース</li>
    <li>「jenkins + jenkins-debian-glue + freight で野良リポジトリ作った」</li>
    <li>etc.</li>
    </ul>
  </li>

  <li><strong>もくもくの会</strong><br>
    各自作業を行なったり、質問、疑問に回答する時間になります。
  </li>

  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>

<strong>2014 年 1 月 25 日 (土) 23:59 までに</strong>
<a href="http://debianmeeting.appspot.com/event?eventid=f49a08934a860a112c9cdce19776a0b7118cfbc0">Debian勉強会予約管理システム</a>を参照して、
事前登録をしてください。登録にはGoogleアカウントが必要になりますが、事情によりGoogleアカウントを使えない/登録できない方は担当者まで連絡してください
（締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。
懇親会 (予定) に参加する場合は、二次会参加の欄にチェックしてください）。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：かわだてつたろう (t3rkwd&#64;{debian.or.jp} ) までお願いいたします。</p>
