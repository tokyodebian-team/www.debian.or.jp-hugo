第 154 回 Debian 勉強会 in オープンソースカンファレンス 2017 Tokyo/Fall

<p>
東京近辺にいらっしゃる皆様こんにちは。2017年9月も 東京エリア Debian 勉強会が開かれます！
</p>

<p>
 東京エリア Debian 勉強会とは、Debian Project に関わる事を目的とす
る熱いユーザたちと、実際に Debian Project にてすでに日夜活動している人
らが Face to Face で Debian GNU/Linux のさまざまなトピック （パッケー
ジ、Debian 特有 の機能の仕組について、Debian 界隈で起こった出来事、
etc ） について語り合う場です。当然ですが、Debian Project に何らかの貢献
をしたいという目的を持つ方であれば、自身で活動できるように手助けをする事
も主な趣旨の一つとしています。
</p>
<p>
参加される方は主に東京を中心に関東近郊の国籍・性別不問の Debian ユーザです 
( Debian をまだ使ったことが無いが興味があるので…とか、かなり遠い所から来てくださる方もいます)。
開発の最前線にいる Debian の公式開発者や開発者予備軍の方も参加する場合がありますので、
普段は聞けないような様々な情報を得るチャンスです。
興味と時間があった方、是非御参加下さい。
（また、勉強会で話をしてみたいという方も随時募集しています）。</p>

<p>
今回は、
<a href="http://www.ospn.jp/osc2017-fall/">オープンソースカンファレンス 2017 Tokyo/Fall</a>
に参加します。普段は聞けないような様々な情報を交換しあうチャンスです。
是非御参加下さい。</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日程：2017年9月10日(土)<br>
  <li>会場：<a href="http://www.meisei-u.ac.jp/access/hino.html">明星大学 日野キャンパス 28号館 2F</a>
（多摩モノレール 「中央大学・明星大学駅」から大学まで直結。会場まで徒歩5分）</li>
  <li>参加費用：無料<br></ul>
</dd>

<dt>
<a href="https://www.ospn.jp/osc2017-fall/modules/eguide/event.php?eid=79"><strong>Debian update</strong></a>
</dt>
<dd>講師：杉本 典充（東京エリアDebian勉強会）担当：Debian JP Project / 東京エリアDebian勉強会</dd>
<dd>対象：Debian に興味、関心のある人</dd>
<dd>開始：15時15分 〜</dd>
<dd>会場：28号館 3F 303教室</dd>
<dd>
<p>
6月にリリースしたDebian 9 (コードネーム Stretch）の話題を中心に最近のDebian Project 界隈のトピックを紹介します。
</p>
</dd>

<dt>ブース展示物</dt>
<dd>
  <ol>
  <li>Debian 勉強会資料「あんどきゅめんてっどでびあん」展示</li>
  <li>Debian 稼働マシン 展示</li>
  <li>Debian インストールCD、ステッカー等の配布</li>
  </ol>
</dd>
</dl>

<p>
この件に関するお問い合わせは 東京エリアDebian 勉強会 担当：岩松 信洋 (iwamatsu &#64; {debian.or.jp} ) までお願いいたします。
</p>
