第50回 東京エリア Debian 勉強会のお知らせ

<p>
東京近辺にいらっしゃる皆様こんにちは。今年も Debian 勉強会が開かれます！
今月は東京大学さんにお邪魔しての開催となります。
</p>

<p>
Debian 勉強会とは、Debian 
の開発者になれることをひそかに夢見るユーザたちと、
ある時にはそれを優しく手助けをし、またある時には厳しく叱咤激励する Debian 
開発者らが Face to Face で Debian GNU/Linux のさまざまなトピック
（新しいパッケージ、Debian 特有の機能の仕組について、Debian 界隈で起こった出来事、etc）
について語り合うイベントです。</p>
<p>
参加される方は主に東京を中心に関東近郊の国籍・性別不問の Debian ユーザです 
(Debian をまだ使ったことが無いが興味があるので…とか、かなり遠い所から来てくださる方もいます)。
開発の最前線にいるDebian の公式開発者や開発者予備軍の方も居ますので、
普段は聞けないような様々な情報を得るチャンスです。
興味と時間があった方、是非御参加下さい。
（また、勉強会で話をしてみたいという方も随時募集しています）。</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
   <li>日時：2009年3月21日(土曜日) 15:00-19:00</li>
   <li>費用：なし</li>
   <li>会場：<a href="http://www.u-tokyo.ac.jp/campusmap/cam01_04_03_j.html">東京大学 工学部2号館 211号講義室</a></li>
   </ul>
</dd>
<dt>内容</dt>
<dd>
  <ol>
  <li><strong>DPN trivia quiz</strong> (担当:上川純一)<br>
Debian 関連のニュースにおいついていますか? 簡単なクイズでDebian常識テストしちゃいます。</li>
  <li><strong>研究室のソフトウェアを Debian パッケージにしてみる</strong> (担当: 藤澤 徹)<br>
研究室で使われているフリーソフトウェアを Debian パッケージにしてみたので、その過程を説明します。</li>
  <li><strong>Debian での Common Lispプログラミング環境</strong> (担当: 日比野 啓)<br>
Debian での Common Lisp 生活方法について説明します。</li>
  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
参加希望者は事前登録してください。期限は 3月19日中です。
<strong>
事前登録については <a href="https://tokyodebian-team.pages.debian.net/2009-03.html">Debian 勉強会の Web ページ</a> を参照下さい。
</strong>
</dd>
<dt>勉強会中継について</dt>
<dd>
<strong>
今回は 勉強会中継を行いません。
</strong>
</dd>
</dl>
<p>
この件に関するお問い合わせは Debian 勉強会主催者：上川純一 (dancer&#64;{debian.org,netfort.gr.jp} )
までお願いいたします。</p>
