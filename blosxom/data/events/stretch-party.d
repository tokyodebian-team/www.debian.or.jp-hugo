6/18 Debian9 "stretch" リリースパーティのお知らせ

<p>
6/17 に Debian9 "stretch" がリリースされます。それを祝って東京・大阪でリリースパーティが開かれます。お早めにお申し込み下さい。</p>


<dl>
<dt>東京：開催日時・会場</dt>
<dd>
   <ul>
   <li>日時：2017 年 6 月 18 日 ( 日曜日 ) 10:30-12:30(勉強会)　13:00-17:00（パーティ）</li>
   <li>費用：パーティ：2000円（当日支払）</li>
   <li>会場：<a href="https://cybozu.co.jp/company/access/tokyo/">サイボウズ株式会社</a> 東京都中央区日本橋2-7-1 (東京日本橋タワー 27階)</li>
   <li>申し込み：<a href="https://debianjp.connpass.com/event/58439/">conpassのページ</a>よりお申し込み下さい</li>
   </ul>
</dd>

<dt>大阪：開催日時・会場</dt>
<dd>
   <ul>
   <li>日時：2017 年 6 月 18 日 ( 日曜日 ) 13:00-19:00</li>
   <li>費用：パーティ：500円（当日支払）</li>
   <li>会場：<a href="">さくらインターネット 大阪本社</a> 大阪府大阪市北区大深町4−1 グランフロントタワーA 35階</li>
   <li>申し込み：<a href="https://debianjp.connpass.com/event/59443/">conpassのページ</a>よりお申し込み下さい</li>
   </ul>
</dd>
