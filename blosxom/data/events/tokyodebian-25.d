第25回 Debian 勉強会のお知らせ

<p>
さて今月も
<a href=" https://tokyodebian-team.pages.debian.net/">「Debian 勉強会」</a>
が開かれます。
Debian 勉強会は月に一回、顔をつき合わせて Debian GNU/Linux のさまざまなトピック
（新しいパッケージ、Debian 特有の機能の仕組について、Debian 界隈で起こった出来事、etc）
について話し合う会です。</p>
<p>
参加者には開発の最前線にいる Debian の公式開発者や開発者予備軍の方も居ますので、
普段は聞けないような様々な情報を得るチャンスです。<br>
興味を持たれた方は是非御参加下さい（また、勉強会で話をしてみたいという方も随時募集しています）。</p>


<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：2007年2月17日(土) 18:00-21:00
  <li>会場：<a href=
"http://www2.city.suginami.tokyo.jp/map/detail.asp?home=H00150">あんさんぶる荻窪</a>　(JR荻窪駅すぐ近く)
  <li>費用：資料代・会場費として 500 円を予定</ul></dd>
<dt>内容</dt>
<dd>
  <ol>
  <li><strong> パッチ管理ツール quilt の使い方</strong><br>
パッケージ作成時にパッチを管理するのに使われるツール quilt の使い方を説明します。
今回の勉強会はパッチ管理特集なので、パッチ管理の基本から話す予定です。

  <li><strong>dbs について</strong><br>
パッチを管理するのに使われるツール dbs の使い方について説明をします。

  <li><strong>svn-buildpackage などとともに dpatch を使う</strong><br>
Debian パッケージ作成の際に独自パッチの管理ツールとして良く使われる dpatch。
以前にも紹介しましたが、今回は特に orig.tar.gz と debian/ ディレクトリだけで
構成されるソースパッケージのバージョン管理の方法に的を絞って説明します。

  <li><strong>Open Source Conference 2007 Tokyo/Spring 参加に向けた準備をしよう</strong><br>
来月3月に開かれる<a href="http://www.ospn.jp/osc2007-spring/">オープンソースカンファレンス 2007 Tokyo/Spring</a>
に Debian 勉強会として参加予定です。その準備に付いて皆で意見交換などできればと思っています</ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
参加希望者は事前登録の上、事前課題の解答を勉強会用メーリングリストに送付してください。
期限は 2 月 15 日中です。<br>
<strong>事前登録、及び事前課題の内容と送付先については 
<a href=
"https://tokyodebian-team.pages.debian.net/2007-02.html">Debian 勉強会の Web ページ
</a>を参照下さい</strong>。
</dd>
</dl>
<p>
この件に関するお問い合わせは Debian 勉強会主催者：上川純一 (dancer&#64;{debian.org,netfort.gr.jp} )
までお願いいたします。</p>
