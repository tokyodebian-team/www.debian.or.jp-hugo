大統一Debian勉強会2013 Call For Presentation のお知らせ

  <p>2013年6月29日(土曜日)に日本大学 駿河台キャンパス 駿河台校舎1号館 131, 132, 133, 134教室において、全国のDebian勉強会合同の勉強会「大統一Debian勉強会 2013」を開催します。それに伴い、大統一Debian勉強会2013で発表を希望される方を募集しています。</p>
  <p>ご興味をもたれた方は、<a href="http://gum.debian.or.jp/2013/cfp_requirement.html">大統一Debian勉強会2013のページ</a>を参照して発表の要件を確認の上、ご応募ください。<br>
<strong class="attention">応募締切りは 4/28 (日)</strong> です（資料の締切りは6/10となります）。</p>

  <h4>大統一Debian勉強会2013 開催概要</h4>

  <dl>
    <dt>開催日時</dt>
    <dd>
      <ul>
	<li>2013年6月29日(土曜日) 10時00分〜18時00分まで(予定)</li>
      </ul>
    </dd>

    <dt>開催場所</dt>
    <dd>
      <ul>
	<li>日本大学 駿河台キャンパス 駿河台校舎1号館 131, 132, 133, 134教室</li>
	<li><a href="http://www.arch.cst.nihon-u.ac.jp/bldgno1/bldgno1.html">http://www.arch.cst.nihon-u.ac.jp/bldgno1/bldgno1.html</a></li>
        <li><a href="http://www.arch.cst.nihon-u.ac.jp/life/access.html">http://www.arch.cst.nihon-u.ac.jp/life/access.html</a></li>
      </ul>
    </dd>

    <dt>参加費</dt>
    <dd>無料</dd>

    <dt>公式タグ</dt>
    <dd>Twitter: <a href="https://twitter.com/search?q=%23gumdebianjp&src=hash">#gumdebianjp</a></dd>
    <dd>ブログ: gumdebianjp2013</dd>
  </dl>

  <h4>お問い合わせ</h4>
  <p>
  大統一Debian勉強会 2013に関するお問い合わせは、grand-meeting _at_ debian.or.jp までメールにてご連絡ください。
  </p>
