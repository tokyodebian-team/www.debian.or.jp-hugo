第69回 関西 Debian 勉強会のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting/20130224">第69回 関西 Debian 勉強会</a>」
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
    <li>日時： 2013 年 2 月 24 日 (日) 13:30 - 17:00</li>
    <li>会場： GREE 大阪オフィスセミナールーム 大阪市北区鶴野町1-9 梅田ゲートタワー10F <a href="http://www.openstreetmap.org/?mlat=34.70677&mlon=135.50105&zoom=18">地図</a>
    (定員：40名)</li>
    <li>費用：500円 (部屋代、印刷代などの諸費用のため)</li>
    <li>事前課題: <br>
      <ol>

      <li>
      リリースノートの<a href="http://www.debian.org/releases/stable/i386/apb.html.ja">付録 B. preseed を利用したインストールの自動化</a>を読んできて下さい。<br>
      また、読んでよくわからない点、気になる点などがあれば、教えて下さい。
      </li>

      <li>
      Wheezy もしくは sid 環境に apt-get install ruby してきて下さい。
      </li>

      <li>
      普段(業務もしくは趣味等)お使いの Ruby ソフトウェアがあれば、教えて下さい。<br>
      また、それが Debian パッケージになっていないならば、ご指摘下さい。
      </li>

      </ol>
    </li>
  </ul>
</dd>

<dt>内容</dt>
<dd>
  <ol>

  <li><strong>Intro</strong><br>
  この勉強会の簡単な説明をします。
  </li>

  <li><strong>「Debian Installer トラブルシューティング」</strong>(担当：Yuryu)<br>
  インストール自動化で preseed ファイルを書くことがありますが、インストーラーということもあり導入にかなり苦労しました。<br>
  その苦労で得られた話を中心に話します。
  </li>

  <li><strong>「Ruby In Wheezy」</strong>(担当：佐々木洋平)<br>
  次期安定版 Debian 7.0 (Wheezy) における Ruby 環境について、特に複数の Ruby 実装の共存と Gem とのお付き合いの仕方についてお話しします。
  </li>

  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
<strong>2013年 2 月 23 日 (土) 23:59 までに</strong>
<a href="http://debianmeeting.appspot.com/event?eventid=9eac870e6eca3453b3cc6ebbd5086c4cbfd49599">Debian勉強会予約管理システム</a>を参照して、
事前登録をしてください。登録にはGoogleアカウントが必要になりますが、事情によりGoogleアカウントを使えない/登録できない方は担当者まで連絡してください
（締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。
懇親会 (予定) に参加する場合は、二次会参加の欄にチェックしてください）。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：かわだてつたろう (t3rkwd&#64;{debian.or.jp} ) までお願いいたします。</p>
