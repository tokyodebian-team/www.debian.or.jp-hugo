2020 年 5 月度 東京エリア・関西合同 Debian 勉強会のお知らせ

<p>
日本にお住まいのDebianにご興味がある皆様こんにちは。今月の勉強会はオンラインによるweb会議のスタイルで東京エリア Debian 勉強会と関西 Debian 勉強会が合同で開催することにいたしました。
</p>

<dl>
<dt>東京エリア Debian 勉強会</dt>
<dd>
<p>
東京エリア Debian 勉強会とは、Debian Project に関わる事を目的とする
熱いユーザたちと、実際に Debian Project にてすでに日夜活動している
人らが Face to Face で Debian GNU/Linux のさまざまなトピック （パッケージ、
Debian 特有 の機能の仕組について、Debian 界隈で起こった出来事、
etc ） について語り合う場です。当然ですが、Debian Project に何らかの貢献
をしたいという目的を持つ方であれば、自身で活動できるように手助けをする事
も主な趣旨の一つとしています。
</p>
<p>
また、開発の最前線にいる Debian の公式開発者や開発者予備軍の方も参加する場合がありますので、
普段は聞けないような様々な情報を得るチャンスです。
興味がありご都合の合う方はぜひともご参加下さい。
</p>
<p>
また、勉強会で話をしてみたいという方も随時募集しています。
</p>
</dd>

<dt>関西 Debian 勉強会</dt>
<dd>
<p>
関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで Debian のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。
</p>
</dd>
</dl>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：2020 年 5 月 16 日 ( 土曜日 ) 14:00-15:30 </li>
  <li>会場：Jitsi を使った Web ビデオ会議（<a href="https://vc2.pcdennokan.wjg.jp/TokyoDebian20200516">ビデオ会議のURL</a>）</li>
  <li>参加費：無料</li>
  </ul>
</dd>
<dt>内容</dt>
<dd>
  <ol>
  <li><strong>セミナー：aptlyを使ったパッケージリポジトリの作成（発表者：岩松さん）</strong></li>
    <ul>
    <li>Debian/Ubuntu のパッケージリポジトリを作成するには、dpkg-scanpackages[0] コマンドやapt-ftparchive[1] コマンド、reprepro[2] などを使った方法があります。</li>
    <li>先日 aptly[3][4] と呼ばれるツールを使ってパッケージリポジトリを作成する機会がありました。今回はこれについての使い方を紹介します。</li>
    <li>参考URL</li>
    <ul>
      <li>[0] <a href="https://manpages.debian.org/unstable/dpkg-dev/dpkg-scanpackages.1.en.html">https://manpages.debian.org/unstable/dpkg-dev/dpkg-scanpackages.1.en.html</a></li>
      <li>[1] <a href="https://manpages.debian.org/unstable/apt-utils/apt-ftparchive.1.en.html">https://manpages.debian.org/unstable/apt-utils/apt-ftparchive.1.en.html</a></li>
      <li>[2] <a href="https://manpages.debian.org/buster/reprepro/reprepro.1.en.html">https://manpages.debian.org/buster/reprepro/reprepro.1.en.html</a></li>
      <li>[3] <a href="https://www.aptly.info/">https://www.aptly.info/</a></li>
      <li>[4] <a href="https://manpages.debian.org/buster/aptly/aptly.1.en.html">https://manpages.debian.org/buster/aptly/aptly.1.en.html</a></li>
    </ul>
    </ul>
  </li>
  </ol>
</dd>
<dt>事前課題／参加申し込み方法</dt>
<dd>
<a href="https://debianjp.connpass.com/event/174544/">勉強会のページ</a>を参照して、参加登録をしてください。
</dd>
<dd>
ビデオ会議への参加方法は<a href="https://debianjp.connpass.com/event/174544/">勉強会のページ</a>に記載しています。
</dd>
<dd>
事前課題は参加登録時に表示されるアンケートに入力してください。提出いただいた回答は全員に配布すること、また内容の再利用を実施することを了承ください。 Debian 勉強会資料は GPL で公開します ( Web での公開が可能な内容でお願いします）。
</dd>
<dd>
課題内容の実施についてなにか質問があれば、 Debian ユーザ用公開メーリングリスト ( debian-users@debian.or.jp ) にてお願いします。
</dd>
</dl>

<p>
この勉強会の開催に関するお問い合わせは Debian 勉強会主催者：杉本 (dictoss@{debian.or.jp} ) までお願いいたします。</p>
