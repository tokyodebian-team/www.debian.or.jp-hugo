第0回 Debian もくもくの会

<p>
<a href="http://debianjp.doorkeeper.jp/events/6890">「第0回 Debian もくもくの会」</a>
のお知らせです。
</p>
<p>Debian 開発者、パッケージメンテナ、ユーザー、ユーザー予備軍が集まって Debian に関してもくもくと作業をします。</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日程：2013 年 11 月 23 日 ( 土 ) <br>
  <li>会場：<a href="http://www.miraclelinux.com/company/access-map">ミラクル・リナックス株式会社</a></li>
  <li>参加費用：無料<br>
  <li>参加登録：必要<br>
  </ul>
</dd>

  <dt>内容</dt>

<dd>Debian / FLOSSに関する作業をもくもくと行います。
  <ul>
  <li>Debianパッケージ作業をしたい人</li>
  <li>Debian 関連の翻訳をしたい人</li>
  <li>Debian インストールしたい人</li>
  <li>Debian について相談などをしたい人</li>
  <li>FLOSS 関連の作業を皆でわいわいとやりたい人</li>
  </ul>
</dd>

<p>詳細は<a href="http://debianjp.doorkeeper.jp/events/6890">「第0回 Debian もくもくの会」(Doorkeeper)</a>を参照ください。</p>
<p>また、この件に関するお問い合わせは 担当: 岩松(iwamatsu at {debian.or.jp})までお願いします。</p>
