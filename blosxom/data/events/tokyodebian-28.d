第28回 Debian 勉強会のお知らせ

<p>
今月も Debian 勉強会が開かれます。
Debian 勉強会とは、勉強会とは銘打っていますが特に堅苦しいことはなく、
Face to Face で Debian GNU/Linux のさまざまなトピック
（新しいパッケージ、Debian 特有の機能の仕組について、Debian 界隈で起こった出来事、etc）
について話し合うイベントです。</p>
<p>
今回は Debian GNU/Linux 4.0 (Etch) のリリースを記念して、
Etch をネタに盛り上がりたいと思います。
</p>
<p>
参加するのは東京を中心に関東近郊の Debian ユーザ です (Debian をまだ使ったことが
無いが興味があるので…という方もいます)。
開発の最前線にいる Debian の公式開発者や開発者予備軍の方も居ますので、
普段は聞けないような様々な情報を得るチャンスです。<br>
興味を持たれた方は是非御参加下さい（また、勉強会で話をしてみたいという方も随時募集しています）。</p>


<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：2007年5月19日(土) 18:00-21:00
  <li>会場：<a href=
"http://www2.city.suginami.tokyo.jp/map/detail.asp?home=H00150">あんさんぶる荻窪</a>　(JR荻窪駅すぐ近く)
  <li>費用：資料代・会場費として 500 円を予定</ul></dd>
<dt>内容</dt>
<dd>
  <ol>
  <li><strong>Debconf7 (リハーサルバージョン)</strong>（担当：岩松 信洋＆上川 純一）<br>
  エジンバラで開催される世界中から Debian 開発者が集まるカンファレンス、
<a href="https://debconf7.debconf.org/wiki/Main_Page">Debconf7</a> 
で発表する予定の「エッチをスーパーエッチで動かす」ネタと
「pbuilder BOF」のネタのリハーサルをかねて発表します。
  <li><strong>サーバをエッチにしてみました</strong>（担当：小室 文）<br>
Debian GNU/Linux 4.0 (Etch) がリリースされて一ヶ月がたち、リリースノートも日本語訳が完了した今日、
サーバをエッチにしてみた人も多いのではないでしょうか。
apache / exim / bind が稼働しているサーバをエッチにしてみた体験を告白します。
  <li><strong>Debian Etch についてディスカッション</strong><br>
皆さんの Debian Etch の印象・体験・良かったこと・困ったことなどなど。
色々とディスカッションしてみましょう。</ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
参加希望者は事前登録の上、事前課題の解答を勉強会用メーリングリストに送付してください。
期限は 5 月 17 日中です。<br>
<strong>事前登録、及び事前課題の内容と送付先については 
<a href=
"https://tokyodebian-team.pages.debian.net/2007-05.html">Debian 勉強会の Web ページ
</a>を参照下さい</strong>。
</dd>
</dl>
<p>
この件に関するお問い合わせは Debian 勉強会主催者：上川純一 (dancer&#64;{debian.org,netfort.gr.jp} )
までお願いいたします。</p>
