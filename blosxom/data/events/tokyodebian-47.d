第47回 東京エリア Debian 勉強会のお知らせ (ストリーミング有り)

<p>
東京近辺にいらっしゃる皆様こんにちは。
12 月も Debian 勉強会が開かれます！
</p>

<p>
Debian 勉強会とは、Debian 
の開発者になれることをひそかに夢見るユーザたちと、
ある時にはそれを優しく手助けをし、またある時には厳しく叱咤激励する Debian 
開発者らが Face to Face で Debian GNU/Linux のさまざまなトピック
（新しいパッケージ、Debian 特有の機能の仕組について、Debian 界隈で起こった出来事、etc）
について語り合うイベントです。
</p>
<p>
参加される方は主に東京を中心に関東近郊の国籍・性別不問の Debian ユーザです 
(Debian をまだ使ったことが無いが興味があるので…とか、かなり遠い所から来てくださる方もいます)。
開発の最前線にいるDebian の公式開発者や開発者予備軍の方も居ますので、
普段は聞けないような様々な情報を得るチャンスです。
興味と時間があった方、是非御参加下さい。
（また、勉強会で話をしてみたいという方も随時募集しています）。
</p>

<p>
なお、今回は前回の復習を兼ねた事前課題を用意しています、前回参加してい
ない方には難しすぎる可能性もあるため、無理な場合は「LaTeX+Gitの事前課題
ができなかった、こんなハマり方しました体験記」をお送りください。また、
前回参加していたとしても、今回初のパッチ送信になる方は手間取ることが予
想されますので、お早めに取りかかって見てください。
前回参加されていない方は <a href="http://lists.debian.or.jp/debian-users/200812/msg00068.html">Debian-Users ML [debian-users:51418] 事前課題の処理方法について</a>
を参考にしてください。

また、Lightning Talks と題して、5分の時間制限でプレゼンテーションをして
いただける方を募集しています。我こそはというネタのある方はふるってご応募く
ださい。ご連絡お待ちしています。
</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
   <li>日時：2008年12月20日(土) 18:00-21:00</li>
   <li>費用：資料代・会場費として 500 円</li>
   <li>会場：<a 
href="http://www2.city.suginami.tokyo.jp/map/detail.asp?home=H00150">あんさんぶる荻窪</a>　(JR荻窪駅すぐ近く)</li>
   </ul>
</dd>
<dt>内容</dt>
<dd>
  <ol>
  <li><strong>2008年のDebian勉強会をふりかえって</strong> (担当:上川純一)<br>2008年のDebian勉強会はどうだったか、忘年会らしく(?)振り返ってみましょう。</li>
  <li>Lightning Talks (担当:未定)<br>5分程度で発表者がいれかわるライトニングトーク。厳しい制限時間と緊張感のある中、どんなネタが披露されるか?</li>
  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
参加希望者は事前登録の上、事前課題の回答を勉強会用メーリングリスト
に送付してください。期限は 12 月 18 日中です。<br>
<strong>事前登録、及び事前課題の内容と送付先については 
<a href="https://tokyodebian-team.pages.debian.net/2008-12.html">Debian 勉強会の Web ページ
</a>を参照下さい。</strong>
</dd>
<dt>勉強会中継について</dt>
<dd>
今回は <a href="http://www.ustream.tv/">ustream</a> を使って勉強会の中継および録画を行う予定です。参加者の方で顔出し等ができない方は、主催者まで連絡をください。また、細かい情報等はこのページを参照してください。
  <ul>
  <li>チャンネル名: <a href="http://www.ustream.tv/channel/tokyo-debian-meeting-200812">tokyo-debian-meeting-200812</a></li>
  <li>中継時間: 19:50:10 - 20:59
  <li>注意: ネットワークの状態により、中継ができない場合があります。また、今回はライトニングトークのみの中継を行います。
  </ul>
</dd>
</dl>
<p>
この件に関するお問い合わせは Debian 勉強会主催者：上川純一 (dancer&#64;{debian.org,netfort.gr.jp} )
までお願いいたします。</p>
