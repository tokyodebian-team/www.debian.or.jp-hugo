第 111 回 関西 Debian 勉強会, Debian パッケージング道場のお知らせ

<p>
「<a href="https://wiki.debian.org/KansaiDebianMeeting/20160626">第111回 関西 Debian 勉強会, Debian パッケージング道場</a>」
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで
Debian のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
    <li>日時： 2016 年 6 月 26 日 (日) 13:30 - 17:00 (開場 13:00)</li>
    <li>会場：<a href="https://www.math.kyoto-u.ac.jp/ja/overview/access">京都大学理学部3号館 108 号室</a>
(定員：20名)</li>
  </ul>
</dd>

<dt>内容</dt>
<dd>
第 111 回勉強会は、Debian パッケージの作り方を教える道場です。以下のような方を想定しています。
  <ul>
    <li>あのソフトウェアがDebianパッケージになっていないので、パッケージにしてみたい。</li>
    <li>普段使っているDebianパッケージに修正を加えて、利用したい。</li>
    <li>メンテナンスしているDebianパッケージを更新してみる。</li>
    <li>モダンなDebianパッケージの作成方法とか知りたい。</li>
    <li>etc...</li>
  </ul>
Debianパッケージを作ってみましょう。
Debian Developer と Debian Maintainer が、パッケージの作成をサポートします。
</dd>

<dt>参加方法と注意事項</dt>
<dd>

<a href="https://debianjp.doorkeeper.jp/events/46935">Doorkeeper</a>を参照して、
事前登録をしてください。事情によりDoorkeeperを使えない/登録できない方は担当者まで連絡してください
(締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。)。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：かわだてつたろう (t3rkwd&#64;{debian.or.jp}),
おおつきようすけ (y.otsuki30&#64;{gmail.com})までお願いいたします。</p>
