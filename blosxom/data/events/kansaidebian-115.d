第 115 回 関西 Debian 勉強会

<p>
「<a href="https://wiki.debian.org/KansaiDebianMeeting/20161023">第115回 関西 Debian 勉強会</a>」
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで
Debian のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
    <li>日時： 2016 年 10 月 23 日 (日) 13:30 - 17:00 (開場 13:00)</li>
    <li>会場：<a href="http://www.city.osaka.lg.jp/shimin/page/0000016613.html">港区民センター 楓</a>
(定員：20名)</li>
  </ul>
</dd>

<dt>内容</dt>
<dd>
  <ol>

  <li><strong>Intro</strong><br>
  この勉強会の簡単な説明をします。
  </li>

  <li><strong>sbuild と debci を触ってみた．</strong>(担当：佐々木 洋平)<br>
  Debian パッケージを作成する際に「ビルド時の依存関係を満たしているのか」だけでは
  なく「そのパッケージの更新による他のパッケージへの影響/不具合はどうなのか」とい
  う懸念が残る。今回の発表では、pkg-ruby-extra team で導入している sbuild + debci
  という環境を例に、それらの懸念をどの様に解消しているのかについて簡単にまとめる
  予定である(丁度使わざるを得ない状況に嵌ったので、文書としてまとめてみました。と
  も言う)。
  </li>

  </ol>
</dd>

<dt>参加方法と注意事項</dt>
<dd>

<a href="http://debianjp.connpass.com/event/40806/">connpass</a>を参照して、
事前登録をしてください。事情によりconnpassを使えない/登録できない方は担当者まで連絡してください
(締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。)。
</dd>

</dl>

<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：かわだてつたろう (t3rkwd&#64;{debian.or.jp}),
おおつきようすけ (y.otsuki30&#64;{gmail.com})までお願いいたします。</p>
