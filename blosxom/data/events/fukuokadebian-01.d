第1回 福岡 Debian 勉強会のお知らせ

<p>
Debian 勉強会は、Debian ユーザ／ユーザ予備群／開発者らで Debian のさまざまなトピックについて 
Face to Face で楽しく話し合っていく集まりです。<br>
福岡でも、九州人の九州人による九州人の為の福岡 Debian 勉強会を開催致します。</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
<ul>
<li>日時：2012年7月28日（土）17:00〜20:00</li>
<li>場所：<a href="http://www.guildcafe.jp/%E3%82%A2%E3%82%AF%E3%82%BB%E3%82%B9-%E5%96%B6%E6%A5%AD%E6%99%82%E9%96%93/">GuildCafe Costa</a></li>
<li>費用：1,000円</li>
</ul>
</dd>

<dt>内容</dt>
<dd>
<ul>
<li>Debian に関係する発表: 4人程度を予定</li>
<li>今後の福岡Debian勉強会について、ブレインストーミング</li>
</ul>
アジェンダ大雑把ですが、是非お誘い合わせの上、参加ください！
</dd>
</dl>

<dt>参加方法と注意事項</dt>
<dd>
<a href="http://www.zusaar.com/event/344003">zusaarのエントリ</a>を参照して事前登録をしてください。
</dd>
</dl>
<p>
この件に関するお問い合わせは、主催者の小室さん (ayakomuro _at_ gmail.com) までお願いいたします。</p>
