第77回 関西 Debian 勉強会のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting/20131027">第77回 関西 Debian 勉強会</a>」
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
    <li>日時： 2013 年 10 月 27 日 (日) 13:30 - 17:00</li>
    <li>会場：<a href="http://www.city.osaka.lg.jp/shimin/page/0000016613.html">港区区民センター 梅</a>
(定員：30名)</li>
    <li>費用：500円 (部屋代、印刷代などの諸費用のため)</li>
    <li>事前課題: <br>
      <ol>

      <li>
      パッケージlibasound2-devをインストールし、Wiki ページに添付の pcm_minimal.c をコンパイルして実行ファイルを作成してください。実行ファイルを実行した結果を教えてください。
      </li>

      <li>
      git-buildpackage を install してきて下さい。
      </li>

      <li>
      Debian Developer/Maintainer の方は, 御自身がメンテされているパッケージをどの VCS で管理されているかお答え下さい。<br>
      パッケージをメンテされていない方は、<a href="http://anonscm.debian.org/gitweb">http://anonscm.debian.org/gitweb</a>
      以下にあるパッケージを眺めて触ってみたいパッケージを決めておいて下さい。
      </li>

      </ol>
    </li>
  </ul>
</dd>

<dt>内容</dt>
<dd>
  <ol>

  <li><strong>Intro</strong><br>
  この勉強会の簡単な説明をします。
  </li>

  <li><strong>「ALSAのユーザーランド解説」</strong> (担当：坂本)</li>

  <li><strong>「git-buildpackage 入門 again」</strong> (担当：佐々木)</li>

  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>

<strong>2013 年 10 月 26 日 (土) 23:59 までに</strong>
<a href="http://debianmeeting.appspot.com/event?eventid=f1c8b6f36d400c7d8dd1ae04797a6272eadd7cd2">Debian勉強会予約管理システム</a>を参照して、
事前登録をしてください。登録にはGoogleアカウントが必要になりますが、事情によりGoogleアカウントを使えない/登録できない方は担当者まで連絡してください。
(締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。
懇親会 (予定) に参加する場合は、二次会参加の欄にチェックしてください。)
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：かわだてつたろう (t3rkwd&#64;{debian.or.jp} ) までお願いいたします。
</p>
