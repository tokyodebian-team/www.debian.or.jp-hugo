第55回 関西 Debian 勉強会＠温泉合宿のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting20120129">第55回 関西 Debian 勉強会＠温泉合宿</a>」
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
    <li>日時： 2012 年 1 月 28 日 (土) 14:00 - 29 日 (日) 11:00</li>
    <li>会場：湯の花温泉 <a href="http://www.keizankaku.com">渓山閣</a>
(定員：若干名)</li>
    <li>費用：10,500円 (宿泊費用)</li>
    <li>事前課題: <br>
      <ol>
      <li>取り組む課題を考えてきてください。</li>
      </ol>
  </ul>
</dd>

<dt>内容</dt>
<dd>
  <ol>
  <li><strong>Debian 開発合宿</strong><br>
ゆっくり温泉につかりながらじっくり Debian の開発に取り組みませんか。普段なかなか突っ込めない Debian についてじっくり語り合いましょう。</li>
  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
<strong>2012 年 1 月 14 日 (土) 23:59 までに</strong>
<a href="http://debianmeeting.appspot.com/event?eventid=5d8f67d99585b30c399df4dd42b422fc97200c86">Debian勉強会予約管理システム</a>を参照して、
事前登録をしてください。登録にはGoogleアカウントが必要になりますが、事情によりGoogleアカウントを使えない/登録できない方は担当者まで連絡してください。
（締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。）
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：かわだてつたろう (t3rkwd&#64;{debian.or.jp} ) までお願いいたします。</p>
