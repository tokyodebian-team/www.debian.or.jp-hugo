第 90 回 関西 Debian 勉強会 in 関西オープンソース2014

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting/20141108">第90回 関西 Debian 勉強会@関西オープンソース2014
 </a>」 
のお知らせです。
関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで Debian GNU/Linux のさまざまな
トピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：11月8日(土) 10:00〜18:00
  </li>
  <li>会場：大阪南港ATC (行き方は<a href="http://k-of.jp/2014/access.html">こちら</a>) <br>
 (大阪市交通局 ニュートラム 南港ポートタウン線 トレードセンター前駅より徒歩5分)</li>
  <li>主催：関西オープンフォーラム</li>
  <li>費用：セミナー／ミニセミナーは無料。その他有志による物品の販売を予定</li>
  </ul>
</dd>

<dt><a href="https://k-of.jp/2014/session/583">セミナー<br>
<strong>「Debian 8 "jessie" frozen」</strong><br>
（会場:10F-ショーケース2　11月8日(土) 13:00-13:50)</a></dt>
<dd>
<ul>
<li>
11月5日にフリーズ(予定)の次期リリース Debian 8.0 (コードネーム Jessie) の話題を中心に、最近のDebian Projectのトピックを紹介します。
</li>
<li>
担当：佐々木洋平(Debian JP Project)</li>
</ul>
</dd>

<dt>ブース展示・配布物</dt>
<dd>
  <ol>
  <li><strong>Debian 稼働マシン展示と Debian 体験コーナー</strong>
  <li><strong>有志作成 Debian グッズ（Tシャツ）</strong></li>
  <li><strong>Debian 同人誌「あんどきゅめんてっどでびあん」</strong></li>
  <li><strong>Debian DVD 配布</strong></li>
  </ol>
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会担当：かわだてつたろう (t3rkwd&#64;{debian.or.jp} ) までお願いいたします。</p>
