オープンソースカンファレンス 2015 Niigata 参加 (9/5) のお知らせ

<p>新潟あるいはその近くにお住まいのみなさまにお知らせです。9月5日(土) に新潟市にて開催される「<a href="http://www.ospn.jp/osc2015-niigata/">オープンソースカンファレンス 2015 Niigata</a>」に Debian JP Project が出展します。</p>

<p>
普段、継続的に勉強会を開催している東京／関西地区と違い、新潟地区のユーザー／コントリビューター／開発者が face to face で交流できる貴重な機会です。この機会に、是非参加していただければと思います。
</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
<ul>
  <li>日程: 2015 年 9 月 5 日 ( 土 ) 10:30-17:30 (展示は 16:15 まで)</li>
  <li>会場: <a href="http://www.niigatacitylib.jp/?page_id=186"> ほんぽーと新潟市立中央図書館 </a> 3F 多目的ホール (新潟駅 徒歩約 10 分)</li>
  <li>参加費: 無料</li>
  <li>Twitter: <a href="https://twitter.com/OSC_official">@OSC_official</a>／ ハッシュタグ <a href="https://twitter.com/hashtag/osc15ni?src=hash">#osc15ni</a>
</ul>
</dd>

<dt>セミナー発表</dt>
<dd>
<ul>
<li>タイトル：<a href="https://www.ospn.jp/osc2015-niigata/modules/eguide/event.php?eid=18"><strong>Debian update</strong></a>
<li>時間：16 時 30 分〜16 時 50 分
<li>内容：4 月にリリースされた Debian 8.0 (コードネームJessie) の話題を中心に、最近の Debian Project のトピックを紹介します。<br>
<li>対象：Debian に興味がある人、 Debian を知っている人
<li>講師：野島 貴英 ( 東京エリア Debian 勉強会 )
</ul>
</dd>

<dt>ブース展示内容</dt>
<dd>
<ul>
  <li>Debian "Jessie" 稼働マシンの展示</li>
  <li>東京エリア/関西 Debian 勉強会の紹介</li>
<!--
　<li>コードネーム関連グッズ展示</li>
  <li>「Debian極小ステッカー」配布</li>
-->
</ul>
</dd>
</dl>

<p>この件に関するお問い合わせは 野島 貴英 nozzy at {debian.or.jp} までお願いいたします。</p>
