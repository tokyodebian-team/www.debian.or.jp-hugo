FSIJ の月例会での Debconf2007 報告会のお知らせ

<p>
<a href="http://www.debconf.org/">Debian Conference</a>
 は世界中の Debian Developer を集めて年に一度開催さ
れているもので、今年は6月17-23日にスコットランドで開催されました。
7月11日に開催の SEA &amp; FSIJ 合同フォーラムにて報告しました。
</p>

<p>
国際化・品質管理や組込みなど日本から参加したメンバーの興味のある領域を
中心として、今回の会期でどういう議題が討論され、どういう成果が出たのか
を報告しました。
</p>

<p>
<a href="http://www.fsij.org/index.cgi/wiki/MonthlyMeeting2007Jul">
詳細は FSIJ のサイトをご覧ください。
</a>
</p>
<dt>開催日時・会場</dt>
<dd>
  <li>日時：2007年7月11日（水）18:30-20:30
  <li>会場：産総研秋葉原サイト 11F 会議室
  <li>定員：30 名
  <li>参加費：なし
  <li>話者：岩松 信洋、上川 純一
</dd>
<dt>発表資料</dt>
<dd><a href="https://tokyodebian-team.pages.debian.net/pdf2007/debianmeetingresume200707-presentation-fsij.pdf">
プレゼンテーション（PDF 1MB)
    </a></dd>
</dl>
