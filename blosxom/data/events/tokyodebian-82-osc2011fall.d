第82回 Debian 勉強会 in オープンソースカンファレンス 2011 Tokyo/fall

<p>
Debian 勉強会とは、 Debian の開発者になれることをひそかに夢見るユーザたちと、
ある時にはそれを優しく手助けをし、またある時には厳しく叱咤激励する Debian 
開発者らが Face to Faceで Debian GNU/Linux のさまざまなトピック
（新しいパッケージ、Debian 特有の機能の仕組について、Debian 界隈で起こった出来事、etc）
について語り合うイベントです。</p>
<p>
この 11 月は、
<a href="http://www.ospn.jp/osc2011-fall/">オープンソースカンファレンス 2011 Tokyo/fall</a>
に参加してセミナーの時間を使ったトークやブースでの展示と説明、そしてGPGキーサインパーティなどを予定しています。
普段は聞けないような様々な情報を交換しあうチャンスです。是非御参加下さい。</p>


<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日程：2010年11月19日(土)<br>
  <li>会場：<a href="http://www.meisei-u.ac.jp/access/hino.html">明星大学 日野キャンパス 28号館</a>
（多摩モノレール 「中央大学・明星大学駅」から大学まで直結。会場まで徒歩5分）</li>
  <li>参加費用：無料<br></ul></dd>

<dt><a href="https://www.ospn.jp/osc2011-fall/modules/eguide/event.php?eid=27"><strong>GPGキーサインパーティおよびCAcertサイン</strong></a></dt>
<dd>講師：岩松信洋、山田 泰資 (Debian JP Project) 担当：GPGキーサイン運営チーム</dd>
<dd>対象：オープンソースソフトウェアの開発者、開発者になろうと志している人、GPGを使ったパッケージリポジトリを利用しているディストリビューションユーザ（例: Debian, Ubuntu, CentOS, Vine)
<dd>開始：10時00分〜10時45分</dd>
<dd>会場：28号館520教室 </dd>
<dd>
<p>
GPGキーサインについて: メールへの署名やファイルの暗号化、Debian パッケージへのサイン等などに使われる GnuPG 鍵について、
Debian 関係者が集まるイベントでは信頼の輪 (Web of Trust) を広げるために<a 
href="http://www.debian.org/events/keysigning">キーサインの交換がよく行われます</a>。
今回のオープンソースカンファレンス 2011 Tokyo/Fall も Debian 公式開発者をはじめとする関係者が集まる良い機会ですので、
希望する方と適宜キーサインの交換を行う予定です。</p>
<p>
GPGキーサインを希望される方は<a href="https://sites.google.com/site/kspjapanese/osc2011-tokyo-fall">keysignparty-ja のサイト</a>を確認の上、ブースへご来場ください。</p>
</dd>
<dd>
<p>
CAcertサインについて: CAcertサインパーティー（認証会 - Assurance event）は、cacert.orgの利用者が集まり、
身分証明＋宣誓契約を通じて信頼ポイント（Assurance Point）の獲得をする会です。
多くの信頼ポイントを集めると、高レベルの証明書発行や他者への信頼ポイントの付与ができるようになります。
この活動によってcacert.orgはフリー（無料＋自由）な認証局と証明書環境を構築し、
それを商用認証局並に世界的に普及させることを目指しています。</p>
<p>
参加される方は<a href="https://tokyodebian-team.pages.debian.net/pdf2010/debianmeetingresume201012.pdf">事前資料のCACertの章</a>を参照の上、
下記の準備を行ってご参加下さい：</p>
<ol>
<li>cacert.orgのアカウントを取得する
<li>身分証明が可能なパスポート等を用意する（現在、「署名を確認する」という要件が（欧米の風習由来で）残っているので、
    それが確認できることが望ましいです）
</ol>
</dd>
<dt><a href="https://www.ospn.jp/osc2011-fall/modules/eguide/event.php?eid=29">Debian topics update<strong></strong></a></dt>
<dd>講師：岩松信洋 (Debian JP Project)　担当：東京エリアDebian勉強会</dd>
<dd>対象：Debian の動向に興味のある方
<dd>開始：11時00分〜11時45分</dd>
<dd>会場：28号館520教室</dd>
<dd>内容：
<p>Debian の最新情報に関する情報についてお話しします。</p></dd>

<dt>ブース展示物</dt>
<dd>
  <ol>
  <li>Debian 関連書籍</li>
  <li>Debian 勉強会資料「あんどきゅめんてっどでびあん」「Deb専」</li>
  <li>Debian 稼働マシン</li>
</dd>

<p>
この件に関するお問い合わせは Debian 勉強会 担当者：山本 浩之(yamamoto&#64;debian.or.jp)
までお願いいたします。</p>
