第59回 関西 Debian 勉強会のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting/20120527">第59回 関西 Debian 勉強会</a>」
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
    <li>日時： 2012 年 5 月 27 日 (日) 13:30 - 17:00</li>
    <li>会場：<a href="http://www.city.osaka.lg.jp/shimin/page/0000016603.html">福島区民センター303号会議室</a>
(定員：30名)</li>
    <li>費用：500円 (部屋代、印刷代などの諸費用のため)</li>
    <li>事前課題: <br>
      <ol>
        <li>ITPをしたことがない方は、ITPしたいパッケージを探してみて概要を紹介してください。(<a href="http://www.debian.org/devel/wnpp/">WNPP</a>から探すのもよいでしょう)<br>
         ITPをしたことがある方は、最初のITPパッケージの概要とITPをするきっかけなどを語ってください。
        </li>
        <li>Essential なパッケージをひとつ挙げてパッケージのコントロールフィールドを確認し
          <ol>
            <li>アーカイブエリア</li>
            <li>アーカイブセクション</li>
            <li>パッケージ名</li>
            <li>バージョン</li>
            <li>メンテナ</li>
          </ol>
          をおしえてください。そのメンテナが個人かグループかも判定して下さい。
        </li>
      </ol>
    </li>
  </ul>
</dd>

<dt>内容</dt>
<dd>
  <ol>

  <li><strong>Intro</strong><br>
  この勉強会の簡単な説明をします。
  </li>

  <li><strong>Debian で作る LDAP サーバ(前編)</strong>(担当：佐々木)<br>
  DebianでLDAPサーバをたちあげてアカウント管理ができるようになるまで、についてお話します。<br>
  特に、現安定版SqueezeでのOpenLDAP関連パッケージの変更点(今更?)や他のOSとの連携での嵌り所などについて紹介する予定です。
  </li>

  <li><strong>ITP から始めるパッケージメンテナへの道</strong>(担当：よしだ)<br>
  ITP(Intent To Package)からmentors.debian.netへの登録まで
  </li>

  <li><strong>月刊 Debian Policy 「バイナリパッケージ」</strong>(担当：久保)</li>

  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
<strong>2012年 5 月 25 日 (金) 23:59 までに</strong>
<a href="http://debianmeeting.appspot.com/event?eventid=d39f9edfc6b60977e4f02fda1b9efbacacfc33fd">Debian勉強会予約管理システム</a>を参照して、
事前登録をしてください。登録にはGoogleアカウントが必要になりますが、事情によりGoogleアカウントを使えない/登録できない方は担当者まで連絡してください
（締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。
懇親会 (予定) に参加する場合は、二次会参加の欄にチェックしてください）。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：かわだてつたろう (t3rkwd&#64;{debian.or.jp} ) までお願いいたします。</p>
