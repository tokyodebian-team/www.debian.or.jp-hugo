第92回 Debian 勉強会 in オープンソースカンファレンス 2012 Tokyo/fall

<p>
Debian 勉強会とは、 Debian の開発者になれることをひそかに夢見るユーザたちと、
ある時にはそれを優しく手助けをし、またある時には厳しく叱咤激励する Debian 
開発者らが Face to Faceで Debian GNU/Linux のさまざまなトピック
（新しいパッケージ、Debian 特有の機能の仕組について、Debian 界隈で起こった出来事、etc）
について語り合うイベントです。</p>
<p>
この 9 月は、
<a href="http://www.ospn.jp/osc2012-fall/">オープンソースカンファレンス 2012 Tokyo/fall</a>
に参加を予定しています。普段は聞けないような様々な情報を交換しあうチャンスです。
また、今回は<a href="http://www.ospn.jp/osc2012-fall/">景品としてDebian勉強会作成同人誌もプレゼント予定</a>です。
是非御参加下さい。</p>


<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日程：2012年9月8日(土)<br>
  <li>会場：<a href="http://www.meisei-u.ac.jp/access/hino.html">明星大学 日野キャンパス 28号館</a>
（多摩モノレール 「中央大学・明星大学駅」から大学まで直結。会場まで徒歩5分）</li>
  <li>参加費用：無料<br></ul></dd>

<dt><a href="http://www.ospn.jp/osc2012-fall/modules/eguide/event.php?eid=85"><strong>次期安定版 Debian 7.0 "Wheezy" の紹介</strong></a></dt>
<dd>講師：岩松信洋 (Debian JP Project)　担当：東京エリアDebian勉強会</dd>
<dd>対象：Debian に興味、関心のある人</dd>
<dd>開始：11時00分〜11時45分</dd>
<dd>会場：28号館304教室</dd>
<dd>
<p>
今年、6月に Freeze された Debian の次期リリース候補 Debian7.0“Wheezy”の概要についてお話しします。</p>
</dd>

<dt>ブース展示物</dt>
<dd>
  <ol>
  <li>Debian 関連書籍</li>
  <li>Debian 勉強会資料「あんどきゅめんてっどでびあん」</li>
  <li>Debian "Wheezy" 稼働マシン</li>
  <li>Debian のインフォグラフィック(基礎知識／歴史／派生ディストリビューションの流れを俯瞰できます)</li>
</dd>

