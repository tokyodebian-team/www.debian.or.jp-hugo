Debian/Ubuntuユーザミートアップ in 札幌を開催します

<p>
2016年6月17日と18日に開催される<a href="https://www.ospn.jp/osc2016-do/">OSC北海道2016</a>に
<a href="http://www.debian.or.jp">Debian JP Project</a>、
<a href="https://tokyodebian.alioth.debian.org/">東京エリアDebian勉強会</a>/
<a href="https://wiki.debian.org/KansaiDebianMeeting">関西Debian勉強会</a>)、
<a href="http://ubuntulinux.jp/">Ubuntu Japanese Team</a>
が出展とセミナーを行います。OSCには日本在住のDebian・Ubuntu関係者が参加するのですが、
ぜひこの機会を利用して北海道在住のDebian/Ubuntu
ユーザと交流をしたいと思い、「Debian / Ubuntu ユーザーミートアップ in
札幌」と題したイベントを行うことにしました。Debian/Ubuntuを主題とした15分ほどのセミナー形式を数本行った後、参加者のみなさんと意見交換するミートアップタイムを行うというスケジュールとなっています。
北海道在住のDebian/Ubuntuユーザーの皆さん、Debian/Ubuntu に興味のある方々、参加お待ちしております。
</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日程：2016年6月17日(土) 19:00-20:50<br>
  <li>会場：<a href="http://www.sapporo-shiminhall.org/">わくわくホリデーホール（札幌市民ホール)第2会議室</a></li>
  <li>参加費用：無料
</dd>

<dt>参加方法と内容</dt>
<dd>参加人数を把握したいので、
<a href=" https://debianjp.doorkeeper.jp/events/46366">Debian / Ubuntu ユーザーミートアップ in 札幌 イベントサイト</a>
から登録お願いします。
内容も同サイトからお願いいたします。
</dd>
</dl>

<p>
この件に関するお問い合わせは 東京エリアDebian 勉強会 担当：岩松 信洋 (iwamatsu &#64; {debian.or.jp} )
までお願いいたします。
</p>
