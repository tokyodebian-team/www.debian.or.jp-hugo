第61回 関西 Debian 勉強会のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting/20120722">第61回 関西 Debian 勉強会</a>」
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
    <li>日時： 2012 年 7 月 22 日 (日) 13:30 - 17:00</li>
    <li>会場：<a href="http://www.city.osaka.lg.jp/shimin/page/0000016603.html">福島区民センター301号会議室</a>
(定員：30名)</li>
    <li>費用：500円 (部屋代、印刷代などの諸費用のため)</li>
    <li>事前課題: <br>
      <ol>
      <li>
      Linux(Unix?)における login 時のユーザ認証の流れについて調べて(復習して)おいて下さい。<br>
      大統一 Debian 勉強会の西山さんの発表資料などを参考にしてください。
      </li>

      <li>
      man 5 ldif などを参考に ldif ファイル形式のフォーマットを説明してください。<br>
      何か一つでかまいませんのでディレクティブ、エントリの扱い方などの具体例をあげて説明してください。<br>
      ldif.5.gz は ldap-utils パッケージで提供されています。
      </li>
      </ol>
    </li>
  </ul>
</dd>

<dt>内容</dt>
<dd>
  <ol>

  <li><strong>Intro</strong><br>
  この勉強会の簡単な説明をします。
  </li>

  <li><strong>Debian で作る LDAP サーバ</strong>(担当：佐々木さん)<br>
  DebianでLDAPサーバをたちあげてアカウント管理ができるようになるまで、についてお話します。<br>
  特に、現安定版SqueezeでのOpenLDAP関連パッケージの変更点(今更?)や他のOSとの連携での嵌り所などについて紹介する予定です。
  </li>

  <li><strong>月刊 Debian Policy 「ソースパッケージ」</strong>(担当：甲斐さん)</li>

  <li><strong>大統一 Debian 勉強会の報告</strong>(担当：関西 Debian 勉強会)</li>

  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
<strong>2012年 7 月 21 日 (土) 11:59 までに</strong>
<a href="http://debianmeeting.appspot.com/event?eventid=1a493a0a9d8dd096049ce831ec4b381bac47c532">Debian勉強会予約管理システム</a>を参照して、
事前登録をしてください。登録にはGoogleアカウントが必要になりますが、事情によりGoogleアカウントを使えない/登録できない方は担当者まで連絡してください
（締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。
懇親会 (予定) に参加する場合は、二次会参加の欄にチェックしてください）。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：かわだてつたろう (t3rkwd&#64;{debian.or.jp} ) までお願いいたします。</p>
