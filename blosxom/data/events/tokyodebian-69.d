第69回 東京エリア Debian 勉強会＠niftyのお知らせ

<p>
Debian 勉強会とは、Debian の開発者になれることをひそかに夢見るユーザたちと、ある時にはそれを優しく手助けをし、またある時
には厳しく叱咤激励する Debian 開発者らが Face to Face で Debian のさまざまなトピック（新しいパッケージ、Debian 特有の機
能の仕組について、Debian 界隈で起こった出来事、etc）について語り合うイベントです。</p>
<p>
参加される方は主に東京を中心に関東近郊の国籍・性別不問の Debian ユーザです (Debian をまだ使ったことが無いが興味があるの
で…とか、かなり遠い所から来てくださる方もいます)。開発の最前線にいるDebian の公式開発者や開発者予備軍の方も居ますので、
普段は聞けないような様々な情報を得るチャンスです。興味と時間があった方、是非御参加下さい。（また、勉強会で話をしてみたい
という方も随時募集しています）。</p>

<p>
今回はNiftyセミナールームをお借りしての開催となります（ご調整頂きましたhttp://twitter.com/nifty_engineer 様に御礼申し上げます）。
</p>

<dl>
<dt>開催日時・参加費用</dt>
<dd>
<ul>
   <li>2010年10月16日（土）14:30 〜 </li>
   <li>費用：500円</li>
</ul>

<dt>会場</dt>
<dd>
 Niftyセミナールーム（東京都品川区南大井6-26-1大森ベルポートA館）<br>
(<a href="https://tokyodebian-team.pages.debian.net/2010-10.html">こちらの案内図</a>を参考に来訪ください)</dd>


<dt>内容のアジェンダ案（流動的です）</dt>
<dd>
<ul>
<li>OSC2010 Tokyo/Fall 参加報告</li>
<li>Japan LinuxCon 参加報告</li>
<li>DebConf10 参加報告</li>
<li>俺のDebianな一日</li>
<li>Mini Debconf日本開催について計画する</li>
</ul>
</dd>

<dt>参加申し込み方法</dt>
<dd>
<a href="https://tokyodebian-team.pages.debian.net/2010-10.html">勉強会のページ</a>を参照して、<strong>10/14 までに Debian 
勉強会予約システムへの登録をしてください。
その際、事前課題がありますのでご回答をお願いします。</strong></dd>

<dt>事前課題</dt>
<dd>
「俺のDebianな一日」というタイトルでDebian利用環境をどのように利用しているのか詳細に語ってください。</dd>
<dd>
回答は Debian 勉強会予約システムの課題回答フォームに入力してください。提出いただいた回答は全員に配布すること、
また内容の再利用を実施することを了承ください。 Debian 勉強会資料は GPL で公開します (Web での公開が可能な内容でお願いします）。
</dd>
<dd>
課題内容の実施についてなにか質問があれば、Debianユーザ用公開メーリングリスト (debian-users@debian.or.jp) にてお願いします。</dd>
</dl>

<p>
この件に関するお問い合わせは Debian 勉強会主催者：上川 (dancer@{debian.org} ) までお願いいたします。</p>

