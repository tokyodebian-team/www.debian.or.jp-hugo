第27回 Debian 勉強会のお知らせ

<p>
関西方面でも活発に動きがあるようですが、東京でも
<a href=" https://tokyodebian-team.pages.debian.net/">「Debian 勉強会」</a>
が開かれます。
ちなみに Debian 勉強会とは月に一回、顔をつき合わせて Debian GNU/Linux のさまざまなトピック
（新しいパッケージ、Debian 特有の機能の仕組について、Debian 界隈で起こった出来事、etc）
について話し合う会です。</p>
<p>
今回は特にパッチ管理と SCM （Software Configuration Management System、ソフトウェア構成管理システム）
を中心にした話を聞くことができます。Debian に限らず開発ツールに興味がある人にオススメです。</p>
<p>
参加者には開発の最前線にいる Debian の公式開発者や開発者予備軍の方も居ますので、
普段は聞けないような様々な情報を得るチャンスです。<br>
興味を持たれた方は是非御参加下さい（また、勉強会で話をしてみたいという方も随時募集しています）。</p>


<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：2007年4月21日(土) 18:00-21:00
  <li>会場：<a href=
"http://www2.city.suginami.tokyo.jp/map/detail.asp?home=H00150">あんさんぶる荻窪</a>　(JR荻窪駅すぐ近く)
  <li>費用：資料代・会場費として 500 円を予定</ul></dd>
<dt>内容</dt>
<dd>
  <ol>
  <li><strong> パッチ管理ツール quilt の使い方</strong>（担当：小林儀匡）<br>
quilt というパッチ管理ツールの紹介をします。
Debian パッケージメンテナは、開発元のソースコードに問題修正用のパッチをいくつも当てたり、
新しいソースコードが出たときにパッチを取捨選択し当て直したりしなければなりません。
このような作業はパッチ管理ツールなしでは不可能な芸当です。<br>
今回紹介するのは Linux Kernel などで活用されている quilt です。
quilt は Debian パッケージメンテナンス専用のツールというわけではなく、
パッチ管理を必要とする人なら誰にでもお勧めできるツールです。

  <li><strong>darcs を使ってみよう</strong>（担当：David Smith）<br>
分散 SCMのツールがたくさんありすぎて何つかったらよいのかわからない方、そんなあなたに朗報です。<br>
darcs の素晴らしさについて、
そして darcs をつかって Debian パッケージをメンテナンスする方法について伝授します。

  <li><strong>git で Debian パッケージ</strong>（担当：上川純一）<br>
Linux kernel のソースコードを管理している分散SCMツールとして有名な git。
その git を普段利用している上川が Debian パッケージを git で管理する際の実際についてお伝えします。</ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
参加希望者は事前登録の上、事前課題の解答を勉強会用メーリングリストに送付してください。
期限は 4 月 19 日中です。<br>
<strong>事前登録、及び事前課題の内容と送付先については 
<a href=
"https://tokyodebian-team.pages.debian.net/2007-04.html">Debian 勉強会の Web ページ
</a>を参照下さい</strong>。
</dd>
</dl>
<p>
この件に関するお問い合わせは Debian 勉強会主催者：上川純一 (dancer&#64;{debian.org,netfort.gr.jp} )
までお願いいたします。</p>
