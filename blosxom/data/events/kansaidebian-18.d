第18回 関西 Debian 勉強会のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting20081019">第18回 関西 Debian 勉強会</a>」 
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで 
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：2008 年 10 月 19 日 (日) 13:45 - 17:00 (13:30 より受付)</li>
  <li>会場：<a href="http://www.city.osaka.jp/shimin/shisetu/01/fukushima.html">大阪 福島区民センター 会議室</a>
(定員：25名)</li>
  <li>費用：500円 (部屋代、印刷代などの諸費用のため)</li>
  </ul>
</dd>
<dt>内容</dt>
<dd>
  <ol>
  <li><strong>Intro</strong> 山下 尊也 氏<br>
この勉強会の目的などを説明し、その後、第18回関西 Debian 勉強会の内容について話をします。</li>

  <li><strong>Debian パッケージの安定入手〜cdn.debian.or.jp ならびに cdn.debian.net における取り組み</strong>　荒木 靖宏氏<br>
現在、インターネットではいつでも必要なソフトウェアやコンテンツを安価に入手する手段として CDN (Contents Delivery Network) が広く使われている。<br>
Debian はシステムを構成しているパッケージが安定的に入手可能かどうかの手段の有無が自身の信頼性を左右するシステムであり、その特殊性を考慮した CDN システムが必要となる。<br>
今回はその対策としての cdn.debian.or.jp ならびに cdn.debian.net における取り組みを紹介する。</li>

 
  <li><strong>はじめての CDBS</strong>　佐々木 洋平氏<br>
「CDBS -- Common Debian Build System」は、 debian/rules 内の debhelper 
の呼び出しを抽象化することで、 Debian パッケージの作成・管理をより簡単にするツールです。
今回は CDBS の使い方を実例を元に解説します。</li>

  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
<strong>10 月 18 日 (土) 12:00 までに</strong>
<a href="http://cotocoto.jp/event/28765">京都.cotocoto</a>を参照して、
事前登録をしてください
（締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。
懇親会に参加する場合は、コメントに「懇親会参加希望」と記入してください）。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：山下尊也 (takaya&#64;{debian.or.jp} ) までお願いいたします。</p>


