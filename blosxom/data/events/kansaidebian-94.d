第 94 回 関西 Debian 勉強会のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting/20150125">第94回 関西 Debian 勉強会</a>」
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
    <li>日時： 2014 年 1 月 25 日 (日) 13:30 - 17:00 (開場 13:00)</li>
    <li>会場：<a href="http://www.city.osaka.lg.jp/shimin/page/0000016613.html">港区区民センター 楓</a>
(定員：30名)</li>
    <li>費用：500円 (部屋代、印刷代などの諸費用のため)</li>
    <li>事前課題: <br>
      <ol>

      <li>
      以下の環境を用意してきて下さい。仮想環境でもかまいません。
        <ul>
          <li>
            unstable が動作している環境
            <ul><li>pbuilder or cowbuilder が動作する環境</li></ul>
          </li>
          <li>
            デバッグ用の資料、機材など。
            <ul><li>過去の勉強会資料の PDF が参考になるでしょう。</li></ul>
          </li>
        </ul>
      </li>

      <li>
      <a href="https://bugs.debian.org/release-critical/debian/all.html">https://bugs.debian.org/release-critical/debian/all.html</a>を眺めて、気になるパッケージが無いか確認して下さい。
      </li>

      </ol>
    </li>
  </ul>
</dd>

<dt>内容</dt>
<dd>
  <ol>

  <li><strong>Intro</strong><br>
  この勉強会の簡単な説明をします。
  </li>

  <li><strong>Debian の Bug の眺め方</strong>(担当：佐々木洋平)<br>
    Debian の開発は bug 報告から始まります。
    とはいえ、単に「bug 報告」と言われても「何のことだかさっぱりわからん」みたいな意見もあろうかと思います。
    ここでは，Debian の bug report の見方と、bug 報告の仕方について簡単にまとめてみます。
  </li>

  <li><strong>Bug Squash Party</strong><br>
    Jessie の RC バグを眺めて、可能であれば修正を試みてみましょう。
  </li>

  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
  <a href="http://debianjp.doorkeeper.jp/events/19907">Doorkeeper</a>を参照して、
  事前登録をしてください。事情によりDoorkeeperを使えない/登録できない方は担当者まで連絡してください
  (締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。)。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：佐々木洋平 (uwabami&#64;{debian.or.jp} ) までお願いいたします。</p>
