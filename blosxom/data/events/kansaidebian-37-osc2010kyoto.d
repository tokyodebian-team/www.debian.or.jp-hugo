第37回 関西 Debian 勉強会 @OSC2010 Kyoto のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting20100627">第37回 関西 Debian 勉強会@OSC2010 Kyoto </a>」 
のお知らせです。
関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
  <li>日時：日時：2010 年 7 月 9 日 (金) / 2010 年 7 月 10 日 (土) 10:00〜18:00 </li>

  <li>会場：京都コンピュータ学院 京都駅前校　
     <a href="http://www.kcg.ac.jp/campus/campus/map/image/eki_map.gif">アクセス</a></li>
  <li>費用：無料</li>
  </ul>
</dd>

<dt>ブース展示内容</dt>
<dd>
  <ul>
  <li>Debian 稼働マシンと勉強会資料の展示</li>
  <li>Debian Live DVDの配布</li>
  <li>Debian 同人誌「あんどきゅめんてっどでびあん」の販売(残部少)</li>
  <li>Debian Tシャツの販売</li>
</dd>
<dt> セッション</dt>
<dd>
  <ul>
    <li><strong>野良ビルドから始めるDebianパッケージ作成</strong>(担当: 佐々木 洋平(Debian JP Project)</li>
    <li> 日時 2010 年 7 月 10 日 (土) 14:00〜14:45 (45 min)</li>
    <li>場所: 京都コンピュータ学院 509教室</li>
    <li>内容: 次期安定版Squeezeのリリースはまだ先になりそうです。
    Debianパッケージは古いけど新しいパッケージも使ってみたいな」
    そんな安定版を使っている人のために、その場で試すこともできるLiveDVDを使
    いながら、Debianパッケージ利用のコツからDebian Sidのソースパッケージを
    使って安定版用パッケージの作成までを解説します。</li>
  </ul>
</dd>

</dd>
<dt>参加方法と注意事項</dt>
<dd>
ブース、セッションともに入場無料です。
セッションについては、<a href="http://www.ospn.jp/osc2010-kyoto/modules/eguide/event.php?eid=14">オープンソースカンファレンスのページ</a>よりお申し込みください。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：佐々木 洋平 (uwabami&#64;{debian.or.jp} ) までお願いいたします。</p>
