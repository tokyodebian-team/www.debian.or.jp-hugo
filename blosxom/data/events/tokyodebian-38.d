第38回 Debian 勉強会のお知らせ

<p>
3 月は、なんと 2 回に渡って Debian 勉強会が開かれます！
今回は Google にお邪魔しての開催となります。</p>
<p>
Debian 勉強会とは、Debian 
の開発者になれることをひそかに夢見るユーザたちと、
ある時にはそれを優しく手助けをし、またある時には厳しく叱咤激励する Debian 
開発者らが Face to Face で Debian GNU/Linux のさまざまなトピック
（新しいパッケージ、Debian 特有の機能の仕組について、Debian 界隈で起こった出来事、etc）
について語り合うイベントです。</p>
<p>
参加される方は主に東京を中心に関東近郊の国籍・性別不問の Debian ユーザです 
(Debian をまだ使ったことが無いが興味があるので…とか、かなり遠い所から来てくださる方もいます)。
開発の最前線にいるDebian の公式開発者や開発者予備軍の方も居ますので、
普段は聞けないような様々な情報を得るチャンスです。
お時間が合えば是非御参加下さい。
（また、勉強会で話をしてみたいという方も随時募集しています）。</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：2008年3月15日(土) 14:00-18:00</li>
  <li>費用：資料代として 500 円以内を予定</li>
  <li>会場：<a href="http://maps.google.co.jp/maps?f=q&hl=ja&geocode=&q=%E3%82%BB%E3%83%AB%E3%83%AA%E3%82%A2%E3%83%B3%E3%82%BF%E3%83%AF%E3%83%BC&sll=36.5626,136.362305&sspn=24.230398,46.40625&ie=UTF8&z=16">Google 東京オフィス</a> 
（セルリアンタワー<b>オフィス棟</b>内　7F） 。
入館方法の詳細については
<a href=
"https://tokyodebian-team.pages.debian.net/2008-03.html">勉強会の Web ページを参照ください。
</a><br>
<iframe width="640" height="480" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.co.jp/maps/ms?f=q&amp;hl=ja&amp;geocode=&amp;num=10&amp;ie=UTF8&amp;s=AARTsJonWwNsQ3CAiYRdYL237PoPaZUwRA&amp;msa=0&amp;msid=109811903404931563048.000445ad0dcfd0fd0614a&amp;ll=35.660853,139.700003&amp;spn=0.016737,0.027466&amp;z=15&amp;iwloc=000447b1e2b77800bf4b9&amp;output=embed"></iframe><br /><small><a href="http://maps.google.co.jp/maps/ms?f=q&amp;hl=ja&amp;geocode=&amp;num=10&amp;ie=UTF8&amp;msa=0&amp;msid=109811903404931563048.000445ad0dcfd0fd0614a&amp;ll=35.660853,139.700003&amp;spn=0.016737,0.027466&amp;z=15&amp;iwloc=000447b1e2b77800bf4b9&amp;source=embed" style="color:#0000FF;text-align:left">大きな地図で見る</a></small></li>
  </ul>
</dd>
<dt>内容</dt>
<dd>
  <ol>
  <li><strong>Open Source Conference 展示報告</strong>（担当：やまねひでき）<br>
  
	3月1日に開催された Open Source Conference に東京エリアDebian勉強
	会も出展しました。東京エリアDebian勉強会の参加者のために、当日
	の参加の模様および準備の裏話をおつたえします。 </li>

  <li><strong> Debian パッケージ論1: データだけのパッケージ、ライセンスの考え方</strong>（担当：David Smith・上川純一）<br>
	
	2008年の勉強会では連続企画としてパッケージの作成方法について検
	討していきます。今回はデータだけのパッケージの取扱いとライセン
	スの考え方をご紹介します。</li>

  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
参加希望者は事前登録の上、事前課題の回答を勉強会用メーリングリストに送付してください。
期限は 3 月 13 日中です。<br>
<strong>事前登録、及び事前課題の内容と送付先については 
<a href=
"https://tokyodebian-team.pages.debian.net/2008-03.html">Debian 勉強会の Web ページ
</a>を参照下さい。</strong>
</dd>
</dl>
<p>
この件に関するお問い合わせは Debian 勉強会主催者：上川純一 (dancer&#64;{debian.org,netfort.gr.jp} )
までお願いいたします。</p>
