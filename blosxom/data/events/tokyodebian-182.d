2020 年 1 月度 東京エリア Debian 勉強会のお知らせ

<p>
東京近辺にいらっしゃる皆様こんにちは。今月も東京エリア Debian 勉強会を開催いたします！
</p>

<p>
東京エリア Debian 勉強会とは、Debian Project に関わる事を目的とする
熱いユーザたちと、実際に Debian Project にてすでに日夜活動している
人らが Face to Face で Debian GNU/Linux のさまざまなトピック （パッケージ、
Debian 特有 の機能の仕組について、Debian 界隈で起こった出来事、
etc ） について語り合う場です。当然ですが、Debian Project に何らかの貢献
をしたいという目的を持つ方であれば、自身で活動できるように手助けをする事
も主な趣旨の一つとしています。
</p>
<p>
参加される方は主に東京を中心に関東近郊の国籍・性別不問の Debian ユーザです。
Debian をこれから使ってみたい、Debianに興味がある方の参加も歓迎いたします。
</p>
<p>
また、開発の最前線にいる Debian の公式開発者や開発者予備軍の方も参加する場合がありますので、
普段は聞けないような様々な情報を得るチャンスです。
興味がありご都合の合う方はぜひともご参加下さい。
</p>
<p>
また、勉強会で話をしてみたいという方も随時募集しています。
</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：2020 年 1 月 18 日 ( 土曜日 ) 14:00-18:00 </li>
  <li>会場：サイバートラスト(株) 本社（<a href="https://www.cybertrust.co.jp/corporate/office.html">地図</a>）</li>
  <li>参加費：無料</li>
  </ul>
</dd>
<dt>内容</dt>
<dd>
  <ol>
  <li><strong>勉強会：月刊 Debian Policy（進行役：dictossさん）</strong><br>
  <li><a href="https://www.debian.org/doc/debian-policy/">Debian Policy</a> とは、Debian ディストリビューションのポリシー (方針)、すなわち Debian に要求されるいくつかの必要条件について書かれたドキュメントです。<br>Debian Policy は文章量が多くしばしば更新されるため、更新をチェックしたり、読み深めていく必要があります。<br>今回は「<a href="https://www.debian.org/doc/debian-policy/ch-source.html">4. Source packages</a>」 に書いてある内容を参加者のみなさんと一緒に学習してみようと思います。</li>
　<li><strong>Hack time (全員)</strong><br>
        各自で決めた課題をこなし、わからないことは参加者とFaceToFaceで相談して問題を解決してみてください。</li>
  <li><strong>成果発表 (全員)</strong><br>
        行った作業について簡単に発表いただきます。</li>
  </ol>
</dd>

<dt>事前課題／参加申し込み方法</dt>
<dd>
<a href="https://debianjp.connpass.com/event/161310/">勉強会のページ</a>を参照して、参加登録をしてください。</dd>
<dd>
<br>
<dd>
<strong>【注意点】会場の入館申請の都合で勉強会参加申し込みの期限は、イベント開催日の前日である 2020 年 1 月 17 日（金曜日） 13時までとなっております。</strong>
</dd>
<br>
事前課題は参加登録時に表示されるアンケートに入力してください。提出いただいた回答は全員に配布すること、また内容の再利用を実施することを了承ください。 Debian 勉強会資料は GPL で公開します ( Web での公開が可能な内容でお願いします）。
</dd>
<dd>
課題内容の実施についてなにか質問があれば、 Debian ユーザ用公開メーリングリスト ( debian-users@debian.or.jp ) にてお願いします。</dd>

</dl>

<p>
この勉強会の開催に関するお問い合わせは Debian 勉強会主催者：杉本 (dictoss@{debian.or.jp} ) までお願いいたします。</p>
