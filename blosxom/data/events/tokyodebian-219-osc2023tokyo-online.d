2023 年 3 月度 東京エリア・関西合同 Debian 勉強会 in オープンソースカンファレンス 2023 Online/Spring

<p>
日本にいらっしゃる皆様こんにちは。私たちは、<a href="https://event.ospn.jp/osc2023-online-spring/">オープンソースカンファレンス 2023 Online/Spring</a> に参加します。
</p>

<p>
イベントではセミナー発表を行います。
</p>

<p>
興味と時間がある方は是非イベントにご参加下さい。また、勉強会で話をしてみたいという方も随時募集しています。
</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日程：2023 年 3 月 11 日(土)</li>
  <li>会場：<a href="https://event.ospn.jp/osc2023-online-spring/">オンライン会場（Zoom＆YouTube Live）</a></li>
  <li>参加費用：無料</li>
  </ul>
</dd>

<dt>
<a href="https://event.ospn.jp/osc2023-online-spring/session/791416"><strong>Debian Update</strong></a>
</dt>
<dd>講師：杉本 典充（Debian JP Project 副会長)</dd>
<dd>対象：Debian をサーバ及びデスクトップで利用する人、Debian の情報収集を目的としている人</dd>
<dd>開始：14 時 00 分 〜 14 時 45 分</dd>
<dd>会場：D 会場</dd>
<dd>
<p>
現在フリーズ中の次期安定版リリース「Debian 12 bookworm」の話題を中心に最近のDebian Project 界隈のトピックを紹介します。
</p>
</dd>

<p>
この件に関するお問い合わせは 東京エリアDebian 勉強会 担当：杉本 典充 (dictoss &#64; {debian.or.jp} ) までお願いいたします。
</p>
