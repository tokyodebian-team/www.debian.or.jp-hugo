第13回 関西 Debian 勉強会のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting20080518">第13回 関西 Debian 勉強会</a>」 
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで 
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>
<p>
今月は、前回の勉強会でのインターネット中継の裏側などの話題を中心に行います。興味のある方はぜひご参加ください。</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：2008 年 5 月 18 日 (日) 13:45 - 17:00 (13:30 より受付)</li>
  <li>会場：<a herf="http://www.city.osaka.jp/shimin/shisetu/01/fukushima.html">大阪 福島区民センター 会議室</a>
(定員：25名)</li>
  <li>費用：500円 (部屋代、印刷代などの諸費用のため)</li>
  </ul>
</dd>
<dt>内容</dt>
<dd>
  <ol>
  <li><strong>Intro</strong> 山下 尊也 氏<br>
この勉強会の目的などを説明し、その後、第13回関西Debian勉強会の内容について話をします。</li>

  <li><strong>ustream.tvを使った関西Debian勉強会中継の裏側</strong>　野方 純 氏<br>
姫路で行われた第12回関西Debian勉強会は、Adobe Flash を使ったビデオストリーミング配信サービスの 
ustream.tv を利用し、<a href="http://www.ustream.tv/channel/kansaidebian">Debian上からインターネット中継</a>を行いました。
しかしそこに至るまでにはプロプライエタリソフトウェアの壁を越えなければいけませんでした…
ということで今回はDebianからustream.tv中継を行うためのノウハウとそこから見えた問題点についてお話しします。</li>

  <li><strong>Debianで始めるEmacsエディタ</strong>　木下 達也 氏<br>
GNU Emacsは、Lispでの拡張・カスタマイズ可能なテキストエディタです。GNUシステムの構成要素として1984年から開発が始まり、以降、現在も なお愛され続けているこの強力なテキストエディタについて、概要、インストール、活用法、Debianならではの仕組みなどをお話しします。</li>

  <li><strong>自宅LANでIPv6のルータを作ってみよう（Part1）</strong>　川江 浩 氏<br>
近頃、巷でよく聞く「IPアドレスの枯渇」。確かに、IPv4のアドレスは将来的になくなると思われます？じゃ、その代わりになる「IPv6」って何？本当に必要？使えるの？設定は？等々の疑問をルータの設定を通して、問題点をお話しします。</li>

  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
<strong>5月16日(金) 24:00までに</strong>
<a href="http://cotocoto.jp/event/26232">京都.cotocoto</a>を参照して、
事前登録をしてください
（締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。
懇親会に参加する場合は、コメントに「懇親会参加希望」と記入してください）。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：山下尊也 (takaya&#64;{debian.or.jp} ) までお願いいたします。</p>


