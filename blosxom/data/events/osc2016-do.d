Debian勉強会出張版＠オープンソースカンファレンス2016 Hokkaido

<p>
北海道にお住まいの皆様へ。6月18日に開かれる<a href="http://www.ospn.jp/osc2016-do/">オープンソースカンファレンス2016 Hokkaido</a>にて、
東京エリアDebian勉強会とDebian JP Project がセミナーとブース展示を行います。
北海道在住のユーザー、開発者が交流できる貴重な機会となります。皆さんのご参加お待ちしております。

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日程：2016年6月18日(土)<br>
  <li>会場：<a href="http://www.sora-scc.jp/index.html">札幌コンベンションセンター</a></li>
  <li>参加費用：無料<br></ul>
</dd>

<dt>
<a href="https://www.ospn.jp/osc2016-do/modules/eguide/event.php?eid=5"><strong>Debian Updates</strong></a>
</dt>
<dd>講師：岩松信洋 (Debian Project / Debian JP Project) 担当：Debian JP Project / 東京エリアDebian勉強会</dd>
<dd>対象：Debian に興味、関心のある人</dd>
<dd>開始：11時00分 〜</dd>
<dd>会場：place 7 </dd>
<dd>
<p>
次期リリース版であるDebian 9 (コードネーム Stretch）の話題を中心に最近のDebian Project 界隈のトピックを紹介します。
</p>
</dd>

<dt>ブース展示物</dt>
<dd>
  <ol>
  <li>Debian 勉強会資料「あんどきゅめんてっどでびあん」展示</li>
  <li>Debian 稼働マシン 展示</li>
  <li>Debian インストールCD、ステッカー等の配布</li>
  </ol>
</dd>
</dl>

<p>
この件に関するお問い合わせは 東京エリアDebian 勉強会 担当：岩松 信洋 (iwamatsu &#64; {debian.or.jp} ) までお願いいたします。
</p>
