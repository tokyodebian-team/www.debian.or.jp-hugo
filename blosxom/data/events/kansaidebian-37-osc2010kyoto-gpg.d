OSC 2010 Kansai&#64;Kyoto GPG キーサインパーティのご案内

<p>
OSC 2010 Kansai&#64;Kyoto の会場をお借りして, GPG キーサインパーティを行なわせて頂くことになりました.
</p>

<p>
キーサインパーティーとは, 互いの鍵に署名をすべく, PGP(GPG) 鍵を持つ人々が集まるものです. キーサインパーティーは PGP(GPG) 鍵を利用する上で非常に重要な概念である, 信頼の輪 (Web of Trust) を大規模に拡張するのに有用です. また, このようなキーサインパーティーは実際に面と向かって会う良い機会でもあります.
</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
OSC 2010 Kansai&#64;Kyoto の会場で, 昼休み時間にちょっと時間を頂いて行ないます.
  <ul>
  <li>日時：2010 年 7 月 10 日 (土) 12:10〜12:30 </li>
  <li>会場：京都コンピュータ学院 京都駅前校, 5 階 507 教室
     <a href="http://www.kcg.ac.jp/campus/campus/map/image/eki_map.gif">アクセス</a></li>
  <li>費用：無料</li>
  </ul>
</dd>
<dt>内容</dt>
<dd>
今回のキーサインパーティでは, 一斉に事務的に行なうのではなく相手と軽く会話しながら御互いの ID と GPG フィンガープリントを確認する方法(グループキーサイン)で行ないます. なるべく事前に<a href="http://doluc.org/ksp-osc2010-kyoto/ksp.cgi">登録サイト</a>より GPG 公開鍵の登録を行なって下さい.
</dd>

<dt>参加方法と注意事項</dt>
<dd>
<p>
グループキーサインを行なうためには事前準備が必要となります. お手数ですが<span style="color: #f00;">2010 年 7 月 7 日(水) 12:00(正午)</span>までに, キーサインに使用する自分の公開鍵を<a href="http://doluc.org/ksp-osc2010-kyoto/ksp.cgi">登録サイト</a>から登録して下さい.
</p>

<p>
鍵の登録後, 登録データに問題が無ければコーディネータが<a href="http://doluc.org/ksp-osc2010-kyoto/ack-list.html">参加者一覧</a>に登録し, 確認メッセージをメールで送信します. この参加者リストに乗った時点で登録完了とします. なお, 登録作業から 24 時間経過してもコーディネータからの連絡が無く,かつ参加者一覧にも掲載が無い場合, もしくはメッセージに書かれている情報が間違っている場合には, お手数ですが<a href="mailto:uwabami&#64;debian.or.jp">コーディネータ</a>までご連絡下さい.
</p>

<p>
GPG 鍵については, 利用されているハッシュのうち SHA1 の脆弱性が大きくクローズアップされています. このため, 今回のキーサインパーティでは, より強い暗号強度を持った GPG 鍵に限定します. <span style="color:#f00;">未だ 4096/RSA 以上の暗号強度の鍵を持っていない方は GPG 鍵を作成し, その鍵でキーサインパーティへ参加して下さい.</span>
</p>

<p>
グループキーサインへの登録へ間に合わず, 当日飛び込み参加の場合には
<ul>
  <li>御自身のお名前(本人確認ができる身分証と合致する名前)</li>
  <li>GPG 鍵の ID</li>
  <li>GPG 鍵のフィンガープリント</li>
  <li>メールアドレス</li>
</ul>
の書かれた紙を(それなりの枚数)用意して参加して下さい. 用意して頂いた紙をキーサイン相手に一枚ずつ配って頂きます.</p>
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：佐々木 洋平 (uwabami&#64;{debian.or.jp} ) までお願いいたします。</p>
