オープンソースカンファレンス 2016 Gunma (5/14) にDebian JP Projectと東京エリアDebian勉強会出展

<p>
 東京エリア Debian 勉強会とは、Debian Project に関わる事を目的とす
 る熱いユーザたちと、実際に Debian Project にてすでに日夜活動している人
 らが Face to Face で Debian GNU/Linux のさまざまなトピック （パッケー
 ジ、Debian 特有 の機能の仕組について、Debian 界隈で起こった出来事、
 etc ） について語り合う場です。当然ですが、Debian Project に何らかの貢献
 をしたいという目的を持つ方であれば、自身で活動できるように手助けをする事
 も主な趣旨の一つとしています。
 </p>
 <p>
 参加される方は主に東京を中心に関東近郊の国籍・性別不問の Debian ユーザです  
 ( Debian をまだ使ったことが無いが興味があるので…とか、かなり遠い所から来てくださる方もいます)。 
 開発の最前線にいる Debian の公式開発者や開発者予備軍の方も参加する場合がありますので、
 普段は聞けないような様々な情報を得るチャンスです。
 興味と時間があった方、是非御参加下さい。
 （また、勉強会で話をしてみたいという方も随時募集しています）。
 </p>

<p>
5月は通常の勉強会とは別に <a href="http://www.ospn.jp/osc2016-gunma/">オープンソースカンファレンス 2016 Gunma</a>
に有志によるブース展示とセミナーを行います。</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日程：2016年5月14日(土) 10:20-17:40<br>
  <li>会場：<a href="http://www.yamadalabi.com/takasaki/index.html">LABI1 高崎 4F イベントホール (JR高崎駅 東口直結)</a></li>
  <li>参加費用：無料</dd>
<dt>ブース展示物</dt>
<dd>
  <ol>
  <li>Debian 勉強会資料「あんどきゅめんてっどでびあん」冊子</li>
  <li>Debian 稼働マシン</li>
  <li>有志作成 Debian グッズ (Tシャツ、ステッカー等)</li>
  <li>フライヤー・Live DVD 配布</li>
  </ol>
</dd>

<dt>セミナー</dt>
<dd>タイトル: <a href="https://www.ospn.jp/osc2016-gunma/modules/eguide/event.php?eid=15">Debian Updates</a></dd>
<dd>対象：Debian に興味、関心のある人</dd>
<dd>開始：15時30分 〜 (10分間)</dd>
<dd>Debian 8.0 (コードネームJessie) の話題を中心に、最近のDebian Projectのトピックを紹介します。</dd>

<p>
この件に関するお問い合わせは 東京エリアDebian 勉強会 担当：岩松 信洋 (iwamatsu &#64; {debian.or.jp} )
までお願いいたします。
</p>
