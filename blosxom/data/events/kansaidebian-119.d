第 119 回 関西 Debian 勉強会のお知らせ

<p>
「<a href="https://wiki.debian.org/KansaiDebianMeeting/20170212">第119回 関西 Debian 勉強会</a>」
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<p>
第 119 回 関西 Debian 勉強会は、Debian Strech リリース直前 Bug Squashing Party を行います。
</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
    <li>日時： 2017 年 2 月 12 日 (日) 13:30 - 17:00 (開場 13:00)</li>
    <li>会場：<a href="http://www.sci.kyoto-u.ac.jp/ja/map.html">京都大学 理学部三号館 108 号室 京都大学 吉田キャンパス</a>
(定員：30名)</li>
    <li>費用：500円 (部屋代、印刷代などの諸費用のため)</li>
    <li>事前課題: <br>
      <ol>

      <li>
      以下の環境を用意してきて下さい。仮想環境でもかまいません。
        <ul>
          <li>パソコンを使うので、各自パソコンを持ってきてください。</li>
          <li>使うパソコンで無線LAN、有線LAN が動作する環境を整えてください。</li>

          <li>使うパソコンで Debian / unstable が動作する環境を整えてください（VirtualBox 等の仮想環境で大丈夫です）。Debian /
unstable に 最低限のビルド環境を構築してください。</li>

<pre>
sudo apt-get install build-essential debhelper devscripts cowdancer pbuilder
</pre>
          <li>修正するソフトウェアのソースコード等 </li>

	  <li>デバッグ用の資料、機材</li>

	  <li>Debian 新メンテナーガイド や Debian 開発者リファレンス、バグ報告の仕方に目を通しておいてください。(参考資料欄にリンクがあります)</li>
        </ul>
      </li>

      </ol>
    </li>
  </ul>
</dd>

<dt>内容</dt>
<dd>
  <ol>
  <li>挨拶</li>
  <li>基本的なバグつぶしの説明</li>
  <li>もくもく( Bug Squashing Party )</li>
  <li>成果発表</li>
  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
  <a href="https://connpass.com/event/50374/">connpass</a>を参照して、
  事前登録をしてください。事情によりconnpassを使えない/登録できない方は担当者まで連絡してください
  (締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。)。
</dd>
</dl>

<dt>参考文献</dt>
<dd>
  <ul>
  <li>Jesssie リリース時の <a href="https://wiki.debian.org/KansaiDebianMeeting/20150117">BSP</a></li>
  <li><a href="https://wiki.debian.org/BSP/BeginnersHOWTO">BSP Beginner's How to</a></li>
  <li><a href="https://www.debian.org/doc/manuals/maint-guide/">New Maintainer's Guide</a></li>
  </ul>
</dd>

<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：佐々木洋平 (uwabami&#64;{debian.or.jp} ) までお願いいたします。</p>
