第1回 関西 Debian 勉強会のお知らせ

<p>
東京に引き続き、関西方面でも勉強会を！という声によって
「<a href="http://wiki.debian.org/KansaiDebianMeeting">関西 Debian 勉強会</a>」
が開かれることになりました。関西 Debian 勉強会では、定期的に顔をつき合わせて、
Debian GNU/Linux のさまざまなトピック (新しいパッケージ、Debian 特有の機能の仕組み、Debian 界隈で起こった出来事、などなど) について話し合っていく予定です。</p>

<p>参加者には開発の最前線に立つ Debian の公式開発者や開発者予備軍の方もおりますので、
普段は聞けないようなさまざまな情報を得るチャンスです。<br>
興味を持たれた方はぜひ御参加ください (また、勉強会で話をしてみたいという方も随時募集しています)。</p>


<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：2007年3月31日(土) 13:00-17:00 (受付は12:30以降、終了後に懇親会も別途予定)
  <li>会場：<a href="http://www.olnr.org/%7Ehatanaka/location.html">大阪電気通信大学 (四條畷) 11-403号室</a>
  <li>費用：無償</ul></dd>
<dt>内容</dt>
<dd>
  <ol>
  <li><strong>関西 Debian 勉強会 について</strong><br>
関西で Debian 勉強会を開催するにあたってのご挨拶と今後に向けての説明です。

  <li><strong>参加者の自己紹介 (それぞれ)</strong><br>
初めての機会ですので、参加者の自己紹介、勉強会に参加した動機、Unix (Linux) の経験、
どんな風にDebianを使っているかなど情報交換をしましょう。

  <li><strong>関西 Debian 勉強会で学びたいこと、関西 Debian 勉強会に対してできること。(司会：矢吹)</strong><br>
今後の勉強会で何をしていこうか、という話を皆で交換し合いましょう (話をまとめるために、なるべく事前にメールでお送りください)。

  <li><strong>yc-el について</strong><br>
Emacs から canna を使って日本語を入力を行うパッケージである yc-el をメンテナンスしています。
前担当者からこのパッケージを引き継ぐ際、再パッケージを行って得た経験をお話します。

  <li><strong>Debian Develper と key sign (希望者)</strong><br>
Debian Project のメンテナになるためには、Debian Developer との 
GPG キーサインが必須になります。キーサインを望む方は以下をご用意ください。
      <ul>
        <li>公的証明書 (運転免許証・パスポートなど)
        <li>GPG 鍵作成時に指定したメールアドレス・名前・鍵の指紋 (fingerprint) を書いた用紙 (参加人数分)</ul>
  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
参加希望者は事前登録の上、事前のまとめを作るための情報を送付してください。期限は 3 月 25 日 (日) です。<br>
<strong>事前登録、及び事前送付情報の内容と送付先については
<a href="http://wiki.debian.org/KansaiDebianMeeting">関西 Debian 勉強会のページ
</a>を参照してください。</strong>
</dd>
</dl>
<p>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会主催者：矢吹幸治 (yabuki&#64;{debian.or.jp,netfort.gr.jp} ) までお願いいたします。</p>
