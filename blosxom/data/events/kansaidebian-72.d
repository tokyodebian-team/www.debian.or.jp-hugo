第72回 関西 Debian 勉強会のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting/20130526">第72回 関西 Debian 勉強会</a>」
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
    <li>日時： 2013 年 5 月 26 日 (日) 13:30 - 17:00</li>
    <li>会場：<a href="http://www.grandfront-osaka.jp/access">グランフロント大阪</a> <a href="http://kc-i.jp/facilities/salon/">ナレッジサロン</a> (会場提供： <a href="http://an-nai.jp/">ANNAI</a>さん)
    (定員：20名)</li>
    <li>費用：300円 (部屋代、印刷代などの諸費用のため)</li>
    <li>事前課題: <br>
      <ol>

      <li>
      DebianとUbuntuの違いを挙げてみてください。
      </li>

      <li>
      Debianを使っていて/使い始めて/使おうとして、困った/ていることを、1件以上書いてください。
      </li>

      </ol>
    </li>
  </ul>
</dd>

<dt>内容</dt>
<dd>
  <ol>

  <li><strong>Intro</strong><br>
  この勉強会の簡単な説明をします。
  </li>

  <li><strong>「Debian と Ubuntu の違いを知ろう」</strong>(担当：西田) <br>
  LinuxはとりあえずUbuntuを使っている"という方は多いのではないでしょうか。 この話ではUbuntuのベースであるDebianとUbuntuでどのようなところが異なっているのかを挙げ、互いの理解を深めたいと思います。 
  </li>

  <li><strong>「Debian の歩き方」</strong>(担当：佐々木、倉敷、etc) <br>
  事前課題の内容をネタに、最初のとっかかりでつまづきそうなポイントについてディスカッションします。「この選択肢選ぶとどうなるの?」「こういうセットアップしたいんだけど?」などお気軽に事前課題に登録してみてください。時間に余裕があれば、Debian のインストール方法をいくつかウォークスルーしてみる予定です。
  </li>

  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
<strong>2013年 5 月 25 日 (土) 23:59 までに</strong>
<a href="http://debianmeeting.appspot.com/event?eventid=dd34f853def7ac29ee4887a6725bc680fb0ef00e">Debian勉強会予約管理システム</a>を参照して、
事前登録をしてください。登録にはGoogleアカウントが必要になりますが、事情によりGoogleアカウントを使えない/登録できない方は担当者まで連絡してください
（締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。
懇親会 (予定) に参加する場合は、二次会参加の欄にチェックしてください）。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：かわだてつたろう (t3rkwd&#64;{debian.or.jp} ) までお願いいたします。</p>
