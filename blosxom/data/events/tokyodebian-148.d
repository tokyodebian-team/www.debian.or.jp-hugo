第 148 回 東京エリア Debian 勉強会のお知らせ

<p>
東京近辺にいらっしゃる皆様こんにちは。2018年2月も 東京エリア Debian 勉強会が開かれます！
</p>

<p>
 東京エリア Debian 勉強会とは、Debian Project に関わる事を目的とす
る熱いユーザたちと、実際に Debian Project にてすでに日夜活動している人
らが Face to Face で Debian GNU/Linux のさまざまなトピック （パッケー
ジ、Debian 特有 の機能の仕組について、Debian 界隈で起こった出来事、
etc ） について語り合う場です。当然ですが、Debian Project に何らかの貢献
をしたいという目的を持つ方であれば、自身で活動できるように手助けをする事
も主な趣旨の一つとしています。
</p>
<p>
参加される方は主に東京を中心に関東近郊の国籍・性別不問の Debian ユーザです 
( Debian をまだ使ったことが無いが興味があるので…とか、かなり遠い所から来てくださる方もいます)。
開発の最前線にいる Debian の公式開発者や開発者予備軍の方も参加する場合がありますので、
普段は聞けないような様々な情報を得るチャンスです。
興味と時間があった方、是非御参加下さい。
（また、勉強会で話をしてみたいという方も随時募集しています）。</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
   <ul>
   <li>日時：2017 年 2 月 11 日 ( 土曜日 ) 11:00-18:00 </li>
   <li>費用：500 円 (資料印刷代・会場費として)</li>
	<li>会場：<a href="http://asahi-net.jp">株式会社朝日ネット</a>さん（<a
   href="http://asahi-net.co.jp/jp/location/">地図</a>）</li>
　 <li>当日について：Debian の次期リリース「stretch」のフルフリーズに合わせ、バグ潰しパーティ（BugSquashing
Party)）を行います。詳しくは<a href="debianjp.connpass.com/event/50010/">勉強会のページ</a>を参照してください。</li>
   </ul>
</dd>
<dt>内容</dt>
<dd>
  <ol>
  <li>バグ潰しパーティ（BugSquashing Party)<br>
  皆で集まって、次期リリースに向けたバグ(<a href="https://bugs.debian.org/release-critical/debian/main.html">main
		  セクションのバグ</a>など)を潰します。パッケージのバグを潰すだけではなく、テストや翻訳などの作業も大歓迎です。
</li>
  </ol>
</dd>

<dt>事前課題／参加申し込み方法</dt>
<dd>
<a href="http://debianjp.connpass.com/event/50010/">勉強会のページ</a>を参照して、参加登録をしてください。</dd>
<dd>
<br>
事前課題は参加登録時に表示されるアンケートに入力してください。提出いただいた回答は全員に配布すること、また内容の再利用を実施することを了承ください。 Debian 勉強会資料は GPL で公開します ( Web での公開が可能な内容でお願いします）。
</dd>
<dd>
課題内容の実施についてなにか質問があれば、 Debian ユーザ用公開メーリングリスト ( debian-users@debian.or.jp ) にてお願いします。</dd>

</dl>

<p>
この件に関するお問い合わせは Debian 勉強会主催者：岩松 (iwamatsu@{debian.or.jp} ) までお願いいたします。</p>
