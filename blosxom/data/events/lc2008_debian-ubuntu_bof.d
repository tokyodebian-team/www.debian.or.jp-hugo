Debian-Ubuntu BoF 開催のお知らせ

<p>
Debian JP Project と Ubuntu Japanese Team は <a href="http://lc.linux.or.jp/lc2008/">Linux Conference 2008</a> にて<a href="http://lc.linux.or.jp/lc2008/11.html"><strong>「Debian/Ubuntu の立ち位置と日本でのコミュニティ活動の連携について」</strong>をテーマに BoF</a> を開くことになりました。</p>
<p>
プロジェクト開始から15年を迎えるボランティアによる「自由なソフトウェア」によるディストリビューションである Debian と、その Debian をベースに「他者への思いやりと協調」をモットーに、特にデスクトップユースにフォーカスを当てた開発と活動で今一番注目を集めているディストリビューション Ubuntu。</p>
<p>
この2つのスタンスと特に国内での今後の活動連携について話をします。Debian/Ubuntu について少しでも興味のある方であれば、どなたでも歓迎です。開催時間について、皆さんが参加しやすいよう遅めの時間としました。BoF ですので構えずに「気軽な歓談の場」として気軽にご参加くださればと思います。</p>


<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：2008年9月11日(木)　19:00-20:30</li>
  <li>会場：<a href="http://www.sunplaza.jp/access/index.html">中野サンプラザ 7階 研修室8</a>　(JR中央線・総武線／東京メトロ東西線「中野」駅 北口より徒歩1分)</li>
  <li>費用：無料</li>
  </ul>
</dd>

<dt>講演者予定（変更の可能性あり）</dt>
<dd>
Debian JP Project    :  やまねひでき</dd>
<dd>
Ubuntu Japanese Team :  小林 準</dd>


<dt>参加方法と注意事項</dt>
<dd>
特に参加資格、事前申し込みや費用はありません。ただし、参加者は会場にて<a href="http://lc.linux.or.jp/lc2008/participation.html">当日受け付け</a>(名刺を2枚ご用意頂くとスムースです)を済ませてから BoF 開催部屋への入室をお願いいたします。</dd>
</dl>

<p>
この件に関するお問い合わせは Debian-Ubuntu BoF 担当者：やまねひでき (board&#64;debian.or.jp)
までお願いいたします。</p>
