第52回 関西 Debian 勉強会のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting20111023">第52回 関西 Debian 勉強会</a>」
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
    <li>日時： 2011 年 10 月 23 日 (日) 13:30 - 17:00</li>
    <li>会場：<a href="http://www.city.osaka.lg.jp/shimin/page/0000016603.html">福島区民センター302号会議室</a>
(定員：45名)</li>
    <li>費用：500円 (部屋代、印刷代などの諸費用のため)</li>
    <li>事前課題: <br>
      <ol>
      <li>EmacsもしくはVimの拡張機能のDebianパッケージを挙げてください(何個でも)。また、それらに含まれるファイル一覧を見ておいてください(これは記入不要)。</li>
      <li>OmegaT が動作する環境1を用意し, お手軽スタートガイドに目を通してきて下さい。翻訳をしたことがある人は作業環境を教えて下さい。</li>
  </ul>
</dd>

<dt>内容</dt>
<dd>
  <ol>
  <li><strong>Intro</strong><br>
この勉強会の簡単な説明をします。</li>

  <li><strong>「Emacs, Vimの拡張機能で学ぶDebianパッケージ」</strong> (担当:西田)<br>
Emacs, Vimの拡張機能のDebianパッケージの作成方法についてハンズオン形式で紹介します。<br>
既存パッケージのrebuildから始めて、まだ作られていないパッケージを最初から作成するところまでを学習する予定です。
</li>

  <li><strong>「翻訳で Debian に貢献しよう」</strong> (担当:やつお)<br>
Debian を利用する上で英語との付き合いは避けて通れない問題だと思います。<br>
翻訳作業は英語を学び、Debian について学び、さらに Debian に貢献できる最良の自習教材です。<br>
あなたも翻訳で Debian に貢献してみませんか？
</li>
  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
<strong>2011年 10 月 21 日 (金) 23:59 までに</strong>
<a href="http://debianmeeting.appspot.com/event?eventid=7ec526af2b8bdc87cde4839ff7a176acde3e0604">Debian勉強会予約管理システム</a>を参照して、
事前登録をしてください。登録にはGoogleアカウントが必要になりますが、事情によりGoogleアカウントを使えない/登録できない方は担当者まで連絡してください
（締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。
懇親会 (予定) に参加する場合は、二次会参加の欄にチェックしてください）。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：かわだてつたろう (t3rkwd&#64;{debian.or.jp} ) までお願いいたします。</p>
