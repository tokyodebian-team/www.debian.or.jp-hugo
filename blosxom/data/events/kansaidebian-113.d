第 113 回 関西 Debian 勉強会

<p>
「<a href="https://wiki.debian.org/KansaiDebianMeeting/20160828">第113回 関西 Debian 勉強会</a>」
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで
Debian のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
    <li>日時： 2016 年 8 月 28 日 (日) 13:30 - 17:00 (開場 13:00)</li>
    <li>会場：<a href="http://www.city.osaka.lg.jp/shimin/page/0000016603.html">福島区民センター304号会議室</a>
(定員：20名)</li>
  </ul>
</dd>

<dt>内容</dt>
<dd>
  <ol>

  <li><strong>Intro</strong><br>
  この勉強会の簡単な説明をします。
  </li>

  </ol>
</dd>

<dt>参加方法と注意事項</dt>
<dd>

<a href="https://debianjp.doorkeeper.jp/events/51111">Doorkeeper</a>を参照して、
事前登録をしてください。事情によりDoorkeeperを使えない/登録できない方は担当者まで連絡してください
(締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。)。
</dd>

</dl>

<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：かわだてつたろう (t3rkwd&#64;{debian.or.jp}),
おおつきようすけ (y.otsuki30&#64;{gmail.com})までお願いいたします。</p>
