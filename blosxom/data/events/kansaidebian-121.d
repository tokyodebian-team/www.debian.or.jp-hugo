第 121 回 関西 Debian 勉強会(10 周年記念会)

<p>
「<a href="https://wiki.debian.org/KansaiDebianMeeting/20170319">第121回 関西 Debian 勉強会</a>」
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで
Debian のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
    <li>日時： 2017 年 3 月 19 日 (日) 13:30 - 17:00 (開場 13:00)</li>
    <li>会場：<a href="http://www.enokojima-art.jp/e/">江之子島文化芸術創造センター/enoco, Room 12</a>
(定員：20名)</li>
  </ul>
</dd>

<dt>内容</dt>
<dd>
  <ol>

  <li><strong>Intro</strong><br>
  この勉強会の簡単な説明をします。
  </li>

  <li><strong>10 年間の振り返り</strong><br>
  関西 Debian 勉強会の 10 年を振り返ります。
  </li>

  </ol>
</dd>

<dt>参加方法と注意事項</dt>
<dd>

<a href="https://debianjp.connpass.com/event/52512/">connpass</a>を参照して、
事前登録をしてください。事情によりconnpassを使えない/登録できない方は担当者まで連絡してください。
(締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。)
</dd>

</dl>

<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：おおつきようすけ (y.otsuki30&#64;{gmail.com}),
かわだてつたろう (t3rkwd&#64;{debian.or.jp})までお願いいたします。</p>
