第44回 関西 Debian 勉強会のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting20110227">第44回 関西 Debian 勉強会</a>」
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
    <li>日時： 2011 年 2 月 27 日 (日) 13:30 - 17:00(13:15 より受付)</li>
    <li>会場：<a href="http://www.city.osaka.lg.jp/shimin/page/0000016613.html">大阪 港区民センター 楓の間</a>
(定員：30名)</li>
    <li>費用：500円 (部屋代、印刷代などの諸費用のため)</li>
    <li>事前課題: <br>
        ついに Squeeze がリリースされました。そんなわけで Squeeze になって
      <ul>
        <li> よかったこと</li>
        <li> うれしかったこと</li>
        <li> (些細な事だけど)こんな所が変わった!</li>
        <li> いきなりナニカ踏んだ!!</li>
      </ul>
      なんて事柄を、なにか一つご報告下さい。</li>
  </ul>
</dd>

<dt>内容</dt>
<dd>
  <ol>
  <li><strong>Intro</strong><br>
この勉強会の簡単な説明をします。</li>

  <li><strong>「pbuilderを使ってみよう」</strong> (担当: 水野 源)<br>
自分でパッケージを作ったり、あるいは既存のソースパッケージをリビルドすることはよくあると思います。安全でクリーンな環境でパッケージのビルドをしたり、異なるリリース向けのパッケージを作るためには、pbuilder/cowbuilderといったツールを使うと便利です。そんなpbuilder/cowbuilderの使い方に加え、Ubuntu的なpbuilderでのパッケージビルド方を紹介します。</li>

  <li><strong>「Squeezeリリースパーティ」</strong> (担当: のがたじゅん, 倉敷悟)<br>
2月6日に無事、Squeezeがリリースされました。そこでリリース記念のお祝いをしたいと思います。</li>

  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
<strong>2011年 2 月 24 日 (金) 24:00 までに</strong>
<a href="http://debianmeeting.appspot.com/event?eventid=69a3023eedfa03a2eebced5d5a57e5674d396fc4">Debian勉強会予約管理システム</a>を参照して、
事前登録をしてください。登録にはGoogleアカウントが必要になりますが、事情によりGoogleアカウントを使えない/登録できない方は担当者まで連絡してください
（締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。
懇親会 (予定) に参加する場合は、二次会参加の欄にチェックしてください）。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：のがたじゅん (nogajun&#64;{debian.or.jp} ) までお願いいたします。</p>
