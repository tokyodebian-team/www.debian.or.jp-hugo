第33回 Debian 勉強会 in オープンソースカンファレンス 2007 Tokyo/fall のお知らせ

<p>
Debian 勉強会は、メールや web だけでは語り切れない Debian 
に関わる話題について浅いところから深いところまであらゆることを 
Face to Face で 情報交換するイベントです。</p>
<p>
10月は<a href="http://www.ospn.jp/osc2007-fall">オープンソースカンファレンス 2007 Tokyo/Fall</a>
に参加してセミナーやブースでの展示と説明などを予定しています。
普段は聞けないような様々な情報を得るチャンスです。是非御参加下さい。</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：2007年10月6日(土) 10:00-17:30</li>
  <li>会場：<a href=
"http://www.pio.or.jp/plaza/access/index.htm">大田区産業プラザ(PiO)</a>
 (東京都大田区・京急蒲田駅徒歩3分)<br>
<iframe width="640" height="480" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.co.jp/maps/ms?ie=UTF8&amp;hl=ja&amp;om=1&amp;s=AARTsJrQHjq5X2oqsgJF7E3H44KZJxiKEg&amp;msa=0&amp;msid=109811903404931563048.0004393d34d8902867402&amp;ll=35.567003,139.723778&amp;spn=0.033513,0.054932&amp;z=14&amp;output=embed"></iframe><br /><small><a href="http://maps.google.co.jp/maps/ms?ie=UTF8&amp;hl=ja&amp;om=1&amp;msa=0&amp;msid=109811903404931563048.0004393d34d8902867402&amp;ll=35.567003,139.723778&amp;spn=0.033513,0.054932&amp;z=14&amp;source=embed" style="color:#0000FF;text-align:left">拡大地図を表示</a></small></li>
  <li>費用：無料</li></ul></dd>
<dt>セミナー (12:30-13:20＠会議室Ｅ)</dt>
<dd>
  <ul>
  <li><strong>GNU/Linuxはふちなし印刷の夢を見るか？</strong>（担当：武藤 健志）<br>
	諸君、サーバ戦線を突破した我らの次なる目標はデスクトップである。<br>
        難攻不落たるその城壁も、今やWebブラウザをはじめとする英雄たちによってほころびが見え始めてきた。<br>
        しかし、諸君、私はそこにあるプリンタに、テキストを、イラストを、写真を今すぐに印刷したいのだ。<br>
        ――本セッションでは、Debian Printing Teamの開発者が、CUPSを中心としたLinux印刷環境の概要および今後の技術動向を紹介する。</li>
   </ul></dd>

<dt>ミニセミナー (15:10-15:20＠Ｂ会場)</dt>
<dd>
  <ul>
  <li><strong>Debian ミニQ&amp;A</strong> (担当：やまねひでき）<br>
      会場その他で集めた Debian に関するあれこれに回答したりしなかったり？</li></ul>
</dd>
<dt>ブース展示物</dt>
<dd>
  <ol>
  <li>Debian 関連書籍</li>
  <li>有志作成 Debian グッズ (Ｔシャツ・ステッカー)</li>
  <li>Debian 勉強会資料</li>
  <li>チラシ・CD（配布も予定）</li>
  <li>Debian 稼働マシン (NAS、サーバ等)</li>
  <li>Debian 掲示板</li></ol>
</dd>

<dt>参加方法と注意事項</dt>
<dd>
セミナー参加希望者は<a href="http://www.ospn.jp/osc2007-fall/">事前登録</a>してください。<br>
ブース展示・ミニセミナーについては特に登録などはありません。</dd>
</dl>
<p>
この件に関するお問い合わせは Debian 勉強会主催者：上川純一 (dancer&#64;{debian.org,netfort.gr.jp} )
までお願いいたします。</p>
