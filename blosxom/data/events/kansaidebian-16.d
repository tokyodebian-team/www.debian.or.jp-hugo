第16回 関西 Debian 勉強会のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting20080817">第16回 関西 Debian 勉強会</a>」 
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで 
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：2008 年 8 月 17 日 (日) 13:15 - 17:00 (13:30 より受付)</li>
  <li>会場：<a href="http://www.city.osaka.jp/shimin/shisetu/01/fukushima.html">大阪 福島区民センター 会議室</a>
(定員：25名)</li>
  <li>費用：500円 (部屋代、印刷代などの諸費用のため)</li>
  </ul>
</dd>
<dt>内容</dt>
<dd>
  <ol>
  <li><strong>Intro</strong> 山下 尊也 氏<br>
この勉強会の目的などを説明し、その後、第16回関西 Debian 勉強会の内容について話をします。</li>

  <li><strong>オープンソースカンファレンス 2008 Kansai を振り返る</strong>　山下 尊也 氏<br>
7月の18日、19日に行われました、オープンソースカンファレンス2008 Kansaiについて振り返ります。
今後のイベントを関西 Debian 勉強会がどのように行うのかについて見当します。</li>

  <li><strong>Debian を Windows な PC でも楽しもう ~応用編~</strong>　名村 知弘 氏<br>
coLinux を使うと、Windows 上で Debian を楽しむことができるのですが、
VMWare のような仮想マシンではないため、何かと制約があります。<br>
そこで今回は応用編として、coLinux 用 Debian イメージの作成方法や、X
アプリケーションの起動方法などを簡単にご紹介します。</li>

  <li><strong>フリーディスカッション</strong><br>
Debian について参加者同士であんな話題やこんな話題について情報交換をしましょう。</li>

  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
<strong>8 月 14 日 (木) 24:00 までに</strong>
<a href="http://cotocoto.jp/event/28510">京都.cotocoto</a>を参照して、
事前登録をしてください
（締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。
懇親会に参加する場合は、コメントに「懇親会参加希望」と記入してください）。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：山下尊也 (takaya&#64;{debian.or.jp} ) までお願いいたします。</p>


