大統一Debian勉強会のお知らせ

  <p>6月23日(土曜日)に京都大学 理学研究科3号館 数学教室 108, 109, 110 号室において、全国のDebian勉強会合同の勉強会「大統一Debian勉強会」を開催します。</p>

  <h3>開催趣旨</h3>
  <p>Debian JP Project では2005年1月から東京地域を中心とした Debian 
  に関する勉強会「東京エリアDebian勉強会」を、2007年3月からは関西地域
  を中心とした「関西Debian勉強会」を毎月行なっています。</p>
  <p>Debian 勉強会の大きな目的として、
  <ul>
    <li>Debian Developer の育成</li>
    <li>普段ばらばらな場所にいる人々が face-to-face で出会える場を提供する</li>
  </ul>
  があります。</p>
  <p>勉強会を始めてから、多くのDebianユーザおよび開発者との交流が行われ、
  Debian 開発者とDebianパッケージメンテナを輩出することができました。</p>
  <p>また Debianに関する資料も多く作成され、各Debian 勉強会は日本での Debian 
  ユーザおよび開発者の情報交換できる場となりました</p>
  <p>これまで関東と関西、他の地域でDebian勉強会が開催されてきましたが、
  地域勉強会同士の交流はあまり行われていません。今回各地で行われている Debian 
  勉強会を合同で行ない、各地のDebianユーザ、Debian開発者の交流を図ることを目的
  とする「大統一Debian勉強会」を開催を行います。</p>


  <h2>開催概要</h2>

  <dl>
    <dt>開催日時</dt>
    <dd>
      <ul>
	<li>2012年6月23日(土曜日) 10時00分〜18時00分まで(予定)</li>
      </ul>
      <p>勉強会終了後、懇親会を開催します。</p>
    </dd>

    <dt>開催場所</dt>
    <dd>
      <ul>
	<li>京都大学 理学研究科3号館 数学教室 108, 109, 110 号室</li>
	<li><a href="http://www.math.kyoto-u.ac.jp/">http://www.math.kyoto-u.ac.jp/</a></li>
	<li><a href="http://www.kyoto-u.ac.jp/ja/access/campus/map6r_n.htm">http://www.kyoto-u.ac.jp/ja/access/campus/map6r_n.htm</a></li>
      </ul>
      <iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://www.openstreetmap.org/export/embed.html?bbox=135.76621,35.01832,135.80097,35.04137&amp;layer=mapnik&amp;marker=35.03009,135.78271" style="border: 1px solid black"></iframe><br /><small><a href="http://www.openstreetmap.org/?lat=35.029845&amp;lon=135.78359&amp;zoom=15&amp;layers=M&amp;mlat=35.03009&amp;mlon=135.78271">大きな地図で見る...</a></small>
    </dd>

    <dt>プログラム</dt>
    <dd>公募は終了しました。ありがとうございました。プログラムの内容はタイムテーブルを参照してください。<del>公募（CFP）による発表者の募集を行ないます。応募者は<a href="cfp.html">応募要項</a>を参照してください。</del></dd>

    <dt>参加費</dt>
    <dd>無料です。</dd>

    <dt>事前参加登録</dt>
    <dd>必要です。ATND の<a href="http://atnd.org/events/29310">登録ページ</a>から登録してください。</dd>

    <dt>勉強会資料について</dt>
    <dd>勉強会資料は当日PDFで公開しますが、本資料は<a href="https://tokyodebian-team.pages.debian.net/undocumenteddebian.html">
    あんどきゅめんてっどでびあん/Deb専</a> 2012夏号に収録し、会場で希望の方に販売します。
    ご希望の方は<a href="http://www.zusaar.com/event/309151">冊子購入登録サイト</a>に登録して、当日チケットを持ってきてください。</dd>

    <dt>ネット中継</dt>
    <dd>Ustream 配信(予定)</dd>

    <dt>公式タグ</dt>
    <dd>Twitter: #gumdebianjp</dd>
    <dd>ブログ: gumdebianjp2012</dd>

    <dt>懇親会</dt>
    <dd>懇親会受付は終了しました。<del>参加される方は<a href="http://www.zusaar.com/event/301001">懇親会用の登録ページ</a>から登録を行なってください。</del></dd>

    <dt>運営</dt>
    <dd>「大統一Debian勉強会」実行委員会
    <ul>
      <li><a href="https://tokyodebian-team.pages.debian.net/">東京エリアDebian勉強会</a></li>
      <li><a href="http://wiki.debian.org/KansaiDebianMeeting">関西Debian勉強会</a></li>
    </ul>
    </dd>

    <dt>後援</dt>
    <dd><a href="http://www.debian.or.jp">Debian JP Project</a></dd>
  </dl>

  <h2>タイムテーブル</h2>

  <table id='timetable'>

  <tr>
  <th class='time'>時間</th>
  <th class='no110'>110教室(60人)</th>
  <th class='no108'>108教室(45人)</th>
  <th class='no109'>109教室(20人)</th>
  </tr>

  <tr class='even'>
    <td>10:00-10:10</td>
    <td>開会の挨拶</td>
    <td></td>
    <td></td>
  </tr>

  <tr class='odd'>
    <td>10:10-11:00</td>
    <td>
      <h5>TeXLive 2011(2012/dev) in Debian</h5>
      <ul>
      <li>発表者: 佐々木洋平</li>
      <li>DebianにおけるTeX環境(特に日本語処理)の導入や設定および次期安定版Wheezyでの変更点および開発状況について解説します。</li>
      </ul>
    </td>
    <td>
      <h5>Linux-PAMの設定について</h5>
      <ul>
      <li>発表者: 西山和広</li>
      <li>pam-auth-update がまだなかったころに libpam-ldap や libpam-tmpdir を使うためにがんばって調べた/etc/pam.d/* の設定方法の話をする予定です。</li>
      </ul>
    </td>
    <td rowspan=9>
      <h5>常時開放</h5>
      <ul>
      <li>ハック部屋、BOF用として利用できます。PGP/GPG キーサイン、開発、雑談等にご利用ください。</li>
      </ul>
    </td>
  </tr>

  <tr class='even'>
    <td>11:10-12:00</td>
    <td>
      <h5>数学ソフトウェア使ってますか？</h5>
      <ul>
      <li>発表者: 濱田龍義</li>
      <li>
      Debian パッケージには「数学」というセクションが存在し、すでに多数の数学ソフトウェアが提供されています。
      一方で、パッケージには未収録ですが、数学研究の最前線で使われているフリーソフトウェアが、まだ数多く存在します。
      2003年頃から、そういったソフトウェアをライブCDであるKNOPPIX に収録して紹介してきたプロジェクトが KNOPPIX/Math です。
      KNOPPIX/Math では、2006年からLiveDVDを採用し，2012年からは KNOPPIX 以外のメディアの利用も考慮し、MathLibreとプロジェクト名を変更いたしました。
      本発表では数学ソフトウェアの最新事情について紹介いたします。
      </li>
      </ul>
    </td>
    <td>
      <p>
        <h5>ipython notebookとその周辺 (25分)</h5>
        <ul>
        <li>発表者: 本庄 弘典</li>
        <li>ipython notebookとその周辺 ipython qtconsoleおよびipython nodebookの導入や、周辺で使われている技術に関して簡単にご紹介します。</li>
        </ul>
      </p>
      <p>
        <h5>PGP/GPG キーサインパーティ</h5>
        <ul>
        <li>発表者: 岩松 信洋</li>
        <li>PGP/GPGキーサインパーティを行います。参加されたい方は<a href="ksp/">PGP/GPG キーサインパーティ</a>を参照してください。</li>
        </ul>
      </p>
    </td>
  </tr>

  <tr class='odd'>
    <td>12:00-13:30</td>
    <td>昼食</td>
    <td></td>
  </tr>

  <tr class='even'>
    <td>13:30-14:20</td>
    <td>
      <h5>DebianとLibreOffice</h5>
      <ul>
      <li>発表者: あわしろ いくや</li>
      <li>
      DebianとLibreOffice、その元となったOpenOffice.orgの過去と現在を、OpenOffice.orgのこと、Debianでメンテされるようになったこと、LibreOfficeのフォークのこと、Apache
      OpenOfficeの騒動のこと、LibreOffice関連パッケージのことなどについて、ざっくばらんにお話しします。爆弾発言はありませんよ！
      </li>
      </ul>
    </td>
    <td>
      <h5>debug.debian.net</h5>
      <ul>
      <li>発表者: 岩松 信洋</li>
      <li>
      Debianはサーバから組み込みまでサポートするLinuxディストリビューションを提供しています。また、使っている方はユーザから開発者までさまざまです。
      フリーソフトウェア/オープンソースソフトウェアを開発および利用しているとき、バグに直面することがあります。
      自分でデバッグ情報を有効にしている場合はGDBなどで問題を追うことができますが、Debianの場合デバッグ情報はパッケージ化されるときに全て削除されているので、
      自分でこれらが削除されていないパッケージを用意する必要があります。これはユーザにとっても開発者にっては大変な作業です。
      いくつかデバッグ情報データを持ったパッケージがありますが、まだ少ないのが現状です。
      今回この問題を解決するために、Debian でデバッグ情報を含んだ全てのパッケージを提供する方法を検討したので、その方法などについて発表します。
      </li>
      </ul>
    </td>
  </tr>

  <tr class='odd'>
    <td>14:30-15:20</td>
    <td>
      <h5>Rabbit: 時間内に終われるプレゼンツール</h5>
      <ul>
      <li>発表者: 須藤 功平</li>
      <li>
      時間内に終われるプレゼンツール Debian GNU/Linux上で動作するプレゼンテーションツールRabbitを紹介します。
      Debian GNU/Linux上で動作するプレゼンテーションツールにはLibreOfficeのImpress、LaTeXのBeamerクラス
      PDFビューアー（Evinceやpdfcubeなど）、JavaScript +
      Webブラウザ（Impress.jsやshowoffなど）、Webサービス（Google DocsやPreziなど）、MagicPointなどがあります。
      それぞれのツールは特徴が大きく異なっており、それぞれよいところがある。Rabbitにもまた特徴があり、
      他のツールにはない便利な機能があります。それが「プレゼンテーションを時間内に終わらせるための機能」です。
      本プレゼンテーションではRabbitの特徴的な「プレゼンテーションを時間内に終わらせるための機能」を中心に、
      Rabbitの他の機能や他のプレゼンテーションツールをRabbitで紹介します。Debian GNU/Linuxでプレゼンテーションをする人たちの参考になることを期待します。
      </li>
      </ul>
    </td>
    <td>
      <h5> U-Bootについてあれこれ</h5>
      <ul>
      <li>発表者: 野島 貴英</li>
      <li>
      流行りのARM CPUの機材を例にブートローダのU-Bootについて語ります。
      U-Bootとは主に組み込み用途で利用されるブートローダです。
      Debianを使って、U-Bootを活用しARM CPU向けのブートローダの開発を
      行ってみました。これについて発表します。ターゲットは
      Android端末でおなじみのARM CPUを搭載した電子書籍端末（Barnes &amp; Noble社Nook Color）
      を使います。
      </li>
      </ul>
    </td>
  </tr>

  <tr class='even'>
    <td>15:20-16:00</td>
    <td>休憩</td>
    <td></td>
  </tr>

  <tr class='odd'>
    <td>16:00-16:50</td>
    <td>
      <h5>Debian Multiarch Support</h5>
      <ul>
      <li>発表者: なかお けいすけ</li>
      <li>Debian Multiarch Supportの概要を説明します。</li>
      </ul>
    </td>
    <td>
      <h5>家庭内LANを高速に！InfiniBand on Debian</h5>
      <ul>
      <li>発表者: 山田 泰資</li>
      <li>最近ようやく一般に普及を始めた10Gbps級ネットワークの1つにInfiniBandがありますが、早速家庭内LANとして導入してみました。
      本セッションでは単純な導入方法からストレージネットワークの構築などの応用をユーザー向けの話題に、
      また、後半では開発者向けの話題として10Gbps級LANの性能を引き出すために必要なRDMAベースのプログラミングについて（自分が学んだことを）紹介します。
      Debian に関連する部分としては、商用ベースに乗っているOSSにありがちな、各所のRedHat-ismを乗り越えて戦っていく苦労について語ります。
      </li>
      </ul>
    </td>
  </tr>

  <tr class='even'>
    <td>17:00-17:50</td>
    <td>
      <h5>Gentoo/Prefix on Debian</h5>
      <ul>
      <li>発表者: 青田 直大</li>
      <li>Gentoo/PrefixとDebianとを融合したpackage systemの紹介を行います。</li>
      </ul>
    </td>
    <td>
      <h5>Debianでもマルチタッチ！</h5>
      <ul>
      <li>発表者: 赤部 晃一</li>
      <li>Ubuntuに含まれているマルチタッチ関連のパッケージをDebian環境に取り込む方法を紹介し、Debian上でマルチタッチデバイスによる操作を実演します。</li>
      </ul>
    </td>
  </tr>

  <tr class='odd'>
    <td>17:50-18:10</td>
    <td>Lighting Talks</td>
    <td></td>
  </tr>

  <tr class='even'>
    <td>18:10-18:20</td>
    <td>閉会の挨拶</td>
    <td></td>
    <td></td>
  </tr>

  </table>

  <h2>「大統一Debian勉強会」実行委員会</h2>

  <h4>実行委員長</h4>
  <ul>
    <li>岩松 信洋</li>
  </ul>

  <h4>実行委員</h4>
  <ul>
    <li>河田 鉄太郎</li>
    <li>倉敷 悟</li>
    <li>佐々木 洋平</li>
    <li>野方 純</li>
    <li>前田 耕平</li>
    <li>山田 泰資</li>
    <li>山根 秀樹</li>
  </ul>
  <p>(50音順)</p>


  <h3>お問い合わせ</h3>
  <p>
  大統一Debian勉強会に関するお問い合わせは、grand-meeting _at_ debian.or.jp までメールにてご連絡ください。
  </p>
