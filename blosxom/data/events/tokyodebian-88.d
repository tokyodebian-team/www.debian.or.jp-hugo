第 88 回 東京エリア Debian 勉強会のお知らせ

<p>
東京近辺にいらっしゃる皆様こんにちは。今月も 東京エリア Debian 勉強会が開かれます！
</p>

<p>
Debian 勉強会とは、Debian 
の開発者になれることをひそかに夢見るユーザたちと、
ある時にはそれを優しく手助けをし、またある時には厳しく叱咤激励する Debian 
開発者らが Face to Face で Debian GNU/Linux のさまざまなトピック
（新しいパッケージ、Debian 特有の機能の仕組について、Debian 界隈で起こった出来事、 etc ）
について語り合うイベントです。</p>
<p>
参加される方は主に東京を中心に関東近郊の国籍・性別不問の Debian ユーザです 
( Debian をまだ使ったことが無いが興味があるので…とか、かなり遠い所から来てくださる方もいます)。
開発の最前線にいる Debian の公式開発者や開発者予備軍の方も居ますので、
普段は聞けないような様々な情報を得るチャンスです。
興味と時間があった方、是非御参加下さい。
（また、勉強会で話をしてみたいという方も随時募集しています）。</p>

<dl>
<dt>開催日時・会場</dt>
<dd>
   <ul>
   <li>日時：2012 年 5 月 19 日 ( 土曜日 ) 18:00-21:00</li>
   <li>費用：500 円</li>
   <li>会場：<a href="http://www2.city.suginami.tokyo.jp/map/detail.asp?home=H00150">あんさんぶる荻窪 第一教室</a></li>
   </ul>
   </ul>
</dd>
<dt>内容</dt>
<dd>
  <ol>
  <li><strong>Debian trivia quiz</strong><br>
Debian 関連のニュースにおいついていますか? 簡単なクイズで Debian 常識テストしちゃいます。</li>
  <li><strong>事前課題発表</strong><br>
皆さんの事前課題の回答を発表しちゃいます。
</li>
  <li><strong>coffeescriptを使ってみた</strong><br>
 Coffeescriptとはjavascriptを使いやすくしたプログラミング言語です。入門書を読んだので紹介します。</li>
  <li><strong>Python初心者が「Pythonプロフェッショナルプログラミング」を読んでみた</strong><br>
主にDebianパッケージ方面の視点から、先日読んだ書籍にツッコミを入れてみます
</li>
  </ol>
</dd>

<dt>参加申し込み方法</dt>
<dd>
<a href="https://tokyodebian-team.pages.debian.net/2012-05.html">勉強会のページ</a>を参照して、<strong> 5/17 （木） までに Debian 勉強会予約システムへの登録をしてください。
その際、事前課題がありますのでご回答をお願いします。</strong></dd>
<br>
<dt>事前課題（どちらかにご回答ください）</dt>
<dd>
<ol>
<li> Debian 勉強会参加者に紹介したい書籍を１冊以上挙げて、内容を簡単に紹介してください（特に技術書には限りません）</li>
<li> あなたが何かスクリプト言語をプログラミング初心者にお勧めするとして「その言語を選んだ理由」と「最初の一歩として案内する書籍／サイト」を教えてください。</li>
</ol>
</dd>
<dd>
<br>
回答は Debian 勉強会予約システムの課題回答フォームに入力してください。提出いただいた回答は全員に配布すること、
また内容の再利用を実施することを了承ください。 Debian 勉強会資料は GPL で公開します ( Web での公開が可能な内容でお願いします）。
</dd>
<dd>
課題内容の実施についてなにか質問があれば、 Debian ユーザ用公開メーリングリスト ( debian-users@debian.or.jp ) にてお願いします。</dd>

</dl>

<p>
この件に関するお問い合わせは Debian 勉強会主催者：上川 (dancer@{debian.org} ) までお願いいたします。</p>
