第 95 回 関西 Debian 勉強会のお知らせ

<p>
「<a href="http://wiki.debian.org/KansaiDebianMeeting/20150222">第95回 関西 Debian 勉強会</a>」
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>

<dl>
<dt>開催日時・会場・持ち物等</dt>
<dd>
  <ul>
    <li>日時： 2015 年 2 月 22 日 (日) 13:30 - 17:00 (開場 13:00)</li>
    <li>会場：<a href="http://www.city.osaka.lg.jp/shimin/page/0000016603.html">福島区民センター304号会議室</a>
(定員：20名)</li>
    <li>費用：500円 (部屋代、印刷代などの諸費用のため)</li>
  </ul>
</dd>

<dt>内容</dt>
<dd>
  <ol>

  <li><strong>Intro</strong><br>
  この勉強会の簡単な説明をします。
  </li>

  <li><strong>ライトニングトーク</strong>
  </li>

  <li><strong>もくもくの会</strong>
  </li>

  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>

<a href="http://debianjp.doorkeeper.jp/events/21030">Doorkeeper</a>を参照して、
事前登録をしてください。事情によりDoorkeeperを使えない/登録できない方は担当者まで連絡してください
(締切りに間に合わなかった方は、下記連絡先から担当者と連絡を取ってください。)。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：かわだてつたろう (t3rkwd&#64;{debian.or.jp} ) までお願いいたします。</p>
