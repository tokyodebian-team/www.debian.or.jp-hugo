第7回 関西 Debian 勉強会のお知らせ

<p>
10月の「<a href="http://wiki.debian.org/KansaiDebianMeeting20071007">第7回 関西 Debian 勉強会</a>」 
のお知らせです。関西 Debian 勉強会は、関西方面の Debian ユーザ／ユーザ予備群／開発者らで 
Debian GNU/Linux のさまざまなトピックについて Face to Face で楽しく話し合っていく集まりです。</p>
<p>
参加者には一般ユーザをはじめ、開発の最前線に立つ Debian の公式開発者や開発者予備軍の方もおりますので、
普段は聞けないような Debian に関する様々な情報を得るチャンスです。<br>
興味を持たれた方はぜひ御参加ください (また、勉強会で話をしてみたいという方も随時募集しています)。</p>


<dl>
<dt>開催日時・会場</dt>
<dd>
  <ul>
  <li>日時：2007年10月7日 (日) 13:45〜17:00 (13:30 より受付。17:30〜19:30 にて懇親会予定)</li>
  <li>会場：<a href="http://www.city.osaka.jp/shimin/shisetu/01/fukushima.html">大阪 
福島区民センター 301会議室</a></li>
  <li>費用：500円</li>
  </ul>
</dd>
<dt>内容</dt>
<dd>
  <ol>
  <li><strong>Intro (関西 Debian 勉強会についての説明)</strong>　山下 尊也 氏<br>
       参加される方々へ、この勉強会の目的の説明をさせていただきます。<br>
       前回、姫路獨協大学で行われた第 6 回関西 Debian 勉強会についてもその様子をお話しします。</li>

  <li><strong>Debianの日本語入力入門</strong>　あわしろ いくや 氏<br>
      Debian上での日本語入力を、Etchでの変更点を中心にお話しします。
      難しい話は一切ない予定なので、気軽にお聞きください。
      X上での日本語入力に限定しますので、あらかじめご了承ください。</li>

  <li><strong>Debian で使えるSPAMフィルタ(CRM114)</strong>　たなか としひさ氏<br>
      Debian etch では、Spamassassin や POPFile、bsfilter等、
      色々な SPAM フィルタを使う事が出来ますが、今回は、CRM114 
      と言う SPAM フィルタについてお話します。<br>
      まだ詳細な統計は取れていないのですが、CRM114 の使いかた、CRM114 
      での日本語 SPAM の検出方法についてお話します。
      サーバーサイドで動く SPAM フィルタをお探しの方にとって参考になればと思います。</li>
  </ol>
</dd>
<dt>参加方法と注意事項</dt>
<dd>
<a href="http://cotocoto.jp/event/1960">京都.cotocoto</a>を参照して、
<strong>10月5日まで</strong>に事前登録をしてください
（17:30 からの懇親会に参加する場合は、コメントに「宴会参加」と記入してください）。
</dd>
</dl>
<p>
この件に関するお問い合わせは 関西 Debian 勉強会 担当：山下尊也 (takaya&#64;{debian.or.jp} ) までお願いいたします。</p>
