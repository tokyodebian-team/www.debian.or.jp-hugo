Lenny 用 Debian-Installer RC1 リリース

<p>
次期リリースバージョン<a href="http://www.debian.org/devel/debian-installer/News/2008/20081112">「Lenny」用インストーラの最初のリリース候補版 (Release Candicate、RC版) の公開アナウンス</a>がありました（注：Debian 5.0 Lenny そのものではなく、<strong>Lenny 用インストーラの RC1 がリリースされた</strong>、ということ）。</p>
<p>
今回のリリースでは、既知の問題点の修正の他、サポート言語の追加、Xen ゲストのインストールイメージ追加、Live CD での安定性・速度の向上や、バッファロー社の玄箱Proのサポートなどが行われています。</p>
<p>
既知の問題点として、以下の３点が追加されています。
<ul>
<li>Linux カーネルのサイズが巨大になっているため、フロッピーディスクからのインストールはサポートされない
<li>グラフィカルインストーラでのタッチパッドのサポート機能が限定的
<li>PowerPC でのグラフィカルインストーラーの動作（ATI グラフィックカード以外での動作が不明）</ul>
皆さんも可能な限り、様々な環境でテストして、<a href="http://www.debian.org/devel/debian-installer/errata">正誤表</a>を確認の上、結果をメーリングリストやバグ報告システムなどにて教えて下さい。今ならまだ修正が間に合うでしょう :-)
</p>
