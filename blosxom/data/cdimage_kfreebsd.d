kFreeBSD イメージを cdimage.debian.or.jp にて提供開始

<p>
Debian JP Project は、国内 Debian CD イメージミラーサーバーとして運用している <a 
href="http://cdimage.debian.or.jp">cdimage.debian.or.jp</a> 
に <a href="http://cdimage.debian.or.jp/kfreebsd.html">Debian GNU/kFreeBSD のインストールイメージ</a>を追加しました。</p>
<p>
現在利用可能な kFreeBSD イメージは以下のとおりです。</p>
<ul>
<li>kfreebsd-amd64 </li>
<li>kfreebsd-i386 </li>
</ul>
<p>
利用された方は、ぜひフィードバックを<a href="http://www.debian.or.jp/community/ml/openml.html#usersML">メーリングリスト</a>や<a
href="http://www.debian.or.jp/project/organization.html">システム管理チーム</a>までお寄せください。</p>
<p>
cdimage.debian.or.jp の提供と運用にあたりましては、<a 
href="http://itsherpa.com/">株式会社アイティーシェルパ</a>様にご協力を頂いております。
この場を借りましてお礼申し上げます。</p>

