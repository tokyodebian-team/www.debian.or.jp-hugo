Software Design 2009 年 10 月号記事掲載「Debian GNU/Linuxカンファレンス「Debconf9」取材レポート」「第3世代LinkStationをハックしよう」

<p>
<a href="http://gihyo.jp/magazine/SD/archive/2009/200910"><img src="../image/book/sd200910.jpg"></a>
Debian JP Project 会員の執筆記事が雑誌に掲載されていますので、皆様にお知らせです。<br>
<a href="http://gihyo.jp/magazine/SD/archive/2009/200910">Software Design 2009 年 10 月号<strong>「Debian GNU/Linuxカンファレンス「Debconf9」取材レポート」</strong></a>をやまねひできが担当しました。こちらのイベントに関してはblog記事が<a href="http://kmuto.jp/d/index.cgi/travel/20090717-spain.htm">武藤さんのところ</a>からも参照出来ます。</p>
<p>
「第3世代LinkStationをハックしよう」として Debian をベースに使った、<a href="http://wiki.debian.org/KansaiDebianMeeting">関西 Debian 勉強会</a>などにもご参加頂いている<a href="http://www.yamasita.jp/">山下康成さん</a>の記事がのっています。こちらについても、興味のある方は現在発売中ですので、これを機会にぜひ書店でお手に取ってみてください。</p>
