さくらインターネット Debian ミラー提供開始

<p>
<a href="http://www.sakura.ad.jp/">さくらインターネット</a>樣にご協力いただき、Debian ミラーサーバーを <a href="http://debian-mirror.sakura.ne.jp/">debian-mirror.sakura.ne.jp
</a>として稼働開始しました。これにより、同社のVPS/クラウドサービスを利用するDebianユーザーは高速で安定したパッケージ取得が可能になります。ご協力頂きました さくらインターネット樣に感謝致します。</p>
<p>
また、同ミラーサーバーは CDN ミラーサーバに加わり、ftp.jp.debian.org として利用可能となっています。</p>
<p>
<a href="http://www.debian.or.jp/project/organization.html">システム管理チーム</a>では、CDN 対応ミラーサーバを随時募集しています。また、既存の pull 型ミラーサイトを push ミラー化することも推奨しております。同様に安定したミラーを提供頂ける組織／企業樣等ございましたら、ご相談ください。</p>
