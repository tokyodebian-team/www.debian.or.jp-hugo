Debian 7.4 リリース

<p>Debian GNU/Linux 7 コードネーム “wheezy” のポイントリリースが 2 月 8 日に行われ、バージョンが 7.4 となりました。</p>
<p>
本アップデートには、既存パッケージのセキュリティ修正および重要な問題への対応が行われています。更新は、通常のセキュリティアップデート同様 apt/aptitude を利用してインターネット経由で実施可能です。</p>
<p>
更新された内容の詳細については、<a href="http://www.debian.org/News/2014/20140208">7.4 についてのニュースリリース</a>、および<a href="http://ftp.debian.org/debian/dists/wheezy/ChangeLog">Changelog</a>の内容を参照してください。
</p>

