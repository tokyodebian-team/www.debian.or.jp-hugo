Software Design 2012 年 8 月号記事掲載「いま改めてお勧めするOSはこれだ！ FreeBSD/Debian/Ubuntu/CentOS/Gentoo」

<p>
<a href="http://gihyo.jp/magazine/SD/archive/2012/201208"><img src="../image/book/sd201208.jpg"></a>
Debian JP Project 会員の執筆記事が雑誌に掲載されていますので、皆様にお知らせです。<br>
<a href="http://gihyo.jp/magazine/SD/archive/2012/201208">Software Design 2012 年 8 月号<strong>第2特集
「いま改めてお勧めするOSはこれだ！ FreeBSD/Debian/Ubuntu/CentOS/Gentoo」</strong></a>を 
のがたじゅん と やまねひできが担当しました。</p>
<p>
6頁ほどの短かな記事ですが、Debian の魅力（フリーであること、パッケージ開発への努力、利用環境の多彩さと選択の広さ、
国内外で活発なコミュニティ）について語っています。是非、お手に取ってみてください。</p>
<p>
また、同誌の Ubuntu Monthly Report では、Debian のパッケージメンテナと Ubuntu 側とのやりとりや協力の過程を
「Debian からのパッケージインポート」と題した記事を読むことが出来ますので、こちらもおすすめです。</p>
