Debconf 7 のスポンサード登録および CFP の締切迫る

<p>
2007年6月17日〜6月23日にかけてスコットランドのエディンバラで開催される、Debian Project の大規模な会議 <a href="http://debconf7.debconf.org/">Debconf 7</a> のスポンサード参加事前登録 (宿泊・食事・旅費等の支援) および <a href="https://debconf7.debconf.org/wiki/Call_for_Papers">CFP</a> (発表論文応募) が1月31日で締め切られます。参加を検討している方はお早めに登録・応募を行ってください。
[<a href="http://lists.debian.org/debian-devel-announce/2007/01/msg00001.html">詳細</a>]
</p>
