Software Design 2014 年 8 月号「Debian Hot Topics」

<p>
<a href="http://gihyo.jp/magazine/SD/archive/2014/201408"><img src="http://www.debian.or.jp/image/book/sd201408.jpg"></a>
Debian JP Project 会員の執筆記事が雑誌に掲載されていますので、皆様にお知らせです。<br>
<a href="http://gihyo.jp/magazine/SD/archive/2014/201408">Software Design 2014 年 8 月号 にて<strong>
「Debian Hot Topics」</strong></a>が連載中です。
今回は
<ul>
<li>Debian GNU/Hurd の進捗状況
<li>Debian 6.0 "Squeeze" LTS の導入について
</ul>
などを紹介しています。<br>

どうぞご覧になって、是非感想等を Software Design 編集部樣にお寄せください (twitter で <a href="https://twitter.com/gihyosd">SoftwareDesign (@gihyosd)</a> や <a href="https://twitter.com/debianjp">DebianJP広報局 (@debianjp)</a> などへもどうぞ)。</p>
