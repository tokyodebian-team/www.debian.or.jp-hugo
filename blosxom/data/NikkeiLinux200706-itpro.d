日経 Linux 2007 年 6 月号「Debian 新入門」

<p>
<a href="http://itpro.nikkeibp.co.jp/article/MAG/20070507/270094/">日経 Linux 2007 年 6 月号</a>にて、
<strong>「Debian 新入門」</strong>として、Debian JP Project メンバーの
武藤健志・やまだあきらの両氏による記事が掲載されています。</p>
<p>
記事内容は、リリースされたばかりの Debian GNU/Linux 4.0 (Etch) の 
GUIインストーラを利用した導入とその後のソフトのインストールの説明から始まって、
どこでも読めるメールサーバ (IMAP4 サーバ) の構築など利用方法を紹介していく、
というものです。是非書店などでお手に取ってご覧ください
（日経 BP 社 ITPro のサイトにて、
<a href="http://itpro.nikkeibp.co.jp/article/MAG/20061204/255779/">web からも購入可能</a>です）。</p>
<p>
<small>なお、残念ながら 付属 DVD に収録されているのはインストール用 CD イメージであり、
DVD からは直接起動しないのでそのままではインストールは実行できません。
イメージを一旦 CD-R に書き込みをする必要があります。詳しくは
<a href="http://itpro.nikkeibp.co.jp/article/MAG/20070507/270081/">日経 Linux の補足情報ページ</a>
などをご覧ください。</small></p>
<p>
また、日経関連では ITPro のサイトで独自に編集された
<a href="http://itpro.nikkeibp.co.jp/article/COLUMN/20070425/269461/?ST=print">
「インストール完全ガイド　Debian GNU/Linux 4.0」</a>
というページが公開されています。
インストールの流れや雰囲気がどのようなものかをザッと知りたいという人は一度ご覧ください。
</p>

