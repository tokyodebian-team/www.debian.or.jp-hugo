OpenSSL/OpenSSH〜セキュリティ更新が確実に実施されているかを確認しましょう

<p>
既に OpenSSL パッケージの脆弱性に起因する問題については周知の通りかと思いますが、
古くからシステムを運用していてパッケージが更新されない、一度 testing などの stable 以外のリポジトリを指定した関係で stable での更新されたパッケージが導入されない場合があるなどの症例が一部報告されています。</p>
<p>
再度、サーバを運用されている方は、以下の点をチェックしてください。</p>
<ul>
<li>今回は aptitude upgrade ではなく aptitude <strong>dist-upgrade</strong> による更新が必要です。</li>
<li>openssl のライブラリパッケージ (libssl0.9.8) がセキュリティ更新されたものであるかを確認してください。<a href="http://packages.debian.org/etch/libssl0.9.8">こちら</a>で現在の安定版でのバージョンの確認が可能になっています。<br>
<pre>
$ dpkg -s libssl0.9.8 | grep Version
Version: 0.9.8c-4etch3</pre></li>
<li>openssh-server がセキュリティ更新されたものであるかを確認してください。<a href="http://packages.debian.org/etch/openssh-server">こちら</a>で現在の安定版でのバージョンの確認が可能になっています。<br>
<pre>
$ dpkg -s openssh-server | grep Version  
Version: 1:4.3p2-9etch2</pre></li>
<li>openssh-server がインストールされている場合は、openssh-blacklist も共にインストールされていることを確認してください。<a href="http://packages.debian.org/etch/openssh-blacklist">こちら</a>で現在の安定版でのバージョンの確認が可能になっています。<br>
<pre>
$ dpkg -s openssh-blacklist|grep Version
Version: 0.1.1</pre></li>
</ul>




