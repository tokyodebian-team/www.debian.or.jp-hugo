Software Design 2013 年 1 月号記事掲載「キーパーソンに聞く 2013年に来そうな技術・ビジョンはこれだ！」

<p>
<a href="http://gihyo.jp/magazine/SD/archive/2013/201301"><img src="../image/book/sd201301.jpg"></a>
Debian JP Project 会員の執筆記事が雑誌に掲載されていますので、皆様にお知らせです。<br>
<a href="http://gihyo.jp/magazine/SD/archive/2013/201301">Software Design 2013 年 1 月号<strong>第2特集
「キーパーソンに聞く 2013年に来そうな技術・ビジョンはこれだ！」</strong></a>内の Debian 関連部分を やまねひでき が担当しました。</p>
<p>
1.5頁ほどの短かな記事ですが、2013 年に Debian 界隈で起こりそうなトレンドを簡単に予測して語っています。是非、お手に取ってみてください。</p>

