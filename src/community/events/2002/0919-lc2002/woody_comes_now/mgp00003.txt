
Woody: What does it provide ?


  - Total of 11 architectures supported
         New in Woody: IA-64, HP PA-RISC, MIPS, and S/390
         Potato: PowerPC and ARM
         Slink: Alpha and Sparc
         Hamm: Motorola 680x0
              (1st multi-architecture support)
         Bo and the former: Intel x86 only

  - Over 9000 packages!
         Potato(2000.8): over 3900
         Slink(1999.3): 2250
         Hamm(1998.7): 1500+
         Bo(1997.7): 974
         
