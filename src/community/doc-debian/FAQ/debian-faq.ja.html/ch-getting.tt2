[% title = "The Debian GNU/Linux FAQ - Debian GNU/Linux の入手とインストール" %]

<p><a name="ch-getting"></a></p>
<hr>

<p>
[ <a href="ch-basic_defs.html">前のページ</a> ]
[ <a href="index.html#contents">目次</a> ]
[ <a href="ch-basic_defs.html">1</a> ]
[ 2 ]
[ <a href="ch-compat.html">3</a> ]
[ <a href="ch-software.html">4</a> ]
[ <a href="ch-ftparchives.html">5</a> ]
[ <a href="ch-pkg_basics.html">6</a> ]
[ <a href="ch-pkgtools.html">7</a> ]
[ <a href="ch-uptodate.html">8</a> ]
[ <a href="ch-kernel.html">9</a> ]
[ <a href="ch-customizing.html">10</a> ]
[ <a href="ch-support.html">11</a> ]
[ <a href="ch-contributing.html">12</a> ]
[ <a href="ch-redistrib.html">13</a> ]
[ <a href="ch-nexttime.html">14</a> ]
[ <a href="ch-faqinfo.html">15</a> ]
[ <a href="ch-compat.html">次のページ</a> ]
</p>

<hr>

<h2>
The Debian GNU/Linux FAQ
<br>第 2 章 - Debian GNU/Linux の入手とインストール
</h2>

<hr>

<h3><a name="s-version"></a>2.1 Debian の最新バージョンは何ですか?</h3>

<p>
現在 Debian GNU/Linux には 2 つのバージョンがあります。
</p>
<dl>
<dt><em>リリース 2.2。別名「stable (安定版)」</em></dt>
<dd>
<p>
これは安定版で充分テストされたソフトウェアですが、セキュリティ
や使用上の大きな修正が組み入れられたときには変更があります。
</p>
</dd>
</dl>
<dl>
<dt><em>「unstable (開発版)」ディストリビューション</em></dt>
<dd>
<p>
これは現在開発中のバージョンであり、絶え間なく更新されています。 任意の Debian
FTP サイトにある「unstable」なアーカイブからパッケージ
を取得でき、それらを使って好きなときにシステムをアップグレードできま
すが、システムが以前と同じように有用で安定していることを期待してはい
けません。だからこそ「<strong>unstable (開発版)</strong>」と呼ばれているので
す!
</p>
</dd>
</dl>

<p>
より詳しくは <a href="ch-ftparchives.html#s-dists"><samp>dists</samp>
ディレクトリにはどれくらいの Debian ディ ストリビューションがありますか?, 第
5.2 節</a> を見てください。
</p>

<hr>

<h3><a name="s-boot-floppies"></a>2.2 Debian のインストールディスクはどこからどのよう に入手できますか?</h3>

<p>
インストールディスクを入手するには Debian FTP サイトから適切なファイ
ルをダウンロードします。場所は <code><a
href="ftp://ftp.debian.org/pub/debian/">ftp://ftp.debian.org/pub/debian/</a></code>
とその <code><a href="http://www.debian.org/distrib/ftplist">ミラー</a></code>
です。
</p>

<hr>

<h3><a name="s-cdrom"></a>2.3 Debian を CD-ROM からインストールするにはどうしますか?</h3>

<p>
Linux は ISO 9660 (CD-ROM) ファイルシステムとその Rock Ridge 拡張 (以
前は「High Sierra」として知られていたもの) をサポートしています。いくつ かの
<code><a href="http://www.debian.org/distrib/vendors">ベンダ</a></code> がこ
のフォーマットの Debian GNU/Linux を提供しています。
</p>

<p>
警告: CD-ROM からインストールする場合、dselect の <samp>cdrom</samp> アク
セスメソッドを選択するのは通常あまり良い考えではありません。このメソッド
はとても遅いのが普通です。CD-ROM からインストールするには、例えば
<samp>mountable</samp> や <samp>apt</samp> メソッドの方がずっと良いでしょう (<a
href="ch-uptodate.html#s-dpkg-mountable">dpkg-mountable, 第 8.2.5 節</a> と <a
href="ch-uptodate.html#s-apt">APT, 第 8.2.1 節</a> を参照)。
</p>

<hr>

<h3><a name="s-cdimages"></a>2.4 私は CD ライターを持っています。どこかに CD イメージ はありますか?</h3>

<p>
あります。CD ベンダが高品質なディスクを容易に提供できるよう、我々は <code><a
href="http://cdimage.debian.org/">オフィシャル CD イメージ</a></code> を
提供しています。
</p>

<hr>

<h3><a name="s-floppy"></a>2.5 フロッピィをたくさん使ってインストールできますか?</h3>

<p>
初めに警告しておきます。標準的な 1.44MB のフロッピィディスクのように
小さなメディアからインストールするには Debian GNU/Linux
は大きすぎます。フロッピィ
からのインストールはあまり楽しい経験とはならないでしょう。
</p>

<p>
フォーマットされたフロッピィディスクにDebian パッケージをコピーしてく
ださい。DOS フォーマット、ネイティブな Linux の ext2 フォーマット、 minix
フォーマットが使えます。あとは使用するフロッピィに対して mount コ
マンドを適切に使うだけです。
</p>

<p>
フロッピィを使うのは次の点で厄介です。
</p>
<ul>
<li>
<p>
MS-DOS の短いファイル名: Debian パッケージを MS-DOS フォーマット
のディスクに配置しようとすると、たいていファイル名が長すぎるため、 MS-DOS の
8.3 文字というファイル名の制限を超えてしまうことに気付くで
しょう。これを解決するために Debian の開発者はすべてのパッケージを 8.3
のファイル名で利用できるようにしています。それらは別に分けた
「msdos」というサブディレクトリ (<samp>stable/msdos-i386/</samp>、
<samp>contrib/msdos-i386/</samp>、<samp>non-free/msdos-i386/</samp>)にありま
す。これらのディレクトリの中のファイルは Debian アーカイブのファイル
への単なるシンボリックリンクで、<samp>binary-i386/</samp> などのディレク
トリにあるファイルと何も違いません。
</p>
</li>
</ul>
<ul>
<li>
<p>
大きなファイルサイズ: パッケージの中には 1.44 M バイトを超えるも
のがあり、これらは 1 枚のフロッピィに収まりません。この問題を解決す るには
dpkg-split ツールを使います (<a
href="ch-pkgtools.html#s-dpkg-split">dpkg-split, 第 7.1.5 節</a> を参照)。
これは <code><a
href="ftp://ftp.debian.org/debian/">ftp://ftp.debian.org/debian/</a></code>
とその <code><a href="http://www.debian.org/distrib/ftplist">ミ ラー</a></code>
の <samp>tools</samp> ディレクトリで入手できます。
</p>
</li>
</ul>

<p>
フロッピィディスクに読み書きするにはカーネルでフロッピィディスクをサ
ポートするようにしなければなりません。ほとんどのカーネルにはフロッピィド
ライブのサポートがあらかじめ組み入れられています。
</p>

<p>
マウントポイント <samp>/floppy</samp> にフロッピィディスクをマウントする
には次のようにします (マウントポイントとなるディレクトリははインストール
中に作成されていなければなりません)。
</p>
<ul>
<li>
<p>
フロッピィディスクが A: ドライブにあって MS-DOS ファイルシステム の場合
</p>

<pre>
     mount -t msdos /dev/fd0 /floppy/
</pre>
</li>
</ul>
<ul>
<li>
<p>
フロッピィディスクが B: ドライブにあって MS-DOS ファイルシステム の場合
</p>

<pre>
     mount -t msdos /dev/fd1 /floppy/
</pre>
</li>
</ul>
<ul>
<li>
<p>
フロッピィディスクが A: ドライブにあって ext2 ファイルシステム (通常の Linux
のファイルシステム) の場合
</p>

<pre>
     mount -t ext2 /dev/fd0 /floppy/
</pre>
</li>
</ul>

<hr>

<h3><a name="s-remoteinstall"></a>2.6 Debian を遠隔のインターネットサイトから直接入手 してインストールできますか?</h3>

<p>
できます。このためにはいくつかの方法があります。
</p>
<ul>
<li>
<p>
<strong>APT を使う:</strong> <code>apt</code> パッケージをインストールし、
<samp>/etc/apt/sources.list</samp> ファイルをニーズに合わせて編集します。
最も近い Debian のミラーについての情報を含めるように変更するべきです。
これに関する詳細は <code>apt-get(8)</code> と <code>sources.list(8)</code>
のマニュアルページにあります。APT ユーザーズガイド
<samp>/usr/share/doc/apt/guide.html/index.html</samp> にもあります。
</p>

<p>
そうしてから
</p>

<pre>
     apt-get update
</pre>

<p>
を実行し、続いて
</p>

<pre>
     apt-get upgrade
</pre>

<p>
を実行します。するとシステムは最新の Debian リリースにアップグレード されます。
</p>
</li>
</ul>
<ul>
<li>
<p>
<strong>dselect と dpkg-ftp を使う:</strong> <code>dpkg-ftp</code> パッケージ
(<a href="ch-uptodate.html#s-dpkg-ftp">dpkg-ftp, 第 8.2.2 節</a> を参照)
をインス トールします。
</p>

<p>
それからプログラム <code>dselect</code> を起動します。これは
<code>dpkg-ftp</code> を呼び出し、パッケージ選択について案内して、パッケー
ジ自体をあなたのマシンにいちいちダウンロードせずにインストールします。
この方法はディスク容量と時間の両方を節約するために設計されています。
</p>

<p>
dselect は、使おうとする ftp サイトの完全修飾ドメイン名を聞いてき ます。どの
ftp サイトを指定したらよいのか分からなかったら ftp.debian.org か
http.us.debian.org を試してみてください。
</p>

<p>
インストールしたいファイルが入っているディレクトリまたはサブディ
レクトリも聞かれます。このディレクトリには「Packages」(もしくは圧縮
された「Packages.gz」) というファイルがなければなりません。通常
dists/stable/main/binary-ARCH となります。ARCH はあなたのマシンのアー
キテクチャです。
</p>
</li>
</ul>

<p>
注意していただきたいのは、パッケージを取得しインストールするときにそ れらが
/var ディレクトリ階層に保持されるということです。したがって余分な
ファイルを削除するかどこか他へ移動させておいてください (ヒント:
<code>apt-move</code> を使ってください)。そうしないとパーティションがオーバー
フローするかもしれません。
</p>

<hr>

<h3><a name="s-tape"></a>2.7 Debian をテープデバイスから取得・インストールできますか?</h3>

<p>
現在のところ、テープからの直接のインストールはサポートされていません。
しかし、<samp>tar</samp> や <samp>cpio</samp>・<samp>afio</samp> を使って
Debian アー
カイブファイルをテープにコピーし、それらをローカルのディスクにコピーして
インストールすることは可能です。同じような調子で、「tar」ファイルの入って
いるフロッピィディスクを、Debian パッケージツールで管理される前にローカ
ルのディスクにコピーしておかなくてはなりません。
</p>

<hr>

<p>
[ <a href="ch-basic_defs.html">前のページ</a> ]
[ <a href="index.html#contents">目次</a> ]
[ <a href="ch-basic_defs.html">1</a> ]
[ 2 ]
[ <a href="ch-compat.html">3</a> ]
[ <a href="ch-software.html">4</a> ]
[ <a href="ch-ftparchives.html">5</a> ]
[ <a href="ch-pkg_basics.html">6</a> ]
[ <a href="ch-pkgtools.html">7</a> ]
[ <a href="ch-uptodate.html">8</a> ]
[ <a href="ch-kernel.html">9</a> ]
[ <a href="ch-customizing.html">10</a> ]
[ <a href="ch-support.html">11</a> ]
[ <a href="ch-contributing.html">12</a> ]
[ <a href="ch-redistrib.html">13</a> ]
[ <a href="ch-nexttime.html">14</a> ]
[ <a href="ch-faqinfo.html">15</a> ]
[ <a href="ch-compat.html">次のページ</a> ]
</p>

<hr>

<p>
The Debian GNU/Linux FAQ
</p>

<address>
version CVS, 2 January 2007<br>
<br>
<a href="ch-faqinfo.html#s-authors">著者, 第 15.1 節</a><br>
<br>
</address>
<hr>

