<!doctype debiandoc public "-//DebianDoc//DTD DebianDoc//EN" [
  <!entity % faqstaticdata  system "faqstatic.ja.ent"  > %faqstaticdata;
  <!entity % faqdynamicdata system "faqdynamic.ja.ent" > %faqdynamicdata;
]>
<debiandoc>
 <book id="debian-faq">

  <titlepag>
  <title>The &debian; FAQ
  <author><ref id="authors"></author>
  <version>version &docversion;, &docdate;</version>
  <abstract>
<!--
  This document answers questions frequently asked about &debian;.
-->
  この文書は &debian; についてたびたび尋ねられる質問に答えたものです。
  </abstract>

  <copyright>
  <copyrightsummary>
  Copyright &copy; 1996-2000 by Software in the Public Interest
  </copyrightsummary>

  <p>Permission is granted to make and distribute verbatim copies of
  this document provided the copyright notice and this permission notice
  are preserved on all copies.

  <p>Permission is granted to copy and distribute modified versions of
  this document under the conditions for verbatim copying, provided that
  the entire resulting derived work is distributed under the terms of
  a permission notice identical to this one.

  <p>Permission is granted to copy and distribute translations of this
  document into another language, under the above conditions for
  modified versions, except that this permission notice may be included
  in translations approved by the Free Software Foundation instead of
  in the original English.

  <p>この著作権表示と許諾条件告知が記載してあれば、何も手を加えずにこの
  文書を複製し配布してもかまいません。

  <p>上記の複写条件に加えて、この文書から派生した著述全体がこの許諾条件
  と同じ条件で配布されるならば、この文書に変更を加えたものを複写して配布
  してもかまいません。

  <p>変更した版に対する上記の許諾条件と同じ条件で、この文書を他の言語に
  翻訳したものを複写して配布してもかまいません。ただし、この許諾条件告知
  は、原文の英語版の代わりにフリーソフトウェア財団が承認した翻訳を含めて
  もかまいません。

  <p>(訳註) 上記翻訳は FSF の承認を得たものではありません。SPI に問い合
  わせたところ、この FAQ の再配布は以下の条件に従うようにとの返答をいた
  だきました。

  <p>Copyright 1997 Software in the Public Interest (SPI)

  <p>P.O. Box 70152 

  <p>Pt. Richmond, CA 94807-0152.

  <p>Verbatim copying and distribution is permitted in any medium,
  provided this notice is preserved. You may translate these documents
  and their licence into another language providing:

  <list>
  <item>
    <p>You do not deliberately change their meaning beyond changes
    meant to achieve a coloquial rendering in another language.
  </item>

  <item>
  <p>Translations of the licence must be clearly marked as
  translations, and the licence in its original language shall
  continue to apply to all translations.
  </item>

  <item>
  <p>In the case of hypertext pages, you must maintain a copy of the
  oroginal page on the sme site, and must provide a link from the
  translated page to its original.
  </item>
  </list>

  <p>この著作権表示が記載してあるならば、どのような媒体にでも何も手を加
  えずに複写して再配付することを許可します。これらのドキュメントとその使
  用許諾を他の言語に翻訳する際は、以下の条件に従って下さい。

  <list>
  <item>
  <p>他の言語に翻訳する際に、他の言語での口語表現への変更の範囲を越えて
  故意に意味を変更しないこと。
  </item>

  <item>
  <p>翻訳した使用許諾には、それが翻訳であること、また原文の使用許諾が翻
  訳全体に依然適用されることを明示すること。
  </item>

  <item>
  <p>ハイパーテキストの場合、同一サイト上で原文のコピーを保守すること。
  また、翻訳したハイパーテキストのページからその原文へのリンクを提供する
  こと。
  </item>
  </list>

  <p>なお日本語訳については、笹井 崇司、橋本 喜代太、八田 修三、
  前原 恵太 (1997 年)、関戸 幸一 (2000 年) に著作権があります。

  </copyright>

  </titlepag>

  <toc detail="sect1">

  &BasicDefs;
  &Getting;
  &Compat;
  &Software;
  &FtpArchives;
  &PkgBasics;
  &PkgTools;
  &UpToDate;
  &Kernel;
  &Customizing;
  &Support;
  &Contrib;
  &Redist;
  &NextTime;
  &FAQInfo;

 </book>
</debiandoc>
