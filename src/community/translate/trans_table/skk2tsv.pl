#!/usr/bin/perl
# -*- mode: perl -*-
# Copyright (C) 1998, Mitsuru Oka <oka@debian.or.jp>

%words = {};                    # insert a,b,c,...,z,etc.. of array

# 擬似 skk フォーマットをパース、中間テーブル構築
while(<>) {
    next if /^(;.*|)$/;
    m|([a-zA-Z][a-zA-Z0-9-.\s]*)\s+/(.+)/$| || die "error `$_'";
    my $keyword = $1;
    my $key = uc(substr($keyword,0,1));
    my $transwords = $2;

    if ($word{$key}) {
        $word{$key}->{$keyword} = $transwords;
    } else {
        $word{$key} = {};
        $word{$key}{$keyword} = $transwords;
    }
}

$current_time = localtime time; # 更新日付

# tsv 生成
foreach (sort keys %word) {
    my $key = $_;

    my $c_word = $word{$key};
    foreach (sort keys %$c_word) {
        my $keyword = $_;
        print <<"EOL";
$keyword\t$c_word->{$keyword}\t
EOL
    }
}
