#!/usr/bin/perl
# table.skk を dict 形式辞書に変換

# Copyright (c) 1999, Yoshizumi Endo <y-endo@debian.or.jp> *)
#
# *) Base64 encoding については dictd-1.4.9 (c) Rickard E. Faith
#    (faith@acm.org) のコードを参考にしました。

# dictd で利用する場合 index は sort が必要
open(OINDEX, "| sort -f > ./trans_table.index" ) || die "Can't open: $!\n"; 
open(OBODY,  "> ./trans_table.dict"  ) || die "Can't open: $!\n"; 

# ヘッダ部
@time = localtime;
@date = ($time[5],$time[4]+1,$time[3]);
$fomat_date = join("/",@date);

$headline = "00-database-short";
$body = "$headline\n    Debian JP 対訳表 ($fomat_date)\n";

&output;

# 辞書本体
while (<>) { 
    unless (/^[;\n]/) {
	chomp;
	@word=split(' +/');
	$headline = $word[0];             # 英単語の見出し
	$body = "$headline\n\n    /$word[1]\n\n";
	&output;
    }
}

close(OINDEX);
close(OBODY);

exit;

# ファイル出力
sub output{
    $length = length($body);
    $en_start = &encode($start);
    $en_length = &encode($length);
    
    print OBODY "$body";
    print OINDEX "$headline\t$en_start\t$en_length\n";
	
    $start = $start + $length;  
}

# Base64 エンコーディング
sub encode{
    @b64=('A'..'Z','a'..'z','0'..'9','+','/');

    $decoded[0] = $b64[(($_[0] & 0xc0000000) >> 30)] ;
    $decoded[1] = $b64[(($_[0] & 0x3f000000) >> 24)] ;
    $decoded[2] = $b64[(($_[0] & 0x00fc0000) >> 18)] ;
    $decoded[3] = $b64[(($_[0] & 0x0003f000) >> 12)] ;
    $decoded[4] = $b64[(($_[0] & 0x00000fc0) >>  6)] ;
    $decoded[5] = $b64[ ($_[0] & 0x0000003f)       ] ;

    $result = join("",@decoded);
    $result =~ s/^A{1,5}//g;

    return $result;
}
