[% title = "Debian ポリシーマニュアル - ファイル" %]

<p><a name="ch-files"></a></p>
<hr>

<p>
[ <a href="ch-opersys.html">previous</a> ]
[ <a href="index.html#contents">Contents</a> ]
[ <a href="ch-scope.html">1</a> ]
[ <a href="ch-archive.html">2</a> ]
[ <a href="ch-binary.html">3</a> ]
[ <a href="ch-source.html">4</a> ]
[ <a href="ch-controlfields.html">5</a> ]
[ <a href="ch-maintainerscripts.html">6</a> ]
[ <a href="ch-relationships.html">7</a> ]
[ <a href="ch-sharedlibs.html">8</a> ]
[ <a href="ch-opersys.html">9</a> ]
[ 10 ]
[ <a href="ch-customized-programs.html">11</a> ]
[ <a href="ch-docs.html">12</a> ]
[ <a href="ap-pkg-scope.html">A</a> ]
[ <a href="ap-pkg-binarypkg.html">B</a> ]
[ <a href="ap-pkg-sourcepkg.html">C</a> ]
[ <a href="ap-pkg-controlfields.html">D</a> ]
[ <a href="ap-pkg-conffiles.html">E</a> ]
[ <a href="ap-pkg-alternatives.html">F</a> ]
[ <a href="ap-pkg-diversions.html">G</a> ]
[ <a href="ch-customized-programs.html">next</a> ]
</p>

<hr>

<h2>
Debian ポリシーマニュアル
<br>Chapter 10 - ファイル
</h2>

<hr>

<h2 id="s-binaries">10.1 バイナリファイル</h3>

<p>
二つのパッケージが、異なった機能を持つ同じ名前のプログラムを
インストールする事は許されていません。(二つのパッケージが同
じ機能を提供するが、その実装が異なっている場合には代替 (alternatives)
機能または競合 (Conflicts) 機能を使って処理してください)。これらについては順に
<a href="ch-binary.html#s-maintscripts">メンテナスクリプト, Section 3.9</a> と
<a href="ch-relationships.html#s-conflicts">競合するバイナリパッケージ -
<samp>Conflicts</samp>, Section 7.4</a> を参照ください。
この状況が発生した場合には一方のプログラムが名前を変更しなければなりません。
メンテナは名前が衝突していることを <samp>debian-devel</samp>
メーリングリストに報告して、
どちらのパッケージの名前を変更する方がいいのかの合意を得るようにしてください。
合意が得られない場合には、<em>両方の</em>
プログラムが名前を変更しなければなりません。
</p>

<p>
標準では、パッケージが作成される際には、任意のバイナリはデバッグ情報付きで作成されるべきで、また最適化も行われるべきです。
また、コンパイル時の警告メッセージもできるだけ有効にすべきです。
これは、問題の可能性の有無をビルド時のログを見て判断する移植者の作業を容易にします。
C 言語の場合、上記のことから通常は次のコンパイル時の引数を使うべきです。
</p>
<pre>
     CC = gcc
     CFLAGS = -O2 -g -Wall # warning オプションは変更可です。
     LDFLAGS = # なし
     INSTALL = install -s # (または、debian/tmp で strip をかけてください)
</pre>

<p>
バイナリファイルは標準的には strip
をかけておくべきであることに留意してください。 これは <code>install</code> の
<samp>-s</samp> フラグか、パッケージツリーを作成する前にいったんプログラムを
<code>debian/tmp</code> に移して <code>strip</code>
を呼び出すかのどちらかで行ってください。
</p>

<p>
ビルドツリーでのバイナリはデバッグ情報付きでコンパイルされるべきですが、それでもコンパイラの最適化のためにデバッグはしばしば困難になります。
このため、標準環境変数 <samp>DEB_BUILD_OPTIONS</samp> のサポートを推奨します
(<a href="ch-source.html#s-debianrules-options"><code>debian/rules</code>
および <samp>DEB_BUILD_OPTIONS</samp>, Section 4.9.1</a> 参照)。
この変数は、パッケージコンパイルとビルドのやり方を変更する幾つかのフラグを含んでいます。
</p>

<p>
コンパイルオプションに最も適したものを選ぶのはメンテナの判断に任せています。
ある種のバイナリ (たとえば計算量の多いプログラム) にはそれなりのフラグ
(<samp>-O3</samp> など)
の方が実行時の効率が上がるでしょうし、そういうときには自由にそのようなフラグを使ってもかまいません。
的確な判断を行ってください。 漠然とした考えでフラグを設定しないでください。
そうする理由があるときのみに限ってください。
どのコンパイルオプションが適切かについての上流の作者の考えを変更するのは自由です。
なぜならしばしば私たちの環境では適さないものが使われていますので。
</p>

<hr>

<h2 id="s-libraries">10.2 ライブラリ</h3>

<p>
もし、パッケージが <strong>architecture: any</strong>
となっている場合には、共有ライブラリのコンパイルとリンクフラグには
<samp>-fPIC</samp> を含めなければいけません。
さもなければ、一部のサポートされているアーキテクチャでビルドに失敗します [<a
href="footnotes.html#f84" name="fr84">84</a>] 。この規則の例外は必ず
<em>debian-devel&#64;lists.debian.org</em>
メーリングリストで議論して、一応の合意を得ておく必要があります。
<samp>-fPIC</samp> フラグを使わないでコンパイルした理由は必ず
<samp>README.Debian</samp> ファイルに記載をしなければいけませんし、
同時にアーキテクチャを制限するか、必要なアーキテクチャでは <samp>-fPIC</samp>
を用いるようにするケアを行わなければいけません [<a href="footnotes.html#f85"
name="fr85">85</a>]。
</p>

<p>
言葉を換えれば、もし共有ライブラリとスタティックライブラリの両方をビルドする場合には、各ソース部分
(C 言語では <samp>*.c</samp> に当たる) は二回コンパイルする必要があります。
</p>

<p>
ライブラリは、スレッドサポートを行うようビルドすべきで、さらにライブラリのサポートがある場合はスレッドセーフとすべきです。
</p>

<p>
ビルドツールで強制されているわけではありませんが、共有ライブラリはバイナリ同様、含まれるシンボルを用いる全てのライブラリにリンクするようビルドされていなければいけません。
これにより、<a href="ch-sharedlibs.html#s-sharedlibs-symbols">symbols</a> と <a
href="ch-sharedlibs.html#s-sharedlibs-shlibdeps">shlibs</a>
システムの正しい動作と、全てのライブラリが <samp>dlopen()</samp>
により安全に開くことができることが保証されます。
パッケージメンテナは、共有ライブラリ作成の際に gcc オプション
<samp>-Wl,-z,defs</samp> を使いたいかもしれません。
これらのオプションはビルド時のシンボル解決を強制するため、ライブラリの参照関係の抜けは致命的なビルドエラーとして早いうちに捕まえることが可能です。
</p>

<p>
インストールされる共有ライブラリは次のようにして strip されているべきです。
</p>
<pre>
     strip --strip-unneeded <var>your-lib</var>
</pre>

<p>
(このオプション `--strip-unneeded' は <code>strip</code>
にリロケーション処理に必要のないシンボルのみを削除するように指示します)
ダイナミックリンクの際に使用するシンボルは別の ELF
オブジェクトにあるので、共有ライブラリは strip されても完全に機能 [<a
href="footnotes.html#f86" name="fr86">86</a>] します。
</p>

<p>
ある種の場合、たとえばデバッグ機能を持った別パッケージをビルドする場合など、
strip
しない共有ライブラリを作成したほうがいい場合もあることに気をつけてください。
</p>

<p>
共有のオブジェクトファイル (多くの場合は、 <code>.so</code> ファイルです)
で、汎用のライブラリではない (すなわち、一連のものとは独立の実行ファイル
(バイナリや他のパッケージからリンクされることを想定していない)
ものは、<code>/usr/lib</code>
ディレクトリのサブディレクトリにインストールすべきです。
このようなファイルは通常の共有ライブラリに適用される規則に沿わなくともかまいませんが、
実行可能ビットを立ててインストールしてはいけないこと、及び strip
すべきこと、の二つの規則は守る必要があります [<a href="footnotes.html#f87"
name="fr87">87</a>]。
</p>

<p>
共有ライブラリを作成・インストールする際に <code>libtool</code>
を使うパッケージは、追加のメタデータを含むファイル (<code>.la</code>
という拡張子を持つファイル) をライブラリ以外にインストールします。
他のパッケージからの利用を想定した公開ライブラリでは、これらのファイルを Debian
パッケージには通常は収録すべきではありません。
これは、このファイルに含まれる情報は、Debian
では共有ライブラリとリンクする際に必要ではなく、さらに他のプログラムやライブラリへの不要な依存関係を追加してしまうためです[<a
href="footnotes.html#f88" name="fr88">88</a>]。 ライブラリに <code>.la</code>
ファイルが必要な場合 (例えば、 メタ情報が必要となるやり方で
<samp>libltdl</samp> よりロードされる場合など) <code>.la</code> ファイルの
<samp>dependency_libs</samp> 設定には、通常は空文字列をセットすべきです。
共有ライブラリの開発向けパッケージで、歴史的な経緯から <code>.la</code>
が含まれている場合、開発向けパッケージには、それに依存している全てのライブラリに収録された
<code>.la</code> ファイル内の <samp>dependency_libs</samp>
が削除されるか空になるまで (つまり、<code>libtool</code>
を使った他のパッケージのリンクが失敗しないようになるまで)、 それを維持
(<samp>dependency_libs</samp> を空にして) しなければいけません。
</p>

<p>
<code>.la</code> を含めなければならない場合で、ライブラリが
<code>libtool</code> の <samp>libltdl</samp>
ライブラリからロードされるのでなければ、 <code>.la</code>
ファイルは開発用パッケージ (<samp>-dev</samp>) に含めるべきです。
<samp>libltdl</samp> での利用を想定している場合には、<code>.la</code>
ファイルはランタイムパッケージに含めなければいけません。
</p>

<p>
以上の <code>.la</code> ファイルの扱いに関する規定は、ローダブルモジュールや、
標準状態ではダイナミックリンカがサーチしないディレクトリにインストールされるライブラリに対しては適用されません。
ローダブルモジュールをインストールするパッケージは、多くの場合モジュールに加えて
<code>.la</code> ファイルをインストールし、<samp>libltdl</samp>
によってロードできるようにする必要があるでしょう。 <samp>dependency_libs</samp>
は、ダイナミックリンカがサーチしないディレクトリにインストールされ、
他のパッケージからの利用を意図していないライブラリやモジュールの場合は、変更する必要はありません。
</p>

<p>
自分のパッケージを作成する際には、リリースされている版の共有ライブラリのみを使うように気をつけなければいけません。
ここで間違えるとほかのユーザはあなたのプログラムを正しく実行することができません。
リリースされていないコンパイラに依存するパッケージを作成することも通常好ましくありません。
</p>

<hr>

<h2 id="s10.3">10.3 共有ライブラリ</h3>

<p>
この節は <a href="ch-sharedlibs.html">共有ライブラリ, Chapter 8</a>
に移されました。
</p>

<hr>

<h2 id="s-scripts">10.4 スクリプト</h3>

<p>
パッケージ中に含まれ <code>dpkg</code>
が使用するメンテナスクリプトも含めて、すべてのコマンドスクリプトはスクリプトを解釈するインタープリタを
<samp>#!</samp> 行を追加して指定しなければいけません。
</p>

<p>
それが perl スクリプトの場合は、その行は <samp>#!/usr/bin/perl</samp>
でなければいけません。
</p>

<p>
シェルスクリプトをシステムの PATH
内のディレクトリにインストールする場合、スクリプト名には <samp>.sh</samp> や
<samp>.pl</samp>
などの、スクリプトを実装するのに現在使っている言語を示す拡張子を含めるべきではありません。
</p>

<p>
シェルスクリプト (<code>sh</code> や <code>bash</code> を使う)
で、<code>init.d</code> 以外のものは、特殊な場合をのぞいて最初に <samp>set
-e</samp> を書いてエラーを検出するようにすべきです。 <code>init.d</code>
スクリプトはここでは特殊な場合で、失敗することが許されるコマンドを実行する必要がある頻度が異なるため、代わりにコマンドの終了ステータスをチェックするほうが容易でしょう。
<code>init.d</code> スクリプトについての詳細は <a
href="ch-opersys.html#s-writing-init">スクリプトの書き方, Section 9.3.2</a>
を参照ください。
</p>

<p>
全てのスクリプトは、<samp>set -e</samp> を使うか、<em>すべての</em>
コマンドの終了ステータスをチェックすべきです。
</p>

<p>
スクリプトは、SUSv3 Shell Command Language 規定[<a href="footnotes.html#f89"
name="fr89">89</a>]の仕様に沿っており、かつ SUSv3
で必須とはされていない以下の機能 [<a href="footnotes.html#f90"
name="fr90">90</a>] をもつような <code>/bin/sh</code>
の実装であることを仮定してかまいません。
</p>
<ul>
<li>
<p>
<samp>echo -n</samp>
これがシェルの内部コマンドとして実装されている場合、改行を挿入してはいけません。
</p>
</li>
</ul>
<ul>
<li>
<p>
<samp>test</samp> これがシェルの内部コマンドとして実装されている場合、
<samp>-a</samp> と <samp>-o</samp>
を二値の論理値を返すオペレータとしてサポートしていなければいけません。
</p>
</li>
</ul>
<ul>
<li>
<p>
スコープをもつ変数を作成する <samp>local</samp> を、複数の変数を一つの local
コマンドに並べて、ローカル変数として生成するとともに初期値を設定することを含めてサポートしていなければいけません。
但し、<samp>local</samp>
が代入を伴わない場合は、スコープ外からの変数の値を保持してもしなくてもかまいません。
即ち、以下のような使用方法はサポートされ、<samp>c</samp> の値は
<samp>delta</samp> に設定されなければいけません。
</p>
<pre>
     fname () {
         local a b c=delta d
         # ... use a, b, c, d ...
     }
</pre>
</li>
</ul>
<ul>
<li>
<p>
<samp>kill -<var>signal</var></samp> という書式を許している <code>kill</code>
の XSI 機能拡張は、<code>kill</code>
がシェルのビルトインコマンドとして実装されている場合でも、<var>signal</var>
としてシグナル名か、XSI 機能拡張で列挙されている数字で指定したシグナル (0, 1,
2, 3, 6, 9, 14, および 15) を、サポートしている必要があります。
</p>
</li>
</ul>
<ul>
<li>
<p>
数字で指定するシグナルを許す <code>trap</code> の XSI
機能拡張をサポートする必要があります。
機能拡張で列挙されている、<code>kill</code> と同じシグナル番号群以外に、 13
(SIGPIPE) を許さなければいけません。
</p>
</li>
</ul>

<p>
もし、シェルスクリプトが上記記載以外の SUSv3
規定外の機能をシェルインタープリタで必要とする場合、適切なシェルをスクリプトの最初の行に
(例えば <samp>#!/bin/bash</samp> のように) 指定しなければならず、
そのシェルを提供しているパッケージに対する依存関係をパッケージで指定しなければいけません
(そのシェルパッケージが &quot;Essential&quot; 扱いになっている、例えば
<code>bash</code> のような場合は依存関係の指定は不要です)。
</p>

<p>
スクリプトをできる限り SUSv3
規定の機能および上記の追加機能だけを用いて書くようにし、 <code>/bin/sh</code>
をインタープリタとして使えるようにしたいと思う場合、あなたのスクリプトを
<code>devscripts</code> パッケージの <code>checkbashisms</code>
を使ってチェックするか、<code>posh</code>
などの代替シェルでスクリプトを動かしてテストを行えば、上記の制限に対する違反の発見に有益でしょう。
但し、疑問があるなら明示的に <code>/bin/bash</code> を使ってください。
</p>

<p>
Perl
スクリプトでは、システムコールを使ったときにはエラーをチェックしてください。
システムコールには <samp>open</samp>、
<samp>print</samp>、<samp>close</samp>、<samp>rename</samp>、
<samp>system</samp> などが含まれます。
</p>

<p>
<code>csh</code> や <code>tcsh</code>
をスクリプト言語に使うのは避けるべきです。 この理由は <samp>comp.unix.*</samp>
の FAQ の一つ <em>Csh Programming Considered Harmful</em> (<code><a
href="http://www.faqs.org/faqs/unix-faq/shell/csh-whynot/">http://www.faqs.org/faqs/unix-faq/shell/csh-whynot/</a></code>)
を参考にしてください。 上流のパッケージに <code>csh</code>
スクリプトが含まれているときには、 そのスクリプトが <samp>#!/bin/csh</samp>
で始まり、そのパッケージを <code>c-shell</code>
仮想パッケージに依存するよう設定したことをよく確認してください。
</p>

<p>
誰からでも書けるディレクトリに (例えば <code>/tmp</code> に)
ファイルを作成するスクリプトは、作成しようとするファイルと同じ名前のファイルが存在したら、他に何もせずに失敗するような機構を組み込んでください。
</p>

<p>
Debian の base システムに含まれる <code>tempfile</code> と <code>mktemp</code>
ユーティリティはこの目的でスクリプト中で使うためのものです。
</p>

<hr>

<h2 id="s10.5">10.5 シンボリックリンク</h3>

<p>
通常、トップレベルディレクトリ (ルートディレクトリ <code>/</code>
のサブディレクトリがトップレベルディレクトリです)
内のシンボリックリンクは相対参照とすべきです。
また、トップレベルディレクトリ間のシンボリックリンクは絶対参照とすべきです。
例えば、<code>/usr/lib/foo</code> から <code>/usr/share/bar</code>
へのシンボリックリンクは相対参照とすべき (<code>../share/bar</code>) ですが、
<code>/var/run</code> から <code>/run</code>
へのシンボリックリンクは絶対参照とすべきです [<a href="footnotes.html#f91"
name="fr91">91</a>]。
</p>

<p>
それに加えて、シンボリックリンクは可能な限り短くすべきです。 例えば
<code>foo/../bar</code> のような参照は好ましくありません。
</p>

<p>
<code>ln</code> を使って相対リンクを作成するときに、<code>ln</code>
を実行する作業用ディレクトリからの相対位置にリンク先が存在している必要はありません。
また、リンクを作成しようしているディレクトリに作成の際にディレクトリを移動する必要もありません。
やるべき事は単にリンク先
(リンクが存在するディレクトリからの相対参照になるようなパス名です) が
<code>ln</code> の最初の引数に現れるよう文字列を与えるだけです。
</p>

<p>
例えば、<code>Makefile</code> や <code>debian/rules</code>
で次のような処理を行ってください。
</p>
<pre>
     ln -fs gcc $(prefix)/bin/cc
     ln -fs gcc debian/tmp/usr/bin/cc
     ln -fs ../sbin/sendmail $(prefix)/bin/runq
     ln -fs ../sbin/sendmail debian/tmp/usr/bin/runq
</pre>

<p>
圧縮されたファイルを参照するシンボリックリンク (<code>unzip</code> または
<code>zless</code> などで伸長することを意図したもの)
は、対象ファイルと同じ拡張子を持つようにすべきです。
例えば、<code>foo.gz</code>
をシンボリックリンクで参照する場合、リンクのファイル名も同様に
&quot;<code>.gz</code>&quot; で終わる、<code>bar.gz</code>
のような名前にしてください。
</p>

<hr>

<h2 id="s10.6">10.6 デバイスファイル</h3>

<p>
パッケージファイルツリーにデバイスファイルや名前付きパイプを含めることは許されていません。
</p>

<p>
パッケージが base
システムに含まれていない特殊なデバイスファイルを必要とする場合には、
<code>postinst</code> スクリプト中でユーザに許可を問い合わせた後
<code>MAKEDEV</code> を呼び出してください[<a href="footnotes.html#f92"
name="fr92">92</a>] 。
</p>

<p>
パッケージから <code>postrm</code>
や、その他のスクリプト中でデバイスファイルを削除してはいけません。
そのような処置は管理者に任せてください。
</p>

<p>
Debian ではシリアルデバイスファイル名に <code>/dev/ttyS*</code>
を使います。昔流の <code>/dev/cu*</code> を使っているプログラムは
<code>/dev/ttyS*</code> に変更すべきです。
</p>

<p>
パッケージで必要な名前付きパイプは、<code>postinst</code>
スクリプトで作成しなければいけません [<a href="footnotes.html#f93"
name="fr93">93</a>]。また、必要に応じて <code>prerm</code> または
<code>postrm</code> スクリプトで削除しなければいけません。
</p>

<hr>

<h2 id="s-config-files">10.7 設定ファイル</h3>

<hr>

<h3 id="s10.7.1">10.7.1 定義</h4>
<dl>
<dt>設定ファイル</dt>
<dd>
<p>
これは、プログラムの実行に影響を与えるファイル、
またはサイトやホスト固有の情報を提供するファイル、
またはプログラムの挙動を制御するためのファイルです。
通常、設定ファイルはシステム管理者の手で、
ローカルの方針やサイト個別要件にあわせた挙動をさせるために、
必要に応じて変更されることを想定しています。
</p>
</dd>
</dl>
<dl>
<dt><samp>conffile</samp></dt>
<dd>
<p>
パッケージの <samp>conffiles</samp>
ファイル中に列挙されたファイルで、<code>dpkg</code> から特別の扱いを受けます
(<a href="ch-maintainerscripts.html#s-configdetails">設定の詳細, Section
6.7</a> 参照)。
</p>
</dd>
</dl>

<p>
この二つの違いは重要で、置きかえ可能な概念ではありません。 ほとんどすべての
<samp>conffiles</samp> は設定ファイルですが、 多くの設定ファイルは
<samp>conffiles</samp> ではありません。
</p>

<p>
別途記載されているとおり、<code>/etc/init.d</code> スクリプト、
<code>/etc/default</code> ファイル、
<code>/etc/cron.{hourly,daily,weekly,monthly}</code> 中のスクリプトや
<code>/etc/cron.d</code> 中の cron
の設定ファイルなどは「設定ファイル」として扱わなければいけません。
一般的に、設定情報を内部に持つスクリプトはは事実上の設定ファイルですし、そのように扱われます。
</p>

<hr>

<h3 id="s10.7.2">10.7.2 設定ファイルの置き場所</h4>

<p>
パッケージによって作られ使われる設定ファイルは <code>/etc</code>
以下に配置しなければいけません。
関連した設定ファイルが複数ある場合には、そのパッケージの名前がついたサブディレクトリを
<code>/etc</code> 以下に作成することを検討してください。
</p>

<p>
あなたのパッケージが <code>/etc</code>
以外の場所に設定ファイルを作成し、かつそのパッケージが <code>/etc</code>
を使うように修正するのが望ましくない場合でも、 設定ファイル自体は
<code>/etc</code> に置き、
パッケージの期待する場所からシンボリックリンクで参照するようにするべきです。
</p>

<hr>

<h3 id="s10.7.3">10.7.3 設定ファイルの扱い</h4>

<p>
設定ファイルの扱いは下記の挙動に従ったものとしなければいけません。
</p>
<ul>
<li>
<p>
ローカルで行った変更は、パッケージをアップグレードした際に保持されていなければいけません。
</p>
</li>
<li>
<p>
設定ファイルはパッケージ削除の際にはそのまま保持し、パッケージの完全削除
(purge) の際にのみ削除するようにしなければいけません。
</p>
</li>
</ul>

<p>
ローカルでの修正の無い、廃止になった設定ファイルはアップグレードの際に削除しても構いません[<a
href="footnotes.html#f94" name="fr94">94</a>]。
</p>

<p>
この挙動を行わせるやさしい方法は設定ファイルを <samp>conffile</samp>
にしてしまうことです。 これはほとんどのインストールの場合にそのままで使え、
一部のシステム管理者が変更するかもしれない、そういう設定ファイルを添付できる場合だけに適した方法です。
更に、この方法を使うためには標準設定のファイルがパッケージの配布物に含まれていること、またインストール中
(やその他の際に)
にメンテナスクリプトから書き換えを行わない、という二条件を満たしている必要があります。
</p>

<p>
パッケージの conffile
にハードリンクを張ることは、ローカルで加えた変更を正しく残せなくなるため、許されていません
[<a href="footnotes.html#f95" name="fr95">95</a>]。
</p>

<p>
もう一つのやり方は、上記の挙動をメンテナスクリプトから実現する方法です。
この場合には設定ファイルは <samp>conffile</samp>
として列挙してはならず、またパッケージ配布物に含まれていてもいけません。
パッケージにまずまずの設定を行うために、なんらかの設定ファイルが存在していることが必要な場合でも、
設定ファイルを作成し、更新し、維持し、完全削除の際に削除するようなメンテナスクリプトを提供することはパッケージメンテナの責任です
(詳細は <a
href="ch-maintainerscripts.html">パッケージ管理スクリプトとインストールの手順,
Chapter 6</a> を参照ください)。
このスクリプトは結果の再現性を持たなければいけません
(すなわち、<code>dpkg</code>
がインストール中のエラーや、パッケージの削除のためスクリプトを再実行した場合に正しく動作しなければならないということです)。
また、このスクリプトは <code>dpkg</code>
がメンテナスクリプトを呼び出す様々なやり方に対応できねばならず、
ユーザの設定ファイルを前もって問い合わせることなしに上書きしたり壊したりしてはいけません。
またこのスクリプトは、特にアップグレードの時には、不必要な質問を行ったりせず、善良な市民として振る舞わなければいけません。
</p>

<p>
スクリプトはパッケージに設定しうるすべてのオプションを設定しなければならないわけではなく、
与えられたシステムでパッケージが動作するために必要な設定だけを行えばかまいません。
理想的なことを言えば、システム管理者が <code>postinst</code> で (半)
自動的に行われた設定以外のことを行う必要がないのが、あるべき姿です。
</p>

<p>
通常取られる手法は <code><var>package</var>-configure</code>
という名のスクリプトを作成し、パッケージの <code>postinst</code>
から設定ファイルが存在していないときのみ、そのスクリプトを呼ぶようにするやり方です。
時にはメンテナスクリプトが使う例や雛形ファイルを用意すると便利なこともあります。
そのようなファイルがある場合、アーキテクチャへのの依存関係の有無に沿って、
<code>/usr/share/<var>package</var></code> または
<code>/usr/lib/<var>package</var></code> に置きます。 それが例なら
<code>/usr/share/doc/<var>package</var>/examples</code>
からそこへシンボリックリンクを作成してください。また、通常の <code>dpkg</code>
で処理するファイルにして (すなわち、 設定ファイルでは<em>無いようにする</em>)
ください。
</p>

<p>
これまで説明してきた設定ファイルの扱いの手法は混在させてはいけません。
それは、はちゃめちゃへの一本道です。 <code>dpkg</code>
がパッケージをアップグレードする際に毎回ファイルを上書きするかどうか問い合わせてくるようになり、頭を掻きむしることになります。
</p>

<hr>

<h3 id="s10.7.4">10.7.4 設定ファイルの共用</h4>

<p>
もし二つ以上のパッケージが同じ設定ファイルを使用し、
それらのパッケージを同時にインストールしても問題がない場合には、
これらのパッケージのうちの一つを設定ファイルの <em>所有者 (owner)</em>
と定義してください。 これはその設定ファイルを管理し、扱うパッケージです。
ほかのその設定ファイルを使用するパッケージは、
動作時にその設定ファイルが必要ならば、所有者となったパッケージに依存 (depend)
する必要があります。
もしほかのパッケージが、その設定ファイルがあれば使うけれども、そのファイルが無くても動作はするということならば、依存関係の宣言は不要です。
</p>

<p>
もし二つ以上のパッケージが同じ設定ファイルを使用し、 <em>かつ</em>
これらのパッケージが設定ファイルを変更する場合がある、という状況下では、以降の各項を満たすようにしてください。
</p>
<!-- ol type="1" start="1"  -->
<li>
<p>
関連したパッケージの一つ (『所有者』パッケージ)
が前節で説明した方法で、メンテナスクリプトから設定ファイルを管理するようにします。
</p>
</li>
<li>
<p>
『所有者』パッケージは他のパッケージが設定ファイルを変更する際に用いるプログラムを提供しなければいけません。
</p>
</li>
<li>
<p>
関連するパッケージは設定ファイルを変更する際には上で記した
『所有者』パッケージの提供するプログラムを使わなければいけません。
また、関連するパッケージは変更のためのプログラムが存在することを保証するために『所有者』
パッケージに依存することを宣言するか、変更のためのプログラムがなかったときにはエラー等を出さず変更をあきらめるようにするかの、
どちらかとしておかなければいけません
(後者のシナリオでは、設定ファイルが存在していない場合に加えこの考慮も必要になります)。
</p>
</li>
</ol>

<p>
場合によっては、他のパッケージの基本的な土台を作成し、共有の設定ファイルを管理する新パッケージを作成した方がいいこともあります
(例として <samp>sgml-base</samp> パッケージを参考にしてください)。
</p>

<p>
設定ファイルが上記記載のように共有できない場合は、これらのパッケージは互いに競合していると指定されなければいけません。
同じファイルを指定しているパッケージ間は <em>競合 (conflict)</em>
していると指定しなければいけません。
これはファイルを共有してはいけないという一般則から来ています。 この場合は代替
(alternative) や待避 (diversion) の指定は適しません。 特に、<code>dpkg</code>
は待避時の <samp>conffile</samp> をうまく扱えません。
</p>

<p>
2つのパッケージが同じ <samp>conffile</samp>
を宣言している場合、それらパッケージが互いに conflict
を指定している場合でも、設定が残っている状況になる場合があります。
もしユーザが一方のパッケージを削除 (完全削除ではなく)
し、他方のパッケージをインストールした場合、新しいパッケージは古いパッケージの
<samp>conffile</samp> を引き継ぐことになります。
もしファイルがユーザの手で変更されていた場合、これはアップグレードの際には、
ローカルで変更されていた <samp>conffile</samp> と同じ扱いを受けます。
</p>

<p>
The maintainer scripts must not alter a <samp>conffile</samp> of <em>any</em>
package, including the one the scripts belong to.
メンテナスクリプトは、それ自身が収録されたパッケージを含む
<em>いかなる</em>パッケージの <samp>conffile</samp> を変更してもいけません。
</p>

<hr>

<h3 id="s10.7.5">10.7.5 ユーザ設定ファイル (ドットファイル)</h4>

<p>
<code>/etc/skel</code> にあるファイルは <code>adduser</code> に
よって自動的に新ユーザのアカウント下にコピーされます。この
<code>/etc/skel</code> 以下は他のどのプログラムからも参照しては いけません。
</p>

<p>
この関係で、プログラムが動作するのに <code>$HOME</code>
ディレクトリにドットファイルをあらかじめ用意しておく必要があるならば、
ドットファイルはあらかじめ <code>/etc/skel</code>
にインストールしておき、設定ファイルとして扱うべきです。
</p>

<p>
とは言っても、正しく動作するために自分で自動的に作成するもの以外のドットファイルが必要なプログラムというものは、望ましいものではありません。
</p>

<p>
加えて、プログラムは Debian
の標準インストール状態で、上流のデフォルトにできるだけ近い動作をするように設定されているべきです。
</p>

<p>
したがって、Debian
パッケージのプログラムを真っ当に動くようにする設定が必要なら、
<code>/etc</code> 内にサイト共通で利用する設定ファイルを用意してください。
プログラムがサイト共通の標準設定ファイルをサポートしておらず、
パッケージメンテナがその機能を追加する余裕がないときは、 <code>/etc/skel</code>
にユーザ個別のファイルをおいてもかまいません。
</p>

<p>
<code>/etc/skel</code> はできる限り空になるようにしましょう。
パッケージをインストールしたときに、ドットファイルを現存のユーザにコピーする簡単な
(そして、適切な) 仕組みがないことからも、そうすべきなのは明らかだと思います。
</p>

<hr>

<h2 id="s10.8">10.8 ログファイル</h3>

<p>
ログファイルは通常 <code>/var/log/<var>package</var>.log</code>
という名にします。たくさんの log ファイルを持つ場合や、
読み書き権限の設定のために独立のディレクトリを必要とする場合には
(<code>/var/log</code> は <code>root</code> ユーザからのみ書き込めます)、
通常は <code>/var/log/<var>package</var></code>
という名のディレクトリを作成してそこにログを置くべきです。
</p>

<p>
ログファイルは時々循環させるようにして、どこまでも大きくなることがないようにしなければいけません。
これを実現する最良の方法は、<code>/etc/logrotate.d</code>
ディレクトリにログ循環設定ファイル (通常、
<code>/etc/logrotate.d/<var>package</var></code> という名前です)
を置き、logrotate の提供する機能を使うやり方 [<a href="footnotes.html#f96"
name="fr96">96</a>] です。 次に logrotate の設定ファイルのいい例を挙げましょう
(詳しくは <code>logrotate(8)</code> を見てください)。
</p>
<pre>
     /var/log/foo/*.log {
         rotate 12
         weekly
         compress
         missingok
         postrotate
             start-stop-daemon -K -p /var/run/foo.pid -s HUP -x /usr/sbin/foo -q
         endscript
     }
</pre>

<p>
上記の例は <code>/var/log/foo</code> 以下の全ファイルを巡回させ、12
世代分保存し、循環終了時にデーモンに設定ファイルを再オープンさせるものです。
もしログがなかった場合にはログ循環をスキップする (<samp>missingok</samp>
経由で)
ようになっており、パッケージが削除されているが完全削除状態でない場合にエラーにならないようにしています。
</p>

<p>
パッケージが完全削除 (purge) された時には、ログファイルがすべて消去されるよう
(パッケージが削除されたときには消しません) にすべきです。 これは
<code>postrm</code> スクリプトが引数 <samp>purge</samp>
で呼ばれた際に行ってください (<a
href="ch-maintainerscripts.html#s-removedetails">削除と設定の完全削除の詳細,
Section 6.8</a> 参照)。
</p>

<hr>

<h2 id="s-permissions-owners">10.9 ファイル属性と所有者</h3>

<p>
この節のここ以降で記載されているのは一般的なガイドラインですので、
必要に応じて細部でここから離れてもかまいません。
しかしながらやっていることがセキュリティ上問題がないこと、
またシステムのほかの部分との整合性を可能な限り維持していることを必ず確認してください。
おそらくまず <code>debian-devel</code> で相談するのがいいでしょう。
</p>

<p>
ファイルは <samp>root:root</samp> の所有権で、所有者書き込み可能で誰でも読める
(および適宜実行可能である) ようにしてください。 すなわち、ファイルモードは 644
か 755 になります。
</p>

<p>
ディレクトリのパーミッションは 755、あるいは、グループが書き込み可であれば 2775
にすべきです。ディレクトリの所有者は パーミッションに合わせてください。
つまりディレクトリのパーミッションが 2775
であれば、そこに書き込む必要のあるグループのユーザを所有者に設定してください
[<a href="footnotes.html#f97" name="fr97">97</a>]。
</p>

<p>
制御情報ファイルは、<samp>root:root</samp> の所有権で、パーミッション 644
(ほとんどのファイルでは) か、755 (<a
href="ch-binary.html#s-maintscripts">maintainer scripts</a>
などの実行可能なファイル) にすべきです。
</p>

<p>
setuid や setgid された実行ファイルのパーミッションはそれぞれ 4755、2755
で、適切な所有者とグループに設定されねばなりません。 それらを読み込み不可 (4711
や 2711、4111 など) にしてはいけません。 誰でも自由に利用可能な Debian
パッケージのバイナリを探してくることができるので、読み込み不可にすることはセキュリティ対策にはならず、単に不便にするだけです。
同じ理由で set-id
していない実行ファイルに対して読み込み・実行許可を制限すべきではありません。
</p>

<p>
setuid を使う一部のプログラムではファイルのパーミッションを使って、
実行をある特定のユーザのみに制限する必要があります。 この場合 set-id
したいユーザの uid
を所有者として、実行を許可されたグループに実行許可を与えます。
この時のパーミッションは 4754 です。
繰り返しますが、実行を許さないユーザに対して読み込み不可にすることは、前述の理由で意味がありません。
</p>

<p>
システム管理者がローカルなセキュリティの方針に合わせるため、
各バイナリのパーミッションを変えてパッケージを再設定する枠組にしてもかまいません。
その場合は、以下で記載の通り <code>dpkg-statoverride</code> [<a
href="footnotes.html#f98" name="fr98">98</a>] を使うことができます。
もう一つの方法として、例えばプログラムを利用することができるグループを作り、
setuid
した実行ファイルはそのグループだけが実行できるような設定とすることもできます。
</p>

<p>
パッケージのために新しいユーザまたはグループを作成する必要がある場合、二つの方法があります。
第一の方法は、このユーザまたはグループを所有者としてバイナリパッケージの所定のファイルを作成します。
または、バイナリに単なる名前ではなくユーザあるいはグループ ID
をコンパイル時に埋め込むようにもできます (この方法は静的に割り当てられた ID
が必要となるため、可能な限り避けるべきです)。
</p>

<p>
静的に割り当てられた id が必要な場合、<samp>base-passwd</samp>
システムの管理者にユーザあるいはグループ ID の割り当てを求めます。
割り当てられるまでパッケージをリリースすることはできません。
このようなユーザやグループが割り当てられたらならば、パッケージは当該 ID を
<code>/etc/passwd</code> ないしは <code>/etc/group</code> に含むような
<samp>base-passwd</samp>
パッケージの特定以降のバージョンに依存するようにするか、 パッケージ中の
<code>preinst</code> または <code>postinst</code>
スクリプト等で、割り当てられた ID を (<samp>adduser</samp> などを使って)
自分で作成するようにしてください。 <code>postinst</code>
で行うほうが、可能ならばより良いやり方です。 <code>preinst</code>
を使う場合、<samp>adduser</samp>
パッケージに対する先行依存関係の宣言が必要になります。
</p>

<p>
第二の方法は、プログラムが実行時にグループ名から uid、gid
を決定するようにするもので、ID は動的に [<a href="footnotes.html#f99"
name="fr99">99</a>] 割り当てられます。この場合、<code>debian-devel</code>
で討議を行ない、また <code>base-passwd</code>
のメンテナにその名前が一意であること、静的に ID
を割り当てたほうが望ましいということがないか、の二点を問い合わせて、その後適切なユーザ名あるいはグループ名を選ぶべきです。
これらの確認がすんだ後パッケージの <code>preinst</code> か
<code>postinst</code> スクリプトで (繰り返しますが後者が好ましいです)
必要に応じて <code>adduser</code>
を使ってユーザあるいはグループを作成するようにしてください。
</p>

<p>
名前に関連した ID
の値を変えることはとても難しく、全ての関連したファイルをファイルシステムから検索する必要があることに注意してください。
後になってからの変更は問題を起こしますので、ID
は静的か動的か良く考えて決めてください。
</p>

<hr>

<h3 id="s10.9.1">10.9.1 <code>dpkg-statoverride</code> の使い方</h4>

<p>
この節はポリシーの一部ではなく、<code>dpkg-statoverride</code>
の使い方について解説することを意図しています。
</p>

<p>
システム管理者が配布状態の Debian
パッケージと異なる所有者またはパーミッションでファイル (またはディレクトリなど)
をインストールしたい場合、
ファイルがインストールされる際に毎回異なった設定を使うよう <code>dpkg</code>
に指示を出すため <code>dpkg-statoverride</code> を使うことができます。
これによって、パッケージメンテナは標準のパーミッションでファイルを配布しておき、システム管理者が望む変更を加えられるようにすることができます。
例えばあるデーモンは通常 setuid root が必要になりますが、 ある特定の状況では
setuid なしに使えるという場合、 <samp>.deb</samp> パッケージでは setuid
付きでインストールすべきです。
この場合、ローカルのシステム管理者が望みに応じてこれを変更することができます。
また、もしインストールの際に標準的な設定が二つある場合であれば、メンテナは
<samp>debconf</samp> を使って好ましい選択を聞き、メンテナスクリプト中から
<code>dpkg-statoverride</code>
を呼んでシステム管理者の選択を適用することができます。
アップグレードの際に、既存の設定を上書きしないような配慮を行わなければいけません。
</p>

<p>
上記の通り、<code>dpkg-statoverride</code>
は主にシステム管理者のためのツールで、メンテナスクリプトで必要になることは普通ありません。
但し、このような <code>dpkg-statoverride</code>
の呼び出しがメンテナスクリプトで必要になる場合があります。
その一例が、動的に割り当てられたユーザやグループ ID
を使う場合です。この場合、パッケージの <code>postinst</code>
で以下の定石が参考になるでしょう。 ここで、<samp>sysuser</samp>
が動的に割り当てられた ID 名です。
</p>

<pre>
     for i in /usr/bin/foo /usr/sbin/bar
     do
       # only do something when no setting exists
       if ! dpkg-statoverride --list $i &gt;/dev/null 2&gt;&amp;1
       then
         #include: debconf processing, question about foo and bar
         if [ &quot;$RET&quot; = &quot;true&quot; ] ; then
           dpkg-statoverride --update --add sysuser root 4755 $i
         fi
       fi
     done
</pre>

<p>
以下はパッケージ完全削除 (purge) の際、override を削除するコードです。
</p>

<pre>
     for i in /usr/bin/foo /usr/sbin/bar
     do
       if dpkg-statoverride --list $i &gt;/dev/null 2&gt;&amp;1
       then
         dpkg-statoverride --remove $i
       fi
     done
</pre>

<hr>

<h2 id="s-filenames">10.10 ファイル名</h3>

<p>
システムの PATH (具体的には <samp>/bin</samp>, <samp>/sbin</samp>,
<samp>/usr/bin</samp>, <samp>/usr/sbin</samp> および <samp>/usr/games</samp>)
にバイナリパッケージからインストールするファイル名は ASCII
エンコーディングを用いなければいけません。
</p>

<p>
システム PATH 外にバイナリパッケージからインストールするファイル名は UTF-8
でエンコーディングしなければいけません。 また、可能な限り ASCII
範囲内に制限するべきです。
</p>

<hr>

<p>
[ <a href="ch-opersys.html">previous</a> ]
[ <a href="index.html#contents">Contents</a> ]
[ <a href="ch-scope.html">1</a> ]
[ <a href="ch-archive.html">2</a> ]
[ <a href="ch-binary.html">3</a> ]
[ <a href="ch-source.html">4</a> ]
[ <a href="ch-controlfields.html">5</a> ]
[ <a href="ch-maintainerscripts.html">6</a> ]
[ <a href="ch-relationships.html">7</a> ]
[ <a href="ch-sharedlibs.html">8</a> ]
[ <a href="ch-opersys.html">9</a> ]
[ 10 ]
[ <a href="ch-customized-programs.html">11</a> ]
[ <a href="ch-docs.html">12</a> ]
[ <a href="ap-pkg-scope.html">A</a> ]
[ <a href="ap-pkg-binarypkg.html">B</a> ]
[ <a href="ap-pkg-sourcepkg.html">C</a> ]
[ <a href="ap-pkg-controlfields.html">D</a> ]
[ <a href="ap-pkg-conffiles.html">E</a> ]
[ <a href="ap-pkg-alternatives.html">F</a> ]
[ <a href="ap-pkg-diversions.html">G</a> ]
[ <a href="ch-customized-programs.html">next</a> ]
</p>

<hr>

<p>
Debian ポリシーマニュアル
</p>

<address>
バージョン 3.9.5.0, 2014-07-03<br>
<br>
<a href="ch-scope.html#s-authors">The Debian Policy Mailing List</a><br>
<br>
</address>
<hr>

