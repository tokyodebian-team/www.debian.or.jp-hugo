[% title = "Debian Packaging Manual - dselect の インストールメソッドへのインターフェース" %]

<p><a name="ch-methif"></a></p>
<hr>

<p>
[ <a href="ch-sharedlibs.html">previous</a> ]
[ <a href="index.html#contents">Contents</a> ]
[ <a href="ch-scope.html">1</a> ]
[ <a href="ch-binarypkg.html">2</a> ]
[ <a href="ch-sourcepkg.html">3</a> ]
[ <a href="ch-controlfields.html">4</a> ]
[ <a href="ch-versions.html">5</a> ]
[ <a href="ch-maintainerscripts.html">6</a> ]
[ <a href="ch-descriptions.html">7</a> ]
[ <a href="ch-relationships.html">8</a> ]
[ <a href="ch-conffiles.html">9</a> ]
[ <a href="ch-alternatives.html">10</a> ]
[ <a href="ch-diversions.html">11</a> ]
[ <a href="ch-sharedlibs.html">12</a> ]
[ 13 ]
[ <a href="ch-conversion.html">14</a> ]
[ <a href="ch-conversion.html">next</a> ]
</p>

<hr>

<h2>
Debian Packaging Manual
<br>Chapter 13 - <code>dselect</code> の インストールメソッドへのインターフェース
</h2>

<hr>

<p>
<code>dselect</code> は、ディストリビューションのデータに実際に
アクセスする必要があるときには、インストールメソッドから
スクリプトを呼び出します。コアプログラムである <code>dselect</code>
自身は、スクリプトを呼び出すだけであり、
パッケージ選択とアクセスメソッド選択のための
インターフェースを提供します。必要に応じて、<code>dpkg</code>
を起動する役目は、このインストールメソッドが受け持ちます。
</p>

<p>
それぞれのインストールメソッドは、3 つのスクリプトを 持っています。
</p>
<ul>
<li>
<p>
インストールパラメータの設定
</p>
</li>
<li>
<p>
利用可能なパッケージリストの更新
</p>
</li>
<li>
<p>
インストール
</p>
</li>
</ul>

<p>
<code>dselect</code> は、<samp>/usr/lib/dpkg/methods</samp> と、
<samp>/usr/local/lib/dpkg/methods</samp> から利用できる
インストールメソッドを検索します。
</p>

<hr>

<h3><a name="s13.1"></a>13.1 メソッドスクリプトの機能</h3>

<p>
ユーザがインストールメソッドを選択したすぐ後に、
設定スクリプトが実行されます。ここでは、環境によって
異なる値についてユーザに入力を求めます。例えば、 NFS マウントする、または FTP
によってアクセスされるサイトの 名前、使用ディレクトリ名、<samp>.deb</samp>
ファイルが置かれている ファイルシステムやディレクトリ名、テープから
インストールするのか、または、フロッピーディスクを
使用するのか、などです。これらに対するユーザの入力結果は、
<samp>/var/lib/dpkg/methods</samp> に保存します。詳しくは、以下を
ごらん下さい。また、利用可能なパッケージリストが得られない
場合には、利用可能なパッケージを検索する機能が使用できなければ いけません。
</p>

<p>
更新スクリプトは、もし可能であれば、利用可能なパッケージの
リストを読みこみます。そして、パッケージのリストを <code>dpkg</code> と
<code>dselect</code> との利用可能な パッケージのデータベースに読みこむために、
<samp>dpkg --update-avail</samp> と、 <samp>dpkg --merge-avail</samp>
を実行します。場合によっては さらに、<samp>dpkg --forget-old-unavail</samp>
も実行します。 利用可能なパッケージリストが存在しない場合で、実際の
ファイルからデータを走査することをユーザが選択した場合に
おいては、その走査作業はこの段階で実行されます。そのとき <samp>dpkg
--record-avail</samp> が用いられます。
</p>

<p>
インストールスクリプトは、すべての利用可能な <samp>.deb</samp>
ファイルを、<samp>dpkg --iGOEB</samp> に供給します (これは、 <samp>dpkg
--install --refuse-downgrade --selected-only --skip-same-version
--auto-deconfigure</samp> と等価です)。 <samp>-R</samp>
(<samp>--recursive</samp>) オプションは、
サブディレクトリも同時に考慮してくれるので、便利かもしれません。
</p>

<p>
いくつかのスクリプトは、メッセージを画面に出力することも
あります。この時、あっという間に dselect がスクリーンを
再描画して、その出力メッセージが流れていってしまっては
困ります。そこで、スクリプトは、ユーザが「リターン」を
たたくまで、メッセージの出力を待っていなければいけません。
</p>

<p>
メソッドスクリプトが (終了ステータス 0 を返して) 成功した
場合、<code>dselect</code> は、メインメニューへと即座に
戻ります。その際、`次の' オプションがハイライト表示されて、
ユーザがそのまま選択できるようになっています。
メソッドスクリプトが失敗した場合には、<code>dselect</code>
は、メッセージを画面に出力して、ユーザが「リターン」を 叩くのを待ちます。
</p>

<hr>

<h3><a name="s13.2"></a>13.2 メソッドスクリプトの場所と引数</h3>

<p>
あるスクリプトの集合 (以後、グループと表記します) は、
メインメニュー中で違う動作のいくつかのインストールメソッドを
提供します。例えば、一般的な get-packages-by-FTP グループは、
メインメニュー中でミラーサイトからのインストール、および
ユーザの指定したサイトからのインストールのメソッドを 提供します。
</p>

<p>
同一のスクリプトセットによって複数のインストールメソッドが
実装された場合、それぞれのグループについてサブディレクトリ
<samp>/usr/lib/dpkg/methods/<var>group</var></samp> または
<samp>/usr/local/lib/dpkg/methods/<var>group</var></samp>
がなければなりません。このサブディレクトリには以下が 含まれます。
</p>
<dl>
<dt><samp>names</samp></dt>
<dd>
<p>
これらのスクリプトによって提供されるユーザに見える インストールメソッド
</p>
</dd>
<dt><samp>setup</samp></dt>
<dt><samp>update</samp></dt>
<dt><samp>install</samp></dt>
<dd>
<p>
実行可能なプログラム、スクリプト自身
</p>
</dd>
<dt><samp>desc.<var>option</var></samp></dt>
<dd>
<p>
説明ファイル
</p>
</dd>
</dl>

<p>
<samp>names</samp> は、行のリストとして構成されます。それぞれの
行は、以下のものを含みます。
</p>

<pre>
      <var>sequence</var> <var>method</var> <var>summary</var>
</pre>

<p>
<var>sequence</var> は、2 桁の数字で、メインメニューに並べる
順序を制御するために、<samp>rc.d</samp> の接頭辞のように
使われます。よく分からないときは、50 を使用します。
</p>

<p>
<var>method</var> は、インストールメソッドの名前として <code>dselect</code>
によって、画面に出力される名前です。 <samp>setup</samp>
と、<samp>update</samp>、<samp>unpack</samp> に 渡される第一引数となります。
</p>

<p>
<var>summary</var> は、<code>dselect</code> のメニューに
使用される簡単な説明文です。
</p>

<p>
三つのスクリプトはそれぞれ同じ三つの引数をとります。
それらは、<var>vardir</var> と <var>group</var>、 <var>method</var>
です。<var>vardir</var> は、 <code>dpkg</code> と <code>dselect</code> の状態を
保存するための基本となるディレクトリで、通常、 <samp>/var/lib/dpkg</samp>
です。この引数が渡されるのは、 <code>dselect</code> で指定された
<samp>--admindir</samp> を尊重するためです。
</p>

<p>
それぞれのインストール方式 (option) は、さらに詳細な説明文を、
<samp>desc.<var>option</var></samp>ファイル に書くことができます。
このファイルの書式は、<samp>Description</samp> フィールドの
エントリの拡張説明文のように、<em>行頭に一つのスペースを
入れなければいけません。</em>
</p>

<p>
また、<samp><var>vardir</var>/methods</samp> ディレクトリが
あります。インストールメソッドのグループは、現在の状態の保存に
<samp><var>vardir</var>/methods/<var>group</var></samp>
ディレクトリを使用できます。
</p>

<p>
グループ名とインストールメソッド名は、C 言語の識別子の規則に
従っていなければいけません。
</p>

<hr>

<p>
[ <a href="ch-sharedlibs.html">previous</a> ]
[ <a href="index.html#contents">Contents</a> ]
[ <a href="ch-scope.html">1</a> ]
[ <a href="ch-binarypkg.html">2</a> ]
[ <a href="ch-sourcepkg.html">3</a> ]
[ <a href="ch-controlfields.html">4</a> ]
[ <a href="ch-versions.html">5</a> ]
[ <a href="ch-maintainerscripts.html">6</a> ]
[ <a href="ch-descriptions.html">7</a> ]
[ <a href="ch-relationships.html">8</a> ]
[ <a href="ch-conffiles.html">9</a> ]
[ <a href="ch-alternatives.html">10</a> ]
[ <a href="ch-diversions.html">11</a> ]
[ <a href="ch-sharedlibs.html">12</a> ]
[ 13 ]
[ <a href="ch-conversion.html">14</a> ]
[ <a href="ch-conversion.html">next</a> ]
</p>

<hr>

<p>
Debian Packaging Manual
</p>

<address>
version revision 1.13, 2000-08-22<br>
<br>
Ian Jackson <code><a href="mailto:ijackson&#64;gnu.ai.mit.edu">ijackson&#64;gnu.ai.mit.edu</a></code><br>
校正: David A. Morris <code><a href="mailto:bweaver&#64;debian.org">bweaver&#64;debian.org</a></code><br>
メンテナー: Christian Schwarz <code><a href="mailto:schwarz&#64;debian.org">schwarz&#64;debian.org</a></code><br>
メンテナー: Manoj Srivastava <code><a href="mailto:srivasta&#64;debian.org">srivasta&#64;debian.org</a></code><br>
メンテナー: Julian Gilbey <code><a href="mailto:J.D.Gilbey&#64;qmw.ac.uk">J.D.Gilbey&#64;qmw.ac.uk</a></code><br>
メンテナー: The Debian Policy group <code><a href="mailto:debian-policy&#64;lists.debian.org">debian-policy&#64;lists.debian.org</a></code><br>
日本語版メンテナー: 早瀬 茂規 <code><a href="mailto:shayase&#64;tky3.3web.ne.jp">shayase&#64;tky3.3web.ne.jp</a></code><br>
日本語版校正: Japanese Debian Documentation Project <code><a href="mailto:debian-doc&#64;debian.or.jp">debian-doc&#64;debian.or.jp</a></code><br>
<br>
</address>
<hr>

