[% title = "Bug Report Format" %]
<h2>バグレポートのフォーマット</h2>
<hr>

<p>Debian JP Project 版 Bug Tracking System へバグ報告する際は、次のルールに従うようにしてください。</p>

<ul>
 <li>まず、メールのサブジェクトにバグの内容を簡潔に記載してください。英語でも日本語でも構いません。</li>
 <li>バグ報告の本文の最初の 2 行には、必ずバグが発生したパッケージの名前とバージョンを明記してください。</li>
 <li>必要なら、パッケージ名とバージョン番号の後にこのバグの重要度 (severity) を記入してください。severity のレベルには、次の5つがあります。

 <dl>
  <dt>critical(致命的)</dt>
  <dd><ul>
      <li>システム上(またはシステム全体)の無関係なソフトウェアを破壊する場合</li>
      <li>重大なデータの欠落を引き起こす場合</li>
      <li>該当するパッケージをインストールしたシステム上にセキュリティーホールが発生する場合</li>
      </ul>

  <dt>grave(重大)</dt>
  <dd><ul>
      <li>該当するパッケージが全く(またはほとんど)使用できない場合</li>
      <li>データの欠落を引き起こす場合</li>
      <li>そのパッケージを使用するユーザのアカウントにアクセスを許してしまうセキュリティーホールがある場合</li>
      </ul></dd>

  <dt>important(重要)</dt>
  <dd>上記以外に、該当のパッケージをリリースするには不適切と思われるバグがある場合。</dd>
  <dt>normal(通常)</dt>
  <dd>デフォルト値。通常のバグ。</dd>

  <dt>wishlist(要望)</dt>
  <dd><ul>
      <li>将来的な要望事項</li>
      <li>重大な設計上の問題のために修正が非常に困難なバグの場合</li>
      </ul></dd>
 </dl>
 </li>
</ul>

<blockquote class="mail"><pre>
To: submit&#64;bugs.debian.or.jp
Subject: muleが立ち上がりません
----
Package: mule
Version: 2.3-19.34b-10

muleを起動させると、すぐに CORE DUMP してしまう。
</pre></blockquote>

<p>また、<a href="/Bugs/">Bug tracking system</a>のドキュメントも御覧ください。</p>

<hr>
<div class="back">
<a href="../index.html">Debian JP Project ホームページ</a>に戻る。
</div>

<hr>
<div class="comment">
このウェブページに関するご感想/ご質問は
<a href="/contact.html#webadmin">webadmin at debian.or.jp</a>
までお送りください
</div>

<address>
$Id: Bug-Report.html,v 1.13 2005/01/03 02:21:38 tomos Exp $<br>
Copyright &copy; 1997-2005 Debian JP Project;
<a href="../License.html">ライセンス条項</a>を見る
</address>
<hr>
