function img(url, file, name, width, height, weight) {
	  this.url = url;
	  this.file = file;
	  this.name = name;
	  this.width = width;
	  this.height = height;
	  this.weight = weight;
}

var image = new Array(
	new img("image/gnome.png", "image/gnomesmall.png", "Debian GNOME desktop image", "272", "204", 5)
	, new img("image/desktop02-takayan.png", "image/desktop02-takayan_small.png", "Debian desktop image", "272", "204", 1)
	, new img("image/desktop03-kubo.png", "image/desktop03-kubo_small.png", "Debian desktop image", "272", "218", 1)
	, new img("image/desktop04-nogata.png", "image/desktop04-nogata_small.png", "Debian desktop image", "272", "204", 1)
	, new img("image/desktop05-nakamoto.png", "image/desktop05-nakamoto_small.png", "Debian desktop image", "272", "204", 1)
	, new img("image/desktop06-hatanaka.png", "image/desktop06-hatanaka_small.png", "Debian desktop image", "272", "204", 1)
	, new img("image/desktop07-nakao.png", "image/desktop07-nakao_small.png", "Debian desktop image", "272", "204", 1)
	, new img("image/desktop08-seino.jpg", "image/desktop08-seino_small.jpg", "Debian desktop image", "272", "204", 1)
	// You can add another images:
	// , new img(fullsize_image, thumbnail_image, ALT_message, width, height, weight )
	);

var choice = new Array();
for (i = 0; i < image.length; i++) {
	for (i2 = 0; i2 < image[i].weight; i2++) {
		choice = choice.concat(i);
	}
}

var presented = new Array(choice.length);

function show_screen(pat) {
	check = 1;
	count = 0;
	while (check == 1 && count < 20) {
		n = parseInt(Math.random() * choice.length);
		for (i = 0; i < pat; i++) {
			if (choice[n] == presented[i]) break;
		}

		if (choice[n] != presented[i]) {
			presented[pat] = choice[n];
			check = 0;
		}
		count++;
		if (count == 20) n = 0;
	}

	document.writeln("<a href=\"" + image[choice[n]].url + "\">"
			 + "<img src=\"" + image[choice[n]].file + "\""
			 + " alt=\"" + image[choice[n]].name + "\""
			 + " title=\"" + image[choice[n]].name + "\""
			 + " width=\"" + image[choice[n]].width + "\""
			 + " height=\"" + image[choice[n]].height + "\""
			 + "></a>");
}
