[% title = "Project について" %]
	<h2>Debian JP Project とは</h2>
	<p>
	Debian JP Project は、<a href="[% wwworg %]/">Debian</a> を普及させることを目的とした任意団体です。<a href="regulation.html">会則</a>に基づいて活動しています。<br>
	取材や記事執筆依頼、Debian JP Project 組織自体への質問・ご意見等については、<a href="mailto:board&#64;debian.or.jp">Debian JP Project理事会 &lt;board&#64;debian.or.jp&gt;</a> までご連絡ください。より詳細な<a href="organization.html">組織構成</a>もあります。理念に賛同いただいた上で活動されたい方は、ぜひ私たちの Project に<a href="join.html">ご参加</a>ください。私たちへの<a href="donations.html#donation">寄付</a>による支援も随時募集しております。
	</p>

	<h2>日本における Debian の普及</h2>

	<p>
    私たち
    <a href="../">Debian JP Project</a> は
「<a href="[% wwworg %]/social_contract">Debian 社会契約</a>」の理念に賛同し、
「<a href="[% wwworg %]/intro/free">フリー</a>」で高品質なオペレーティングシステムである
<a href="[% wwworg %]/">Debian</a>をより改善／普及させるために、次の活動を行っています。</p>

	<ul>
	<li>
	<p>Debian の国際化 (Internationalization、略して I18N) と
     地域化 (Localization、略して L10N) を推進し、
     Debian システム上で実用的な日本語環境を利用できるよう、
     Debian の開発者たちと協力してソフトウェアの日本語対応
     (<a href="../community/devel/index.html">開発者のコーナー</a>を参照)
     や、リリースノート、インストールマニュアルなどの各種の文書の日本語訳、
     また
     <a href="[% wwworg %]/">Debian の Web サイト</a>
     の日本語対応などを行います
     (<a href="../community/translate/">執筆者/翻訳者のコーナー</a>を参照)。</p>

     <p>ソフトウェアの日本語対応を進めていく際は、Debian のパッケージ開発者
     (公式開発者) と相談しながら、既存の Debian パッケージに適用
     できるような日本語対応のパッチ (可能ならほかのマルチバイト言語にも対応させるためのパッチ)
     を提供する、あるいは JP Project で作成したパッケージをそのまま提供するなど、状況に応じて最適と思われる方法で行います。</p>

     <p>Debian JP Project から Debian に提供するパッケージの作成者が
        Debian Project のパッケージメンテナにまだ登録されていない場合は、
        Debian JP Project のメンバーである Debian 公式開発者が必要に応じて
        Debian Project へのアップロードを代行します。</p>

     <p>Debian Project のメーリングリストは開発者連絡用メーリングリスト
        (debian-devel&#64;lists.debian.org) もパッケージ登録連絡用メーリングリスト
        (debian-devel-changes&#64;lists.debian.org) も流量がたいへん多いため、
        Debian に新たなパッケージを提供した場合はほかの Debian JP Project
        のメンバーにもそのことを知らせるために、
        <a href="../community/ml/openml.html#develML">Debian JP Developers メーリングリスト</a>
        への投稿によって事後報告することが推奨されています。</p>
	</li>

	<li>
	<p>なお、1999年 5 月に行われた議論の結果、今後 Debian JP Project 独自の
     追加パッケージ集は独自に公式リリースという形にはしないことになりました。</p>

     <p>これからは Debian Project によって開発・配布されている
     <a href="[% wwworg %]/">Debian</a>
     単独で十分実用的な日本語環境を提供できるよう、日本語サポートを必要と
     しているユーザの声を今まで以上に Debian Project に反映させていきます。</p>
	</li>

	<li>
     <p>Debian の公式パッケージだけでは日本語サポートが不足している分野、あるいは
	日本オリジナルのソフトウェアでまだ Debian に登録されていないものなどについて、
	Debian JP Project の開発者が作成したパッケージを一般に公開します。
	また、Debian のパッケージもミラーリングし、公開します。</p></li>

	<li>
	<p><a href="./index.html#webadmin">Debian JP Web サイトの制作・運営</a>
	を通じて Debian に関する国内情報を (主に日本語で、また可能な場合は英語でも) 発信していきます。</p></li>
	</ul> 
	</div>

	<div class="section">
	<h2>Debian に関する情報交換の場の提供</h2>

	<p>上記のような Debian Project / Debian JP Project に関する作業のほかに、
	利用者や開発者が相互に支援できるよう、情報交換のための場所である
	メーリングリストや、それらの過去ログを参照できる場所を提供します。</p>

	<ul>
	<li>
	<p>Debian JP Project 内の開発者を含む、多くの Debian ユーザが情報交換するための
     場所を提供します
     (<a href="../community/ml/openml.html#usersML">Debian JP Users メーリングリスト</a>など)。</p></li>

	<li><p>また、開発者同士の情報交換の場も提供します。
	(<a href="../community/ml/openml.html#develML">Debian JP Developers メーリングリスト</a>、
	<a href="../community/ml/openml.html#docML">Debian JP Documentation メーリングリスト</a>、
	<a href="../community/ml/openml.html#wwwML">Debian JP WWW メーリングリスト</a> など)</p></li>
	</ul>

	<p><a href="https://tokyodebian-team.pages.debian.net/">東京エリア Debian 勉強会</a> (主催：上川 純一) 
ならびに<a href="http://wiki.debian.org/KansaiDebianMeeting">関西 Debian 勉強会</a> （主催：倉敷 悟、佐々木 洋平、野方 純）を 
Debian JP Project 公認セミナーとしているほか、
<a href="http://www.ospn.jp/">オープンソースカンファレンス</a>や<a href="http://k-of.jp/">関西オープンソース (K-OF)</a>
などの Debian JP Project 会員が参加する各種活動を応援しています。
開催、参加しているイベントについては<a href="../community/events/index.html">こちら</a>をご覧下さい。</p>
	</div>

	<div class="section">
	<h2><a id="webadmin" name="webadmin">webadmin&#64;debian.or.jp について</a></h2>

	<p>Debian JP Project の Web ページに関するご感想・ご質問は
	<a HREF="mailto:webadmin&#64;debian.or.jp">webadmin&#64;debian.or.jp</a>
	までお送りください。このアドレスへのメールは
	<a href="../community/ml/openml.html#wwwML">Debian JP WWW メーリングリスト</a>
	に転送されます。Debian JP WWW メーリングリストは
	<a href="http://lists.debian.or.jp/debian-www/">公開されています</a>。</p>
