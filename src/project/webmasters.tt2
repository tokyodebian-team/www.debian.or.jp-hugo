[% title = "Debian JP のウェブページを変更するには" %]

<h2>Debian JP のウェブページを変更するには</h2>
<p>
本サイトは、Subversion を利用した
Debian JP Project 会員有志の共同作業により、コンテンツの維持・変更を管理しています。
実際にどのようにして行われているのか、どうしたらコンテンツの変更ができるのかなどについて、
興味がある方は以下の文書をご覧ください。</p>
<ol>
<li><a href="#joinml">Debian JP WWW メーリングリストに参加しよう</a></li>
<li><a href="#subversion">Subversion クライアントをインストール</a></li>
<li><a href="#get">Web ページソースの取得</a></li>
<li><a href="#look">中身を見てみよう</a></li>
<li><a href="#workflow">作業の一般的な流れ</a></li>
<li><a href="#blog">blog 記事の編集</a></li>
<li><a href="#joinjp">Debian JP Project の会員になろう</a></li>
<li><a href="#commit">変更を反映しよう</a></li>
<li><a href="#agreement">どんどん勝手に変えちゃっていい?</a></li>
<li><a href="#exclude">Subversion で管理されていないページ</a></li></ol>


<h3><a name="joinml" id="joinml">1. Debian JP WWW メーリングリストに参加しよう</a></h3>

<p>Debian JP WWW メーリングリストでは、
<a href="[% wwworg %]/">Debian Project</a>
の Web ページを和訳する作業や、現在ご覧になっている
<a href="http://www.debian.or.jp/">Debian JP Project</a>
の Web ページコンテンツに関する議論を行っています。</p>

<p>
どのようなやり取りが行われているか／いたのかについては、<a 
href="http://lists.debian.or.jp/pipermail/debian-www/">過去ログ</a>と
<a href="http://lists.debian.or.jp/archives/debian-www/">旧過去ログ</a>が参考になります。</p>

<p>参加方法は<a href="../community/ml/openml.html#wwwML">公開メーリングリスト</a>のページを参照してください。</p>


<h3><a name="subversion" id="subversion">2. Subversion クライアントをインストール</a></h3>

<p>Debian JP Project の Web ページはバージョン制御システムの Subversion
を用いて管理されているので、Web ページのソースを手元にコピーしたり
更新したりするには、Subversion の実行環境 (クライアント) を準備しておかなければなりません。
</p>
<ul>
<li>Debian GNU/Linux の場合: 「<strong>aptitude install subversion</strong>」で、Subversion がインストールされます。</li>
<li>その他の UNIX/Linux の場合: たいていは Subversion がパッケージ化されているので、どれをインストールすべきかすぐに見つかるはずです。見つからない場合でも、<a href="http://subversion.tigris.org/">開発元のページ</a>からソースコードをダウンロードしてコンパイル、実行できます。</li>
<li>Windows の場合：コマンド版もありますが、エクスプローラに統合された <a href="http://tortoisesvn.tigris.org/">TortoiseSVN</a> という強力なクライアントソフトウェアがあります。メニューを日本語化する「Language packs」も提供されています。</li>
</ul>
<p>ただし、Web ページソースから実際の完成品 Web ページに近い「プレビュー」を手元で作成するには、Debian GNU/Linux のほうが便利です。</p>


<h3><a name="get" id="get">3. Web ページソースの取得</a></h3>

<p>では、Debian JP Project の Web ページのソースを取得してみましょう。ネットワークに接続している状態で次のように実行します。</p>

<pre>
$ svn co https://svn.debian.or.jp/repos/www/trunk www.debian.or.jp
</pre>

<p>これで、カレントディレクトリ (コマンド実行時のワーキング
ディレクトリ) の下に "www.debian.or.jp" ディレクトリが作成され、
その中に 現在 Subversion で管理されているコンテンツが取得されます。管理している場所は「リポジトリ」と呼びます。</p>

<h3><a name="look" id="look">4. 中身を見てみよう</a></h3>

<p>Debian JP Project の Web ページでは、Template Toolkit (TT) という Perl を基盤にしたテンプレートシステムを採用しています。このシステムは機能はさほど多くありませんが、わかりやすく、たくさんの静的な Web ページを生成・管理するのに便利です。Debian では、libtemplate-perl パッケージをインストールすると利用できます。</p>

<p>また、Subversion のディレクトリは次のようにファイルおよびサブディレクトリで構成されています。</p>

<table summary="ディレクトリ構成">
<tr>
  <td>src/</td>
  <td>各 Web ページのソース。TT を使うファイルは .tt2 という拡張子を持ちます。</td></tr>
<tr>
  <td>src/blog/</td>
  <td>新着ニュース用ページ</td></tr>
<tr>
  <td>src/community/</td>
  <td>コミュニティ関連ページ。devel は開発者のコーナー、translate はドキュメント執筆者/翻訳者のコーナー</td></tr>
<tr>
  <td>src/css/</td>
  <td>CSS スタイルシートファイル</td></tr>
<tr>
  <td>src/image/</td>
  <td>画像・アイコン</td></tr>
<tr>
  <td>src/project/</td>
  <td>Debian JP Project 自体の組織構成、商標関連など</td></tr>
<tr>
  <td>src/using/</td>
  <td>導入と利用のページ</td></tr>
<tr>
  <td>include/</td>
  <td>TT のインクルードファイル。page がページ全体のテンプレートファイル。header/footer がヘッダ/フッタ、sidebar がメニューのテンプレート。config がメニューの構成やディレクトリ、代替マクロ名を設定。warnmsg.txt が工事・障害情報</td></tr>
<tr>
  <td>progs/</td>
  <td>主に、一部の半自動的に作成するページ用のツール集</td></tr>
<tr>
  <td>blosxom/</td>
  <td>blog/ディレクトリを構成するのに使われているblosxomデータとプラグイン。記事はblosxom/data/に拡張子.dのファイルを置く(EUC-JP、1行目にタイトル、2行目以降にタグ付きコンテンツ)</td></tr>
<tr>
  <td>materials/</td>
  <td>画像やアイコンの元データとなる各種ファイル</td></tr>
<tr>
  <td>ttreerc</td>
  <td>TT のサンプル設定ファイル</td></tr>
<tr>
  <td>www.memo.txt</td>
  <td>用語や設定情報などに関するいくつかのメモ</td></tr>
<tr>
  <td>Iconv.pm</td>
  <td>ファイルの文字エンコーディング変換を行う、TT 向けの追加モジュール</td></tr>
<tr>
  <td>XML</td>
  <td>RSS などの XML ファイルを解析する、TT 向けの追加モジュール</td></tr>
</table>

<p>
各 TT ソースファイルは、次のような単純な構成になっています。[% "[\% %\]" %]が TT の命令です。</p>

<pre>
[% "[\% title = \"ページのタイトル(ブラウザのタイトルバーに出るもの)\" %\]" %]
&lt;h2&gt;本文ページタイトル&lt;h2&gt;

...本文...</pre>

<p>
一般の作業者向けには、次のマクロが利用できます(include/config ファイルで定義しています)。</p>
<table summary="規定マクロ">
<tr>
  <td class="macro">[% "[\% wwworg %\]" %]</td>
  <td>http://www.debian.org (過去はwww.jp.debian.orgがあった場合にはこれを指定していた)</<td></tr>
<tr>
  <td class="macro">[% "[\% stable_codename %\]" %]</td>
  <td>安定版コードネーム (現在は [% stable_codename %])</td></tr>
<tr>
  <td class="macro">[% "[\% stable_version %\]" %]</td>
  <td>安定版バージョン (現在は [% stable_version %])</td></tr>
<tr>
  <td class="macro">[% "[\% stable_pointrelease %\]" %]</td>
  <td>安定版ポイントリリース (現在は [% stable_pointrelease %])</td></tr>
<tr>
  <td class="macro">[% "[\% stable_initialdate %\]" %]</td>
  <td>安定版初回リリース日 (現在は [% stable_initialdate %])</td></tr>
<tr>
  <td class="macro">[% "[\% stable_pointrelease_date %\]" %]</td>
  <td>安定版最新ポイントリリース日 (現在は [% stable_pointrelease_date %])</td></tr>
<tr>
  <td class="macro">[% "[\% oldstable_codename %\]" %]</td>
  <td>旧安定版コードネーム (現在は [% oldstable_codename %])</td></tr>
<tr>
  <td class="macro">[% "[\% oldstable_version %\]" %]</td>
  <td>旧安定版バージョン (現在は [% oldstable_version %])</td></tr>
<tr>
  <td class="macro">[% "[\% oldstable_eol %\]" %]</td>
  <td>旧安定版のサポート終了日。すでに終了している場合には「yes」。 (現在は [% oldstable_eol %])</td></tr>
<tr>
  <td class="macro">[% "[\% testing_codename %\]" %]</td>
  <td>テスト版コードネーム (現在は [% testing_codename %])</td></tr>
<tr>
  <td class="macro">[% "[\% testing_version %\]" %]</td>
  <td>テスト版バージョン (現在は [% testing_version %])</td></tr>
</table>

<p>
詳細については、include/ 内の各ファイル、libtemplate-perl-doc パッケージでインストールされる /usr/share/doc/libtemplate-perl-doc/html/index.html を参照してください。IF 条件分岐、INCLUDE インクルードなどを使って高度なページを構成することもできます。</p>

<p>
この TT から Debian JP Project のページを構成し、プレビューを行うには、次の手順になります。</p>

<ol>
<li>取得したディレクトリ  (www.debian.or.jp/) 直下をカレントディレクトリとします。</li>
<li>まず次のパッケージ (およびその依存パッケージ) をインストールします: <strong>libtemplate-perl, libxml-parser-perl, libxml-rss-perl, libtext-iconv-perl, nkf, wget, libtemplate-plugin-xml-perl(lenny 以降)</strong></li>
<li>カレントディレクトリにある libtemplate 用のプラグインファイル Iconv.pm を /usr/lib/perl5/Template/Plugin/ にコピーするかシンボリックリンクを作成します。</li>
<li>カレントディレクトリにある ttreerc を ~/.ttreerc にコピーし、src を「取得したディレクトリ/src」(たとえば /home/kmuto/www.debian.or.jp/src)に、dest をプレビューページを出力するディレクトリ (たとえば /home/kmuto/public_html/www.debian.or.jp) に、lib を「取得したディレクトリ/include」(たとえば /home/kmuto/www.debian.or.jp/include) に変更します。</li>
<li>「<strong>progs/fetch-dsa debug</strong>」を実行します。これでトップページに挿入される DSA 情報が /tmp に用意されます (こうしておかないと、トップページの作成時にエラーが発生します)。プレビューなので、ファイルさえ残っていれば一度だけでかまいません。</li>
<li>「<strong>ttree -v</strong>」を実行します(-v は冗長メッセージモードです)。これで、src ディレクトリからファイルのコピーおよび TT 変換が実行されて dest で指定したディレクトリに配備されます。</li>
<li>プレビューして正しく生成されたか確認しましょう。</li>
<li>src のソースファイルを修正して (この時点ではあなたの手元で作業しているだけなので、Debian JP Project の公開ページには影響しませんからご心配なく) 再度「ttree -v」を実行すれば、更新したファイルだけを対象として TT 変換されます。全部のファイルを TT 変換し直したい場合には、「ttree -a」とします。</li>
</ol>

<p>ttree は追加/更新のみで、src からファイルがなくなっても dest からの削除は行いません。削除する場合には dest に対して手動で行う必要があります。</p>


<h3><a name="workflow" id="workflow">5. 作業の一般的な流れ</a></h3>
<p>
<img src="../image/www-workflow.png" alt="ワークフロー概要図" height="468" width="438"></p>
<ol>
<li>「<strong>svn up</strong>」として、最初に手元の作業ディレクトリを更新しておきましょう。ファイルが変更された場合は「U 〜」、追加された場合は「A 〜」、削除された場合は「D 〜」のようにずらずらと表示され、最後に「リビジョン 98 です。」のように表示されるでしょう。「G 〜」や「C 〜」のように表示された場合には少し注意が必要です (後述)。「M 〜」と出るのはあなたが手元で修正をしていてまだリポジトリには反映されていないファイルです。<br>
更新後、プレビュー確認しておきます。</li>
<li>気になった点をピックアップします。大規模な変更が予想されたりする場合や他の作業者と被らない様に調整が必要な場合であれば、メーリングリストで事前にコミュニケーションしておきましょう。</li>
<li>お気に入りのエディタでソースファイルを頑張って編集します。</li>
<li>変更点が分かりやすくなるように「<strong>LANG=C svn diff src/index.tt2>/tmp/index.tt2</strong>」などとして手元の編集したファイルと元のファイルとの変更点を取得しましょう (ここで LANG=C しているのは、subversion のメッセージが UTF-8 で記録され、ソースファイルの EUC-JP エンコーディングと混在してしまうので、エディタによってはうまく開けないのを避けるためです)。</li>
<li>取得した diff を説明とともにメーリングリストに送付しましょう。</li>
<li>webmaster チームを中心とした Debian JP Project 会員が diff による変更をコミットして web に反映します。</li>
</ol>
<p>「svn up」を実行したときに、「G 〜」や「C 〜」が表示されたときには注意すべきであると述べました。これらはいずれも手元で変更してコミットしていないファイルに対し、リポジトリ側で更新が行われた (つまりほかの人が先に変更をコミットした) ことを示します。「G 〜」は、Subversion が賢く変更をマージできたことを意味します。とはいえ、形而上マージに成功しただけで、意味的にマージするわけではないので、悪影響を及ぼしていないか確認する必要があります。「C 〜」は、手元の変更とリポジトリの変更で「競合」が発生した状態です。競合している場合、ディレクトリには〜.r99のようにリビジョン数字が付いたリポジトリ内での状態のファイルと、〜.mineの手元で変更したファイルが置かれ、競合の発生したファイル自体は次のようになります。</p>
<pre>
...
&lt;&lt;&lt;&lt;&lt;.mine
 (あなたの変更していた内容)
=====
 (リポジトリでの変更内容)
&gt;&gt;&gt;&gt;&gt;.r99
...</pre>
<p>
競合を解決するには、手元の変更を破棄する方法と、手動で変更をマージする方法の2つがあります。
前者は「svn revert ファイル名」で、手元の変更は破棄され、リポジトリのものに戻されます (svn revert は競合の解決に限らず、手元でいろいろ修正したものの「やり直し」をしたいときにも便利です)。
後者は、上記の不等号で囲まれたファイル内容を適切に修正したあと、「svn resolved ファイル名」で競合解決を知らせます。
このあとにコミット (svn ci) 作業をすれば、競合解決の済んだファイルをコミットできます (競合内容を抱えたまま競合解決指示およびコミットしないよう注意してください)。</p>

<p>詳細については、『<a href="http://svnbook.red-bean.com/">Subversion によるバージョン管理</a>』 (<a href="http://www.caldron.jp/~nabetaro/hiki.cgi?SubversionWork">Tez さんの訳をベースに鍋太郎さんが作業されている日本語版</a>) および書籍『Subversion実践入門』(オーム社) などを参照してください。</p>


<h3><a name="blog" id="blog">6. blog 記事の編集</a></h3>

<p>blog/ で示される記事には、blosxom というシンプルな blog システムを採用しています。</p>
<p>blosxom/ ディレクトリが blosxom の本体です。plugins は機能拡張プラグインで、最小限のプラグインを登録してあります (実際に使用するには /etc/blosxom/plugins へのリンクが必要となります。新しいプラグインの登録が必要なときは管理チームに問い合わせてください)。data/flavours は見た目のテンプレートファイルです。この中にある maketemplate.pl ファイルは、head.html と foot.html を src/blog/dummy.tt2 の TT 変換後データから生成するものです (ヘッダ、メニュー、フッタの更新に対応させるため)。記事は、data ディレクトリ直下にコミットします。記事の拡張子は .d、文字エンコーディングは EUC-JP にしてください。記事は次のような構成になります。</p>

<pre>
記事タイトル

...内容...
</pre>

<p>内容には HTML タグを利用できます (タグの構造が壊れていると特に RSS 配信でエラーになるので注意してください)。TT タグは使えません。拡張子より前のファイル名について特に規定はありませんが、内容を示しかつ衝突のないもの (たとえば dwn-2000.d など) にしておくとよいでしょう。</p>

<p>日付を指定する必要はありません。ファイルの作成・更新時間に基づいて自動的に blog 上で表現されます。</p>


<h3><a name="joinjp" id="joinjp">7. Debian JP Project の会員になろう</a></h3>

<p>これまで述べた通り、Debian JP Project の Web ページはいずれも Subversion で管理されています。
Subversion は非常に強力な共同作業ツールで、Debian だけでなく、Windows などで作業することも可能です。</p>

<p>ただ、誰にでも変更を許してしまうと悪い人に書き換えられてしまうかもしれませんから :)、
Debian JP Project では、会員 (正会員) のみに書き換えの権限 (コミット権) を許可しています
(手元にソースを取得して中身を見ることは誰でもできます)。</p>

<p>これまでの作業内容を理解し、もっともっと積極的に Debian JP Project 
のページを良くしていきたい、と思われた方は<a 
href="./join.html">参加方法</a>の説明を読んで 
Debian JP Project への参加手続きを行ってください。
参加にあたっては<a href="[% wwworg %]/social_contract">Debian 社会契約</a>への賛同と
Debian JP Project の<a href="./regulation.html">会則</a>の遵守
が前提となります。</p>


<h3><a name="commit" id="commit">8. 変更を反映しよう</a></h3>
<ol>
<li>無事 Debian JP Project 会員となったら、リポジトリに提案や作業内容やを反映 (コミット) しましょう。
既存のファイルを変更しただけであれば、「svn ci -m "コミットメッセージ" 対象ファイル名」を実行します。
"コミットメッセージ" は、変更内容を簡潔に記したメッセージ文です (たとえば「fixed a typo」など)。
日本語も利用できないことはないですが、現時点では英語を使うことをお勧めします。</li>
<li>続いて、ユーザ名とパスワード (会員に提供されたもの) が問い合わせられます。正しく回答してください。<br>
「送信しています ... ファイルのデータを送信中です ... リビジョン 99 をコミットしました。」のように表示されれば成功です。</li>
<li>新規のファイルやディレクトリを追加したいときには、「svn add ディレクトリあるいはファイル名」とします (ディレクトリ内のファイル・サブディレクトリは自動的に追加されます)。削除は「svn rm ディレクトリあるいはファイル名」、移動は「svn mv 旧ファイル名(またはディレクトリ名) 新ファイル名(またはディレクトリ名)」です。ただし、移動には1つのファイル名またはディレクトリ名しか指定できないので注意してください。これらの操作は、まだリポジトリには反映されていないので、上記の「コミット (svn ci)」によって反映します。</li>
<li>
コミットは単一ファイルに基いて説明しましたが、Subversion のコミット単位はファイル単位ではなく、前回のコミットから今回のコミットまでに行われたすべてをもって1単位です。つまり、複数のファイルの変更・追加・移動・削除 (その他に属性の設定など) を一度のコミットで行うこともできます (ファイル名を複数指定するか、何も指定しない (対象を全部コミットすることになる) )。全体に対して大規模に何か行ったものの、手戻りに備えたいときなどには、このように一度のコミットにしておくことで簡単に操作できるようになります。</li>
<li>
include/ 内のファイルを操作するときには注意してください。ttreeがエラーなく進み、プレビューできることを確認しておくのがよいでしょう。不安なようであれば、Debian JP WWW メーリングリストに問い合わせてください。</li>
<li>
用語や注意については取得したディレクトリにある www.memo.txt も参照してください。</li>
<li>あとはワークフローの通り、繰り返しです。</li></ol>

<p>
会員は何時でもウェブページの修正を実施できます。
修正が登録されるとコミットログが Debian JP WWW メーリングリストに流れます。
そして、15分ごと(毎時2分,17分,32分,47分)に修正された内容が
<a href="http://www.debian.or.jp/">Debian JP Web ページ</a>に反映されます。</p>


<h3><a name="agreement" id="agreement">9. どんどん勝手に変えちゃっていい?</a></h3>

<p>ちょっとした (明らかな入力ミスなどの) 修正なら、
特に確認なしで「svn up」した内容を見てどんどん行ってかまいません。</p>

<p>内容の大幅な変更や、ベーステンプレート、ファイル構成の変更を含む場合には、
まずメーリングリストに変更案を出して、意見を聞いてみましょう。</p>

<h3><a name="exclude" id="exclude">10. Subversion で管理されていないページ</a></h3>

<p>Debian JP Project の Web ページの大部分は Subversion で管理されていますが、
一部、自動生成する必要から Subversion では管理されていなかったり、TT のスクリプトで自動生成しているページもあります。</p>
<ul>
 <li>DSA 情報<br>
トップページの右にある DSA 情報は、cron で 30 分ごとに更新されています。</li>
 <li><a href="http://lists.debian.or.jp/mailman/listinfo">http://lists.debian.or.jp/mailman/listinfo</a><br>
 Debian JP Project が管理するメーリングリストのアーカイブです。
 <li><a href="../using/book.html">書籍情報</a><br>
 書籍情報は、include/book.txt をデータベースとして、TT で自動生成しています。</li>
 <li><a href="../using/mirror.html">ミラーサイト</a><br>
 ミラーサイトは、MirrorsJP.list をデータベースとして progs/make-sources.list.sh を実行して更新します (progs/update mirror)。</li>
</ul>

<p>Subversion 管理外の Web ページに手を加えたい場合は、
debian-www メーリングリストで相談してください。
</p>


<h2>おわりに</h2>

<p>Debin JP Project の Web ページが「完成」することはありません。
全体構成についても「このほうがナビゲーションとして良いのでは」
「こういうドキュメントもあると良いのでは」ということがあることでしょう。
あなたもぜひ、Web コンテンツの作成を通して自分の「作品」を
Debian JP Project に contribute しませんか ?</p>
