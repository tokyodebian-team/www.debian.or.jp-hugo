[% title = "2002年のニュース" %]

<H2>2002年のニュース</H2>

[% INCLUDE newsdesc %]

<UL class="linkmenu">
<LI><A href="#20021216">Debian GNU/Linux 3.0 updated (r1)</A>
	<SPAN class="lastmodified">(2002年12月16日)</SPAN></LI>
<LI><A href="#20020920">Debian 商標の登録について</A>
	<SPAN class="lastmodified">(2002年09月20日)</SPAN></LI>
<LI><A href="#20020904">Debian BOF 開催と展示デモのお知らせ(再)</A>
	<SPAN class="lastmodified">(2002年09月04日)</SPAN></LI>
<LI><A href="#20020815">Debian BOF 開催と展示デモのお知らせ</A>
	<SPAN class="lastmodified">(2002年08月15日)</SPAN></LI>
<LI><A href="#20020719">Debian GNU/Linux 3.0 (woody) がリリース</A>
        <SPAN class="lastmodified">(2002年07月19日)</SPAN></LI>
<LI><A href="#20020713">Debian GNU/Linux 2.2r7 がリリース</A>
        <SPAN class="lastmodified">(2002年07月13日)</SPAN></LI>
<LI><A href="#20020618">stable/testing/unstable の日本語訳について
        <SPAN class="lastmodified">(2002年06月18日)</SPAN></LI>
<LI><A href="#20020525">Debian JP 開発版アーカイブ (woody-jp/sid-jp) の品質向上について
        <SPAN class="lastmodified">(2002年05月25日)</SPAN></LI>
<LI><A href="#20020403">Debian GNU/Linux 2.2r6 がリリース</A>
        <SPAN class="lastmodified">(2002年04月03日)</SPAN></LI>
<LI><A href="#20020110">Debian GNU/Linux 2.2r5 がリリース</A>
        <SPAN class="lastmodified">(2002年01月10日)</SPAN></LI>
</UL>

<HR>
<H2><A name="20021216">2002年12月16日: Debian GNU/Linux 3.0 updated (r1)</A></H2>

<P>Debian GNU/Linux 3.0r1 は、Debian GNU/Linux 3.0 (コードネーム
「woody」) の最初のアップデートです。主に、安定版 (stable) に
セキュリティアップデートを追加するものですが、深刻なバグも
いくつか修正されています。security.debian.org からのアップデートを
頻繁に行っている方は、それほど多くのパッケージをアップデートする
必要はないでしょう。また、security.debian.org に登録された
アップデートはほとんど、今回のアップデートに含まれています。</P>

<P>今回のアップデートは、Debian GNU/Linux 3.0 の新バージョンではなく、
単に一部のパッケージをアップデートしたものです。
バージョン 3.0r0 の CD を捨てる必要はありませんが、インストール後、
ftp.debian.org を用いてアップデートをする必要があります。</P>

<P>オンラインでこのリビジョンへアップデートするには、通常、
Debian の FTP/HTTP ミラーのどれか一つを参照するように
「apt」パッケージツールを設定します。 
(sources.list(5) のマニュアルページを参照してください)</P>
<P>ミラーの一覧は、

   <A href="http://www.debian.org/distrib/ftplist">http://www.debian.org/distrib/ftplist</A>

で参照できます。</P>

<H3>セキュリティ上の修正</H3>

<P>本リビジョンは、安定版リリースに対して、以下のセキュリティアップデートを
追加します。セキュリティチームはすでに、以下の個々のアップデートに対して
セキュリティ勧告を発表済みです。</P>

<PRE>
Debian セキュリティ勧告番号    パッケージ

        DSA 137                mm
        DSA 138                gallery
        DSA 139                super
        DSA 140                libpng
        DSA 141                mpack
        DSA 142                openafs
        DSA 143                krb5
        DSA 144                wwwoffle
        DSA 145                tinyproxy
        DSA 146                dietlibc
        DSA 147                mailman
        DSA 148                hylafax
        DSA 149                glibc
        DSA 150                interchange
        DSA 151                xinetd
        DSA 152                l2tpd
        DSA 153                mantis
        DSA 154                fam
        DSA 155                kdelibs
        DSA 156                epic4-script-light
        DSA 157                irssi-text
        DSA 158                gaim
        DSA 159                python 1.5
        DSA 160                scrollkeeper
        DSA 161                mantis
        DSA 162                ethereal
        DSA 163                mhonarc
        DSA 166                purity
        DSA 167                kdelibs
        DSA 168                php3, php4
        DSA 169                htcheck
        DSA 170                tomcat4
        DSA 171                fetchmail, fetchmail-ssl
        DSA 172                tkmail
        DSA 173                Bugzilla
        DSA 174                heartbeat
        DSA 176                gv
        DSA 179                gnome-gv
        DSA 180                nis
        DSA 182                kdegraphics
        DSA 183                krb5
        DSA 186                log2mail
        DSA 187                Apache
        DSA 188                Apache-SSL
        DSA 189                luxman
        DSA 191                squirrelmail
        DSA 192                html2ps
        DSA 194                masqmail
        DSA 195                Apache-Perl
        DSA 197                Courier sqwebmail
        DSA 198                nullmailer
        DSA 199                mhonarc
        DSA 200                samba
        DSA 201                Free/SWan
        DSA 202                im
        DSA 204                kdelibs
        DSA 205                gtetrinet
        DSA 206                tcpdump
        DSA 207                tetex-bin
</PRE>

<H3>その他のバグ修正</H3>

<P>本リビジョンは、以下のパッケージに対して重要な修正を追加します。
その多くはシステムのセキュリティには関係しませんが、データが
損なわれる可能性があります。</P>

<PRE>
    arcboot, tip22             SGI MIPS マシンの tftp ローダ
    bastille                   壊れたパッケージの修正
    cron-apt                   重要な修正
    debiandoc-sgml             壊れたアップグレードの修正
    defrag                     ジャーナルに関するファイルシステム破壊の修正
    docbook-xml-slides         壊れた依存関係の修正
    eroaster                   重要な修正
    gnome-pim                  重要な修正
    initrd-tools               新しいカーネルのサポートの追加
    kernel-image-2.2.22-alpha  セキュリティ上の修正
    kernel-image-sparc-2.4     セキュリティ上の修正
    kernel-patch-2.4.17-s390   重要な修正
    kernel-patch-2.4.19-mips   セキュリティ上の修正
    kernel-source-2.2.22       セキュリティ上の修正
    libgd                      壊れたパッケージの修正
    libquota-perl              重要な修正
    logtool                    壊れたパッケージの修正
    mpqc                       壊れた依存関係の修正
    msttcorefonts              フォントのパスの修正
    muddleftpd                 セキュリティ上の修正の訂正
    murasaki                   重要な修正
    ocaml                      重要な修正
    octave2.1                  壊れた依存関係の修正
    pcmcia-modules-2.2.22      kernel-source-2.2.22 に対してビルド
    qpopper                    重要な修正
    rio500                     重要な修正
    silo                       UltraSPARC III+ マシンのサポートの追加
    snort                      重要な修正
    sympa                      壊れた依存関係の修正
    tendra                     ライセンス違反の修正
    uptimed                    アップグレードパスの修正
    weex                       重要な修正
    yaclc                      重要な修正
</PRE>

<H3>削除されたパッケージ</H3>

<P>以下のパッケージは、ディストリビューションから削除しなければなりませんでした。</P>

<PRE>
    cdrdao, gcdmaster          ライセンス上の問題
</PRE>

<P>パッケージの、受理/拒否の理由の一覧が、以下の
ページ (準備中) にあります。</P>
<blockquote>
  <a href="http://people.debian.org/~joey/3.0r1/">http://people.debian.org/~joey/3.0r1/</a>
</blockquote>

<H3>URL</H3>

<P>本リビジョンで変更されたパッケージの一覧:</P>
<blockquote>
  <a href="http://http.us.debian.org/debian/dists/Debian3.0r1a/ChangeLog">http://http.us.debian.org/debian/dists/Debian3.0r1a/ChangeLog</a>
  <a href="http://non-us.debian.org/debian-non-US/dists/Debian3.0r1/non-US/ChangeLog">http://non-us.debian.org/debian-non-US/dists/Debian3.0r1/non-US/ChangeLog</a>
</blockquote>

<P>現在の安定版 (stable) ディストリビューション:</P>
<blockquote>
  <a href="ftp://ftp.debian.org/debian/dists/stable">ftp://ftp.debian.org/debian/dists/stable</a>
  <a href="ftp://non-us.debian.org/debian-non-US/dists/stable">ftp://non-us.debian.org/debian-non-US/dists/stable</a>
</blockquote>

<P>安定版ディストリビューションに対する更新の候補:</P>
<blockquote>
  <a href="ftp://ftp.debian.org/debian/dists/proposed-updates">ftp://ftp.debian.org/debian/dists/proposed-updates</a>
  <a href="ftp://non-us.debian.org/debian-non-US/dists/proposed-updates">ftp://non-us.debian.org/debian-non-US/dists/proposed-updates</a>
</blockquote>

<P>安定版ディストリビューションの情報 (リリースノート、訂正など):</P>
<blockquote>
  <a href="http://www.debian.org/releases/stable/">http://www.debian.org/releases/stable/</a>
</blockquote>

<P>セキュリティに関する告知と情報:</P>
<blockquote>
  <a href="http://www.debian.org/security/">http://www.debian.org/security/</a>
</blockquote>

<H3>Debian について</H3>

<P>Debian Project は、完全にフリーなオペレーティングシステムである
Debian GNU/Linux と Debian GNU/Hurd の開発のために時間と労力を
進んで提供している、フリーソフトウェア開発者の組織です。</P>

<H3>連絡先</H3>

<P>より詳しい情報を希望する場合は、Debian のウェブページ
<a href="http://www.debian.org/">http://www.debian.org/</a> をご覧になるか、
<a href="mailto:press&#64;debian.org">press&#64;debian.org</a>にメールを送ってください。</P>

<HR>
<H2><A name="20020920">2002年09月20日: Debian 商標の登録について</A></H2>

<P>Debian JP Project では、このたび Software in the Public Interest, Inc.
(<A href="#20020920-spi">*1</A>)
と共同で、「デビアン/Debian」 を商標登録 
(<A href="#20020920-tm">*2</A>) しました。</P>

<UL>
<LI><A name="20020920-spi">*1</A>)
 Software in the Public Interest, Inc 
 (<A href="http://www.spi-inc.org/">http://www.spi-inc.org/</A>)
     略称 SPI。Debian Project の資産管理団体であり、Debian の開発を
     支援する目的で設立された非営利組織。米国およびその他の国における
     Debian 商標は SPI によって登録、管理されています。
<LI><A name="20020920-tm">*2</A>) 
     出願人は Software in the Public Interest, Inc.
     および出願当時の Debian JP Project 理事会メンバーです。
     (Debian JP Project は法人格を持たないため、出願した時点における
     代表者として当時の理事である鵜飼文敏、佐野武俊、石川睦、八田修三、
     武藤健志の連名で登録しております。)
</UL>

<H3>日本国内で Debian 商標を登録した理由</H3>

<P>ここ数年、Linux が日本国内でも有名になってきたせいか、JLA 等の
Linux 関連団体と無関係な第三者が Linux に関する商標を登録した上で、
関係する企業・団体に「商標法上違反である」との警告を通知してくる
といった
<A href="http://jla.linux.or.jp/WG/TradeMark/">事例が起きています</A>。</P>

<P>幸いにして Debian についてはまだ実際にそうした事例が起きたことは
ありませんが、Debian Project と無関係な第三者に商標登録されてしまった
場合、Debian Project / Debian JP Project を運営していく上で大きな問題
となる可能性や、Debian をビジネスに適用する際の障害となる危険性を無視
できないと考えました。(最近もSpainでDebianの商標が登録されたようです。
  <A href="http://lists.debian.org/debian-legal/2002/debian-legal-200209/msg00025.html">http://lists.debian.org/debian-legal/2002/debian-legal-200209/msg00025.html</A>,
  <A href="http://lists.debian.org/debian-legal/2002/debian-legal-200209/msg00034.html">http://lists.debian.org/debian-legal/2002/debian-legal-200209/msg00034.html</A>,
  <A href="http://lists.debian.org/debian-legal/2002/debian-legal-200209/msg00057.html">http://lists.debian.org/debian-legal/2002/debian-legal-200209/msg00057.html</A>)</P>

<P>Linux の場合と異なり、Debian は (まだ) Linux ほど一般の認知も
高くないため、取引者・需用者間に浸透して周知・著名になっていると
判断されることを期待できません。実際、商標を登録できたこと自体が
Linux のように公益性を有する OS の名前であると判断されていない
ことを示しています。</P>

<P>従って、商標を登録しなければ、将来において問題のおきる可能性が
高いものと判断し、Debian の本来の商標登録者である Software in 
the Public Interest, Inc. と共同で、日本における Debian 開発者を
代表して (当時の) Debian JP Project 理事の連名により Debian 商標を
出願、登録したものです。</P>

<H3>Debian 商標の使用について</H3>

<P>Debian に関する商品を販売するなどの際、Debian 商標の表示が必要な
場合には、以下のように表現することを推奨します。</P>

<PRE>
     Debian は Software in the Public Interest, Inc. の登録商標です。
</PRE>

<P>その他、Debian 商標の使用に関する詳細は 
<A href="http://www.spi-inc.org/trademarks">http://www.spi-inc.org/trademarks</A>
を参照してください。</P>

<P>なお、日本国内における Debian 商標は日本の Debian 開発者を代表して
Debian JP Project が管理しています。</P>

<H3>寄付のお願い</H3>

<P>Debian JP Project ではサーバーの維持・管理に必要な機材やネットワーク資源、
およびそれらの調達や今回の商標登録などのような日本での Debian の普及に
必要と思われる活動のための資金をみなさまからの寄付によって得ることで
運営しています。</P>

<P>今後も充実した活動およびサービスの向上を図っていくために、みなさまの
御協力をよろしくお願いします。</P>

<P>なお、寄付についての詳細は 
<A href="http://www.debian.or.jp/Donations.html">http://www.debian.or.jp/Donations.html</A>を
御覧ください。</P>

<HR>
<H2><A name="20020904">2002年09月04日: Debian BOF 開催と展示デモのお知らせ(再)</A></H2>

<P><A href="http://lc.linux.or.jp/lc2002/">Linux Conference 2002</A> 
において、Debian BOFの開催、および .org展示コーナーにおいてDebianマシン等
の展示をおこないます。事前登録の期限が近づいていますので、再度アナウンス
いたします。

<PRE>
日時: 2002 年 9月 18日(木)〜20日(金)
場所: 東京、サンケイプラザ
<A href="http://www.s-plaza.com/outline/map.html">http://www.s-plaza.com/outline/map.html</A>
</PRE>

<H3>BOF</H3>

<H4>タイトル: 「<A href="http://lc.linux.or.jp/lc2002/detail.html#bof-debian">祝 Debian 3.0 リリース --- Woody がやってきた!</A>」</H4>
<DL>
 <DT>日時:</DT>
 <DD>2002 年 9月 19日 (木)   17:00〜19:00</DD>
</DL>
<DL>
 <DD>前作の 2.2 ポテトから 2 年半の時を経てようやく登場した Woody。
   そのリリースを記念して、新しく登場した便利な機能や追加された
   パッケージ、またインストールや設定における変更点などなど、
   Woody についての tips や Q&amp;A をみんなで話しましょう。<BR>
   Woody 以降に登場してくる大きな変化についての予測とか、
   日頃 Debian を使っていて感じていることや、やりたいと
   思っていること、あるいはこの機会に聞いてみたいと思って
   いることなど、なんでも OK。</DD>
</DL>

<H3>Debian JP Project ブース</H3>

<P>Linux Conference 2002 の会場で期間中開かれる .org 出展コーナーでは、
Debian JP Project ブースにおいて、さまざまなアーキテクチャのマシンで 
Debian woody が動く様子を実演展示する予定です。</P>

<P>BOF および展示ブースには、Debian/Debian JP Project のメンバーも参加し
ますので、メンテナを目指している方はぜひこの機会に GnuPG 鍵交換を行な
いましょう。作成した GnuPG 鍵の fingerprint (gpg --fingerprint あなた
のID で表示されます) を印刷しておき、身分を証明できるもの(例: パスポー
ト、運転免許証など写真つきのもの)とともに携行してきてください。
<A href="http://www.debian.org/events/keysigning">http://www.debian.org/events/keysigning</A></P>

<P>※.org 出展コーナーの運営はボランティアによって行なわれます。まだ人手
が不足していますので、運営を手伝ってもよいという方がいらっしゃいました
ら、jp-events&#64;debian.or.jp ML へご参加ください (参加方法は
debian-users ML などと同様です)。運営に参加して下さった方には、
Debian/Namazu T シャツを進呈します。</P>

<P>多くの皆様の参加をお待ちしております。</P>

<P>以上、Debian JP Project からのお知らせでした。</P>

<H3>Linux Conference 2002について</H3>

<P>Linux Conference は、<A href="http://jla.linux.or.jp/">日本 Linux 協会</A>
が主催する、Linux およびオープンソースソフトウェアの本格的なカンファレンス
イベントです。</P>

<P>※日本 Linux 協会の会長は、Debian JP Project 会長でもある鵜飼 文敏が務
  めています。</P>

<P>本年の Linux Conference 2002 は、東京大手町のサンケイプラザにて、
2002年9月18日(水)〜2002年9月20日(金) の期間開催されます。</P>

<P>カンファレンスの期間中には、本リリースでご案内した BOF (19日) および
.orgブース出展 (18日〜20日) のほか、Debian Project および Debian JP
Project のメンバーである 八田 真行 (18日「
<A href="http://lc.linux.or.jp/lc2002/detail.html#c1">『コピーレフト』とは何か</A>」)
、武藤 健志 (19日「<A href="http://lc.linux.or.jp/lc2002/detail.html#orca">ORCA インフラストラクチャとしての Linux システム構築</A>」) による講演・チュートリアルも行われます。</P>

<P>Debian に限らず、Linux Conference はさまざまな分野の最新情報を得たり人
脈を広げたりするのに絶好の機会ですので、ぜひお申し込み、ご来場ください。</P>

<P>Linux Conferenceのお申し込みやプログラム詳細につきましては
<A href="http://lc.linux.or.jp/">http://lc.linux.or.jp/</A>
をご覧ください。</P>

<HR>
<H2><A name="20020815">2002年08月15日: Debian BOF 開催と展示デモのお知らせ</A></H2>

<P><A href="http://lc.linux.or.jp/lc2002/">Linux Conference 2002</A>
において、Debian BOF が開催されます。</P>

<PRE>
 日時: 2002 年 9月 19日 (木)   17:00〜19:00
 場所: 東京、サンケイプラザ
 <A href="http://www.s-plaza.com/outline/map.html">http://www.s-plaza.com/outline/map.html</A>
</PRE>

<H3>タイトル: 「祝 Debian 3.0 リリース --- Woody がやってきた!」</H3>

<DL>
<DT>概要:</DT>
<DD>前作の 2.2 potato から 2 年半の時を経てようやく登場した Woody。
   そのリリースを記念して、新しく登場した便利な機能や追加された
   パッケージ、またインストールや設定における変更点などなど、
   Woody についての tips や Q&amp;A をみんなで話しましょう。<BR>
   Woody 以降に登場してくる大きな変化についての予測とか、
   日頃 Debian を使っていて感じていることや、やりたいと
   思っていること、あるいはこの機会に聞いてみたいと思って
   いることなど、なんでも OK。</DD>
</DL>

<P>また、Linux Conference 2002 の .org 出展コーナーでは、数種類の
Debian マシンによる woody の実演展示が行なわれる予定です。
  .org 出展コーナーの運営はボランティアによって行なわれます。まだ
人手が不足していますので、運営を手伝ってもよいという方がいらっしゃ
いましたら、jp-events&#64;&#64;debian.or.jp ML へご参加ください (参加方法は
debian-users ML などと同様です)。</P>

<P>多くの皆様の参加をお待ちします。</P>

<HR>
<H2><A name="20020719">2002年07月19日: Debian GNU/Linux 3.0 (woody) がリリース</A></H2>

<P>2002 年 7 月 19 日、Debian GNU/Linux 3.0 (woody)
が正式にリリースされました。</P>

<P>詳しくは、<A href="http://www.jp.debian.org/releases/woody/">Debian 
のリリース情報</A>および<A href="../releases/woody-jp/">Debian JP
のリリース情報</A>を参照してください。</P>

<P>woody には、日本語を扱えるパッケージが多数含まれていますので、
Debian 単体だけで、十分実用的な日本語環境を構築することが
可能となっています。
そのため、Debian JP パッケージ集 for woody (woody-jp) は、
依然として存在しますしダウンロード可能ですが、
<A HREF="../releases/jp-release">正式にはリリースしません。</A></P>

<P><B>Debian JP パッケージ集 for woody (woody-jp)
は、まだ準備ができていません。準備ができるまで、いましばらく
お待ちください。</B></P>


<HR>
<H2><A name="20020713">2002年07月13日: Debian GNU/Linux 2.2r7 がリリース</A></H2>

<P>Debian GNU/Linux 2.2 'potato' の新しいリビジョンがリリースされまし
      た。今回の更新であるバージョン 2.2r7 は、2.2r6 以降で発見された
      安定版に対するセキュリティホールの修正と重大なバグの修正がなされ
      たものです。potato を使っているユーザーがこのリビジョンにアップ
      グレードするのは簡単です。<a href="../Near-Mirror.html">近くのミ
        ラーサイトを探すには</a>に沿って
      /etc/apt/sources.list を編集、apt-get update; apt-get upgrade
      を行うだけであなたのマシンは自動的にアップグレードされます。</P>
      <P>本リビジョンに伴う変更について詳しくは
      <a href="http://www.jp.debian.org/News/2002/20020713">Debian GNU/Linux 2.2 r7 リリース情報</a>を御覧ください。</P>

<H2><A name="20020618">2002年6月18日:
stable/testing/unstable の日本語訳について</A></H2>

<P>
Debian には stable/testing/unstable の 3 つの
<A HREF="http://www.jp.debian.org/releases/">リリース</A>
がありますが、
これらに対して統一された日本語訳が決められていませんでした。
そこで今回、誤解を受けにくく、それぞれを明確に区別できるように、
統一された訳語を以下のように決定しました。
</P>

<UL>
 <LI>stable: 安定版
 <LI>testing: テスト版
 <LI>unstable: 不安定版
</UL>

<P>
従来、「testing」には「テスト版」とならんで「試験版」という
訳語が主に用いられてきました。しかし、この訳語は、
一時期 unstable の日本語訳として使われた「実験版」と
紛らわしいため、使用を避けることとします。
</P>

<P>
また、従来、「unstable」には主に「開発版」という訳語が
用いられてきました。しかし、「開発環境が同梱された版」
と誤解されるおそれがあるため、使用を避けることとします。
一部で使われてきた「実験版」も、「experimental」
(当分は訳語を設けないこととします) と混同のおそれがあるため、
使用しません。
「開発版」とは異なる新たな訳語を考案するにあたり、
原語との対応が分かりやすく、混乱を招くおそれが少ない語を選びました。
</UL>

<P>
「stable」については、従来通りの訳語です。
</P>

<P>
今後、Debian JP Project および Debian Project
における日本語訳作業では、この訳語を用いるようにします。
また、現時点ですでにウェブページ等で用いられている訳語については、
今後すみやかに新しい訳語に統一する予定です。
</P>

<H2><A name="20020525">2002年5月25日:
Debian JP 開発版アーカイブ (woody-jp/sid-jp) の品質向上について</A></H2>

<P>Debian JP Project では、現在開発中の woody-jp/sid-jp に
含まれる JP パッケージについて、その品質を向上し、
ユーザーが安心して利用できるようにすることを目的として
以下のルールを制定しました。</P>

<OL>
  <LI>
    <P><A HREF="http://www.debian.or.jp/Lists-Archives/jp-policy/200202/msg00011.html">
       Debian JP tree に upload を行う場合
       debian/control に以下のフィールドを付加し 
       Debian JP package であることを明示すること (必須)</A>
       </P>
     <PRE>
              Origin: debian-jp
              Bugs: debbugs://bugs.debian.or.jp
     </PRE>
  </LI> 

  <LI>
    <P>パッケージが project-jp/experimental/orphaned に
       移動されてから一年を経過した場合、該当パッケージを
       Debian JP のアーカイブから削除する</P>
  </LI> 
</OL>
 
<P>
また、このルールをアーカイブに反映させるため、また間近に
迫ってきた Debian 3.0 (woody) のリリースに向け woody-jp 
アーカイブを「使える」状態にするため、</P>

  <UL>
     <LI>a) Debian 3.0 (woody) リリース</LI>
     <LI>b) 2002 年 7/1</LI>
  </UL>

<P>
のどちらか「あとに」発生した事象のタイミングで
以下の作業を実施します。<P>

  <UL>
   <LI>c) 上記 1. のルールを満たしていないパッケージの
       project-jp/experimental/orphaned への移動 
       (woody-jp/sid-jp からの削除)</LI>
 

   <LI>d) <A HREF="#20020525-2">
      Severity: important 以上の JP Bug リポートのある
      パッケージをリリースクリティカルバグだとみなし
      </A>、
      woody-jp からの削除。</LI>

  </UL>
 
<P>
  (ただし、c) は d) に優先するものとします。つまり、
   1. のルールに従っていないパッケージは woody-jp から
   だけでなく、sid-jp からも削除されるということです。)
</P>
 
<BLOCKQUOTE>
<A NAME="20020525-2">
     Debian Project では「リリースクリティカルバグ」の
     重要度を Severity: serious としていますが、Debian JP では
     BTS のバージョンが古く、Severity: serious が設定できません。
     このため、今回の作業では Severity: important を基準とします。
</A></BLOCKQUOTE>

<P>
以上、Debian JP Project から、「Debian JP 開発版アーカイブ
 (woody-jp/sid-jp) の品質向上について」のお知らせでした。
</P>

<P>
 (注: 現在の「安定版」である potato-jp や、過去にリリースされた
  slink-jp、hamm-jp などのアーカイブにこのルールを遡って適用する
  ことはありません。)
</P>

<H2><A name="20020403">2002年04月03日: Debian GNU/Linux 2.2r6 がリリース</A></H2>

<P>Debian GNU/Linux 2.2 'potato' の新しいリビジョンがリリースされまし
      た。今回の更新であるバージョン 2.2r6 は、2.2r5 以降で発見された
      安定版に対するセキュリティホールの修正と重大なバグの修正がなされ
      たものです。potato を使っているユーザーがこのリビジョンにアップ
      グレードするのは簡単です。<a href="../Near-Mirror.html">近くのミ
        ラーサイトを探すには</a>に沿って
      /etc/apt/sources.list を編集し、apt-get update; apt-get upgrade
      を行うだけであなたのマシンは自動的にアップグレードされます。</P>
      <P>本リビジョンに伴う変更について詳しくは
      <a href="http://www.jp.debian.org/News/2002/20020403">Debian GNU/Linux 2.2r6 リリース情報</a>を御覧ください。</P>

<H2><A name="20020110">2002年01月10日: Debian GNU/Linux 2.2r5 がリリース</A></H2>

<P>Debian GNU/Linux 2.2 'potato' の新しいリビジョンがリリースされまし
      た。今回の更新であるバージョン 2.2r5 は、2.2r4 以降で発見された
      安定版に対するセキュリティホールの修正と重大なバグの修正がなされ
      たものです。potato を使っているユーザーがこのリビジョンにアップ
      グレードするのは簡単です。<a href="../Near-Mirror.html">近くのミ
        ラーサイトを探すには</a>に沿って
      /etc/apt/sources.list を編集し、apt-get update; apt-get upgrade
      を行うだけであなたのマシンは自動的にアップグレードされます。</P>
      <P>本リビジョンに伴う変更について詳しくは
      <a href="http://www.jp.debian.org/News/2002/20020110">Debian GNU/Linux 2.2r5 リリース情報</a>を御覧ください。</P>
