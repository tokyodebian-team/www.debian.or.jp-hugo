[% title = "Debian GNU/Linux クイックインストール解説" %]

<h2>Debian GNU/Linux クイックインストール解説</h2>

<p class="small">この文書は、『Software Design』誌（技術評論社）<a href="http://www.gihyo.co.jp/magazines/SD/archive/200606">2006年6月号</a>の特集「Debian GNU/Linuxを究める」に寄稿された記事を、筆者および技術評論社の許可の下、一部加筆修正を行った上で公開するものです。</p>
<p>
<a href="http://www.debian.org/News/2008/20080229">2008年3月末を以って、<strong>Debian GNU/Linux 3.1 "Sarge" はすべてのサポートを終了</strong></a>しています。今後導入を検討される場合は、<a href="../quick-etch/index.html">Debian GNU/Linux 4.0 "Etch" のクイックインストール解説</a>などを参考にしてください。</p>

<p class="right">武藤 健志 &lt;kmuto&#64;debian.org&gt;</p>

<p>Sarge ではインストーラが一新され、「Debian-Installer」（d-i）と呼ばれるモジュール型インストーラとなりました。d-i は小さな機能を持つモジュールの集合体であり、組み合わせを変えることで、CD、ネットワーク、USBメモリ、フロッピーディスクといったさまざまな媒体に対応できるようになっています。</p>

<p>公式 CD あるいは DVD をすでに手に入れているならそれに越したことはありませんが、ADSL や光などのブロードバンドネットワークと内蔵ネットワークデバイス（あるいはほとんどの PC カードと、有線の CardBus デバイス）があるなら、巨大な公式 CD イメージをダウンロードしなくても、小さなブート CD を作るだけで残りはネットワークから随時取得するようにできます。ここでは、Intel IA-32 (x86) アーキテクチャのマシン向けに、最小 CD + ネットワークを使って、基本インストールからデスクトップ環境を構築するまでの手順を説明します。いずれにせよ、Debian を使う上でインターネットアクセスはほぼ必須です。</p>

<h3 id="preparation"><a name="preparation" href="#preparation">CD とディスクの準備</a></h3>
<p>公式 CD あるいは DVD を入手していない場合、まず、配布 Web ページ（<a href="[% wwworg %]/CD/netinst/">[% wwworg %]/CD/netinst/</a>）から、ネットワークインストール CD の ISO イメージファイル（debian-31r4-i386-netinst.iso、112MB）をダウンロードし、適当な CD 作成ツールを使って CD-R に焼き付けます。</p>

<p>d-i は Windows の FAT および NTFS のパーティションのサイズ変更が可能なので、インストール対象のディスクに事前に空き領域を作らなくても一応は対処できますが、これは若干わかりにくいので、操作中に誤って全部を消してしまう恐れはあります。シマンテック社の Partition Magic や Knoppix の QtParted といったよりわかりやすいツールを使って事前に空き領域を作っておくのもよいでしょう。500MB 程度でも文字ベースであれば一応の環境は整えられますが、GUI デスクトップ環境を構築する場合、最低 3GB 程度の領域が必要です。いずれにせよ、ディスクにある重要なデータはバックアップしておくのが賢明です。</p>

<h3 id="stage1"><a name="stage1" href="#stage1">起動と基本セットアップ</a></h3>
<p>では早速、作成した CD を使ってインストールします。d-i はさまざまな補助機能を持っていますが、ここでは最小手順のみ紹介します。</p>

<ol>
<li>作成した CD を入れて起動すると、Debian のロゴとプロンプトが表示されます（図1）。
<div class="img">
<a href="full/d-i1.png"><img src="d-i1.png" alt="Debian-Installer の起動"></a><br />
図1●Debian-Installer の起動
</div>
そのまま Enter を押すとカーネルバージョン 2.4.27 のインストーラが始まりますが、あえてこのバージョンにこだわらないなら、「linux26」と入れて Enter を押し、カーネルバージョン 2.6.8 のインストーラを起動します（こちらのカーネルのほうが、シリアル ATA などの最新デバイスにより良く対応しています）。</li>
<li>インストーラがロードされ終わると、まず言語の選択画面が表示されるので、↑↓キーを使って「Japanese - 日本語」を選び、Enter を押します（図2）。続くキーボードの選択では、日本語キー配列の場合、デフォルトの「日本（106キー）」のまま Enter を押します。
<div class="img">
<a href="full/d-i1.2.png"><img src="d-i1.2.png" alt="言語の選択"></a><br />
図2●言語の選択
</div>
</li>
<li>拡張インストーラモジュールを読み込むために必要なネットワークデバイスの自動認識が行われ、このマシンに付ける「ホスト名」と「ドメイン名」を指定するよう要求されます。利用しているネットワークに合ったものを入力してください。これで、DHCPによる自動ネットワーク設定が行われます（DHCPサーバが見つからないときには静的指定するかどうか尋ねられます）。</li>
<li>必要なインストーラモジュールがダウンロードされ、ハードディスクが検出されます。これからいよいよパーティショニングという注意を要する作業に入ります。発見されたディスクデバイスと、「ディスク全体を消去する」「手動でパーティションテーブルを編集」、それに前述したように空き領域を事前に作成している場合には「最大の空き領域を使う」という選択肢が示されます。

  <ul>
  <li><strong>ディスク全体を消去する</strong>：何も OS がインストールされていない、あるいは全内容を消してもよい場合に選択する。Windows がインストールされており削除したくないときには選ばないように！</li>
  <li><strong>手動でパーティションテーブルを編集</strong>：Windows パーティションをここで縮小したいときや、インストール時点で複雑なパーティション構成、あるいは LVM や RAID を使いたいときに選択する。図3の画面に直接行き、手動でパーティションを作成・削除・変更・設定する。</li>
  <li><strong>最大の空き領域を使う</strong>：事前に用意した空き領域を使う場合に選択する。</li>
  </ul>
</li>
<li>「ディスク全体を消去する」や「最大の空き領域を使う」では、お任せのパーティション構成として「すべてのファイルを1つのパーティションに」「デスクトップマシン」「マルチユーザワークステーション」を選べます。デスクトップ用途では「すべてのファイルを1つのパーティションに」で十分です。</li>
<li>パーティショニング計画の結果が表示されます（図3）。問題がなければ「パーティショニングの終了とディスクへの変更の書き込み」に合わせて Enter を押します。各パーティションを選んで Enter を押すと、パーティションのサイズの拡大・縮小（Windows パーティションでも可能）や、ファイルシステム形式を ext3 以外のものに変更できます。
<div class="img">
<a href="full/d-i10.png"><img src="d-i10.png" alt="パーティショニング計画"></a><br />
図3●パーティショニング計画
</div>
</li>
<li>基本システムのインストールが終わると、ブートローダ GRUB をディスクの MBR 領域にインストールしてよいか尋ねられます。「はい」を選んでブートできるようにしましょう（GRUB は、メニュー形式で Linux だけでなく Windows もブートできます）。最後に再起動を要求されるので、CD を取り出し、再起動を行います。</li>
</ol>

<p>なお、（特にシリアル ATA の）ハードディスクが見つからないという問題に遭遇した場合、Sarge の標準のインストーラでは対応できません。<a href="http://kmuto.jp/debian/d-i/">http://kmuto.jp/debian/d-i/</a>で新しいカーネルバージョンを移植した ISO イメージを筆者が提供しているので、これを試してみるのがよいでしょう。</p>

<h3 id="stage2"><a name="stage2" href="#stage2">デスクトップ環境の構築</a></h3>
<p>再起動してGRUBのメニューが表示され、そのまま待つと、Debianの残りのインストール手順に入ります。</p>

<ol>
<li>歓迎メッセージのあと、時間帯を設定します。通常は「ハードウェアの時計はGMTに合わせていますか」に「いいえ」、「Asia/Tokyoの時間帯でよろしいですか」に「はい」でよいでしょう。</li>
<li>続いて、root のパスワードの入力とその確認、新規に作成する一般ユーザの名前、ログイン名（アカウント名）、パスワードとその確認を行います。</li>
<li>環境構築に必要なパッケージをダウンロードするための、APT 管理スイートの設定を行います（図4）。
<div class="img">
<a href="full/d-i23.png"><img src="d-i23.png" alt="APTの設定"></a><br />
図4●APT の設定
</div>
ネットワークから取得するために、「http」を選び、国には「日本」、アーカイブミラーには「ftp.jp.debian.org」を選択します。最後に尋ねられる HTTP プロキシについては、必要なときにのみ設定してください（通常はそのまま Enter でよいはずです）。
</li>
<li>ネットワークからの情報のダウンロードが終わると、機能別にパッケージを集約したソフトウェアコレクション（タスク）の選択になります。ここではデスクトップ環境を構築するために、「デスクトップ環境」を選んでスペースキーでマークを付けてインストール対象にします（ディスク容量が小さかったり、サーバ環境では、特に何も選択せず、後で必要に応じてパッケージをインストールするほうがよいでしょう）。選択したら Tab を押して「了解」に移動し、Enter を押してパッケージをダウンロードします。</li>
<li>ダウンロードが終わると、debconf と呼ばれる事前設定インターフェイスが起動します。次のような項目について質問されます。これが終わるとパッケージの実際の展開と設定が行われ、インストールが完了します。</li>
</ol>

<h4 id="setup-x"><a name="setup-x" href="#setup-x">X ウィンドウシステムの設定</a></h4>
<ul>
<li><strong>X サーバドライバ</strong>：適切なネイティブドライバがわからなければ「vesa」を選ぶとよい。</li>
<li><strong>カーネルフレームバッファ</strong>：NVidiaビデオカードでないなら「はい」。</li>
<li><strong>マウスの自動認識</strong>：「はい」にしてみるのがよい。認識されなかった場合、PS/2 マウスは /dev/psaux、USB マウスは /dev/input/mice を選ぶ。プロトコルは PS/2（2/3 ボタン）、ImPS/2（ホイール付き）、ExplorerPS/2（ホイール+側面ボタン）のいずれか。</li>
<li><strong>モニタの自動検出</strong>：NVidia ビデオカードでないなら「はい」。</li>
<li><strong>CRT/LCD デバイス</strong>：液晶モニタなら「はい」。</li>
<li><strong>モニタ特性の選択方法</strong>：「Simple」または「Medium」がよい。Simple ではモニタのサイズを指定し、Medium では最大解像度とその周波数のリストから選ぶ（たとえば「1280x960&#64;60Hz」）。</li>
</ul>

<h4 id="setup-exim"><a name="setup-exim" href="#setup-exim">Exim メールサーバの設定</a></h4>
<ul>
<li><strong>メール設定のタイプ</strong>：一般的なデスクトップ環境、つまり ISP などのメールサーバからメールをローカルに受信し、送信時にはやはり ISP のメールサーバに送りたいときには、「スマートホストでメール送信; ローカルメールなし」を選ぶ。</li>
<li><strong>スマートホスト</strong>：ISP のメールサーバを指定する。</li>
<li><strong>root と postmaster のメール受信者</strong>：システムメールの受信者を指定する。通常はインストール時に作成した一般ユーザ（デフォルト）。</li>
</ul>

<h3 id="login"><a name="login" href="#login">ログインとGNOMEの操作</a></h3>
<p>「デスクトップ環境」でのインストールが完了すると、GUI のログイン画面が表示されます（図5）。root のログインはデフォルトでは禁止されているので注意してください。なお、GNOME と KDE の両方がインストールされていますが、デフォルトの環境には GNOME が使われます（セッションから選ぶか、Debian のデフォルト設定を変更することで KDE にすることもできます）。インストール時に作成した一般ユーザのユーザ名、パスワードを入力してログインしましょう（図6）。</p>

<div class="img">
<a href="full/d-i40.png"><img src="d-i40.png" alt="GUI ログイン画面"></a><br />
図5●GUI ログイン画面
</div>
<div class="img">
<a href="full/d-i41.png"><img src="d-i41.png" alt="GNOME デスクトップ環境"></a><br />
図6●GNOME デスクトップ環境
</div>

<p>GNOME の操作についてはここでは説明しませんが、基本的にマウス操作のわかりやすいインターフェイスなので混乱することは少ないでしょう。上部の端末アイコンをクリックすればなじみ深いシェル端末を呼び出せます。</p>

<p>パッケージの追加インストールなど、root の操作が必要なときには、一般ユーザでログインしたあと、メニューの「アプリケーション」→「Root Terminal」を呼び出して、root のパスワードを入力します（または一般ユーザの端末で su コマンドを実行します）。ログアウトやシャットダウンを行うには、「アクション」→「ログアウト」を選びます。もちろん、root の状態で次のようにしてもシャットダウンできます。</p>

<pre>
# shutdown -h now
</pre>

<p>ウィンドウシステムがうまく起動しない、あるいはデフォルトの解像度を 800×600 から変更したい、といった際の再設定は、root の状態で日本語コンソールまたはシェル端末を起動したあと、次のように行います。</p>

<pre>
# dpkg-reconfigure --default-priority xserver-xfree86
</pre>

<h3 id="setup-console"><a name="setup-console" href="#setup-console">コンソールでの日本語表示</a></h3>
<p>Debian をインストールした多くのユーザがとまどうのが、インストールが終わった途端にコンソールが突如として英語モードになってしまい、日本語とおぼしき箇所が全部化けてしまって読めないという現象です。ウィンドウシステムが使えない状態で、インストーラのときと同じように日本語表示を行うには、jfbterm というアプリケーションを使います。最初に、カーネルフレームバッファモジュールをロードする必要があります（インストールした直後の場合、モジュールはロード済みです）。</p>

<pre>
# modprobe vga16fb ←ビデオカード固有のものに変更してもよい
# modprobe fbcon ←カーネル 2.4 でインストールしたときは不要
</pre>

<p>以降は、root でも一般ユーザーでも、次のように jfbterm を実行することで、インストールで使ったものと同じ日本語表示ができます。「exit」で jfbterm は終了します。</p>

<pre>
# jfbterm -c other,EUC-JP,iconv,UTF-8 -q
</pre>

<p>うまく動作したようなら、/etc/modules に上記のモジュール名を追加して、起動時にモジュールを自動ロードするようにしておくとよいでしょう。</p>

<pre>
# (echo vga16fb; echo fbcon) &gt;&gt; /etc/modules
</pre>

<h3 id="setup-desktop"><a name="setup-desktop" href="#setup-desktop">デスクトップ環境の強化</a></h3>
<p>ここまでの手順で、ひととおりの Sarge のデスクトップ環境は出来ましたが、日本語利用者が使うには、まだいくぶん使いにくい箇所があります。もう一手間かけて、使いやすさを向上してみましょう。以下で登場する aptitude や apt-get は、パッケージ管理コマンドです。</p>

<h4 id="setup-japanese-im"><a name="setup-japanese-im" href="#setup-japanese-im">日本語入力</a></h4>
<p>デフォルトでは Canna と Kinput2 という歴史ある組み合わせがインストールされますが、GNOME デスクトップを中心に使うなら、Anthy と uim の組み合わせのほうが面倒な手動での設定に悩まされずに済みます。</p>

<pre>
# aptitude install uim uim-anthy canna- ←uim スイートおよび
                                          Anthy をインストールし、
                                          同時に Canna を削除
</pre>

<p>これで、GNOME（GTK+）アプリケーション上で Shift+スペース を押すことで、いつでも日本語入力モードに切り替えて日本語を記入できるようになります（キー操作の詳細については、uim-pref-gtk を実行して設定を確認してください）。「uim-toolbar-gtk-systray &amp;」を実行しておけば、メニューバーに現在の日本語入力状態を示すことができます。</p>

<h4 id="setup-web"><a name="setup-web" href="#setup-web">Web ブラウザとメーラ</a></h4>
<p>Web ブラウザには Mozilla と Konqueror がインストール済みですが、人気の高い Firefox をインストールして使うこともできます。同様に、Mozilla 派生のメーラ Thunderbird も利用できます（図7）。</p>

<pre>
# aptitude install mozilla-firefox mozilla-firefox-locale-ja ←Firefoxの
                                                             インストール
# aptitude install mozilla-thunderbird ←Thunderbirdのインストール
</pre>

<div class="img">
<a href="full/firefox.png"><img src="firefox.png" alt="FirefoxとThunderbird"></a><br />
図7●FirefoxとThunderbird
</div>

<h4 id="setup-office-suite"><a name="setup-office-suite" href="#setup-office-suite">オフィススイート</a></h4>
<p>Sargeではオフィススイートとして OpenOffice.org バージョン 1 が提供されていますが、実用とするには不満な出来です。より新しい OpenOffice.org バージョン 2 では、かなりの機能向上が行われています（図8）。前章で紹介した backports.org からこのバージョンをインストールしてみましょう。インストール後は、GNOME メニューから起動できます。</p>

<pre>
# echo "deb http://www.jp.backports.org/ sarge-backports main contrib non-free" &gt;&gt; /etc/apt/sources.list ←リポジトリを追加
# cat &lt;&lt;EOT &gt;&gt;/etc/apt/preferences ←backports.orgにあるパッケージに
                                                 ついては明示しない限りインストール・
                                                 アップデートしないように設定
Package: *
Pin: release a=sarge-backports
Pin-Priority: 200
EOT

# apt-get update
# aptitude -t sarge-backports install openoffice.org-writer openoffice.org-draw openoffice.org-impress openoffice.org-math openoffice.org-calc ←backports.orgからOpenOfficeのパッケージをインストール
</pre>

<div class="img">
<a href="full/oo.png"><img src="oo.png" alt="OpenOffice.orgバージョン2"></a><br />
図8●OpenOffice.orgバージョン2
</div>

<h4 id="setup-video-driver"><a name="setup-video-driver" href="#setup-video-driver">ネイティブビデオカードドライバのビルド</a></h4>
<p>ビデオカードによっては、XFree86 ではなくベンダーから提供されているビデオドライバを使わないとパフォーマンスが出ない、あるいはまったく動かないことがあります。NVidia 系のカードであれば、nvidia-kernel-source パッケージを使ってドライバをインストールします。</p>

<pre>
# sed -i 's#ftp.jp.debian.org/debian/ stable main$#&amp; contrib non-free#g' /etc/apt/sources.list ←非DFSGのパッケージを取得するための変更
# aptitude install module-assistant
# m-a prepare ←カーネルヘッダなどを準備
# m-a auto-install nvidia-kernel ←モジュールのビルド
</pre>

<p>インストールが終わったら、/etc/X11/XF86Config-4 をエディタで開き、「Driver "vesa"」のようになっている箇所を「Driver "nvidia"」に変更します。再起動すると、ウィンドウシステムが立ち上がるようになるでしょう。</p>

<p>ATI 系のカードであれば backports.org から fglrx-kernel-src パッケージと X.Org 関連のパッケージを取得して同様にビルドし、/etc/X11/xorg.conf の Driver エントリを「Driver "fglrx"」に変更します（注：執筆時点では fglrx-driver の依存関係を解決できないため、うまくインストールできないようです）。</p>

<h4 id="setup-power-control"><a name="setup-power-control" href="#setup-power-control">電力管理</a></h4>
<p>デフォルトでは電力管理は特に行われていないので、シャットダウン時に電源が落ちることもありません。ACPI あるいは APM の管理パッケージをインストールすると、これが可能になります。</p>

<pre>
# aptitude install acpid （マシンが ACPI で電力管理されている場合）
# aptitude install apmd  （マシンが APM で電力管理されている場合）
</pre>

<p>また、カーネルの CPUFreq ドライバと cpufreqd パッケージを組み合わせれば、CPU 負荷やバッテリに応じて動作周波数を動的に変更することもできます。</p>

<h3 id="setup-printers"><a name="setup-printers" href="#setup-printers">印刷環境の整備</a></h3>
<p>デスクトップ環境の仕上げとして、印刷環境を整備することにしましょう。Sarge では「CUPS」という印刷システムを使ってプリンタ管理を一元化できます。CUPS は、CUPS スケジューラを中心とするサーバクライアントシステムで、旧来の LPR や lp システムとの互換性を提供しつつ、PPD プリンタ定義ファイルを使ってプリンタ固有情報の取り扱いやフィルタリング設定などを容易にしています。</p>

<pre>
# aptitude install cupsys
</pre>

<p>Samba クライアント向けのいくつかの設定（ワークグループ/ドメイン名、パスワードの暗号化の有無、WINS 設定の有無）と、旧来の BSD LPR サーバの起動の有無を尋ねられます。ほとんどはデフォルトの選択でかまわないでしょう。</p>

<p>Linux の事実上の標準印刷出力フォーマットは PostScript ですが、ほとんどのプリンタはこれを直接印刷することはできず、プリンタネイティブコードに変換する必要があります。PostScript からプリンタネイティブコードへの変換に Ghostscript（gs-esp）を使うので、日本語対応のためのいくつかのパッケージも含めてインストールします。</p>

<pre>
# sed -i 's#ftp.jp.debian.org/debian/ stable main$#&amp; contrib non-free#g' /etc/apt/sources.list ←非DFSGのパッケージ（CMap関連）を取得するための変更
# apt-get update
# aptitude install foomatic-filters-ppds gs-esp cmap-adobe-japan1 gs-cjk-resource
</pre>

<p>CMap グループの質問に対しては、デフォルトの選択のままでかまいません。</p>

<p>CUPS の設定方法はいくつかありますが、Web ブラウザ経由で設定するのが最も簡単でしょう。URL http://localhost:631 に接続し、「プリンタ管理」を選んでユーザ名 root と root のパスワードを指定して、プリンタを追加・管理します（図9）。追加したプリンタは、GNOME アプリケーションや OpenOffice.org の印刷メニューから参照して利用できます。</p>

<div class="img">
<a href="full/cups.png"><img src="cups.png" alt="CUPSによるプリンタの管理"></a><br />
図9●CUPSによるプリンタの管理
</div>

<h3 id="references"><a name="references" href="#references">さらなる情報</a></h3>
<p>この文書では、Debian の日本語デスクトップ環境を構築するまでの手順を説明しました。APT の活用方法や、インストールすると便利なパッケージ群、サーバ構築の方法については、他書や Web サイトの情報などを参考にしてください。</p>

<ul>
<li>『<a href="http://kmuto.jp/debian/debian_sarge/">Debian GNU/Linux 徹底入門 第3版 〜Sarge対応</a>』（翔泳社）</li>
<li>『<a href="http://kmuto.jp/debian/debian_dic/">Debian 辞典</a>』（翔泳社）</li>
<li><a href="http://debian.fam.cx/">Debian GNU/Linux スレッドテンプレ</a></li>
</ul>
<hr>
<address>Copyright: 2006 Kenshi Muto</address>
