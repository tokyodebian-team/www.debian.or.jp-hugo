# Template::Plugin::Iconv
#
# DESCRIPTION
#  Plugin for converting encodings.
#
# AUTHOR
#   Kenshi Muto <kmuto@debian.org>
#
# COPYRIGHT
#  This module is free software; you can redistribute it and/or
#  modify it under the same terms as Perl itself.
#
# REVISION
#  1.0
#

package Template::Plugin::Iconv;

require 5.004;
use strict;
use base qw( Template::Plugin );
use vars qw( $VERSION );
use Template::Plugin;
use Text::Iconv;

$VERSION = "1.0";

sub new {
  my($class, $context, $params) = @_;
  $context->define_filter('iconv', [ \&Iconv_filter_factory => 1 ]);
}

sub iconv {
  my $text = shift;
  my $from = shift;
  my $to = shift;

  my $converter = Text::Iconv->new($from, $to);
  $converter->convert($text);
}

sub Iconv_filter_factory {
  my ($context, @args) = @_;
  return sub {
    my $text = shift;
    iconv($text, @args);
  }
}

1;

__END__
